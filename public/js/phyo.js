function showLoading(attr_id)
{
  $(attr_id+" i").addClass('fa fa-spinner fa-spin'); 
}
function setStatus(argument) {
	$('select#status option').remove();
	setTarget(argument,'set_status')
}
function setEmployer(argument) {
	$('select#job_title option').remove();
	setTarget(argument,'set_employer')
}
function setInterview(argument)
{	
	$('div#interview').addClass('hide');
	if (argument == 10 || argument == 14) {
		$('div#interview').removeClass('hide');
	}

}
function setTarget(key,route) {	
	$.get(window.Laravel.baseURL+'/'+route+'/'+key,function(data){		
		if (route == 'set_status') {
			$('select#status').append('<option value="">Select one</option>');
			$.each(data,function(key,value){
				$('select#status').append('<option value='+key+'>'+value+'</option>');
			});	
		}else{
			$.each(data,function(key,value){
				$('select#job_title').append('<option value='+value.job_id+'>'+value.job_title+'</option>');
			});	
		}
	})
}
function  addManual(param,type='show') {
	showLoading("button#manual");
 	var status = $('button#manual').attr('data-status');
	if (status == 'show') {
		$.get( window.Laravel.baseURL+'/automatch'+'/search?jobseeker_id='+param, function( data ) {
			if (data.employer != null) {
				$("div#employer").append("<label>Employer Name</label><select name='employer' required onchange='setEmployer(this.value)' id='employer' class='select2 form-control'><option value=''>Select one</option</select>");
				$("div#job_title").append("<label>Job Title</label><select name='job_position[]' required id='job_title' class='form-control select2'><option value=''>Select one</option</select>");
				$("div#job_title").append("<input name='manual' type='hidden' id='manual' value='manual' class='form-control'>");
				$("div#stage").append('<label>Stage</label><select name="stage[]" onchange="setStatus(this.value)" id="stage" class="form-control"><option value="">Select one</option</select>');
				$("div#status").append("<label>Status</label><select id='status' onchange='setInterview(this.value)' name='status[]' required='required' class='form-control'><option value=''>Select one</option</select>");
				$(".select2").select2();
				$.each(data.employer,function(key,value){
					$('<option value='+value.emp_id+'>'+value.employer_name+'</option>').appendTo('select#employer');
				});
				$.each(data.stages,function(key,value){
					$('<option value='+key+'>'+value+'</option>').appendTo('select#stage');
				});		
			}
		});
		$('button#manual').attr('data-status','hide');
		$('button#manual').html('<i></i> Remove Manual');
	}else{
		$('div#job_title').empty();
		$('div#employer').empty();
		$('div#stage').empty();
		$('div#status').empty();
		$('button#manual').html('<i></i> Add Manual');
		$('button#manual').attr('data-status','show');
	}
}