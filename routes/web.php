<?php
use App\Models\Jobseeker;
use App\Models\Employer;
use Illuminate\Support\Facades\Input;
use App\Models\Fabric;
use Illuminate\Http\Request;
use App\Models\CSV;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/abc','JobpositionController@test');
Route::get('manual_discharged','JobseekerController@manualDischarged');

//jobseeker list come from job browse feature from messenger
Route::get('jobseeker-jobbrowse/{type?}','JobseekerController@jobseeker_jobbrowse');
//Jobseeker list which is applied jobs and which has stage and status
Route::get('jobseeker-jobapplied','JobseekerController@jobseeker_jobapplied');
//dependent dropdown for stage and status
Route::get('myform',array('as'=>'myform','uses'=>'StageController@myform'));
Route::get('myform/ajax/{id}',array('as'=>'myform.ajax','uses'=>'StageController@myformAjax'));

//dependent dropdown for stage and status
Route::get('jobtitle/ajax/{id}',array('as'=>'jobtitle.ajax','uses'=>'JobpositionController@jobtitleAjax'));

// test json response
Route::get('/api/jobposition/{id}','WebhookController@showJobposition');
Route::post('apply-job','WebhookController@applyJob');
Route::get('/api/jobposition-req/{id}','WebhookController@showReqJobposition');
Route::get('/json','WebhookController@json_test');
Route::get('generate_cv/{jobseeker}','WebhookController@autoReplyCV');

//urgent section
Route::get('/reminder','JobseekerController@reminder');
//for web scraping
Route::get('/phantomjs','PhantomjsController@index' );
Route::get('/scraper-24jobs','WebScraperController@scrape_24jobs_com_mm');
Route::get('/scraper-jobnet','WebScraperController@scrape_jobnet_com_mm');
Route::get('scrapetest2','WebScraperController@index');
Route::get('/scapetest','WebScraperController@scrapetest')->name('scrapetest.index');


// Route::post('add-category',['as'=>'add.category','uses'=>'CategoryController@addCategory']);
Route::get('jobfunction',['uses'=>'Jobs\JobfunctionController@manageCategory']);
Route::post('add-category',['as'=>'add.category','uses'=>'Jobs\JobfunctionController@addCategory']);


// job position and job posts
Route::get('freejob','JobpositionController@freejob');
Route::get('paidjob','JobpositionController@paidjob');
Route::post('seft/jobposition/{position}','JobpositionController@update');
Route::resource('jobposition','JobpositionController');
Route::post ( '{route}/global/search', 'SearchController@search');
// Route::resource('jobsavailable','JobavailableController');
Route::resource('jobsavailable-paid','JApaidController');


//checklist section
Route::resource('checklists','ChecklistController');
Route::get('checklist-count','ChecklistController@count');
Route::get('test','ChecklistController@test');
Route::group(['namespace' => 'Checklist'], function()
    {   
Route::resource('checklists-emp','ChecklistempController');
Route::resource('checklists-js','ChecklistjsController');
Route::get('downloadExcelCLEMP/{type}', 'ChecklistempController@downloadExcel');
Route::get('downloadExcelCLJS/{type}', 'ChecklistjsController@downloadExcel');
    });
Route::get('jobseeker-calls','ChecklistController@jobseeker_calls');

//jobposition section
Route::get('jobmatch-free','JobpositionController@jobmatchfree');
Route::get('jobmatch-paid','JobpositionController@jobmatchpaid');
Route::get('jobpositions/{id}/create','JobpositionController@create')->name('jobpositions.create');
Route::patch('/jobpositions/{id}','JobseekerController@update')->name('jobpositions.update');
// Route::get('jobsavailable/show/{id}','JobavailableController@show')->name("jobsavailable.show")->middleware('auth');

//Email Section
Route::get('/email', 'HomeController@index')->name('home.index');
Route::post('email/sending','HomeController@sendingEmail')->name('sending');
Route::get('sendbasicemail','MailController@basic_email');
Route::get('sendhtmlemail','MailController@html_email');
Route::get('sendattachmentemail','MailController@attachment_email');

//EmployerCalllogController Section
Route::resource('employercalllogs','EmployercalllogController');
Route::post('employercalllogs-create','EmployercalllogController@employercallogstore')->name('employercalllogs-create.store');

//for connection with chatfuel and laravel
Route::resource('webhook','WebhookController');
Route::post('webhook2','WebhookController@webhooktest');

//Jobseeker CalllogController
Route::get('/todo','CalllogController@todo')->name('todo.index');
Route::post('/todo/{todo}','CalllogController@todoupdate')->name('todo.update');
Route::get('calllogs/{stage?}','CalllogController@index')->name("calllogs.index");
//Route::resource('calllogs', 'CalllogController');
Route::get('calllogs/create/0','CalllogController@create')->name('calllogs.create');
Route::post('calllogs','CalllogController@store')->name('calllogs.store');
Route::get('calllogs/{callog}','CalllogController@show')->name('calllogs.show');
Route::get('calllogs/{callog}/edit','CalllogController@edit')->name('calllogs.edit');
Route::post('calllogs/{callog}','CalllogController@update')->name('calllogs.update');
Route::delete('calllogs/{callog}','CalllogController@destroy')->name('calllogs.destroy');
Route::patch('/updateisdone/{calllog}','CalllogController@isdone')->name('calllog.isdone');
Route::get('not_match','CalllogController@notMatch');

//EmployerController Section
// autocomplete test
Route::get('/todo-em','EmployercalllogController@todo_em');
Route::get('autocomplete',array('as'=>'autocomplete','uses'=>'EmployerController@autocomplete'));
Route::get('export', 'EmployerController@exportExcel');
Route::get('autocomplete2',array('as'=>'autocomplete2','uses'=>'EmployerController@index2'));
Route::get('searchajax',array('as'=>'searchajax','uses'=>'EmployerController@autoComplete2'));
Route::resource('employers','EmployerController');
Route::get('employers/{id}/send','EmployerController@send')->name('employers.send');
Route::get('employers/{id}/send2','EmployerController@send2')->name('employers.send2');
Route::get('employers/{id}/send3','EmployerController@send3')->name('employers.send3');
Route::patch('/updatestatusemp/{employer}','EmployerController@updatestatus')->name('employers.updatestatus');


//export excel
Route::get('downloadExcelEMP/{type}', 'EmployerController@downloadExcel');
Route::get('downloadExcelEMPCL/{type}', 'EmployercalllogController@downloadExcel');
Route::get('downloadExcelJS/{type}', 'JobseekerController@downloadExcel');
Route::get('downloadExcelCL/{type}', 'CalllogController@downloadExcel');

//JobseekerController Section

Route::patch('/updatestatusjs/{jobseeker}','JobseekerController@updatestatus')->name('jobseekers.updatestatus');

Route::patch('/updatestatusjs-additionalfield/{jobseeker}','JobseekerController@add_additionalfield')->name('jobseekers.add_additionalfield');

Route::get('jobseekers/show/{id}','JobseekerController@show')->name("jobseekers.jobseeker_detail")->middleware('auth');

Route::get('appointed-jobseeker','JobseekerController@appointed');
Route::get('newjs','JobseekerController@newjs');
Route::get('mypdf','JobseekerController@pdf');
Route::get('mypdf/{id}','JobseekerController@pdf')->name("jobseekers.mypdf");
Route::get('newjobseekers','NewjobseekerController@index');
Route::get('jobseekers','JobseekerController@index')->name("jobseekers.index")->middleware('auth');
Route::get('jobseekers/send_cv/{id}','JobseekerController@sendcv')->name('jobseekers.send_cv');
Route::get('jobseekers/send','JobseekerController@send');
Route::post('jobseekers/send','JobseekerController@send');
Route::get('jobseekerdata','JobseekerController@index2')->name('jobseekers.datatable');
Route::get('jobseekersjson','JobseekerController@getJoinsData')->name('jobseekers.datajoin');
Route::get('jobseekerslist','JobseekerController@getJoins')->name("jobseekers.json");
Route::get('/old-appointment','JobseekerController@old_appointment_js');
Route::get('jquery',['as'=>'jquery','uses'=>'JobseekerController@loadMore']);
Route::get('/duplicates','JobseekerController@duplicates')->name('duplicates.index');
Route::get('/count','JobseekerController@count')->name('counts.index');
Route::get("js/data", 'JobseekerController@data')->name("js.data");
Route::get('jsdata','JobseekerController@jsdata');
Route::resource('jobseekers','JobseekerController');
Route::get('jobseekers/discharge/{id?}','CalllogController@discharged');

Route::get('/', 'HomeController@welcome')->name('home');

//for the keys
Route::resource('certificates','CertificateController');
Route::resource('jobfunctions','JobFunctionController');
Route::resource('jobtitles','JobTitlesController');
Route::resource('townships','TownshipController');
Route::resource('callreasons','CallreasonController');
Route::resource('skill_1','SkillController');
Route::resource('skill_2','Skill2Controller');
Route::resource('jobindustry','IndustryController');
Route::resource('wards','WardController');
Route::resource('status','StatusController');
Route::resource('users','UserController');
Route::get('category-tree-view',['uses'=>'CategoryController@manageCategory']);
Route::get('automatch/{param}','GeneralController@autoMatch');
Route::get('set_status/{id}','GeneralController@setStatus');
Route::get('set_employer/{id}','GeneralController@setEmployer');
Route::get('csv_import',function(){
	return view('csv_import');
});
Route::post('csv_import',function(Request $request){
	$csv = new CSV($request->all());
	$csv->import();
});
