<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Models\Fabric;
class CustomCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'custom:command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         // DB::table('users')->where('is_active', 1)->delete();
    $data= file_get_contents('https://api.cinelist.co.uk/get/times/cinema/10565');
    $data = json_decode($data);

    foreach($data->listings as $listing){
        $title = $listing->title;
        $time = implode(', ', $listing->times);

       Fabric::updateOrCreate([
                             'name' => $title
                             ]);
    }

         $this->info('I redirected to the web scrape successfully!');
         // return redirect('/testing123');
    }
}
