<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChildJobPosition extends Model
{
   public function parentjobposition()
    {
    	return $this->belongsTo('App\ParentJobPosition');
    }  
}
