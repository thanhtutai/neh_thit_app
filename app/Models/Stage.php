<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stage extends Model
{
    public function jobseekers()
    {
    	return $this->hasMany('App\Models\Jobseeker');
    }  
}
