<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobTitle extends Model
{
    public function jobseekers()
    {
    	return $this->hasMany('App\Models\Jobseeker');
    }    

    public function jobpositions()
    {
    	return $this->hasMany('App\Models\Jobpositions');
    }
}
