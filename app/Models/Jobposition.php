<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jobposition extends Model
{

     protected $fillable = [
       'job_title','post_date','deadline','status','gender','req_nationality','req_quantity','certificate_1','certificate_2','education','skill_1','skill_2','experience_month','salary_range','other_benefits','any_special_requests','any_special_requirements','township','labour_card','remark','jobfunction','jobtitle','age_requirement','experience_requirement','job_requirement'
    ];

    protected $primaryKey = 'job_id';

    public function employer()
    {
        return $this->belongsTo('App\Models\Employer','employer_id','emp_id');
    }

  	public function author()
  	{
  		return $this->belongsTo('App\User','posted_by');
  	}

    public function Myrequire_skill_1(){
        return $this->belongsTo('App\Models\skill','skill_1');
    } 

    public function Myrequire_skill_2(){
        return $this->belongsTo('App\Models\skill','skill_2');
    }

    public function Certificate_1(){
        return $this->belongsTo('App\Models\Certificate','certificate_1');
    } 

    public function Certificate_2(){
        return $this->belongsTo('App\Models\Certificate','certificate_2');
    }

    public function My_position_1(){
        return $this->belongsTo('App\Models\JobTitle','require_position','job_title_key');
    }

    public function My_education(){
        return $this->belongsTo('App\Models\Education','education');
    }
    
    public function My_jobseekers(){
      return $this->belongsToMany("App\Models\Jobseeker","jobposition_jobseeker","jobposition_id","jobseeker_id")->withPivot('stage', 'status','interview_time')
         ->withTimestamps();
    }

    public function JustFlowUp(){
      return $this->belongsToMany("App\Models\Jobseeker","jobposition_jobseeker","jobposition_id","jobseeker_id")->wherePivot('status',9)
         ->withTimestamps();
    }

    public function JobBrowse()
    {
      return $this->belongsToMany("App\Models\Jobseeker","jobposition_jobseeker","jobposition_id","jobseeker_id")->wherePivot('status',9);
    }

    public function Township()
    {
      return $this->belongsTo('App\Models\Township','township');
    }

    public function scopeSearch($query, $request)
    { 
      
      if ($request->job_title!='') {   
        $query->where('job_title', 'LIKE', '%' . $request->job_title . '%' );
      }   
      if ($request->employer_name) {
        $query->join('employers','jobpositions.employer_id','employers.emp_id');  
        $query->where('employers.employer_name', 'LIKE', '%' . $request->employer_name . '%' );
      }   
      
      return $query->select('jobpositions.*');     
    }

}
