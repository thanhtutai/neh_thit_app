<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Busstop extends Model
{
     protected $fillable = [
        'busstop',
    ];

     public function jobseekers()
    {
    	return $this->hasMany('App\Models\Jobseeker');
    }
    
}
