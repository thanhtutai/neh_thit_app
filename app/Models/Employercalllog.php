<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employercalllog extends Model
{

    public function employer()
    {
        return $this->belongsTo('App\Models\Employer','employer_id');
    }

    public function Mystatus()
    {
      return $this->belongsTo('App\Models\Employerstatus' , 'status');
    }
    
  	public function author()
  	{
  		return $this->belongsTo('App\User','caller_receiver');
  	}

    public function Myempcallreason()
    {
      return $this->belongsTo('App\Models\Employercallreason' , 'call_reason');
    }

    public function scopeSearch($query, $request)
          { 
        
        if ($request->employer_name!='' || $request->employer_id){
              $query->join('employers','employercalllogs.employer_id','=','employers.id');
          } 
        if ($request->caller_receiver!=''){
              $query->join('users','employercalllogs.caller_receiver','=','users.id');
          }    
        if ($request->status!=''){
              $query->join('employerstatuses','employercalllogs.status','=','employerstatuses.id');
          }     
        if ($request->call_reason!=''){
              $query->join('employercallreasons','employercalllogs.call_reason','=','employercallreasons.id');
          } 
        if ($request->employer_id!='') {       
            $query->where('employers.id',$request->employer_id);
        }    
        if ($request->remark!='') {       
          $query->where('employercalllogs.remark', 'LIKE', '%' .$request->remark. '%' );
        }    
        if ($request->type_of_call!='') {       
          $query->where('type_of_call', 'LIKE', '%' . $request->type_of_call . '%' );
        }
        if ($request->call_reason!='') {       
          $query->where('employercallreasons.description', 'LIKE', '%' . $request->call_reason . '%' );
        }    
        if ($request->status!='') {       
          $query->where('employerstatuses.description', 'LIKE', '%' . $request->status . '%' );
        }
        if ($request->call_back_date!='') {       
          $query->where('call_back_date',$request->call_back_date);
        }
        if ($request->created_at!='') {       
          $query->where('created_at', '=',$request->created_at.' 00:00:00');
        }
        if ($request->caller_receiver!='') {
         $query->where('users.name',$request->caller_receiver);
        }
        if ($request->employer_name!='') {
         $query->where('employer_name', 'LIKE', '%' . $request->employer_name . '%' );
        }          
       
        return $query->select('employercalllogs.*');     
      }



}


