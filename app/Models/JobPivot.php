<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobPivot extends Model
{
    protected $table = 'jobposition_jobseeker';

    //protected $fillable = [''];

    public function jobseeker()
    {
    	return $this->hasOne(Jobseeker::class,'jobseeker_id','jobseeker_id');
    }
}
