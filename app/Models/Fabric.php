<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fabric extends Model
{
	 protected $fillable = ['title','company_name','postdate','township','job_requirement','company_address','email','phone'];
      public function jobseekers()
    {
    	return $this->hasMany('App\Models\Jobseeker');
    }
}
