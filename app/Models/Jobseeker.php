<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\Employer;
class Jobseeker extends Model
{
      protected $primaryKey = 'jobseeker_id';
     protected $fillable = [
        'id','jobseeker_id','active_inactive','is_followup','time','date','fb_name','fullname','stage','status','phone', 'viber_number','sign_up_from','gender','age','wards','bus_stop','bus_stops','education','previous_position_1','previous_position_other','experience_month_1','company_name_1','previous_position_2','previous_position_other_2','experience_month_2','company name_2','certification_and_license','fabrics','type_of_license','desire_position_y_n','previous_function_1','desire_position_1','desire_function_1','desire_position_other_1','previous_function_2','desire_position_2','desire_function_2','desire_position_other_2','job_industry_1','job_industry_2','skill_1','skill_other_1','expected_salary','expected_salary_other','skill_2','skill_other_2','certificate','certificate_other','township','viber_number_2','remark','usertype_1','chatfuel_user_id','address','last_user_freedom_input','messenger_user_id','id_number','call_back_date','start_time','company_id','additional_field','additional_field2','additional_field3','interview_time','nationality','nrc_no','profile_pic_url','marital_status','position_status',
    ];

    public function My_jobpositions(){
      return $this->belongsToMany("App\Models\Jobposition",'jobposition_jobseeker','jobseeker_id','jobposition_id')->withPivot('stage', 'status','interview_time')
         ->withTimestamps();
    }

    public function Js_key_desireposition_1(){
      return $this->belongsTo('App\Models\JobTitle','desire_position_1','job_title_key');
    }  
    public function Js_id_desireposition_1(){
      return $this->belongsTo('App\Models\JobTitle','desire_position_1','job_title_id');
    }
    
    public function Js_desireposition_2(){
      return $this->belongTo('App\Models\JobTitle','desire_position_2');
    }

    public function employer()
    {
        return $this->belongsTo('App\Models\Employer','company_id');
    }

    public function calllogs()
    {
        return $this->hasMany('App\Models\Calllog','jobseeker_id','jobseeker_id');
    }

    public function Myfreerequirepoistion(){
       return $this->belongsTo('App\Models\Jobavailable','desire_position_1','ja_position');
    }   
    public function Mypaidrequirepoistion(){
       return $this->belongsTo('App\Models\Jobposition','desire_position_1','require_position');
    }
    public function Myjob_position_require_skill_1(){
       return $this->belongsTo('App\Models\Jobposition','skill_1','skill_1');
    }
    public function Mycertificate()
    {
      return $this->belongsTo('App\Models\Certificate' , 'certificate');
    }     
    public function Mydesire_position_1(){
        return $this->belongsTo('App\Models\JobTitle','desire_position_1','job_title_key');
    }
    public function Mydesire_position_2(){
        return $this->belongsTo('App\Models\JobTitle','desire_position_2','job_title_key');
    }
    public function Myprevious_position_1(){
        return $this->belongsTo('App\Models\JobTitle','previous_position_1','job_title_key');
    }

    public function previous_func_1(){
        return $this->belongsTo('App\Models\JobFunction','previous_function_1');
    }
    public function previous_func_2(){
        return $this->belongsTo('App\Models\JobFunction','previous_function_2');
    }
    public function desire_func_1(){
        return $this->belongsTo('App\Models\JobFunction','previous_function_1');
    }
    public function desire_func_2(){
        return $this->belongsTo('App\Models\JobFunction','previous_function_2');
    }

    public function Myprevious_position_2(){
        return $this->belongsTo('App\Models\JobTitle','previous_position_2','job_title_key');
    }
    public function Mystatus()
    {
      return $this->belongsTo('App\Models\Status' , 'status');
    }
       public function Mytownship()
    {
      return $this->belongsTo('App\Models\Mytownship' , 'township');
    }       

    public function Myindustry()
    {
      return $this->belongsTo('App\Models\Industry' , 'job_industry_1');
    }    
    public function Myindustry2()
    {
      return $this->belongsTo('App\Models\Industry' , 'job_industry_2');
    }
       public function Mymultiage()
    {
      return $this->belongsTo('App\Models\Multiage' , 'age');
    }

      public function MyEducation()
    {
      return $this->belongsTo('App\Models\Education' , 'education');
    }
    public function Myskill()
    {
      return $this->belongsTo('App\Models\skill' , 'skill_1');
    }
    public function MySkill2()
    {
      return $this->belongsTo('App\Models\Skill2' , 'skill_2');
    }    
    public function MyStage()
    {
      return $this->belongsTo('App\Models\Stage' , 'stage');
    }
    public function MyFabric()
    {
      return $this->belongsTo('App\Models\Fabric' , 'fabrics');
    }
    public function MyWard()
    {
      return $this->belongsTo('App\Models\Ward' , 'wards');
    }
    public function MyGender()
    {
      return $this->belongsTo('App\Models\Gender' , 'gender');
    }
    public function MyBusstop()
    {
      return $this->belongsTo('App\Models\Busstop' , 'bus_stops');
    }
       public function scopeSearch($query, $request)
    { 


      if ($request->age!=''){
            $query->join('multiages','jobseekers.age','=','multiages.age');
        } 
      if ($request->gender!=''){
            $query->join('genders','jobseekers.gender','=','genders.id');
        } 
      if ($request->township!=''){
            $query->join('mytownships','jobseekers.township','=','mytownships.id');
        }       

      // if ($request->status!=''){
      //       $query->join('statuses','jobseekers.status','=','statuses.id');
      //   } 
      // if ($request->status!=''){
      //       $query->join('statuses','calllogs.status','=','statuses.id');
      //   } 
      if($request->skill_1!=''){
        $query->join('skills','jobseekers.skill_1','=','skills.id');
      }  
         
      // if($request->desire_position_1!=''){
      //   $query->join('desirepositions','jobseekers.desire_position_1','=','desirepositions.id');
      // }   
      // if($request->desire_position_1!=''){
      //   $query->join('job_titles','jobseekers.desire_position_1','=','job_titles.job_title_key');
      // }  
      // if($request->desire_position_2!=''){
      //   $query->join('job_titles as job_titles_dp2','jobseekers.desire_position_2','=','job_titles_dp2.job_title_key');
      // }        
      // if($request->previous_position_1!=''){
      //   $query->join('job_titles as job_titles_pp1','jobseekers.previous_position_1','=','job_titles_pp1.job_title_key');
      // }
      // if($request->previous_position_2!=''){
      //   $query->join('job_titles as job_titles_pp2','jobseekers.previous_position_2','=','job_titles_pp2.job_title_key');
      // }
      if($request->skill_2!=''){
        $query->join('skill2s','jobseekers.skill_2','=','skill2s.id');
      }
      if ($request->js_id!='') {       
        $query->where('jobseeker_id', 'LIKE', '%' . $request->js_id . '%' );
      }
      if ($request->nrc_no!='') {       
        $query->where('nrc_no', 'LIKE', '%' . $request->nrc_no . '%' );
      }
      if ($request->fullname!='') {       
        $query->where('fullname', 'LIKE', '%' . $request->fullname . '%' );
      }     
      if ($request->experience_month_1!='') {       
        $query->where('experience_month_1', $request->experience_month_1 );
      }          
      if ($request->experience_month_2!='') {       
        $query->where('experience_month_2', $request->experience_month_2 );
      }      
      if ($request->fb_name!='') {       
        $query->where('fb_name', 'LIKE', '%' . $request->fb_name . '%' );
      }
      if ($request->phone!='') {       
        $query->where('phone', 'LIKE', '%' . $request->phone . '%' );
      }       
       if ($request->status!='') {       
       $query->where('active_inactive',  $request->status);
       }      
      if($request->education!=''){
        $query->where('education',$request->education);
      }  
      if ($request->date!='') {       
        $query->whereDate('date','=', $request->date);
      }      

      if ($request->first_age!='') {       
        $query->whereBetween('age', [$request->first_age, $request->second_age]);

      }



      if ($request->gender!='') {
        $gender = explode(',', $request->gender);    
        $query->whereIn('genders.gender_description',$gender);
      }      
      if ($request->township!='') {
        $exp_1 = explode(',', $request->township);    
        $query->whereIn('mytownships.mytownship_name',$exp_1);
      }
      if ($request->desire_position_1!='') {
        if ($request->desire_position_1 == 19) {
          $exp_2 = [19,null];
          $query->whereIn('jobseekers.desire_position_1',$exp_2);
          $query->orWhere('jobseekers.desire_function_1',19);
        }else{
          $exp_2 = explode(',', $request->desire_position_1); 
          $query->whereIn('jobseekers.desire_position_1',$exp_2);
        }
        
      }    
      if ($request->desire_position_2!='') {
        if ($request->desire_position_2 == 19) {
          $exp_3 = [19,null];
          $query->whereIn('jobseekers.desire_position_2',$exp_3);
          $query->orWhere('jobseekers.desire_function_2',19);
        }else{
          $exp_3 = explode(',', $request->desire_position_2); 
          $query->whereIn('jobseekers.desire_position_2',$exp_3);
        }
      }
      if($request->previous_position_1!=''){
        if ($request->previous_position_1 == 19) {
          $exp_4 = [19,null];
          $query->whereIn('jobseekers.previous_position_1',$exp_4);
          $query->orWhere('jobseekers.previous_function_1',19);
        }else{
          $exp_4 = explode(',', $request->previous_position_1); 
          $query->whereIn('jobseekers.previous_position_1',$exp_4);
        }
      }      
      if($request->previous_position_2!=''){
        if ($request->previous_position_2 == 19) {
          $exp_5 = [19,null];
          $query->whereIn('jobseekers.previous_position_2',$exp_5);
          $query->orWhere('jobseekers.previous_function_2',19);
        }else{
          $exp_5 = explode(',', $request->previous_position_2); 
          $query->whereIn('jobseekers.previous_position_2',$exp_5);
        }
      }  
      
      if($request->certificate!=''){
        $exp_5 = explode(',', $request->certificate);    
        $query->where('jobseekers.certificate',$request->certificate);
      }       
  
      if($request->age!=''){
        $exp_6 = explode(',', $request->age);    
        $query->whereIn('multiages.age',$exp_6);
      }
      if ($request->skill_1!='') {
        $exp_7 = explode(',', $request->skill_1);    
       $query->where('skills.id',$exp_7);
      }
      if($request->skill_2!=''){
         // dd($request->skill_2);
        $exp_8 = explode(',', $request->skill_2);    
        $query->where('skill2s.id',$exp_8);
      } 

      return $query->select('jobseekers.*');     
    }



  public function scopeMysearch($query, $request)
    { 
        
      $query->leftjoin('jobposition_jobseeker','jobseekers.jobseeker_id','=','jobposition_jobseeker.jobseeker_id');
      if ($request->age!=''){
            $query->join('multiages','jobseekers.age','=','multiages.age');
        } 
      if ($request->gender!=''){
            $query->join('genders','jobseekers.gender','=','genders.id');
        } 
      if ($request->township!=''){
            $query->join('mytownships','jobseekers.township','=','mytownships.id');
        }       

      // if ($request->status!=''){
      //       $query->join('statuses','jobseekers.status','=','statuses.id');
      //   } 
      // if ($request->status!=''){
      //       $query->join('statuses','calllogs.status','=','statuses.id');
      //   } 
      if($request->skill_1!=''){
        $query->join('skills','jobseekers.skill_1','=','skills.id');
      }     
      // if($request->desire_position_1!=''){
      //   $query->join('desirepositions','jobseekers.desire_position_1','=','desirepositions.id');
      // }   
      // if($request->desire_position_1!=''){
      //   $query->join('job_titles','jobseekers.desire_position_1','=','job_titles.job_title_key');
      // }  
      // if($request->desire_position_2!=''){
      //   $query->join('job_titles as job_titles_dp2','jobseekers.desire_position_2','=','job_titles_dp2.job_title_key');
      // }        
      // if($request->previous_position_1!=''){
      //   $query->join('job_titles as job_titles_pp1','jobseekers.previous_position_1','=','job_titles_pp1.job_title_key');
      // }
      // if($request->previous_position_2!=''){
      //   $query->join('job_titles as job_titles_pp2','jobseekers.previous_position_2','=','job_titles_pp2.job_title_key');
      // }
      if($request->skill_2!=''){
        $query->join('skill2s','jobseekers.skill_2','=','skill2s.id');
      }
      if ($request->js_id!='') {       
        $query->where('jobseekers.jobseeker_id', 'LIKE', '%' . $request->js_id . '%' );
      }
      if ($request->fullname!='') {       
        $query->where('fullname', 'LIKE', '%' . $request->fullname . '%' );
      }
      if ($request->fb_name!='') {       
        $query->where('fb_name', 'LIKE', '%' . $request->fb_name . '%' );
      }
      if ($request->phone!='') {       
        $query->where('phone', 'LIKE', '%' . $request->phone . '%' );
      }      
      if ($request->status!='') {       
        
        
        //$query->where('statuses.description', 'LIKE', '%' . $request->status . '%' );
      }      
          
      if ($request->date!='') {       
        $query->whereDate('jobposition_jobseeker.created_at',$request->date);
      }      

      if ($request->first_age!='') {       
        $query->whereBetween('age', [$request->first_age, $request->second_age]);

      }



      if ($request->gender!='') {
        $gender = explode(',', $request->gender);    
        $query->whereIn('genders.gender_description',$gender);
      }      
      if ($request->township!='') {
        $exp_1 = explode(',', $request->township);    
        $query->whereIn('mytownships.mytownship_name',$exp_1);
      }
      
      if ($request->desire_position_1!='') {
        if ($request->desire_position_1 == 19) {
          $exp_2 = [19,null];
          $query->whereIn('jobseekers.desire_position_1',$exp_2);
          $query->orWhere('jobseekers.desire_function_1',19);
        }else{
          $exp_2 = explode(',', $request->desire_position_1); 
          $query->whereIn('jobseekers.desire_position_1',$exp_2);
        }
        
      }    
      if ($request->desire_position_2!='') {
        if ($request->desire_position_2 == 19) {
          $exp_3 = [19,null];
          $query->whereIn('jobseekers.desire_position_2',$exp_3);
          $query->orWhere('jobseekers.desire_function_2',19);
        }else{
          $exp_3 = explode(',', $request->desire_position_2); 
          $query->whereIn('jobseekers.desire_position_2',$exp_3);
        }
      }
      if($request->previous_position_1!=''){
        if ($request->previous_position_1 == 19) {
          $exp_4 = [19,null];
          $query->whereIn('jobseekers.previous_position_1',$exp_4);
          $query->orWhere('jobseekers.previous_function_1',19);
        }else{
          $exp_4 = explode(',', $request->previous_position_1); 
          $query->whereIn('jobseekers.previous_position_1',$exp_4);
        }
      }      
      if($request->previous_position_2!=''){
        if ($request->previous_position_2 == 19) {
          $exp_5 = [19,null];
          $query->whereIn('jobseekers.previous_position_2',$exp_5);
          $query->orWhere('jobseekers.previous_function_2',19);
        }else{
          $exp_5 = explode(',', $request->previous_position_2); 
          $query->whereIn('jobseekers.previous_position_2',$exp_5);
        }
      }  
      if($request->age!=''){
        $exp_6 = explode(',', $request->age);    
        $query->whereIn('multiages.age',$exp_6);
      }
      if ($request->skill_1!='') {
        $exp_7 = explode(',', $request->skill_1);    
       $query->where('skills.id',$exp_7);
      }
      if($request->skill_2!=''){
         // dd($request->skill_2);
        $exp_8 = explode(',', $request->skill_2);    
        $query->where('skill2s.id',$exp_8);
      } 
      return $query->select('jobseekers.*');     

          }
}