<?php

namespace App\Models\Checklist;

use Illuminate\Database\Eloquent\Model;

class Checklistjs extends Model
{
	public function author()
	{
		return $this->belongsTo('App\User','user_id');
	}
    public function scopeSearch($query, $request)
    { 

      if ($request->user!=''){
            $query->join('users','checklistjs.user_id','=','users.id');
        } 

      if ($request->user!='') {
       $query->where('users.name',$request->user);
      }
      if ($request->status!='') {
         if($request->status == 'applied'){
            $query->whereNotIn('applied', [0]);
         }
         elseif ($request->status == 'schedule') {
           $query->whereNotIn('scheduled_interview', [0]);
         }  
         elseif ($request->status == 'interview') {
           $query->whereNotIn('interviewed', [0]);
         }
         elseif ($request->status == 'got_job') {
           $query->whereNotIn('got_job', [0]);
         } 
         elseif ($request->status == 'job_browsed') {
           $query->whereNotIn('job_browsed', [0]);
         }
         else{
           $query->whereNotIn('followed_up', [0]);
         }
      }

      if ($request->from_created_at!='') {       
        $query->whereBetween('checklistjs.date', [$request->from_created_at, $request->to_created_at]);
      }
          return $query->select('checklistjs.*');     
    }

}
