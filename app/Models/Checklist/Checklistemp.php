<?php

namespace App\Models\Checklist;

use Illuminate\Database\Eloquent\Model;

class Checklistemp extends Model
{
	public function author()
	{
		return $this->belongsTo('App\User','user_id');
	}

    public function scopeSearch($query, $request)
    { 

      if ($request->user!=''){
            $query->join('users','checklistemps.user_id','=','users.id');
        } 

      if ($request->user!='') {
       $query->where('users.name',$request->user);
      }

      if ($request->created_at!='') {       
        $query->whereDate('checklistemps.created_at', '=',$request->created_at);
      }
     
      return $query->select('checklistemps.*');     
    }


}
