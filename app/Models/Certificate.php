<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    protected $table = 'certificates';

    public function jobseekers()
    {
    	return $this->hasMany('App\Models\Jobseeker');
    } 
}

