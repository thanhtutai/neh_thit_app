<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    
    public function calllogs()
    {
      return $this->hasMany('App\Models\Calllog');
    }
    
    public function jobseekers()
    {
    	return $this->hasMany('App\Models\Jobseeker');
    }    
    
    public function employercalllogs()
    {
    	return $this->hasMany('App\Models\Employercalllog');
    }

    public function allStatus()
    {
        return $query->where('stage',$this->stage);
    }

}
