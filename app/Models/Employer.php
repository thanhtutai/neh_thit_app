<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
class Employer extends Model
{
  
  // public $fillable = ['employer_name','phone_number'];
    public function employercalllogs()
    {
        return $this->hasMany('App\Models\Employercalllog','employer_id');
    }

    public function jobpositions()
    {
        return $this->hasMany('App\Models\Jobposition','employer_id','emp_id');
    }

    public function jobseekers()
    {
      return $this->hasMany('App\Models\Jobseeker','company_id','emp_id');
    }

    public function ja_paids()
    {
        return $this->hasMany('App\Models\JApaid','id','emp_id');
    }

       public function Mystatus()
    {
      return $this->belongsTo('App\Models\Employerstatus' , 'status');
    }

     public function scopeSearch($query, $request)
        { 
     
      if ($request->employer_name!='') {     
        $query->where('employer_name', 'LIKE', '%' . $request->employer_name . '%' );
      }      
     
      return $query->select('employers.*');     
    }

    public function lastEmpID()
    { 
      return (is_null($this->orderBy('emp_id','desc')->value('emp_id'))?1:$this->orderBy('emp_id','desc')->value('emp_id')+1);
    }
}
