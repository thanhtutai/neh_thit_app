<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;

class Calllog extends Model
{
    protected $fillable = [
        'id','jobseeker_id','active_inactive','id_number','delivery_date','type_of_call','start_time','minute','second','to_fullname','phone_number','call_reason','remark_calllog','call_receiver','published_at_2','status','company_id'
    ];

    public function Mystatus(){
        return $this->belongsTo('App\Models\Status','status');
    }

    public function jobseeker()
    {
        return $this->belongsTo('App\Models\Jobseeker','jobseeker_id')->latest();
    }

    public function Mycallreason()
    {
      return $this->belongsTo('App\Models\Callreason' , 'call_reason');
    }

	 public function author()
	 {
		return $this->belongsTo('App\User','caller_receiver');
   }
   public function latestIncomingCall() 
    {
       return $this->hasOne('App\Models\Jobseeker', 'jobseeker_id')->latest();
    }


    public function scopeSearch($query, $request,$stage)
        { 

      if ($stage != null) {
        $query->join('jobposition_jobseeker','calllogs.jobseeker_id','=','jobposition_jobseeker.jobseeker_id');
        $query->where('jobposition_jobseeker.stage',$stage);
      }
      if ($request->fullname!=''|| $request->js_id){
            $query->join('jobseekers','calllogs.jobseeker_id','=','jobseekers.id');
        } 
      if ($request->caller_receiver!=''){
            $query->join('users','calllogs.caller_receiver','=','users.id');
        } 
      if ($request->status!=''){
            $query->join('statuses','calllogs.status','=','statuses.id');
        } 
      if ($request->js_id!='') {       
               $query->where('jobseekers.id',$request->js_id);
      }    
      if ($request->remark_calllog!='') {       
        $query->where('remark_calllog', 'LIKE', '%' . $request->remark_calllog . '%' );
      }    
      if ($request->type_of_call!='') {       
        $query->where('type_of_call', 'LIKE', '%' . $request->type_of_call . '%' );
      }
      if ($request->call_reason!='') {       
        $query->where('call_reason', 'LIKE', '%' . $request->call_reason . '%' );
      }
      if ($request->call_back_date!='') {       
        $query->where('call_back_date',$request->call_back_date);
      }
      if ($request->created_at!='') {       
        $query->whereDate('calllogs.created_at', '=',$request->created_at);
      }
      if ($request->caller_receiver!='') {
       $query->where('users.name',$request->caller_receiver);
      }
      if ($request->fullname!='') {
       $query->where('fullname', 'LIKE', '%' . $request->fullname . '%' );
      }      

      if ($request->status!='') {
       $query->where('statuses.description',  $request->status);
      }
      
      return $query->select('calllogs.*');     
    }

    public function employer()
    {
      return $this->hasOne(Employer::class,'emp_id','company_id');
    }

    public function joinToPivot(){     
      
      return $this->join('jobposition_jobseeker', 'calllogs.jobseeker_id', '=', 'jobposition_jobseeker.jobseeker_id')
            ->join('jobpositions','jobposition_jobseeker.jobposition_id','=','jobpositions.job_id')
            ->where('calllogs.id',$this->id)
            ->where('jobposition_jobseeker.status',$this->status)
            ->select('calllogs.id','calllogs.jobseeker_id','jobposition_jobseeker.stage','jobposition_jobseeker.interview_time','jobposition_jobseeker.status','jobposition_jobseeker.jobposition_id','jobpositions.job_title','jobpositions.subtitle','jobposition_jobseeker.updated_at')
            ->groupBy('jobposition_jobseeker.jobposition_id')
            //->orderBy('jobposition_jobseeker.updated_at','desc')
            ->first();
    }
}
