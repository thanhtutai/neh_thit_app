<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Checklist extends Model
{
	public function author()
	{
		return $this->belongsTo('App\User','user_id');
	}

}
