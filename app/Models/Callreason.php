<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Callreason extends Model
{

	
    public function calllogs()
    {
      return $this->hasMany('App\Models\Calllog');
    }
	
    public function employercalllogs()
    {
      return $this->hasMany('App\Models\Employercalllog');
    }
}
