<?php 

namespace App\Models;
use App\Models\Jobposition;
use App\Models\Employer;
/**
* csv import
*/
use Illuminate\Http\Request;
use Session;

class CSV
{
    public function __construct($request)
    {   
        $this->request = $request;
    }

    public function Import()
    {   
        $mineType = $this->request['file']->getClientOriginalExtension();
        if ($mineType == 'csv') {
            $file_name = 'area_import.' . $mineType; 
            $file = fopen($this->request['file'], "r");
            $key = fgetcsv($file, 10000, ",");
            while (($value = fgetcsv($file, 10000, ",")) !== FALSE)
            {   
                $defined = array_combine($key, $value);
                switch ($defined['Gender']) {
                    case 'Male':
                        $gender = 1;
                        break;
                    case 'Female':
                        $gender = 2;
                        break;
                    case 'Both':
                        $gender = 3;
                        break;
                    default:
                        $gender = 1;
                        break;
                }
                $jobposition = new Jobposition;
                $jobposition->post_date = date('Y-m-d',strtotime($defined['DATE']));
                $jobposition->deadline = date('Y-m-d',strtotime($defined['Expired Date']));
                $jobposition->status = ($defined['Status'] == 'Available') ?1:0;
                $jobposition->subtitle = $defined['Name'];
                $jobposition->employer_id = $defined['Company ID'];
                $jobposition->job_id = $defined['Job ID (no dash)'];
                $jobposition->employer_name = $defined['Employer Name'];
                $jobposition->job_title = $defined['Job Title - Actual'];
                $jobposition->remark = $defined['Remark'];
                $jobposition->job_function_id = $defined['Job Function - Assigned 1'];
                $jobposition->require_position = $defined['Job Title - Assigned 1 (key)'];
                $jobposition->free_or_paid = ($defined['Contracted / Not Contracted'] == 'Contracted')?1:0;
                Employer::where('emp_id',$jobposition->employer_id)->update([
                    'phone_number'=>$defined['Phone'],
                    'address'=> $defined['Address'],
                    'email_address'=> $defined['Email'],
                    'zone' => $defined['Zone or Township']
                ]);
                $jobposition->email_address = $defined['Email'];
                $jobposition->address = $defined['Address'];
                $jobposition->zone = $defined['Zone or Township'];
                $jobposition->education = $defined['Education'];
                $jobposition->gender = $gender;
                $jobposition->industry = $defined['Industry'];
                $jobposition->any_special_requirements = $defined['# required'];
                $jobposition->certificate_1 = $defined['Certificate'];
                $jobposition->skill_1 = $defined['Skill'];
                $jobposition->experience_month = $defined['Exp (mths)'];
                $jobposition->salary_range = $defined['Salary Range'];
                $jobposition->ot_rate_per_day = $defined['OverTime'];
                $jobposition->transport = $defined['Transport'];
                $jobposition->accomodation = $defined['Accommodation'];
                //$jobposition->job_title = $defined['Off Day'];
                //$jobposition->job_title = $defined['Family List'];
                $jobposition->certificate_2 = $defined['Education Certificate or Degree or Diploma'];
                $jobposition->police_and_ward_rec = $defined['Police and Ward Rec'];
                //$jobposition->transport = $defined['Bus Stop'];
                $jobposition->food = $defined['Food'];
                $jobposition->certificate_2 = $defined['Driving Licence'];
                $jobposition->nrc = $defined['NRC'];                
                $jobposition->labour_card = $defined['Labour Card'];
                $jobposition->jp_img = $defined['Licence Photo'];
                $jobposition->minimum = $defined['Age Min'];
                $jobposition->maximum = $defined['Age Max'];
                $jobposition->save();
            }            
            fclose($file); 
          //Session::flash('message','Successfully excel import');
            return redirect('/freejob');
        }
        //Session::flash('message','Import type only accept csv');
        return redirect('/freejob');
    }
}