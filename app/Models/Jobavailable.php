<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jobavailable extends Model
{
     public function jobseekers()
    {
      return $this->hasMany('App\Models\Jobseeker','ja_position');
    }  
    
    public function Mydesire_position_1(){
        return $this->belongsTo('App\Models\Desireposition','ja_position');
    } 

     public function scopeSearch($query, $request)
    { 


      if ($request->ja_company_name!='') {       
        $query->where('ja_company_name', 'LIKE', '%' . $request->ja_company_name . '%' );
      }
      if ($request->ja_status!='') {       
        $query->where('ja_status', 'LIKE', '%' . $request->ja_status . '%' );
      }      

      if ($request->ja_position!='') {       
        $query->where('ja_position', 'LIKE', '%' . $request->ja_position . '%' );
      }     
       if ($request->ja_gender!='') {       
        $query->where('ja_gender', 'LIKE', '%' . $request->ja_gender . '%' );
      }      
      if ($request->township!='') {       
        $query->where('township', 'LIKE', '%' . $request->township . '%' );
      }      
      if ($request->ja_education!='') {       
        $query->where('ja_education', 'LIKE', '%' . $request->ja_education . '%' );
      }
      if ($request->ja_skill1!='') {       
        $query->where('ja_skill1', 'LIKE', '%' . $request->ja_skill1 . '%' );
      }
      if ($request->ja_skill2!='') {       
        $query->where('ja_skill2', 'LIKE', '%' . $request->ja_skill2 . '%' );
      }
      if ($request->ja_industry!='') {       
        $query->where('ja_industry', 'LIKE', '%' . $request->ja_industry . '%' );
      }

      return $query->select('jobavailables.*');     
    }

      
}