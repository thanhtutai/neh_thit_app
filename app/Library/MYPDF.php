<?php namespace App\Library ;
use Carbon\Carbon;
use TCPDF;

/**
 * Created by PhpStorm.
 * User: Nyi Nyi Lwin
 * Date: 6/17/2016
 * Time: 10:10 AM
 */

class MYPDF extends TCPDF
{
    public function Header() {
        // Logo
//        $image_file = public_path().'/img/oway.png';
//        $this->Image($image_file, 20, 20, 15, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
//        // Set font
//        $this->SetFont('helvetica', 'B', 13);
//        // Title
//        $this->Cell(0, 10, '', 0, false, 'C', 0, '', 0, false, 'M', 'M');

        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);
        // set bacground image
        $image_file = public_path().'/img/water.jpeg';
        $this->Image($image_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        $this->setPageMark();

    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
// $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// $fontname = TCPDF::addTTFfont('fonts/zawgyi.ttf', 'TrueTypeUnicode', '', 32);
        // $font = TCPDF::addTTFfont("fonts/zawgyi.ttf");
        // Set font
        // $this->SetFont($fontname, 'I', 8);
        // Page number
        $this->Cell(0, 10, date('Y',strtotime( Carbon::now())) . ' © Neh Thit', 0, false, 'L', 0, '', 0, false, 'T', 'M');
    }
}