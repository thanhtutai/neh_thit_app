<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use League\Fractal\Serializer\ArraySerializer;
use League\Fractal\Manager;

class DingoSerializerProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
         $this->app['Dingo\Api\Transformer\Factory']->setAdapter(function ($app) {
        $fractal = new \League\Fractal\Manager;
        $fractal->setSerializer(new \App\Transformers\NoDataArraySerializer());
        return new \Dingo\Api\Transformer\Adapter\Fractal($fractal);

    });

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
       
    }
}
