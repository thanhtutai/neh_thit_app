<?php

namespace App\Http\Controllers;

use App\Models\JobTitle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class JobTitlesController extends Controller
{
      public function __construct()
     {
        $this->middleware('auth');
     }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobtitles = JobTitle::orderBy('job_titles.job_title_key','asc')->get();
        return view('backend.jobtitles.index',compact('jobtitles')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          return view("backend.jobtitles.index");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
        $jobtitles = new JobTitle();
        $jobtitles->job_title_key = Input::get('job_title_key');
        $jobtitles->description = Input::get('description');
        $jobtitles->job_function_id = Input::get('j_function');


        if($jobtitles->save())
        {
            Session::flash('message','Job Title was successfully created');
            Session::flash('m-class','alert-success');
            return back()->with('success','Job Title successfully added!');
        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return redirect('jobtitles');
        }

   
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\JobTitles  $jobtitle
     * @return \Illuminate\Http\Response
     */
    public function show(JobTitles $jobtitle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JobTitles  $jobtitle
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $jobtitles = JobTitle::findOrFail($id);
        return view("backend.jobtitles.edit", compact('jobtitles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\JobTitles  $jobtitle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $jobtitles = JobTitle::find($id);

        $jobtitles->description = Input::get('description');

        if($jobtitles->save())
        {
            Session::flash('message','Job Title was successfully updated');
            Session::flash('m-class','alert-success');
            return redirect('jobtitles')->with('success','Job Title successfully updated!');
        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return redirect('jobtitles');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
