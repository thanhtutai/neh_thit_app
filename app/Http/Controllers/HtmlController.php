<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use App\Post;
use App\User;
use App\Jobseeker;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use DB;
use Yajra\Datatables\Html\Builder;
use Illuminate\Http\Request;

class HtmlController extends Controller
{
	  protected $htmlBuilder;

    public function getColumns(Datatables $datatables)
    {
        $columns = ['id', 'name', 'email', 'created_at', 'updated_at'];

        if ($datatables->getRequest()->ajax()) {
            return $datatables->of(User::select($columns))->make(true);
        }

        $html = $datatables->getHtmlBuilder()->columns($columns);

        return view('datatables.html.columns', compact('html'));
    }

}
