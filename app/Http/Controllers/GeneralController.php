<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Jobseeker;
use App\Models\Jobposition;
use App\Models\JobTitle;
use App\Models\Employer;
use DB;
use Carbon\Carbon;

class GeneralController extends Controller
{
    public function autoMatch(Request $request)
    {	
    	$data['stages'] = DB::table("stages")->pluck("description","id");
    	//$jobseekers = Jobseeker::where('jobseeker_id',$request->jobseeker_id)->first();
    	$data['employer'] = Employer::select('emp_id','employer_name')->groupBy('emp_id')->get();
    	// if ($jobseekers!=null) {
    	// 	$data['job_title'] = Jobposition::select('job_id','job_title')->get();    	

    	// }
    	
    	return $data;
    }

    public function setEmployer($id)
    {	 
        $current = Carbon::today();
    	return Jobposition::where('employer_id',$id)
                            //->whereDate('deadline', '>=', $current)
                            ->select('job_id','job_title')->get(); 
    }

    public function setStatus($id)
    {
    	$status = DB::table("statuses")
                    ->where("stage_key",$id)
                    ->orderby("statuses.id",'desc')
                    ->pluck("description","id")
                    ;
        return $status;
    }
}
