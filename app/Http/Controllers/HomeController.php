<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail,Session;
use App\Models\JApaid;
use App\Models\Jobavailable;
use App\Models\Calllog;
use Carbon\Carbon;
use DB;
use App\Models\Employer;
use App\Models\Employercalllog;
use App\Models\Jobseeker;
use App\Models\Jobposition;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function welcome(){
         $current = Carbon::today();
         $newjs              = Jobseeker::count();
         $j_available        = Jobposition::where('free_or_paid',0)->whereDate('jobpositions.deadline', '>=', $current)->get()->count();
         $j_paid             = Jobposition::where('free_or_paid',1)->whereDate('jobpositions.deadline', '>=', $current)->get()->count();
         $employers          = Employer::get()->count();
         $employercalllog    = Employercalllog::get()->count();
         $todo               = Calllog::orderBy('calllogs.created_at','DESC')->where('call_back_date', '<=',                  Carbon::             now()->toDateString())->get();

         $todo_count         = $todo->count();

         $todo_em            = Employercalllog::get()->count();

         $jobseekers         = DB::table('jobseekers')->count();
         $latest_jobseekers  = DB::table('jobseekers')->where('date', Carbon::now()->toDateString())->count();
         
         $calllogs           = DB::table('calllogs')->count();
         $team               = DB::table('users')->count();
         $subQuery           = DB::table('jobseekers')
                                ->select('fullname')
                                ->groupBy('fullname')
                                ->havingRaw('count(*) > 1');

         $duplicates         = DB::table('jobseekers')
                                ->select('jobseekers.*')
                                ->whereIn('fullname', $subQuery)
                                ->orderBy('fullname', 'desc')->count();


         return view('welcome',compact('jobseekers','calllogs','team','duplicates','todo_count','j_available','j_paid','employers','employercalllog','todo_em','newjs')); 
    }
   

    public function sendingEmail(Request $request,$file_name='')
    {   


       if($request['files']!= null){
        foreach ($request['files'] as $key => $file) {
            list($type, $file) = explode(';', $file);
            list(, $file) = explode(',', $file);
            list(,$ext) = explode('/', $type);
            $path = "uploads/";

            if (!is_dir($path)) {
                mkdir($path, 0777, true);
                fopen($path . "index.php", "wp");
            }

            $name = $file_name.time(). '.' . $ext;
            $encodedData = base64_decode($file);
            file_put_contents($path.'/'.$name, $encodedData);
            $upload_data[] = $path.$name;
        }
      }

        if($request['files']!= null){
        $app_user = ['subject'=> $request->subject,'email'=>$request->email,'myfile'=>$upload_data,'body'=>$request->body];       

        }else{
         $app_user = ['subject'=> $request->subject,'email'=>$request->email,'body'=>$request->body];
        }

        Mail::send('backend.email.test', ['user' => $app_user], function ($message) use ($request,$app_user)
        {  
            $route = public_path() . '/';

            $message->attach(storage_path('freestuff/Product_and_Service_Introduction.pdf'));   
            $message->attach(storage_path('freestuff/pricingtable.pdf'));   
            
         if($request['files']!= null){  
            for ($i=0; $i < count($app_user['myfile']); $i++) { 
                $message->attach($route.$app_user['myfile'][$i]);  
            }          
         }    
            $message->to($app_user['email'], $app_user['subject'])->subject($request->subject);
        });

        return back()->with('success','Email successfully sent !');
    } 
}
