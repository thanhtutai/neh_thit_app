<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Models\Jobseeker;
use App\Models\Employer;
use App\Models\Image;
use DB;
//use Response;
use \App\Models\Fabric;
use Illuminate\Http\Response;
use App\Transformers\JobTransformer;
use Dingo\Api\Routing\Helpers;
use Carbon\Carbon;
use App\Models\JobFunction;
use App\Models\Jobposition;
use Illuminate\Support\Facades\Log;
use Illuminate\Pagination\LengthAwarePaginator;
use Exception;
use App\Library\MYPDF;
use MMTCPDF;

class WebhookController extends Controller
{
    function generateRandomString($length = 10) {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
    }
    // json response test

    public function json_test(Request $request){
      $search_term = $request->desire_position;
      // $message =["messages"=>[["text" => "Hi.  is a lucky number..."]]];
      //$message = ["attachment"=>["type"=>"template","payload"=>["template_type"=>"list","top_element_style"=>"large","elements"=>Fabric::orderBy('id', 'DESC')->where('title', 'LIKE', "%$search_term%")
        //  ->select('title','postdate as image_url',"company_name as subtitle","")->get()]]]; 
      $page = LengthAwarePaginator::resolveCurrentPage();
      $total=DB::table('jobfunctions')->count('id'); 
      $perPage = 9;

      $job_functions_list = JobFunction::forPage($page, $perPage)->get();

      $jobs = new LengthAwarePaginator($job_functions_list, $total, $perPage, $page, [
            'path' => LengthAwarePaginator::resolveCurrentPath(),
        ]);
      // $job_functions_list = JobFunction::findOrFail(2);
      // $job_info = Fabric::orderBy('id', 'DESC')->where('title', 'LIKE', "%$search_term%")
                  // ->get();
     
      $job_function_array = array();
      foreach ($job_functions_list as $key => $value) {

          $characters = 'abcdefghijklmnopqrstuvwxyz';

          $charactersLength = strlen($characters);
          
          $randomString = '';
          
          for ($i = 0; $i < 10; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
          
            }
          
            $js_firstword = substr($randomString, 0, 1);

            $js_firstword = strtolower($js_firstword);
          
          if(!empty($js_firstword)){
            
            $image = Image::where('first_word',$js_firstword)->first();

            $image_url = $image['image'];
          }else{

            $image_url = "logo_a.jpg";
          
          } 

        $job_function_array[]  = ["title"=>$value['description_mm'],"image_url"=>url('img/'.$image_url) ,
                                  "subtitle"=>"test subtitle",
                                  "buttons"=>[["type"=>"json_plugin_url","url"=>url('/api/jobposition/'.$value['id']),"title"=>"ရွာမယ္"]] 
                                 ];           
      
      }
        $number = $page+1;

        if(!empty($job_function_array)){
        // $job_function_array = array();
          $job_function_array_2[]  = ["title"=>"တျခားအလုပ္ရွာမယ္","image_url"=>url('img/u.png') ,
                                    "subtitle"=>"test subtitle",
                                    "buttons"=>[["type"=>"json_plugin_url","url"=>url('/json?page='.$number.''),"title"=>"တျခားအလုပ္"]] 
                                   ];  

          $result = array_merge($job_function_array,$job_function_array_2);
        }else{

//           $result = ["text"=>"ေနာက္သုိ႔ျပန္သြားမယ္","quick_replies"=>[["title"=>"ေနာက္သုိ႔ျပန္သြားမယ္
// ","block_names"=>["Job Browsing Dynamic"]]]];

          $result = $job_function_array;

        } 


      $quick_reply = ["text"=>"ေနာက္သုိ႔ျပန္သြားမယ္","quick_replies"=>[["title"=>"ေနာက္သုိ႔ျပန္သြားမယ္
      ","block_names"=>["Menu"]]]];
      // dd($quick_reply);
      return ["messages"=>[ 
                           ["attachment"=>
                             ["type"=>"template","payload"=>
                                ["template_type"=>"generic","image_aspect_ratio"=>"square",
                                  "elements"=>$result                                      
                                ]     
                              ]
                           ],$quick_reply
                          ]
              ]; 

    }


    public function showJobposition($id){

     $currentdate = Carbon::today();

     $page = LengthAwarePaginator::resolveCurrentPage();
     $total=DB::table('jobpositions')->whereDate('jobpositions.deadline', '>=', $currentdate)->count('id'); 
     $perPage = 9;   
     
     $jobposition_info = Jobposition::where("job_function_id",$id)->whereDate('jobpositions.deadline', '>=', $currentdate)->forPage($page, $perPage)->orderBy('post_date','desc')->get();

     $jobs = new LengthAwarePaginator($jobposition_info, $total, $perPage, $page, [
            'path' => LengthAwarePaginator::resolveCurrentPath(),
        ]);

      $my_array =   json_decode($jobposition_info,true);

      $job_position_array = array();
      foreach ($my_array as $key => $value) {
        // var_dump($value);
  
          $employer_id = $value['employer_id'];

          $employer = Employer::where('emp_id',$employer_id)->first();
          // dd($employer);
          $employer_name = $employer['employer_name'];

          $emp_firstword = substr($employer_name, 0, 1);

          $emp_firstword = strtolower($emp_firstword);

          if(!empty($emp_firstword)){
            $image = Image::where('first_word',$emp_firstword)->first();

            $image_url = $image['image'];
          }else{

            $image_url = "logo_a.jpg";
          
          }

          switch ($value['gender']) {
            case 1:
              $gender = 'က်ား';
              break;
            case 2:
              $gender = 'မ';
            case 3:
              $gender = 'က်ား/မ';
            default:
              $gender = 'က်ား/မ';
            break;
          }
          $req = ($value['req_quantity'])?'-'.$value['req_quantity'].' post':'';
          $town = ($value['township'])?','.$value['township']:'';

          $job_title_mix = $value['job_title'] .'('. $gender .')'.$req.','.$employer_name.$town; 

          $job_body = $value['age_requirement']."\n".$value['education']."\n".$value['experience_requirement'];

          // dd($emp_firstword);
        // dd($value);
        if($value['free_or_paid'] == 1){
         $job_position_array[]  = ["title"=>$job_title_mix,"image_url"=>url('/img/'.$image_url),
                                "subtitle"=>$job_body,
                              "buttons"=>[["type"=>"json_plugin_url","url"=>url('/api/jobposition-req/'.$value['job_id']),"title"=>"အေသးစိတ္"],["set_attributes"=>["apply_job_id"=>$value['job_id']],"block_names"=>["userattribute"],
                                "type" =>"show_block",
                                "title"=>"ေလွ်ာက္မယ္"
                                ]] 
                             ];

         }else{
          $job_position_array[]  = ["title"=>$job_title_mix,"image_url"=>url('/img/'.$image_url),
                                "subtitle"=>$job_body,
                              "buttons"=>[["type"=>"json_plugin_url","url"=>url('/api/jobposition-req/'.$value['job_id']),"title"=>"အေသးစိတ္"]] 
                             ];
         }                    
      
      }

        $number = $page+1;
        // $job_function_array = array();
        $job_position_array_2[]  = ["title"=>"ေနာက္ထပ္ အလုပ္မ်ားၾကည့္မယ္","image_url"=>url('img/u.png') ,
                                    "subtitle"=>"test subtitle",
                                    "buttons"=>[["type"=>"json_plugin_url","url"=>url('/api/jobposition/'.$id.'?page='.$number.''),"title"=>"ၾကည့္မယ္"]] 
                                   ];  
        
        $result = array_merge($job_position_array,$job_position_array_2);

      $quick_reply = ["text"=>"ေနာက္သုိ႔ျပန္သြားမယ္","quick_replies"=>[["title"=>"ေနာက္သုိ႔ျပန္သြားမယ္
","block_names"=>["Job Browsing Dynamic"]]]];
      // dd($quick_reply);

      return ["messages"=>[ 
                           ["attachment"=>
                             ["type"=>"template","payload"=>
                                ["template_type"=>"generic","image_aspect_ratio"=>"square",
                                  "elements"=>$result                                      
                                ]     
                              ]
                           ],$quick_reply
                          ]
              ]; 
   
    }

    public function showReqJobposition($id){

      $jobposition_req_info = Jobposition::where('job_id',$id)->first();

      $job_req = $jobposition_req_info->job_requirement;

      $job_free_or_paid = $jobposition_req_info->free_or_paid;

      $job_position_id = $jobposition_req_info->job_id;

      // dd($job_position_id);
      if($job_free_or_paid == 1){
        $job_rquirement_button = ["set_attributes"=>["apply_job_id"=>$job_position_id],"block_names"=>["userattribute"],
                                "type" =>"show_block",
                                "title"=>"ေလွ်ာက္မယ္"
                                ];

       }else{
        $job_rquirement_button = ["set_attributes"=>["apply_job_id"=>$job_position_id],"block_names"=>["userattribute_unpaid"],
                                "type" =>"show_block",
                                "title"=>"ေလွ်ာက္မယ္"
                                ];
       }                         

      return ["messages"=>[
                          // ["redirect_to_blocks"=>["Name"]],
                           ["text" =>"အလုပ္ေလွ်ာက္ထားရန္အတြက္ေအာက္မွ ေလွ်ာက္မယ္ ဆိုတာေလးကိုု နွိပ္၍ ေလွ်ာက္ထားေပးပါရွင္႔။ အလုပ္ေလွ်ာက္ျပီးရင္ လူျကီးမင္း အခ်က္အလက္ေတြကိုု ကုမၸဏီ သို႔ပို႔ေဆာင္ေပးမွာျဖစ္တဲ႔ အတြက္ ေသခ်ာစဥ္းစားျပီး ေလွ်ာက္ပါရွင္႔။ ေက်းဇူးတင္ပါတယ္ရွင္႔။"],
                           ["attachment"=>
                             ["type"=>"template","payload"=>
                                ["template_type"=>"button",
                                  "text"=> $job_req,
                                 "buttons"=>[$job_rquirement_button]                                     
                                ]     
                              ]
                           ]
                          ]
              ]; 
   
    }

    public function applyJob(Request $request){
      Log::info($request);
      // Log::info($message);
      $requestData = $request->all();
      $jobposition = Jobseeker::where('messenger_user_id',$requestData['messenger_user_id'])->first();
        // $jobposition->job_position= Input::get('job_position');
      if($jobposition == null){
        // $jobposition = Jobseeker::where('id',2000)->first();

         $job_rquirement_button = ["set_attributes"=>["apply_job_id"=>2],"block_names"=>["Name"],
                                        "type" =>"show_block",
                                        "title"=>"Sign Up"
                                        ];

               return ["messages"=>[ 
                                   ["attachment"=>
                                     ["type"=>"template","payload"=>
                                        ["template_type"=>"button",
                                          "text"=> "Thank you for applying this job but you didnt sign up yet click the below button to register.",
                                         "buttons"=>[$job_rquirement_button]                                     
                                        ]     
                                      ]
                                   ]
                                  ]
                      ];

      }

     if(!empty( $requestData['apply_job_id'])){
      
        $jobpositions_array = $requestData['apply_job_id'];

        $jobposition_detail = Jobposition::where('job_id',$jobpositions_array)->first();

        $jobposition_id = $jobposition_detail->job_id;

      }
      else{
        $jobposition_id = 19999;
      }

      $jobseekers = DB::table("jobposition_jobseeker")->where('jobseeker_id',$jobposition['jobseeker_id'])->where('jobposition_id',$jobposition_id)->first();


      if(empty($jobseekers)){ 
        
        if(!empty($jobposition_id)){

       // $jobposition->My_jobpositions()->attach(Jobposition::find($jobpositions_array,['stage' => "2",'status'=>"1",'interview_time'=>"2018-03-09 15:20:31"]));

          $jobposition->My_jobpositions()->attach($jobposition_id, ['stage' => "2",'status'=> "9",'interview_time'=>""]);
          $jobposition->update(['position_status'=>9]);
        
        }
      
      }

     $jobpositions_id = $requestData['apply_job_id'];

     $jobposition_detail = Jobposition::where('job_id',$jobpositions_id)->first();

     $jobposition_free_or_paid = $jobposition_detail->free_or_paid;

     if($jobposition_free_or_paid == 1){

              return ["messages"=>[  [            
                           "text"=> "လူျကီးမင္း အခ်က္အလက္ေတြကိုု လူျကီးမင္းေလွ်ာက္ထားလိုတဲ႔ ကုမၸဏီ သို႔ပို႔ေဆာင္ေပး ေနပါျပီ။ လူျကီးမင္းထံသို႔ ၅ ရက္အတြင္းျပန္အေျကာင္းျကားေပးပါမယ္ရွင္။",
                             ]                                               
                          ]               
                      ]; 


     }else{


          $employer_id = $jobposition_detail->employer_id;

          $employer = Employer::where('emp_id',$employer_id)->first();
          // dd($employer);
          $employer_address = $employer['address'];


              return ["messages"=>[  [            
                           "text"=> "ဒီအလုပ္က မေန႔သစ္နဲ႔ မခ်ိတ္ဆက္ထားတဲ႔ အတြက္ ကိုယ္တိုင္သြားေလွွ်ာက္ရ မွာျဖစ္ပါတယ္ရွင္႔။အလုပ္သြားေလွ်ာက္ရမဲ႔ လိပ္စာကေတာ႔".$employer_address."",
                             ]                                               
                          ]               
                      ]; 
     }
    

      // $jobseeker_latest_row = Jobseeker::orderBy('id', 'desc')->first();
      // $data_array = [
      //                 'jobseeker_id'=> $jobseeker_latest_row->jobseeker_id + 1,
      //                 'fullname'=>$requestData['first_name'],
      //                 'messenger_user_id' =>$requestData['messenger_user_id']
      //               ];

      // $jobseeker = Jobseeker::create($data_array);
    
    }

    public function webhooktest(Request $request){
         $requestData = $request->all();
         $jobseeker_latest_row = Jobseeker::orderBy('id', 'desc')->first();
         $f_name  = $requestData['first_name'];
         $l_name  = $requestData['last_name'];
         $today = Carbon::today()->toDateString();
         $fullname = $f_name .' '.$l_name;


         $data_array = [

               'fb_name'=> $fullname,
               'jobseeker_id'=> $jobseeker_latest_row->jobseeker_id + 1,
               'messenger_user_id' =>$requestData['messenger_user_id'],
               'chatfuel_user_id' =>$requestData['chatfuel_user_id']
             
        ];

         $jobseeker = Jobseeker::create($data_array);
           // $jobseeker = Jobseeker::updateOrCreate(['messenger_user_id' => $requestData['messenger_user_id']],$data_array);
        

    } 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      //https://hooks.zapier.com/hooks/catch/2128531/1hue2x/
      //https://hooks.zapier.com/hooks/catch/2128531/snh2ea/
      // https://hooks.zapier.com/hooks/catch/2128531/snh2ea/
      // https://hooks.zapier.com/hooks/catch/2128531/snh2ea/
      // Log::info('Showing user profile for user: '.$request);
      // Log::info($request);
      $requestData = $request->all();
      
         $jobseeker_latest_row = Jobseeker::orderBy('jobseeker_id', 'desc')->first();

         $jobseeker_old = Jobseeker::where('messenger_user_id',$requestData['messenger_user_id'])->first();

         if($jobseeker_old != null){

            $jobseeker_id = $jobseeker_old->jobseeker_id;

         }else{

            $jobseeker_id = $jobseeker_latest_row->jobseeker_id + 1;
         
         }

         $f_name  = $requestData['first_name'];
         $l_name  = $requestData['last_name'];
         $today = Carbon::today()->toDateString();
         $fullname = $f_name .' '.$l_name;
         $require = [
            $request->Prev_Job_Title,
            $request->Prev_Job_Title_2,
            $request->Desired_Job_Title,
            $request->Desired_Job_Title_2
          ];
        // $post = Jobposition::whereIn('require_position',$require)
        //                     ->where('experience_month','',$request->Expected_Salary)

         $data_array = [
               'fullname'=>$requestData['Name'],
               'jobseeker_id'=> $jobseeker_id,
               'date'=> $today,
               'active_inactive'=> 1,
               'fb_name'=> $fullname,
               'gender' => $requestData['Gender'],
               'age' => $requestData['Age'],
               'expected_salary' => $requestData['Expected_Salary'],
               'expected_salary_other' => $requestData['Expected_Salary_Other'],
               'education' => $requestData['Grade'],
               'nrc_no'=>isset($requestData['NRC_NO'])?$requestData['NRC_NO']:'worng attribute key',
               'township'=>$requestData['Townships'],
               'wards' => $requestData['Wards'],
               'bus_stops' => $requestData['Busstops'],
               // 'bus_stops' => $requestData['Otherbusstops'],
               'previous_function_1' => $requestData['Previous_Function_1'],
               'certification_and_license' => $requestData['certificate'],
               'fabrics' => $requestData['Fabrics'],
               'experience_month_1' => $requestData['Experienced_Month_1'],
               'company_name_1' => $requestData['1st_company_name'],
               'previous_position_1' => $requestData['Prev_Job_Title'],
               'previous_position_2' => $requestData['Prev_Job_Title_2'],
               'previous_position_other' => $requestData['Prev_Job_Title_Other_1'],
               'previous_position_other_2' => $requestData['Prev_Job_Title_Other_2'],
               'previous_function_2' => $requestData['Previous_Function_2'],
               'desire_function_1' => $requestData['Desired_Function_1'],
               'desire_function_2' => $requestData['Desired_Function_2'],
               'type_of_license' => $requestData['certificate_2'],
               'experience_month_2' => $requestData['Experienced_Month_2'],
               'company_name_2' => $requestData['2nd_company_name'],
               'desire_position_1' => $requestData['Desired_Job_Title'],
               'desire_position_other_1' => $requestData['Desired_Job_Title_Other_1'],
               'desire_position_2' => $requestData['Desired_Job_Title_2'],
               'desire_position_other_2' => $requestData['Desired_Job_Title_Other_2'],
              // 'job_industry_1' => $requestData['Desired_Industry'],
              // 'job_industry_2' => $requestData['other_industry'],
               'skill_1' => $requestData['Skills_1'],
               'skill_other_1' => $requestData['other_skills_1'],
               'skill_2' => $requestData['Skills_2'],
               'skill_other_2' => $requestData['other_skills_2'],
               'viber_number' => $requestData['vibernumber'],
               'phone'=>$requestData['Phone'],
               'chatfuel_user_id'=>$requestData['chatfuel_user_id'],
              // 'last_user_freedom_input'=>$requestData['last_user_freeform_input'],
               'messenger_user_id' =>$requestData['messenger_user_id'],
//               'profile_pic_url' =>$requestData['profile_pic_url'],
               'certificate' =>$requestData['Certificates'],
               'certificate_other' =>$requestData['Certificates_other'],
               'position_status'=>9

        ];

          //$jobseeker = Jobseeker::create($data_array);
           $jobseeker = Jobseeker::updateOrCreate(['messenger_user_id' => $requestData['messenger_user_id']],$data_array);


    }

    public function newapplyJob(Request $request)
    {
      Log::info($request);
      $jobseeker = Jobseeker::where('messenger_user_id',$request->messenger_user_id)->first();
      if($jobseeker == null){
        $button = [
          "set_attributes"=>["apply_job_id"=>2],
          "block_names"=>["Name"],
          "type" =>"show_block",
          "title"=>"Sign Up"
        ];
        return [
          "messages"=>[ 
                   ["attachment"=>
                   ["type"=>"template","payload"=>
                   ["template_type"=>"button",
                    "text"=> 'ေဟး..'.$request->first_name.$request->last_name." စာရငး္မသြင္းထားရေသးဘူးေနာ္။ စာရင္းမသြင္းရေသးရင္ အလုပ္ေလွ်ာက္ထားလို႔ရမွာမဟုတ္ပါဘူးရွင္႔။ အလုပ္ေလွ်ာက္ထားရန္ စာရင္းသြင္းေပးပါဦးေနာ္။",
                    "buttons"=>[$button]                                     
                 ]     
               ]
             ]
           ]
         ];
      }      
      $pivot = DB::table("jobposition_jobseeker")
                        ->where('jobseeker_id',$jobseeker->jobseeker_id)
                        ->where('jobposition_id',$request->apply_job_id)
                        ->first();
      // main prority qualify with unit position
      if (!is_null($pivot)) {
        $button = [
          "block_names"=>["Job Browsing Dynamic"],
          "type" =>"show_block",
          "title"=>"ေနာက္သုိ႔ျပန္သြားမယ္"
        ];
         return [
          "messages"=>[ 
                   ["attachment"=>
                   ["type"=>"template","payload"=>
                   ["template_type"=>"button",
                   "text"=> "ဒီအလုပ္ကို လူၾကီးမင္း ေလွ်ာက္ထားျပီးပါျပီရွင္႔။ ေနာက္ထပ္တျခားအလုပ္ကို ေလွ်ာက္ထားလိုေသးပါက အလုပ္ရွာမယ္ကို နွိပ္၍ ေနာက္ထပ္အလုပ္ေတြကို ေလွ်ာက္ထားနုိင္ပါတယ္ရွင္႔။",
                   "buttons"=>[$button]                                     
                 ]     
               ]
             ]
           ]
         ];
      }

      $position = Jobposition::where('job_id',$request->apply_job_id)->first(); 
      // validation with qualify
      $check = $this->qualification($position,$jobseeker);

      if ($check == true) {
        $jobseeker->My_jobpositions()->attach($request->apply_job_id, ['stage' => "2",'status'=> "9",'interview_time'=>""]);
        $jobseeker->update(['position_status'=>9]);
        if($position->free_or_paid == 1){
          return ["messages"=>[  [            
               "text"=> "လူျကီးမင္း အခ်က္အလက္ေတြကိုု လူျကီးမင္းေလွ်ာက္ထားလိုတဲ႔ ကုမၸဏီ သို႔ပို႔ေဆာင္ေပး ေနပါျပီ။ လူျကီးမင္းထံသို႔ ၂ ရက္အတြင္းျပန္အေျကာင္းျကားေပးပါမယ္ရွင္။",
             ]                                               
           ]               
          ]; 
        }else{
          $employer = Employer::where('emp_id',$position->employer_id)->first();
            return ["messages"=>[  [            
                 "text"=> "ဒီအလုပ္က မေန႔သစ္နဲ႔ မခ်ိတ္ဆက္ထားတဲ႔ အတြက္ ကိုယ္တိုင္သြားေလွွ်ာက္ရ မွာျဖစ္ပါတယ္ရွင္႔။အလုပ္သြားေလွ်ာက္ရမဲ႔ လိပ္စာကေတာ႔ ".$employer['address']."",
               ]                                               
             ]               
           ]; 
        }
      }   

        $button = [
          "block_names"=>["Job Browsing Dynamic"],
          "type" =>"show_block",
          "title"=>"ေနာက္သုိ႔ျပန္သြားမယ္"
        ];
        return [
          "messages"=>[ 
                   ["attachment"=>
                   ["type"=>"template","payload"=>
                   ["template_type"=>"button",
                    "text"=>"ဝမ္းနည္းပါတယ္ရွင္.. ".$position->job_title." အတြက္ လူၾကီးမင္းနဲ႔ ကိုက္ညီမူ မရိွပါရွင့္။",
                    "buttons"=>[$button]                                     
                 ]     
               ]
             ]
           ]
         ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function autoReplyCV($user_id)
    {
      $jobseekers = Jobseeker::where('messenger_user_id',$user_id)->first();
      $url = $this->autoDownload($jobseekers);

      $result = [
        'messages'=>[
          [
            'attachment'=>[
              'type'=>'file',
              'payload'=>[
                'url'=>$url
                ]
            ]
          ]
        ]
      ];
      return $result; 
    }

    protected function autoDownload($jobseekers)
    {      
      $name = $jobseekers['name'];        
      $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false) ;
      $pdf->SetAuthor('Neh Thit');
      $pdf->SetTitle($jobseekers['fullname']);
      $pdf->SetSubject('CV Form');
      $pdf->SetKeywords('CV');
      $fontname = MMTCPDF::font('Zawgyi-One');
      $pdf->SetFont($fontname , 11);
      $pdf->setPrintHeader(false);
      $pdf->setPrintFooter(true);
      // set default monospaced font
      $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
      // set margins
      $pdf->SetMargins(PDF_MARGIN_LEFT, 15 , PDF_MARGIN_RIGHT);
      // set auto page breaks
      $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
      // set image scale factor
      $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
      $pdf->AddPage();
      $pdf->SetLineWidth(2);
      $pdf->SetAlpha(0.2);
      $pdf->Image(public_path('img/water1.png'), 80, 50, 50, 50, '', '#', '', true, 72);
      $pdf->Image(public_path('img/water1.png'), 80, 150, 50, 50, '', '#', '', true, 72);
      // restore full opacity
      $pdf->SetAlpha(1);
      $pdf->writeHTML($this->htmlElement($jobseekers), true, false, true, false, '');
      $pdf->SetLineWidth(2);
      $pdf->SetAlpha(0.2);
      $pdf->SetAlpha(0.2);
      $pdf->SetAlpha(1);
      $file_name = time().'_'.str_replace(' ', '_',$jobseekers['fullname']).'.pdf';
      $path = '/pdf/messenger/'.$file_name; 
      $pdf->Output($_SERVER['DOCUMENT_ROOT'] . $path,'F');  // save type
      
      return url($path);
    }

    protected function htmlElement($jobseekers)
    {
        $html = '
          <style> 
            .abc ul{
              font-size: 63px !important;
              list-style: none !important;
                padding-bottom:30px !important;
              padding-left: 0 !important;
            }.a{
              position: relative !important;
              padding-left: 11px !important;
              margin:0 0 3px 0 !important;
            }
            body {
              font-size: 8px;
            }
            #wrapper {
              width: 500px;
              margin: 0 auto;
              margin-top: 60px;
              margin-bottom: 100px;
            }
            .wrapper-top {
              width: 500px;
              height: 19px;
              margin: 0 auto;
              background-repeat: no-repeat;
            }
            .wrapper-mid {
              width: 500px;
              margin: 0 auto;
              background-repeat: repeat-y;
              padding-bottom: 40px;
            }
            .wrapper-bottom {
              width: 500px;
              height: 22px;
              padding: 0;
              margin: 0 auto;
              background-repeat: no-repeat;
            }
            #paper {
              width: 800px;
              margin: 0 auto;
            }
            #wrapper, .wrapper-mid {
              background: none;
            }
            .wrapper-top, .wrapper-bottom {
              display: none;
            }
            .print-pp p {
              font-size: 13px;
            }
            .haha{
              text-align:center;
            }
            h3 {
              font-size: 17px;
              color: #000000;
            }
            h5 {
              font-weight: bold;
              font-size: 10px;
            }
            td{
              font-size: 15px;
            }
            div.line {
              border-bottom: 1px solid #7dc24b;
            }
            .list-symbol-bullet li {
              line-height: 30px;
              list-style-type: none;
            }
            .address {
              margin-top: 20px;
            }
            .fileAttachmentAnnotation{
                background: black;
            }
          </style>
          <table>
            <tr>            
              <td colspan="3" class="haha"><h1>Curriculum Vitae</h1></td>
            </tr>
            <tr>
              <td colspan="3">
                <h2>PERSONAL</h2><hr/>
              </td>
            </tr>
            <tr>
              <td><br><br><b>Name:</b></td>
              <td><br><br>'.$jobseekers['fullname'].'</td>
              <td rowspan="5" class="third"></td>
            </tr>';
            if(!empty($jobseekers['gender'])){
              $html .=  ' 
                <tr>
                  <td><br><br><b>Gender:</b></td>
                  <td><br><br>'.\App\Models\Gender::where('id',$jobseekers['gender'])->pluck('gender_description')->first().'</td>        
                </tr>';
            }
            if(!empty($jobseekers['age'])){
              $html .=  ' 
                <tr>
                  <td><br><br><b>Age:</b></td>
                  <td><br><br>'.$jobseekers['age'].'</td>            
                </tr>';
            }
            if(!empty($jobseekers['education'])){
              $html.='
                <tr>
                  <td><br><br><b>Education:</b></td>
                  <td colspan="3"><br><br>'.\App\Models\Education::where('id',$jobseekers['education'])->pluck('name_en')->first().'</td>            
                </tr>';
            }  
            if(!empty($jobseekers['nationality'])){
              $html.='
                <tr>
                    <td><br><br><b>Nationality:</b></td>
                    <td><br><br>'.$jobseekers['nationality'].'</td>
                </tr>';
            }        
            if(!empty($jobseekers['nrc_no'])){
              $html.='
                <tr>
                    <td><br><br>
                        <b>N.R.C No. :</b>
                    </td>
                    <td><br><br>
                       '.$jobseekers['nrc_no'].'
                    </td>
                    
                </tr>';
            }        
            if(!empty($jobseekers['marital_status'])){
                $html.='
                 <tr>
                    <td><br><br>
                        <b>Marital Status:</b>
                    </td>
                    <td><br><br>
                       '.$jobseekers['marital_status'].'
                    </td>
                    
                </tr>
              ';
            }
            if(!empty($jobseekers['wards']) && $jobseekers['wards'] !=39){
                $html .=  ' 
                  <tr>
                    <td><br><br> <b>Location:</b></td>
                    <td colspan="2"><br><br>'.\App\Models\Ward::where('id',$jobseekers['wards'])->pluck('name')->first() .',';
                      if(!empty($jobseekers['township'])){
                        $html .= ' '. \App\Models\Township::where('id',$jobseekers['township'])->pluck('name')->first() .'';
                      }
                      $html .='
                    </td>
                  </tr>';
            }
            else{
                $html .=  ' 
                  <tr>
                    <td><br><br><b>Location:</b></td>
                    <td colspan="2"><br><br>';
                      if(!empty($jobseekers['township'])){
                        $html .=  ''. \App\Models\Township::where('id',$jobseekers['township'])->pluck('name')->first() .'';
                        }
                        $html .='
                    </td>
                  </tr>';
            }                
            if( !empty($jobseekers['previous_position_other']) || !empty($jobseekers['previous_position_other_2'])|| !empty($jobseekers['previous_position_1']) ){
              $html .=  ' 
                <tr>
                  <td colspan="3" height="35"><h2>PROFESSIONAL</h2><hr></td>
                </tr>';
                if (!empty($jobseekers['desire_position_1']) || !empty($jobseekers['desire_position_other_1'])) {
                  $tip = \App\Models\JobTitle::where('job_title_key',$jobseekers['desire_position_1'])->pluck('description')->first();
                  $desire = ($jobseekers['desire_position_1']!=301)?$tip:'';
                  $html.=' 
                    <tr>
                      <td colspan="3" height="40"><h3>Applied Position:<br></h3></td>
                    </tr>
                    <tr class="abc">
                      <td colspan="3" height="35"><ul> <li>'.$desire.' '.$jobseekers['desire_position_other_1'].'</li></ul>
                      </td>
                    </tr>';
                }        
                $html .=  ' 
                  <tr>
                    <td colspan="3" height="40"><h3>Previous Experience:<br></h3></td>
                  </tr>';
            }
            if(!empty($jobseekers['previous_position_1'])){                    
              if($jobseekers['previous_position_1']!=301){  
                $html.='
                <tr class="abc">
                <td colspan="3" height="35"><ul> <li>'.\App\Models\JobTitle::where('job_title_key',$jobseekers['previous_position_1'])->pluck('description')->first().checkExp($jobseekers['experience_month_1']) .(isset($jobseekers['company_name_1'])? ' at '. $jobseekers['company_name_1'].'' : '') .'</li></ul>
                </td>
                </tr>';
              }    
              else {
                if(!empty($jobseekers['previous_position_other'])){
                  $html.='
                  <tr class="abc">
                  <td colspan="3" height="30"><ul><li>'.$jobseekers['previous_position_other'].(isset($jobseekers['experience_month_1'])? ' Position for '. checkExp($jobseekers['experience_month_1']): '')  .(isset($jobseekers['company_name_1'])? ' at '. $jobseekers['company_name_1'].'' : '') .'</li></ul>
                  </td>
                  </tr>';
                }
              }
            }
            if($jobseekers['previous_position_2'] !=301){
              if(!empty($jobseekers['previous_position_2'])){                            
                $html.='
                <tr class="abc">
                <td colspan="3" >
                <ul> <li>'. \App\Models\JobTitle::where('job_title_key',$jobseekers['previous_position_2'])->pluck('description')->first()
                .checkExp($jobseekers['experience_month_2']).(isset($jobseekers['company_name_2'])? ' at '. $jobseekers['company_name_2'].'' : '') .'
                </li></ul></td>
                <td></td>
                </tr>';
              }  
            }        
            else {
              if(!empty($jobseekers['previous_position_other_2'])){
                $html.='
                <tr>
                <td colspan="3">
                <ul> <li>'. $jobseekers['previous_position_other_2']                 
                . (isset($jobseekers['experience_month_2'])? ' Position for '. checkExp($jobseekers['experience_month_2']): '') .(isset($jobseekers['company_name_2'])? ' at '. $jobseekers['company_name_2'].'' : '') .'
                </li></ul></td>
                </tr>
                ';
              }
            }     
            if($jobseekers['skill_1'] !=25 ){
              if(!empty($jobseekers['skill_1'])){            
                $html .=  ' 
                <tr>
                <td colspan="3"><h3>Skills:<br></h3></td>
                </tr>
                <tr class="abc">
                <td colspan="3" height="35">
                <ul><li>'.($jobseekers['skill_1'] == 25 ? '' : (($jobseekers['skill_1'] == 24) ? $jobseekers['skill_other_1'] : \App\Models\skill::where('id',$jobseekers['skill_1'])->pluck('name')->first())) .'
                </li></ul>
                </td>
                </tr>';
              }
            }
            if(!empty($jobseekers['skill_2']) ){
              if($jobseekers['skill_2'] != 25){
                $html .= ' 
                <tr>
                <td><ul><li>'.($jobseekers['skill_2'] == 25 ? '' : (($jobseekers['skill_2'] == 24) ? $jobseekers['skill_other_2'] : \App\Models\Skill2::where('id',$jobseekers['skill_2'])->pluck('name')->first())) .'</li></ul>
                </td>
                </tr>';
              } 
            }   
            if(!empty($jobseekers['certificate']) ){     
              if($jobseekers['certificate'] != 9){
                $html .=  ' 
                <tr>
                <td colspan="3"><h3>Certificate:<br></h3></td>
                </tr>
                <tr class="abc">
                <td colspan="3" height="35">
                <ul><li>'.($jobseekers['certificate'] == 9 ? '' : (($jobseekers['certificate'] == 8) ? $jobseekers['certificate_other'] : \App\Models\Certificate::where('id',$jobseekers['certificate'])->pluck('description')->first())) .

                '
                </li></ul>
                </td>
                </tr>'
                ;

              }     
              else { 
                if($jobseekers['certificate'] == 8){
                  if(!empty($jobseekers['certificate_other'])){
                    $html.='
                    <tr>
                    <td colspan="3"><h3>Certificate:<br></h3></td>
                    </tr>
                    <tr class="abc">
                    <td colspan="3" height="35">
                    <ul> <li>'.$jobseekers['certificate_other'].'</li></ul>
                    </td>
                    </tr>';
                  }
                } 
              }
            }
            if(!empty($jobseekers['additional_field'])){     
              $html .=  ' 
              <tr>
              <td colspan="3"><h3>Additional: <br></h3></td>
              </tr>
              <tr>
              <td height="30"><ul><li> '.$jobseekers['additional_field'] .'</li></ul></td>
              </tr>
              <tr>
              <td height="30"><ul><li> '.$jobseekers['additional_field2'] .'</li></ul></td>
              </tr>
              <tr>
              <td><ul><li> '.$jobseekers['additional_field3'].'</li></ul></td>
              </tr>';
            }                
          '</table>';

        return $html;
    }

    protected function qualification($position,$jobseeker)
    {
      // first prority qualify with gener
      if ($position['gender'] != 3 && $position['gender'] != $jobseeker['gender']) {
        return false;
      }
      
      $prev_key = array(
                $jobseeker['desire_position_1'],$jobseeker['desire_position_2'],
              );
      $desire_key = array(                
                $jobseeker['previous_position_1'],$jobseeker['previous_position_2'],
                );
      $exp_key = array(
                $jobseeker['experience_month_1'],$jobseeker['experience_month_2'],
              );

      // second prority qualify with desire position
      if (in_array($position['require_position'], $desire_key)) {
        // second child prority quanlify with experience
        if (in_array($position['experience_month'],$exp_key)) {
          return true;
        }
        return false;

      }
      // third prority qualify with previous position
      if(in_array($position['require_position'],$prev_key)){ 
        // third child prority qualify with previous position
        if (in_array($position['experience_month'],$exp_key)) {
          return true;
        }
        return false;
      }
      // four prority qualify with over high school edu & over 1 year exp.
      if ($jobseeker['education'] >= 3 && $jobseeker['experience_month_1'] >= 4 || $jobseeker['experience_month_2'] >= 4) {
        return true;
      } 
      return false;
    }
}
