<?php

namespace App\Http\Controllers;

use App\Models\Employer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Collective\HTML\FormBuilder;
use App\Http\AuthTraits\OwnsRecord;
use Excel;
use DB;
use App\Models\Jobposition;
use Validator;
use Redirect;


class EmployerController extends Controller
{
     use OwnsRecord;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
         $employer_name = $request->employer_name;
         $zone          = $request->zone;
         $age           = $request->age;
        
        if(count($request->all())!=0){  
           
            $employers=Employer::Search($request)->paginate(10);  
             
        }

        else{
        
            $employers = Employer::with('jobpositions')->paginate(12);

        }       

        return view('backend.employers.index',compact('employers'))->with('employer_name',$employer_name)->with('zone',$zone);        //
    }


    public function downloadExcel($type)
    {

        // $data1 = DB::table('employers')
                    
        //             ->select(['employer_name'])->get();
        // $data = json_decode($data1, true);
        $customizedcolumns = DB::table('employers')
                            ->select(['employer_name','email_address'])->get();

        $customizedcolumn_data  = json_decode($customizedcolumns,true);

        $employerdata = Employer::get()->toArray();

        // $data2 = Employer::get()->toArray();
        // $data = Employer::get()->toArray();
        return Excel::create('employer_excel', function($excel) use ($employerdata,$customizedcolumn_data) {

            $excel->sheet('employer_full_column', function($sheet) use ($employerdata)
                
            {
                $sheet->fromArray($employerdata);
            });  
            $excel->sheet('employer_customized_col', function($sheet) use ($customizedcolumn_data)

            {
                $sheet->fromArray($customizedcolumn_data);
            });
        })->download($type);
    }

    public function autocomplete(Request $request)
    {
        $data = Employer::select("employer_name as name")->where("employer_name","LIKE","%{$request->input('query')}%")->get();
        return response()->json($data);
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('backend.employers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       /* request()->validate([
            'emp_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]); */
        $employers = new Employer();
        $employers->emp_id = ($request->employer_id)?$request->employer_id:$employers->lastEmpID();
        $employers->employer_name = Input::get('employer_name');
        $employers->status = Input::get('status');
        $employers->phone_number = Input::get('phone_number');
        $employers->company_type = Input::get('company_type');
        $employers->notes = Input::get('notes');
        $employers->address = Input::get('address');
        $employers->zone = Input::get('zone');
        $employers->email_address = Input::get('email_address');
        $employers->authorized_person = Input::get('authorized_person');
        $employers->latitude = Input::get('latitude');
        $employers->longitude = Input::get('longitude');
        $employers->number_of_call = Input::get('number_of_call');
        $employers->interest =  Input::get('interest');
        $employers->understanding = Input::get('understanding');
        $employers->remark= Input::get('remark');
    
        if($file = $request->hasFile('emp_image')) {
                    
            $file = $request->file('emp_image') ;
            
            $fileName = $file->getClientOriginalName() ;
            $destinationPath = public_path().'/images/' ;
            $file->move($destinationPath,$fileName);
            $employers->emp_image = $fileName ;
        }
    
        if($employers->save())
        {
            Session::flash('message','Employer was successfully created');
            Session::flash('m-class','alert-success');
            //return back()->with('success','Employer successfully added!');
            return redirect('employers')->with('success','Employer successfully added!');
        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return redirect('order/create');
        }
       
 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employer  $employer
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

       $employers = Employer::with('jobpositions')->with('jobseekers')->where('emp_id',$id)->first();
       
// dd($employers);
        return view('backend.employers.show', compact('employers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employer  $employer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employers = Employer::findOrFail($id);
       
        return view("backend.employers.edit", compact('employers'));
    }

    public function send($id)
    {
        $employers = Employer::findOrFail($id);
       
        return view("backend.employers.send", compact('employers'));       
    }    
    public function send2($id)
    {
        $jobpositions = Jobposition::findOrFail($id);
       
        return view("backend.employers.send2", compact('jobpositions'));       
    }
        public function send3($id)
    {
        $jobpositions = Jobposition::findOrFail($id);
       
        return view("backend.employers.send3", compact('jobpositions'));       
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employer  $employer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   


     /*  $validation = Validator::make($request->all(), [
        'emp_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        
    ]);

    if($validation->fails()){

   return Redirect::back()->withErrors($validation)->withInput();
        }
        */
        
        
        $employers = Employer::find($id);
        $employers->emp_id   = Input::get('employer_id');
        $employers->employer_name = Input::get('employer_name');
        $employers->emp_image = Input::get('emp_image');
        $employers->status = Input::get('status');
        $employers->phone_number = Input::get('phone_number');
        $employers->company_type = Input::get('company_type');
        $employers->notes = Input::get('notes');
        $employers->address = Input::get('address');
        $employers->zone = Input::get('zone');
        $employers->email_address = Input::get('email_address');
        $employers->authorized_person = Input::get('authorized_person');
        $employers->latitude = Input::get('latitude');
        $employers->longitude = Input::get('longitude');
        $employers->number_of_call = Input::get('number_of_call');
        $employers->interest = Input::get('interest');
        $employers->understanding = Input::get('understanding');
        $employers->remark = Input::get('remark');

        if($file = $request->hasFile('emp_image')) {                    
            $file = $request->file('emp_image');            
            $fileName = $file->getClientOriginalName() ;
            $destinationPath = public_path().'/employer/images' ;
            $file->move($destinationPath,$fileName);
            $employers->emp_image = $fileName ;
        }

        

        if($employers->save())
        {
            Session::flash('message','Employer was successfully updated');
            Session::flash('m-class','alert-success');
             return redirect()->route('employers.index')->with('success','Employer successfully updated!');

        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return redirect('employers');
        }
    }

    public function updatestatus(Request $request,$id){
        $employers = Employer::find($id);
         $employers->status = Input::get('status');
         $employers->follow_up_time = Input::get('follow_up_time');
    if($employers->save())
        {
            Session::flash('message','Employer was successfully updated');
            Session::flash('m-class','alert-success');
             return back()->with('success','Employer successfully updated!');

        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return redirect('employers');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employer  $employer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employer $employer)
    {
        //
    }

    // test autocomplete 
    public function index2(){
          return view('autocomplete.index');
     }

    public function autoComplete2(Request $request) {
        $query = $request->get('term','');
        
        $products=Employer::where('employer_name','LIKE','%'.$query.'%')->get();
        
        $data=array();
        foreach ($products as $product) {
                $data[]=array('value'=>$product->employer_name,'id'=>$product->id);
        }
        if(count($data))
             return $data;
        else
            return ['value'=>'No Result Found','id'=>''];
    }
    

}
