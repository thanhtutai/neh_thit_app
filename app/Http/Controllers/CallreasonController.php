<?php

namespace App\Http\Controllers;

use App\Models\Callreason;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
class CallreasonController extends Controller
{

     public function __construct()
     {
        $this->middleware('auth');
     }    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $callreasons = Callreason::orderBy('callreasons.description','asc')->paginate(10);
       return view('backend.callreasons.index',compact('callreasons')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $callreasons = new Callreason();
        $callreasons->description = Input::get('description');


        if($callreasons->save())
        {
            Session::flash('message','Callreason was successfully created');
            Session::flash('m-class','alert-success');
            return back()->with('success','Callreason successfully added!');
        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return redirect('jobseekers');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Callreason  $callreason
     * @return \Illuminate\Http\Response
     */
    public function show(Callreason $callreason)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Callreason  $callreason
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $callreasons = Callreason::findOrFail($id);
       
        return view("backend.callreasons.edit", compact('callreasons'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Callreason  $callreason
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $callreasons = Callreason::find($id);

        $callreasons->description = Input::get('description');

        if($callreasons->save())
        {
            Session::flash('message','Callreason was successfully updated');
            Session::flash('m-class','alert-success');
            return redirect('callreasons')->with('success','Callreason successfully updated!');
        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return redirect('jobseekers');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Callreason  $callreason
     * @return \Illuminate\Http\Response
     */
    public function destroy(Callreason $callreason)
    {
        //
    }
}
