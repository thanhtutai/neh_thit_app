<?php

namespace App\Http\Controllers\Checklist;

use App\Models\Checklist\Checklistemp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Excel;
class ChecklistempController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if(count($request->all())!=0){  
            if ($request->has('sub_search')) {
                $jobseekers = Calllog::Subsearch($request)->paginate(10); 
                dd($applicant_information);

            }else{
                
                $checklistemps=Checklistemp::Search($request)->paginate(10);  
              //  dd($orders);
            } 

        }
        else{
            $checklistemps = Checklistemp::get();
        }       

        return view('backend.checklists.employer_sale_list',compact('checklistemps')); 
    }

    public function downloadExcel($type)
    {
        $data = Checklistemp::get()->toArray();
        return Excel::create('checklist_emp_excel', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('backend.checklists.create_employer_sale');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $checklists = new Checklistemp();
        $checklists->user_id = $request->user()->id;
        $checklists->new_employers_called = Input::get('new_employers_called');
        $checklists->new_employers_answered = Input::get('new_employers_answered');
        $checklists->new_employers_leads = Input::get('new_employers_leads');
        $checklists->new_employers_meeting_schedule = Input::get('new_employers_meeting_schedule');
        $checklists->followups_employers_called = Input::get('followups_employers_called');
        $checklists->followups_employers_answered = Input::get('followups_employers_answered');
        $checklists->followups_employers_leads = Input::get('followups_employers_leads');
        $checklists->followups_employers_meeting_schedule = Input::get('followups_employers_meeting_schedule');
        $checklists->followups_employers_called_total = Input::get('followups_employers_called_total');
        $checklists->followups_employer_sale = Input::get('followups_employer_sale');
      

        if($checklists->save())
        {
            Session::flash('message','Employer Sale was successfully created');
            Session::flash('m-class','alert-success');
            return back()->with('success','Employer Sale successfully added!');
        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return redirect('calllogs');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Checklistemp  $checklistemp
     * @return \Illuminate\Http\Response
     */
    public function show(Checklistemp $checklistemp)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Checklistemp  $checklistemp
     * @return \Illuminate\Http\Response
     */
    public function edit(Checklistemp $checklistemp)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Checklistemp  $checklistemp
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Checklistemp $checklistemp)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Checklistemp  $checklistemp
     * @return \Illuminate\Http\Response
     */
    public function destroy(Checklistemp $checklistemp)
    {
        //
    }
}
