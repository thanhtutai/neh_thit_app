<?php

namespace App\Http\Controllers\Checklist;
use App\Models\Checklist\Checklistjs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Excel;
class ChecklistjsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if(count($request->all())!=0){  
            if ($request->has('sub_search')) {
                $jobseekers = Calllog::Subsearch($request)->paginate(10); 
                dd($applicant_information);

            }else{
                
                $checklistjs=Checklistjs::Search($request)->paginate(10);  
              //  dd($orders);
            } 

        }
        else{
            $checklistjs = Checklistjs::orderBy('id', 'desc')->get();
        }       

        return view('backend.checklists.jobseeker_calls_list',compact('checklistjs')); 
    }

    public function downloadExcel($type)
    {
        $data = Checklistjs::get()->toArray();
        return Excel::create('checklist_js_excel', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     return view("backend.checklists.create_jobseeker_calls");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $checklists = new Checklistjs();
        $checklists->user_id = $request->user()->id;
       
        $checklists->date = Input::get('date');
        $checklists->applied = Input::get('applied');
        $checklists->scheduled_interview = Input::get('scheduled_interview');
        $checklists->interviewed = Input::get('interviewed');
        $checklists->got_job = Input::get('got_job');
        $checklists->job_browsed = Input::get('job_browsed');
        $checklists->followed_up = Input::get('followed_up');
        $checklists->total_number_of_calls = Input::get('total_number_of_calls');
      

        if($checklists->save())
        {
            Session::flash('message','Job Seeker Calls was successfully created');
            Session::flash('m-class','alert-success');
            return back()->with('success','Job Seeker Calls successfully added!');
        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return redirect('calllogs');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Checklistjs  $checklistjs
     * @return \Illuminate\Http\Response
     */
    public function show(Checklistjs $checklistjs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Checklistjs  $checklistjs
     * @return \Illuminate\Http\Response
     */
    public function edit(Checklistjs $checklistjs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Checklistjs  $checklistjs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Checklistjs $checklistjs)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Checklistjs  $checklistjs
     * @return \Illuminate\Http\Response
     */
    public function destroy(Checklistjs $checklistjs)
    {
        //
    }
}
