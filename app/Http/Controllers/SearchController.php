<?php

namespace App\Http\Controllers;
use App\Models\Jobseeker;
use App\Models\Employer;
use App\Models\Certificate;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class SearchController extends Controller
{


	public function search(Request $request,$route){

		$q = Input::get ( 'q' );
		switch ($route) {
			case 'jobseekers':
				$query = Jobseeker::select('*');
				$columns = ['fullname','previous_position_other','previous_position_other_2','desire_position_other_1','desire_position_other_2','skill_other_1','skill_other_2','certificate','certificate_other','expected_salary_other','expected_salary'];
				$redirect = 'jobseekers.index';
				break;
			case 'employers':
				$query = Employer::select('*');
				$columns = ['employer_name'];
				$redirect = 'employers.index';
				break;
			case 'reminders':
				# code...
				break;
			case 'payjob':
				# code...
				break;
			
			default:break;
		}
		foreach ($columns as $column) {
			$query->orWhere($column,'LIKE','%'.$q.'%');
			if ($column == 'certificate') {
				$cert_id = Certificate::where('description', 'LIKE', '%' . $q . '%' )->pluck('id')->toArray();
				if (!empty($cert_id)) {
					$query->whereNotIn('certificate',$cert_id);
				}
			}			
		}		
			
		if ($route == 'employers') {
			$employers = $query->paginate(100);
			return view('backend.'.$redirect,compact('employers'));
		}else{
			$result = $query->paginate(100);
		}
		
		return view('backend.'.$redirect,compact('result'));

		// $user = Employer::where( 'employer_name', 'LIKE', '%' . $q . '%' )->get();
		// if(count($user) == 0){
		// $user =Jobseeker::where ( 'fullname', 'LIKE', '%' . $q . '%' )->get();

	// 	foreach ($user as $value) {
	// 		// print_r($value);
	// 		if(!is_object($value)){
	// 			echo "not object";
	// 		}
	// 		else{

	// 			// echo get_class($value);
	// 			if(get_class($value)=="App\Models\Jobseeker"){
	// 			if (count ( $user ) > 0){	

	// 	        $result = null;
	// 	        if(count($user)<0){  

	// 	                $result=Jobseeker::Search($request)->paginate(100);
	// 	              //  dd($orders);
	// 	                dd($result);
	// 	            } 

	// 			$jobseekers =$user;
		       
	// 			return view ( 'backend.search.search_js',compact('result','jobseekers') )->withDetails ( $jobseekers )->withQuery ( $q );

	// 			}
	// 			else{

	// 			return view ( 'backend.search.search_emp' )->withMessage ( 'No Details found. Try to search again !' );
	// 		}
	// 			}
	// 			else{
	// 				echo "Noob!";		
	// 			}

	// 		}    
	// 	}

	// }

	// 	if (count ( $user ) > 0){
	// 		$employers = $user;
	// 		return view ( 'backend.search.search_emp',compact('employers') )->withDetails ( $employers )->withQuery ( $q );
	// 	}
	// 	else {
	// 		return view ( 'backend.search.search_emp',compact('employers') )->withMessage ( 'No Details found. Try to search again !' );
	// 	}
	// 	}
	}
}
