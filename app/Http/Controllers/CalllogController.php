<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Models\Calllog;
use App\Models\Jobseeker;
use App\Models\Employer;
use App\Models\Stage;
use App\Models\Status;
use App\Models\Jobposition;
use App\User;
use Auth;
use Collective\HTML\FormBuilder;
use Carbon\Carbon;
use App\Http\AuthTraits\OwnsRecord;
use Excel;
use DB;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use App\Models\JobTitle;

class CalllogController extends Controller
{
    use OwnsRecord;
    
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,$stage=null)
    {    
         $js_id             = $request->js_id;
         $fullname          = $request->fullname;
         $call_reason       = $request->call_reason;
         $age               = $request->age;
         $caller_receiver   = $request->caller_receiver;
         $remark_calllog    = $request->remark_calllog;
         $type_of_call      = $request->type_of_call;
         $type_of_call      = $request->type_of_call;
         $created_at        = $request->created_at;
         $call_back_date    = $request->call_back_date;
         $status            = $request->status;

        // if(count($request->all())!=0){  
                
        //         $jobseekers=Calllog::Search($request,$stage)->orderBy('calllogs.call_back_date','DESC')->paginate(20); 
        //         $js_row_number = count($jobseekers); 
        // }
        // else{
    
        //     $jobseekers = Calllog::Search($request,$stage)->orderBy('calllogs.call_back_date','DESC')->paginate(20);
      
        // }
        $jobseekers = Calllog::Search($request,$stage)->orderBy('calllogs.id','DESC')->paginate(20);
        $js_row_number = $jobseekers->total();
        
        $stages = Stage::select('id','description')->get();
        $statuses = is_null($stage)?Status::all():
                  Status::where('stage_key',$stage)->get();
        $authors = User::select('id','name')->get();

        return view('backend.calllogs.index',compact('jobseekers','js_row_number','authors','stages','stage','statuses'))->with('js_id',$js_id)->with('caller_receiver',$caller_receiver)->with('remark_calllog',$remark_calllog)->with('type_of_call',$type_of_call)->with('fullname',$fullname)->with('call_reason',$call_reason)->with('created_at',$created_at)->with('call_back_date',$call_back_date)->with('status',$status); 
    }
    
    public function isdone(Request $request, $id)
    {
        $calllogs = Calllog::find($id);
        // dd($jobseekers);
        $calllogs->is_done = 1;

        if($calllogs->save())
        {
            Session::flash('message','Calllog was successfully Done');
            Session::flash('m-class','alert-success');
             return back()->with('success','Calllog successfully Done!');

        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return redirect('jobseekers');
        }
    }

    //for followup todo list 
    public function todo(Request $request)
    {
         $js_id             = $request->js_id;
         $fullname          = $request->fullname;
         $call_reason       = $request->call_reason;
         $age               = $request->age;
         $caller_receiver   = $request->caller_receiver;
         $remark_calllog    = $request->remark_calllog;
         $type_of_call      = $request->type_of_call;
         $created_at        = $request->created_at;
         $call_back_date    = $request->call_back_date;
         $status            = $request->status;

        if(count($request->all())!=0){  
            
            $jobseekers=Calllog::Search($request)
                            ->select(DB::raw("max(calllogs.id) as myid"),"calllogs.jobseeker_id")
                            ->whereNotNull('call_back_date')
                            ->orderBy('call_back_date','desc')
                            ->groupBy('calllogs.jobseeker_id')  
                            ->paginate(10);
        }
        else{
           
            $jobseekers = Calllog::orderBy('calllogs.call_back_date','DESC')->whereNotNull('call_back_date')->where('is_done',null)->get();

            $jobseekers =Calllog::with('jobseeker')
                          ->select(DB::raw("max(id) as myid"),"calllogs.jobseeker_id")
                          ->whereNotNull('call_back_date')
                          ->orderBy('call_back_date','asc')
                          ->groupBy('jobseeker_id')  
                          ->paginate(10);
                
        }
               
        return view('backend.calllogs.todo',compact('jobseekers'))->with('js_id',$js_id)->with('caller_receiver',$caller_receiver)->with('remark_calllog',$remark_calllog)->with('type_of_call',$type_of_call)->with('fullname',$fullname)->with('call_reason',$call_reason)->with('created_at',$created_at)->with('call_back_date',$call_back_date)->with('status',$status); 
    }

    public function downloadExcel($type)
    {
        $data = Calllog::get()->toArray();
        return Excel::create('call_log_excel', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view("backend.calllogs.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function oldstore(Request $request)
    {   
        $dt = Carbon::now();
        $current = Carbon::today();
        // dd($dt);
        $jobseeker = new Calllog();
        $jobseeker->jobseeker_id = Input::get('jobseeker_id');
        $jobseeker->active_inactive = Input::get('active_key');
        $jobseeker->id_number = Input::get('id_number');
        $jobseeker->call_back_date = Input::get('call_back_date');
        // $jobseeker->call_back_date = Input::get('');
        $jobseeker->type_of_call = Input::get('type_of_call');
        $jobseeker->minute = Input::get('minute');
        $jobseeker->second = Input::get('second');
        $jobseeker->to_fullname = Input::get('to_fullname');
        $jobseeker->phone_number = Input::get('phone_number');
        $jobseeker->call_reason = Input::get('call_reason');
        $jobseeker->remark_calllog = Input::get('remark_calllog');
        // $jobseeker->interview_time = Input::get('interview_time');
        $jobseeker->company_id = Input::get('company_id');
        $jobseeker->company_recommended = Input::get('company_recommended');
        $jobseeker->caller_receiver = $request->user()->id;

        $jobseekers = Jobseeker::where('jobseeker_id',$request->jobseeker_id)->first();

        $jobseekers->active_inactive = $request->active_key;
        if ($request->manual) {
            $jobseekers->company_id = $request->employer;   
            $jobseeker->company_id = $request->employer;        
            $jobseekers->is_followup = 1;
            // $jobseekers->My_jobpositions()
            //            ->attach(Jobposition::find($request->job_title),[
            //                 'stage' => $request->stage,'status'=>$request->status,
            //                 'interview_time'=>$request->interview_time
            //             ]);
              
        }else{
            //$jobseekers->company_id = Input::get('company_id');
            //$jobseekers->active_inactive = Input::get('active_inactive');
            $jobseekers->is_followup = 1;
        }

        $jobposition = Jobseeker::where('jobseeker_id',Input::get('jobseeker_id'))->first(); 
        // $jobposition->job_position= Input::get('job_position');
        //$jobposition->require_position;
        $jobpositions = Input::get('job_position');
        $stage = Input::get('stage');
        $status = Input::get('status');
        $interview_time = Input::get('interview_time');
        $jobseeker->status = ($status!=null)?end($status):null;

        if(!empty($jobpositions)){
            $interview = array_values(array_filter($interview_time));
            foreach ($jobpositions as $index => $value) {

            //$jp = Jobposition::where('job_id',$value)->first();
            $jobposition->My_jobpositions()->attach($value,['stage' => $stage[$index],'status'=>$status[$index],'interview_time'=>isset($interview[$index])?date('Y-m-d H:i:s',strtotime($interview[$index])):null]);
            }
            $jobseekers->position_status = 1;
        }

         // dd('abc');
        
        if($jobseeker->save() && $jobseekers->save())
        {
            Session::flash('message','Order was successfully created');
            Session::flash('m-class','alert-success');
            return back()->with('success','Calllog successfully added!');
        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return redirect('calllogs');
        }
    }

    public function store(Request $request)
    {    
        $jobseeker = Jobseeker::find($request->jobseeker_id);
        $jobseeker->update([
          'active_inactive'=>$request->active_key,'position_status'=>1
        ]);
        
        if ($request->job_position) {
          $x=0;
          while ($x < count($request->job_position)) {

              $callog = new Calllog();
              $callog->jobseeker_id = $request->jobseeker_id;
              $callog->type_of_call = $request->type_of_call;
              $callog->call_back_date = $request->call_back_date;
              $callog->minute = $request->minute;
              $callog->remark_calllog = $request->remark_calllog;
              $callog->second = $request->second;
              $callog->active_inactive = $request->active_key;
              $callog->job_position = $request->job_position[$x];
              $callog->stage = $request->stage[$x];
              $callog->status = $request->status[$x];
              $callog->caller_receiver = auth()->id(); 
              $callog->save();
              $job_post = Jobposition::where('job_id',$request->job_position[$x])->first();

              if ($request->stage[$x] == 2 && $request->status[$x] == 8) {
                $response = $this->notMatch([
                  'user_id'=>$jobseeker->chatfuel_user_id,
                  'block'=>'5afc0194e4b03993fa4aa9ff',
                  'position'=>$job_post->job_title,
                  'company' => $job_post->employer_name
                ]);              
                
                // further processing ....
                if ($response['success'] == true) {
                  Session::flash('message','Successfully chatfuel auto Discarded.');
                  Session::flash('m-class','alert-success');
                }else{
                  Session::flash('message','Fails chatfuel auto Discarded.');
                  Session::flash('m-class','alert-danger');
                }
              }

              $jobseeker->My_jobpositions()->attach($request->job_position[$x],[
                'stage' => $request->stage[$x],
                'status'=>$request->status[$x],
                'interview_time'=>isset($request->interview_time[$x])?date('Y-m-d H:i:s',strtotime($request->interview_time[$x])):null
              ]);

              $x++;
          }  
        }else{
          $callog = new Calllog();
          $callog->jobseeker_id = $request->jobseeker_id;
          $callog->type_of_call = $request->type_of_call;
          $callog->call_back_date = $request->call_back_date;
          $callog->minute = $request->minute;
          $callog->remark_calllog = $request->remark_calllog;
          $callog->second = $request->second;
          $callog->active_inactive = $request->active_key;
          $callog->job_position = $request->job_position;
          $callog->caller_receiver = auth()->id(); 
          $callog->save();
        }          

        if(isset($callog))
        {
            Session::flash('message','Order was successfully created');
            Session::flash('m-class','alert-success');
            return back()->with('success','Calllog successfully added!');
        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return back()->with('success','Data is not saved!');
        }

        // $jobpositions = Input::get('job_position');
        // $stage = Input::get('stage');
        // $status = Input::get('status');
        // $interview_time = Input::get('interview_time');
        // $jobseeker->status = ($status!=null)?end($status):null;

        // if(!empty($jobpositions)){
        //     $interview = array_values(array_filter($interview_time));
        //     foreach ($jobpositions as $index => $value) {

        //     //$jp = Jobposition::where('job_id',$value)->first();
        //     $jobposition->My_jobpositions()->attach($value,['stage' => $stage[$index],'status'=>$status[$index],'interview_time'=>isset($interview[$index])?date('Y-m-d H:i:s',strtotime($interview[$index])):null]);
        //     }
        //     $jobseekers->position_status = 1;
        // }

         // dd('abc');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PreviousPosition_2  $previousPosition_2
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $calllogs = Calllog::findOrFail($id);
        // dd($calllogs);
        if($this->userNotOwnerOf($calllogs)) {
            return abort(404);
        }
     return view('backend.calllogs.show', compact("calllogs"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PreviousPosition_2  $previousPosition_2
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $calllogs = Calllog::findOrFail($id);        
        $stages = Stage::pluck('description','id');
        $statuses = Status::where('stage_key',$calllogs->mystatus['stage_key'])->pluck('description','id');
        
        return view("backend.calllogs.edit", compact('calllogs','stages','statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PreviousPosition_2  $previousPosition_2
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    { 
        $calllogs = Calllog::find($id);
        $calllogs->call_back_date = $request->call_back_date;
        $calllogs->jobseeker_id = $request->jobseeker_id;
        $calllogs->type_of_call = $request->type_of_call;
        $calllogs->minute = $request->minute;
        $calllogs->second = $request->second;
        $calllogs->call_reason = $request->call_reason;
        $calllogs->remark_calllog = $request->remark_calllog;
        $calllogs->status = $request->status;
        $calllogs->caller_receiver = auth()->id();

        if($calllogs->save())
        {
            $calllogs->jobseeker->My_jobpositions()->attach($request->job_position,[
              'stage' => $request->stage,
              'status'=>$request->status,
              'interview_time'=>isset($request->interview_time)?date('Y-m-d H:i:s',strtotime($request->interview_time)):null
            ]);

            Session::flash('message','jobseekers was successfully updated');
            Session::flash('m-class','alert-success');
            return redirect('calllogs')->with('success','calllogs update success!');
        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return redirect('jobseekers');
        }
    }

    public function todoupdate(Request $request, $id)
    {
        $calllogs = Calllog::find($id);

      // dd($calllogs);
        $calllogs->call_back_date = Input::get('call_back_date');
        $calllogs->jobseeker_id = Input::get('jobseeker_id');
        $calllogs->type_of_call = Input::get('type_of_call');
        $calllogs->start_time = Input::get('start_time');
        $calllogs->minute = Input::get('minute');
        $calllogs->second = Input::get('second');
        $calllogs->phone_number = Input::get('phone_number');
        $calllogs->call_reason = Input::get('call_reason');
        $calllogs->remark_calllog = Input::get('remark_calllog');
        $calllogs->status = Input::get('status');
        $calllogs->caller_receiver = $request->user()->id;

 

        if($calllogs->save())
        {
            Session::flash('message','jobseekers was successfully updated');
            Session::flash('m-class','alert-success');
            return redirect('todo')->with('success','Todo update success!');
        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return redirect('jobseekers');
        }
 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PreviousPosition_2  $previousPosition_2
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        Calllog::destroy($id);
        Session::flash('message','calllogs was successfully delete');
        Session::flash('m-class','alert-success');
        return back();
    }
            //
    
    protected function notMatch($data)
    { 
      $bot_id = '5a39b9e5e4b0561664a68819';
      $token = 'mELtlMAHYqR0BvgEiMq8zVek3uYUK3OJMbtyrdNPTrQB9ndV0fM7lWTFZbM4MZvD';
      $block = $data['block'];
      $user_id = $data['user_id'];

      $url = 'https://api.chatfuel.com/bots/'.$bot_id.'/users/'.$user_id.'/send?chatfuel_token='.$token.'&chatfuel_message_tag=APPLICATION_UPDATE&chatfuel_block_id='.$block.'&COMPANY='.$data['company'].'&apply_job_id='.$data['position'];
      
      $client = new Client;   
      try {
          $response = $client->post($url);
          return json_decode( $response->getBody()->getContents(),true );
      } catch (ClientException $e) {
        return json_decode( $e->getResponse()->getBody()->getContents(),true );
      }  
    }

    public function discharged($id)
    {
        $jobseeker = Jobseeker::find($id);

        DB::table('jobposition_jobseeker')->where('jobseeker_id',$jobseeker->jobseeker_id)->orderBy('id','desc')->update(['status'=>8]); 
        $pivot = DB::table('jobposition_jobseeker')->where('jobseeker_id',$jobseeker->jobseeker_id)->orderBy('id','desc')->first();

        $callog = new Calllog();
        $callog->jobseeker_id = $id;
        $callog->active_inactive = 0;
        $callog->job_position = $pivot->jobposition_id;
        $callog->stage = 2;
        $callog->status = 8;
        $callog->caller_receiver = auth()->id(); 
        $callog->save();

        $job_post = Jobposition::where('job_id',$pivot->jobposition_id)->first();

        if ($job_post==null) {
          $title = JobTitle::where('job_title_id',$jobseeker->desire_position_1)->first();
          $position = $title->description;
          $employer_name = '';
        }else{
          $employer_name = $job_post->employer_name;
        }
        
        $response = $this->notMatch([
                  'user_id'=>$jobseeker->chatfuel_user_id,
                  'block'=>'5afc0194e4b03993fa4aa9ff',
                  'position'=>$position,
                  'company' => $employer_name
                ]);  
        if ($response['success'] == true) {
          Session::flash('message','Successfully chatfuel auto Discarded.');
          Session::flash('m-class','alert-success');
        }else{
          Session::flash('message','Fails chatfuel auto Discarded.');
          Session::flash('m-class','alert-danger');
        }
        return back();        
    }
}
