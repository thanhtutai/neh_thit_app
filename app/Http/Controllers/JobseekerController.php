<?php

namespace App\Http\Controllers;
use Auth;
use Datatables;
use App\Models\Jobseeker;
use App\Models\skill;
use App\Models\Skill2;
use App\Models\Ward;
use App\Models\JobTitle;
use App\Models\Township;
use App\Models\Calllog;
use App\Models\JApaid;
use App\Models\Jobavaiable;
use App\Models\Desireposition;
use App\Models\DesirePosition_2;
use App\Models\PreviousPosition;
use App\Models\PreviousPosition_2;
use App\Models\Status;
use App\Models\Jobposition;
use Illuminate\Http\Request;
use SiteHelper;
use PDF;
use Mail;
use Illuminate\Support\Facades\Input;
use App\Library\MYPDF;
use Illuminate\Support\Facades\Session;
use Collective\HTML\FormBuilder;
use DB;
use Excel;
use Carbon\Carbon;
use TCPDF;
use MMTCPDF;
use App\Models\JobPivot;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Pagination\LengthAwarePaginator;
use Rap2hpoutre\FastExcel\FastExcel;
// use App\Models\Calllog;
class JobseekerController extends Controller
{

     public function __construct()
     {
        $this->middleware('auth');
     }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

 public function index(Request $request)
    {
         $fullname = $request->fullname;
         $fb_name  = $request->fb_name;
         $nrc_no   = $request->nrc_no;
         $age      = $request->age;
         $phone      = $request->phone;
         $gender   = $request->gender;
         $township = $request->township;
         $status   = $request->status;
         $dp1      = '';
         $dp2      = '';
         $pp1      = '';
         $pp2      = '';
         $sk1      = '';;
         $js_id    = $request->js_id;
         $sk2      = '';
        if ($request->skill_1) {
            foreach (explode(',', $request->skill_1) as $key) {
                $sk1 .= skill::find($key)->name.',';
            }
        }
        if($request->skill_2){
             foreach (explode(',', $request->skill_2) as $key) {
                $sk2 .= Skill2::find($key)->name.',';
            }
        }
        if ($request->desire_position_1) {
            foreach (explode(',', $request->desire_position_1) as $key) {
                $dp1 .= JobTitle::where('job_title_key',$key)->value('description').',';
            }
        }
        if ($request->desire_position_2) {
            foreach (explode(',', $request->desire_position_2) as $key) {
                $dp2 .= JobTitle::where('job_title_key',$key)->value('description').',';
            }
        }
        if ($request->previous_position_1) {
            foreach (explode(',', $request->previous_position_1) as $key) {
                $pp1 .= JobTitle::where('job_title_key',$key)->value('description').',';
            }
        }
        if ($request->previous_position_2) {
             foreach (explode(',', $request->previous_position_2) as $key) {
                $pp2 .= JobTitle::where('job_title_key',$key)->value('description').',';
            }
        }

        $result = null;
        if(count($request->all())!=0){  
                
               $result=Jobseeker::Search($request)->orderBy('jobseeker_id', 'desc')->paginate(10);    
                
            
           } 
            
         
        else{
           
            $jobseekers = Jobseeker::with('calllogs')->orderBy('jobseeker_id', 'desc')->paginate(10);
        
        }       

        // dd($jobseekers);]i=8
        
       return view('backend.jobseekers.index',compact('jobseekers','result'))->with('js_id',$js_id)->with('fullname',$fullname)->with('fb_name',$fb_name)->with('phone',$phone)->with('age',$age)->with('gender',$gender)->with('township',$township)->with('dp1',$dp1)->with('dp2',$dp2)->with('pp1',$pp1)->with('pp2',$pp2)->with('status',$status)->with('sk1',$sk1)->with('sk2',$sk2)->with('nrc_no',$nrc_no); 
    }

    //for reminder section for today and tmr appointed interview

     public function reminder(Request $request)
     {
        
         $fullname = $request->fullname;
         $fb_name  = $request->fb_name;
         $age      = $request->age;
         $gender   = $request->gender;
         $township = $request->township;
         $status   = $request->status;
         $dp1      = $request->desire_position_1;
         $dp2      = $request->desire_position_2;
         $pp1      = $request->previous_position_1;
         $pp2      = $request->previous_position_2;
         $js_id    = $request->js_id;

        $result = null;
        if(count($request->all())!=0){  
            
            $current = Carbon::today();
            $mytime = Carbon::today();
            $trialExpires = $mytime->addDays(2);
            
            $results = DB::table("jobposition_jobseeker")->orderBy('interview_time', 'desc')->whereBetween('interview_time', [$current,$trialExpires])->paginate(2);

            if(!empty($results)){
        
                foreach ($results as $key => $value) {

                    $job_id= $value->jobposition_id;

                     $result[] = Jobposition::find($job_id)->My_jobseekers()->Search($request)->get();

                }                
            } 

        }

        else{

            $current = Carbon::today();
            $mytime = Carbon::today();
            $trialExpires = $mytime->addDays(2);

            $job_list = DB::table("jobposition_jobseeker")->orderBy('interview_time', 'desc')->whereBetween('interview_time', [$current,$trialExpires])->paginate(2);
            
            if(!empty($job_list)){
            
                foreach ($job_list as $key => $value) {
                    $job_id= $value->jobposition_id;

                     $jobseekers[] = Jobposition::find($job_id)->My_jobseekers()->get();

                }                
            } 


        }     
     
       return view('backend.jobseekers.remind_section',compact('jobseekers','result','results','job_list'))->with('js_id',$js_id)->with('fullname',$fullname)->with('fb_name',$fb_name)->with('age',$age)->with('gender',$gender)->with('township',$township)->with('dp1',$dp1)->with('dp2',$dp2)->with('pp1',$pp1)->with('pp2',$pp2)->with('status',$status); 

    }

    public function jobseeker_jobbrowse(Request $request,$type='all'){

         $fullname = $request->fullname;
         $fb_name  = $request->fb_name;
         $age      = $request->age;
         $phone      = $request->phone;
         $gender   = $request->gender;
         $township = $request->township;
         $status   = $request->status;
         $dp1      = '';
         $dp2      = '';
         $pp1      = '';
         $pp2      = '';
         $sk1      = '';;
         $js_id    = $request->js_id;
         $sk2      = '';
        if ($request->skill_1) {
            foreach (explode(',', $request->skill_1) as $key) {
                $sk1 .= skill::find($key)->name.',';
            }
        }
        if($request->skill_2){
             foreach (explode(',', $request->skill_2) as $key) {
                $sk2 .= Skill2::find($key)->name.',';
            }
        }
        if ($request->desire_position_1) {
            foreach (explode(',', $request->desire_position_1) as $key) {
                $dp1 .= JobTitle::where('job_title_key',$key)->value('description').',';
            }
        }
        if ($request->desire_position_2) {
            foreach (explode(',', $request->desire_position_2) as $key) {
                $dp2 .= JobTitle::where('job_title_key',$key)->value('description').',';
            }
        }
        if ($request->previous_position_1) {
            foreach (explode(',', $request->previous_position_1) as $key) {
                $pp1 .= JobTitle::where('job_title_key',$key)->value('description').',';
            }
        }
        if ($request->previous_position_2) {
             foreach (explode(',', $request->previous_position_2) as $key) {
                $pp2 .= JobTitle::where('job_title_key',$key)->value('description').',';
            }
        }

        $result = null;
        // if(count($request->all())!=0){  
        //     if(!empty($request->status)){  
        //         $page = LengthAwarePaginator::resolveCurrentPage();
        //         $total=DB::table('jobposition_jobseeker')->where( 'status',$request->status)->count('jobseeker_id'); 
        //         //Count the total record
        //         $perPage=100;

        //         $result_paginate = DB::table('jobposition_jobseeker')->where( 'status', $request->status)->forPage($page, $perPage)->groupBy('jobseeker_id')->orderBy('created_at', 'desc')->get();
        //         $jobseeker_list = new LengthAwarePaginator($result_paginate, $total, $perPage, $page, [
        //                 'path' => LengthAwarePaginator::resolveCurrentPath(),
        //             ]); 
        //     }
        //     else{
        //         $page = LengthAwarePaginator::resolveCurrentPage();
        //         $total=DB::table('jobposition_jobseeker')->count('jobseeker_id'); //Count the total record
        //         $perPage=100;
                 
        //         //Set the limit and offset for a given page.
        //         $jobseeker_list = DB::table('jobposition_jobseeker')->where('status',9)->groupBy('jobseeker_id')->orderBy('created_at', 'desc')->paginate(10);
        //         // $results[] = Jobseeker::where('jobseeker_id',$value->jobseeker_id)->forPage($page, $perPage)->get();
        //         // $result_paginate = new LengthAwarePaginator($jobseeker_list, $total, $perPage, $page, [
        //         //     'path' => LengthAwarePaginator::resolveCurrentPath(),
        //         // ]);    

        //     }

        //     foreach ($jobseeker_list as $key => $value) {
        //         $result[] = Jobseeker::Mysearch($request)->where('jobseeker_id',$value->jobseeker_id)->orderBy('created_at','desc')->first();
        //     }
        // } 
        // else{
           
        //     $page = LengthAwarePaginator::resolveCurrentPage();
        //     $total=DB::table('jobposition_jobseeker')->count('jobseeker_id'); //Count the total record
        //     $perPage=100;
             
        //     //Set the limit and offset for a given page.
        //     $results = DB::table('jobposition_jobseeker')->where('status',9)->groupBy('jobseeker_id')->orderBy('created_at', 'desc')->paginate(10);
        //     // // $results[] = Jobseeker::where('jobseeker_id',$value->jobseeker_id)->forPage($page, $perPage)->get();
        //     // $jobseekers_paginate = new LengthAwarePaginator($results, $total, $perPage, $page, [
        //     //     'path' => LengthAwarePaginator::resolveCurrentPath(),
        //     // ]);    
            
                   
        //            // dd($results); 
        //     foreach ($results as $key => $value) {
        //     // //Set the limit and offset for a given page.

        //     $jobseekers[] = Jobseeker::where('jobseeker_id',$value->jobseeker_id)->groupBy('jobseeker_id')->first();
        //     // $jobseekers[] = array_unique($jobseekers);
        //     }
        
        // }
        
        if(count($request->all())!=0){
            $ss = ($request->status)?$request->status:9;
            $type = ($type=='old')?['!=',9]:['=',9];
            $result = Jobseeker::Mysearch($request)
                    ->select('jobseekers.jobseeker_id','jobseekers.fb_name','jobseekers.previous_function_1','jobseekers.previous_function_2','jobseekers.previous_position_1','jobseekers.previous_position_2','jobseekers.previous_position_other','jobseekers.fullname','jobseekers.phone','jobseekers.previous_position_other_2','jobseekers.viber_number','jobseekers.desire_function_1','jobseekers.desire_function_2','jobseekers.desire_position_1','jobseekers.desire_position_2','jobseekers.skill_1','jobseekers.skill_2','jobseekers.skill_other_1','jobseekers.skill_other_2','jobseekers.gender','jobseekers.age','jobseekers.township','jobseekers.wards','jobseekers.bus_stops','jobseekers.education','jobseekers.experience_month_1','jobseekers.experience_month_2','jobseekers.certificate_other','jobseekers.certificate','jobseekers.usertype_1','jobseekers.last_user_freedom_input','jobseekers.sign_up_from','jobposition_jobseeker.interview_time','jobseekers.remark','jobposition_jobseeker.status','jobposition_jobseeker.stage','jobseekers.position_status','jobposition_jobseeker.updated_at','jobseekers.created_at'
                        )
                    ->where('jobposition_jobseeker.status',$type[0],$type[1])
                    ->where('jobseekers.position_status',$type[0],$type[1])
                    ->groupBy('jobposition_jobseeker.jobseeker_id')
                    ->orderBy('jobseekers.created_at','desc')
                    ->orderBy('jobposition_jobseeker.updated_at','desc')
                    ->paginate(10);
        }else{  
            $type = ($type=='old')?['!=',9]:['=',9];
            $jobseekers = Jobseeker::leftjoin('jobposition_jobseeker','jobseekers.jobseeker_id','=','jobposition_jobseeker.jobseeker_id')
                    ->select('jobseekers.jobseeker_id','jobseekers.fb_name','jobseekers.previous_function_1','jobseekers.previous_function_2','jobseekers.previous_position_1','jobseekers.previous_position_2','jobseekers.previous_position_other','jobseekers.fullname','jobseekers.phone','jobseekers.previous_position_other_2','jobseekers.viber_number','jobseekers.desire_function_1','jobseekers.desire_function_2','jobseekers.desire_position_1','jobseekers.desire_position_2','jobseekers.skill_1','jobseekers.skill_2','jobseekers.skill_other_1','jobseekers.skill_other_2','jobseekers.gender','jobseekers.age','jobseekers.township','jobseekers.wards','jobseekers.bus_stops','jobseekers.education','jobseekers.experience_month_1','jobseekers.experience_month_2','jobseekers.certificate_other','jobseekers.certificate','jobseekers.usertype_1','jobseekers.last_user_freedom_input','jobseekers.sign_up_from','jobposition_jobseeker.interview_time','jobseekers.remark','jobposition_jobseeker.status','jobposition_jobseeker.stage','jobseekers.position_status','jobposition_jobseeker.updated_at','jobseekers.created_at'
                )
                    ->where('jobposition_jobseeker.status',$type[0],$type[1])
                    ->where('jobseekers.position_status',$type[0],$type[1])
                    ->groupBy('jobposition_jobseeker.jobseeker_id')
                    ->orderBy('jobseekers.created_at','desc')
                    ->orderBy('jobposition_jobseeker.updated_at','desc')
                    ->paginate(10);
        }
        //dd($jobseekers);
        return view('backend.jobseekers.jobseeker_jobbrowse',compact('jobseekers','result','jobseekers_paginate','result_paginate','results','jobseeker_list'))->with('js_id',$js_id)->with('fullname',$fullname)->with('fb_name',$fb_name)->with('age',$age)->with('gender',$gender)->with('township',$township)->with('dp1',$dp1)->with('dp2',$dp2)->with('pp1',$pp1)->with('phone',$phone)->with('pp2',$pp2)->with('sk1',$sk1)->with('sk2',$sk2)->with('status',$status);    
      

    }
    public function jobseeker_jobapplied(Request $request)
    {

         $fullname = $request->fullname;
         $fb_name  = $request->fb_name;
         $age      = $request->age;
         $phone      = $request->phone;
         $gender   = $request->gender;
         $township = $request->township;
         $status   = $request->status;
         $dp1      = '';
         $dp2      = '';
         $pp1      = '';
         $pp2      = '';
         $sk1      = '';;
         $js_id    = $request->js_id;
         $sk2      = '';
        if ($request->skill_1) {
            foreach (explode(',', $request->skill_1) as $key) {
                $sk1 .= skill::find($key)->name.',';
            }
        }
        if($request->skill_2){
             foreach (explode(',', $request->skill_2) as $key) {
                $sk2 .= Skill2::find($key)->name.',';
            }
        }
        if ($request->desire_position_1) {
            foreach (explode(',', $request->desire_position_1) as $key) {
                $dp1 .= JobTitle::where('job_title_key',$key)->value('description').',';
            }
        }
        if ($request->desire_position_2) {
            foreach (explode(',', $request->desire_position_2) as $key) {
                $dp2 .= JobTitle::where('job_title_key',$key)->value('description').',';
            }
        }
        if ($request->previous_position_1) {
            foreach (explode(',', $request->previous_position_1) as $key) {
                $pp1 .= JobTitle::where('job_title_key',$key)->value('description').',';
            }
        }
        if ($request->previous_position_2) {
             foreach (explode(',', $request->previous_position_2) as $key) {
                $pp2 .= JobTitle::where('job_title_key',$key)->value('description').',';
            }
        }

        $result = null;
        // if(count($request->all())!=0){  
        //     if(!empty($request->status)){  
        //         $page = LengthAwarePaginator::resolveCurrentPage();
        //         $total=DB::table('jobposition_jobseeker')->where( 'status',$request->status)->count('jobseeker_id'); 
        //         //Count the total record
        //         $perPage=100;

        //         $result_paginate = DB::table('jobposition_jobseeker')->where( 'status', $request->status)->forPage($page, $perPage)->groupBy('jobseeker_id')->orderBy('created_at', 'desc')->get();
        //         $jobseeker_list = new LengthAwarePaginator($result_paginate, $total, $perPage, $page, [
        //                 'path' => LengthAwarePaginator::resolveCurrentPath(),
        //             ]); 
        //     }
        //     else{
        //         $page = LengthAwarePaginator::resolveCurrentPage();
        //         $total=DB::table('jobposition_jobseeker')->count('jobseeker_id'); //Count the total record
        //         $perPage=100;
                 
        //         //Set the limit and offset for a given page.
        //         $jobseeker_list = DB::table('jobposition_jobseeker')->where('status',9)->groupBy('jobseeker_id')->orderBy('created_at', 'desc')->paginate(10);
        //         // $results[] = Jobseeker::where('jobseeker_id',$value->jobseeker_id)->forPage($page, $perPage)->get();
        //         // $result_paginate = new LengthAwarePaginator($jobseeker_list, $total, $perPage, $page, [
        //         //     'path' => LengthAwarePaginator::resolveCurrentPath(),
        //         // ]);    

        //     }

        //     foreach ($jobseeker_list as $key => $value) {
        //         $result[] = Jobseeker::Mysearch($request)->where('jobseeker_id',$value->jobseeker_id)->orderBy('created_at','desc')->first();
        //     }
        // } 
        // else{
           
        //     $page = LengthAwarePaginator::resolveCurrentPage();
        //     $total=DB::table('jobposition_jobseeker')->count('jobseeker_id'); //Count the total record
        //     $perPage=100;
             
        //     //Set the limit and offset for a given page.
        //     $results = DB::table('jobposition_jobseeker')->where('status',9)->groupBy('jobseeker_id')->orderBy('created_at', 'desc')->paginate(10);
        //     // // $results[] = Jobseeker::where('jobseeker_id',$value->jobseeker_id)->forPage($page, $perPage)->get();
        //     // $jobseekers_paginate = new LengthAwarePaginator($results, $total, $perPage, $page, [
        //     //     'path' => LengthAwarePaginator::resolveCurrentPath(),
        //     // ]);    
            
                   
        //            // dd($results); 
        //     foreach ($results as $key => $value) {
        //     // //Set the limit and offset for a given page.

        //     $jobseekers[] = Jobseeker::where('jobseeker_id',$value->jobseeker_id)->groupBy('jobseeker_id')->first();
        //     // $jobseekers[] = array_unique($jobseekers);
        //     }
        
        // }

        if(count($request->all())!=0){
            $ss = ($request->status)?$request->status:6;
            $result = Jobseeker::Mysearch($request)->leftjoin('jobposition_jobseeker','jobseekers.jobseeker_id','=','jobposition_jobseeker.jobseeker_id')
                    ->where('jobposition_jobseeker.status',$ss)
                    ->groupBy('jobposition_jobseeker.jobseeker_id')
                    ->orderBy('jobseekers.created_at','desc')
                    ->paginate(10);
        }else{  
            $jobseekers = Jobseeker::Mysearch($request)->leftjoin('jobposition_jobseeker','jobseekers.jobseeker_id','=','jobposition_jobseeker.jobseeker_id')
                    ->where('jobposition_jobseeker.status',6)
                    ->groupBy('jobposition_jobseeker.jobseeker_id')
                    ->orderBy('jobseekers.created_at','desc')
                    ->paginate(10);
        }
        
         
        // dd($result);
        return view('backend.jobseekers.jobseeker_jobapplied',compact('jobseekers','result','jobseekers_paginate','result_paginate','results','jobseeker_list'))->with('js_id',$js_id)->with('fullname',$fullname)->with('fb_name',$fb_name)->with('age',$age)->with('gender',$gender)->with('township',$township)->with('dp1',$dp1)->with('dp2',$dp2)->with('pp1',$pp1)->with('phone',$phone)->with('pp2',$pp2)->with('sk1',$sk1)->with('sk2',$sk2)->with('status',$status); 
    }
    /* old appointed function */
    public function oldappointed(Request $request)
    {
        
         $fullname = $request->fullname;
         $fb_name  = $request->fb_name;
         $phone      = $request->phone;
         $age      = $request->age;
         $gender   = $request->gender;
         $township = $request->township;
         $status   = $request->status;
         $dp1      = $request->desire_position_1;
         $dp2      = $request->desire_position_2;
         $pp1      = $request->previous_position_1;
         $pp2      = $request->previous_position_2;
         $js_id    = $request->js_id;

        $result = null;
        if(count($request->all())!=0){  
                   
                $result=Jobseeker::Search($request)->where('status',8)->paginate(100);          
        }
        else{
        
            $current = Carbon::today();
            $trialExpires = $current->addDays(2)->toDateString();
            // dd($trialExpires);
            $jobseekers = Jobseeker:://orderBy('interview_time', 'asc')
                                    leftjoin('jobposition_jobseeker','jobseekers.jobseeker_id','=','jobposition_jobseeker.jobseeker_id')
                                    ->where('jobposition_jobseeker.interview_time','>=',Carbon::today()->toDateString())
                                    //where('interview_time','>=',Carbon::today()->toDateString())
                                    ->where('jobposition_jobseeker.status',8)
                                    ->orderBy('jobposition_jobseeker.interview_time', 'asc')
                                    ->paginate(100);
        }       

        return view('backend.jobseekers.appointed_js',compact('jobseekers','result'))->with('js_id',$js_id)->with('fullname',$fullname)->with('fb_name',$fb_name)->with('age',$age)->with('gender',$gender)->with('township',$township)->with('dp1',$dp1)->with('dp2',$dp2)->with('pp1',$pp1)->with('phone',$phone)->with('pp2',$pp2)->with('status',$status); 

    }
    //for appointed jobseekers list

     public function appointed(Request $request)
     {
        
         $fullname = $request->fullname;
         $fb_name  = $request->fb_name;
         $phone      = $request->phone;
         $age      = $request->age;
         $gender   = $request->gender;
         $township = $request->township;
         $status   = $request->status;
         $dp1      = $request->desire_position_1;
         $dp2      = $request->desire_position_2;
         $pp1      = $request->previous_position_1;
         $pp2      = $request->previous_position_2;
         $js_id    = $request->js_id;

        $result = null;
        if(count($request->all())!=0){  
                   
                $result=Jobseeker::Search($request)->where('status',8)->paginate(100);          
        }
        else{
        
            $current = Carbon::today();
            $trialExpires = $current->addDays(2)->toDateString();
            // dd($trialExpires);
            $jobseekers = Jobseeker:://orderBy('interview_time', 'asc')
                                    leftjoin('jobposition_jobseeker','jobseekers.jobseeker_id','=','jobposition_jobseeker.jobseeker_id')
                                    //->where('jobposition_jobseeker.interview_time','>=',Carbon::today()->toDateString())
                                    //where('interview_time','>=',Carbon::today()->toDateString())
                                    ->whereIn('jobposition_jobseeker.stage',[3,5,4])
                                    ->orderBy('jobseekers.jobseeker_id','desc')
                                    ->paginate(10);
        }       

        return view('backend.jobseekers.appointed_jobseeker',compact('jobseekers','result'))->with('js_id',$js_id)->with('fullname',$fullname)->with('fb_name',$fb_name)->with('age',$age)->with('gender',$gender)->with('township',$township)->with('dp1',$dp1)->with('dp2',$dp2)->with('pp1',$pp1)->with('phone',$phone)->with('pp2',$pp2)->with('status',$status); 

    }

        //test loadmore
    public function loadMore(Request $request)
    {

        $products = Jobseeker::with('calllogs')->orderBy('created_at', 'desc')->paginate(10);
        $html='';
        foreach ($products as $product) {
            $html.='<li>'.$product->id.' <strong>'.$product->fullname.'</strong> : '.$product->fb_name.'</li>';
        }
        if ($request->ajax()) {
            return $html;
        }
         
        return view('files.loadmore',compact('products'));
    
    }

    //count for front page     
    public function count()
    {

        $total = DB::table('jobseekers')->where('published_at', null)->count();
         
        return view('welcome',compact('total')); 
    }

    //calculate how much duplicate in jobseekers
    public function duplicates()
    {

        $subQuery = DB::table('jobseekers')
                    ->select('fullname')
                    ->groupBy('fullname')
                    ->havingRaw('count(*) > 1');


        $duplicates = DB::table('jobseekers')
                        ->select('jobseekers.*')
                        ->whereIn('fullname', $subQuery)
                        ->orderBy('fullname', 'desc')->paginate(100);

    return view('backend.duplicates.index',compact('duplicates')); 

    }

    //for download excel
    public function downloadExcel($type)
    {   
        ini_set('max_execution_time', 3000);
        set_time_limit ( 3000 );
        $user = new Jobseeker;
        $data = $user->query();
        try{
            $filedate = str_replace(' ', '_', Carbon::now());
            return Excel::create('UserList'.$filedate, function($excel) use($data){
 
            $data->chunk(10000, function ($users) use($excel) {
            $collection = $this->transformCollection($users);

            $excel->sheet('OrgSubUserList', function($sheet) use($collection){
                $sheet->fromModel($collection, null, 'A1', true);
            });
            });
            
        })->export('xlsx');
        }
        catch(Exception $e)
        {
            return false;
        }
       
    }

  private function transformCollection($collection){

        return array_map([$this, 'transform'], $collection->toArray());
    }


  private function transform($collection){
        return [
            // 'Full Name' => $collection['jobseeker_id']. ' '. $collection['fullname']. ' '.         $collection['date'],
            // 'Phone No' => $collection['date'],
            // 'Full Address' => $collection['id']. ' '. $collection['fullname']
               'fullname'=>$collection['fullname'],
               'jobseeker_id'=> $collection['jobseeker_id'],
               'date'=>$collection['date'],
               'active_inactive'=>$collection['active_inactive'],
               'fb_name'=> $collection['fb_name'],
               'gender' => $collection['gender'],
               'age' => $collection['age'],
               'expected_salary' => $collection['expected_salary'],
               'expected_salary_other' => $collection['expected_salary_other'],
               'education' => $collection['education'],
               'nrc_no'=>$collection['nrc_no'],
               'township'=>$collection['township'],
               'wards' => $collection['wards'],
               'bus_stops' => $collection['bus_stops'],
               // 'bus_stops' => $collection['Otherbusstops'],
               'previous_function_1' => $collection['previous_position_1'],
               'certification_and_license' => $collection['certification_and_license'],
               'fabrics' => $collection['fabrics'],
               'experience_month_1' => $collection['experience_month_1'],
               'company_name_1' => $collection['company_name_1'],
               'previous_position_1' => $collection['previous_position_1'],
               'previous_position_2' => $collection['previous_function_2'],
               'previous_position_other' => $collection['previous_position_other'],
               'previous_position_other_2' => $collection['previous_position_other_2'],
               'previous_function_2' => $collection['previous_function_2'],
               'desire_function_1' => $collection['desire_function_1'],
               'desire_function_2' => $collection['desire_function_2'],
               'type_of_license' => $collection['type_of_license'],
               'experience_month_2' => $collection['experience_month_2'],
               'company_name_2' => $collection['company_name_2'],
               'desire_position_1' => $collection['desire_position_1'],
               'desire_position_other_1' => $collection['desire_position_other_1'],
               'desire_position_2' => $collection['desire_position_2'],
               'desire_position_other_2' => $collection['desire_position_other_2'],
              // 'job_industry_1' => $collection['Desired_Industry'],
              // 'job_industry_2' => $collection['other_industry'],
               'skill_1' => $collection['skill_1'],
               'skill_other_1' => $collection['skill_other_1'],
               'skill_2' => $collection['skill_2'],
               'skill_other_2' => $collection['skill_other_2'],
               'viber_number' => $collection['viber_number'],
               'phone'=>$collection['phone'],
               'chatfuel_user_id'=>$collection['chatfuel_user_id'],
              // 'last_user_freedom_input'=>$collection['last_user_freeform_input'],
               'messenger_user_id' =>$collection['messenger_user_id'],
//               'profile_pic_url' =>$collection['profile_pic_url'],
               'certificate' =>$collection['certificate'],
               'certificate_other' =>$collection['certificate_other']



        ];
    }
 
     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.jobseekers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //    $data_array = [

        //     'fb_name' => $request->fb_name,
        //     'fullname' =>$request->full_name,
        //     'phone'  =>$request->phone,
        // ];     
        // $myfbname =$request->fb_name;
        // // dd($data_array);
        //  $jobseeker = Jobseeker::updateOrCreate(['fb_name' => $myfbname ],$data_array);

        $jobseeker = new Jobseeker();
        $jobseeker_latest_row = Jobseeker::orderBy('id', 'desc')->first();
        $jobseeker->jobseeker_id = $jobseeker_latest_row->jobseeker_id + 1;
        $jobseeker->fullname = Input::get('fullname');
        $jobseeker->fb_name = Input::get('fb_name');
        $jobseeker->phone = Input::get('phone');
        $jobseeker->status = Input::get('status');
        $jobseeker->viber_number= Input::get('viber_number');
        $jobseeker->sign_up_from = Input::get('sign_up_from');
        $jobseeker->date = Carbon::now()->toDateString();
        $jobseeker->gender = Input::get('gender');

        $jobseeker->age = Input::get('age');
        $jobseeker->wards = Input::get('wards');
        $jobseeker->bus_stops = Input::get('bus_stops');
        $jobseeker->education = Input::get('education');
        $jobseeker->township = Input::get('township');
        $jobseeker->previous_position_1 = Input::get('previous_position_1');
        $jobseeker->experience_month_1 = Input::get('experience_month_1');
        $jobseeker->company_name_1 = Input::get('company_name_1');
        $jobseeker->previous_position_2 = Input::get('previous_position_2');
        $jobseeker->experience_month_2 = Input::get('experience_month_2');
        $jobseeker->company_name_2 = Input::get('company_name_2');
        $jobseeker->desire_position_1 = Input::get('desire_position_1');
        $jobseeker->desire_position_2 = Input::get('desire_position_2');
        $jobseeker->desire_position_other_1 = Input::get('desire_position_other_1');
        $jobseeker->desire_position_other_2 = Input::get('desire_position_other_2');
        $jobseeker->fabrics = Input::get('fabrics');
        $jobseeker->skill_1 = Input::get('skill_1');
        $jobseeker->skill_other_1 = Input::get('skill_other_1');
        $jobseeker->skill_other_2 = Input::get('skill_other_2');
        $jobseeker->skill_2 = Input::get('skill_2');
        $jobseeker->certificate = Input::get('certificate');
        $jobseeker->certificate_other = Input::get('certificate_other');
        $jobseeker->job_industry_1 = Input::get('job_industry_1');
        $jobseeker->job_industry_2 = Input::get('job_industry_2');
        $jobseeker->certification_and_license = Input::get('certification_and_license');
        $jobseeker->type_of_license = Input::get('type_of_license');
        $jobseeker->viber_number_2 = Input::get('viber_number_2');
        $jobseeker->remark = Input::get('remark');
        $jobseeker->usertype_1 = Input::get('usertype_1');
        $jobseeker->chatfuel_user_id = Input::get('chatfuel_user_id');
        $jobseeker->address = Input::get('address');
        $jobseeker->last_user_freedom_input = Input::get('last_user_freedom_input');
        $jobseeker->messenger_user_id = Input::get('messenger_user_id');
        $jobseeker->published_at = Input::get('published_at');
        $jobseeker->nationality = Input::get('nationality');
        $jobseeker->nrc_no = Input::get('nrc_no');
        $jobseeker->marital_status = Input::get('marital_status');
        $jobseeker->additional_field = Input::get('additional_field');
        $jobseeker->additional_field2 = Input::get('additional_field2');
        $jobseeker->additional_field3 = Input::get('additional_field3');

     

        if($jobseeker->save())
        {
            Session::flash('message','Order was successfully created');
            Session::flash('m-class','alert-success');
            return back()->with('success','Jobseeker successfully added!');
        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return redirect('order/create');
        }
       
 
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Jobseeker  $jobseeker
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jobseekers = Jobseeker::where('jobseeker_id',$id)->first();
        // dd($jobseekers);
        $stages = DB::table("stages")->pluck("description","id");
        // return $jobseekers;
        //return view('posts.show')->withPost($post)->withComments($comments);
        return view('backend.jobseekers.jobseeker_detail', compact('jobseekers','stages'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Jobseeker  $jobseeker
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jobseekers = Jobseeker::where('jobseeker_id',$id)->first();
       
        return view("backend.jobseekers.edit", compact('jobseekers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Jobseeker  $jobseeker
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $jobseekers = Jobseeker::find($id);
    
// dd($request);
        $jobseekers->date = Input::get('date');
        $jobseekers->fb_name = Input::get('fb_name');
        $jobseekers->fullname = Input::get('fullname');
        // $jobseekers->status = Input::get('status');
        $jobseekers->active_inactive = Input::get('active_inactive');
        $jobseekers->phone = Input::get('phone');
        $jobseekers->viber_number = Input::get('viber_number');
        $jobseekers->sign_up_from = Input::get('sign_up_from');
        $jobseekers->gender = Input::get('gender');
        $jobseekers->age = Input::get('age');
        $jobseekers->wards = Input::get('wards');
        $jobseekers->bus_stops = Input::get('bus_stops');
        $jobseekers->education = Input::get('education');
        $jobseekers->previous_position_1 = $request->previous_position_1;
        $jobseekers->experience_month_1 = Input::get('experience_month_1');
        $jobseekers->company_name_1 = Input::get('company_name_1');
        $jobseekers->previous_position_2 = Input::get('previous_position_2');
        $jobseekers->previous_position_other = Input::get('previous_position_other');
        $jobseekers->previous_position_other_2 = Input::get('previous_position_other_2');
        $jobseekers->experience_month_2 = Input::get('experience_month_2');
        $jobseekers->company_name_2 = Input::get('company_name_2');
        $jobseekers->certification_and_license = Input::get('certification_and_license');
        $jobseekers->fabrics = Input::get('fabrics');
        $jobseekers->type_of_license = Input::get('type_of_license');
        $jobseekers->desire_position_1 = Input::get('desire_position_1');
        $jobseekers->desire_position_2 = Input::get('desire_position_2');
        $jobseekers->desire_position_other_1 = $request->desire_position_other_1;
        $jobseekers->desire_position_other_2 = $request->desire_position_other_2;
        $jobseekers->job_industry_1 = Input::get('job_industry_1');
        $jobseekers->job_industry_2 = Input::get('job_industry_2');
        $jobseekers->skill_1 = Input::get('skill_1');
        // $jobseeker->skill_other_1 = Input::get('skill_other_1');
         
        $jobseekers->skill_2 = Input::get('skill_2');
        $jobseekers->certificate = Input::get('certificate');
        $jobseekers->certificate_other = Input::get('certificate_other'); 
        $jobseekers->township = Input::get('township');
        $jobseekers->remark = Input::get('remark');
        $jobseekers->usertype_1 = Input::get('usertype_1');
        $jobseekers->address = Input::get('address');
        $jobseekers->last_user_freedom_input = Input::get('last_user_freedom_input');
        
         // $jobseeker = new Jobseeker();
        $jobseekers->skill_other_2 = Input::get('skill_other_2');
        $jobseekers->skill_other_1 = Input::get('skill_other_1');
        $jobseekers->nationality = Input::get('nationality');
        // $jobseekers->nationality = Jobseeker::find($jobseeker->nationality);
        $jobseekers->nrc_no = Input::get('nrc_no');
        $jobseekers->marital_status = Input::get('marital_status');
        $jobseekers->additional_field = Input::get('additional_field');
        $jobseekers->additional_field2 = Input::get('additional_field2');
        $jobseekers->additional_field3 = Input::get('additional_field3');
         
        if($jobseekers->save())
        {
            Session::flash('message','jobseekers was successfully updated');
            Session::flash('m-class','alert-success');
             return back()->with('success','Jobseeker successfully updated!');

        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return redirect('jobseekers');
        }
    }


    public function updatestatus(Request $request, $id)
    {
        $jobseekers = Jobseeker::find($id);

        $jobseekers->status = Input::get('status');
        $jobseekers->interview_time = Input::get('interview_time');
        // $jobseekers->interview_time_hour = Input::get('interview_time_hour');
      
        if($jobseekers->save())
        {
            Session::flash('message','jobseekers was successfully updated');
            Session::flash('m-class','alert-success');
             return back()->with('success','Jobseeker successfully updated!');

        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return redirect('jobseekers');
        }
    }

    public function add_additionalfield(Request $request,$id)
    {
        $jobseekers = Jobseeker::find($id);

        $jobseekers->additional_field = Input::get('additional_field');
        $jobseekers->additional_field2 = Input::get('additional_field2');
        $jobseekers->additional_field3 = Input::get('additional_field3');
        // $jobseekers->interview_time_hour = Input::get('interview_time_hour');
      
        if($jobseekers->save())
        {
            Session::flash('message','jobseekers was successfully updated');
            Session::flash('m-class','alert-success');
             return back()->with('success','Jobseeker successfully updated!');

        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return redirect('jobseekers');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Jobseeker  $jobseeker
     * @return \Illuminate\Http\Response
     */
    public function destroy(Jobseeker $jobseeker)
    {
        //
    }

    
     public function email()
    {
        return view('backend.jobseekers.email');
    }


    public function sendingEmail(Request $request,$file_name='')
    {   
        foreach ($request['files'] as $key => $file) {
            list($type, $file) = explode(';', $file);
            list(, $file) = explode(',', $file);
            list(,$ext) = explode('/', $type);
            $path = "uploads/";

            if (!is_dir($path)) {
                mkdir($path, 0777, true);
                fopen($path . "index.php", "wp");
            }

            $name = $file_name.time(). '.' . $ext;
            $encodedData = base64_decode($file);
            file_put_contents($path.'/'.$name, $encodedData);
            $upload_data[] = $path.$name;
        }
        
        $app_user = ['subject'=> $request->subject,'email'=>$request->email,'myfile'=>$upload_data,'body'=>$request->body];

        Mail::send('email.test', ['user' => $app_user], function ($message) use ($app_user)
        {  
            $route = public_path() . '/';
                
            for ($i=0; $i < count($app_user['myfile']); $i++) { 
                $message->attach($route.$app_user['myfile'][$i]);  
            }          
                   
            $message->to($app_user['email'], $app_user['subject'])->subject('Testing Email from MyApp');
        });

        return back();
    } 


    public function getJoins(){
        return view('backend.jobseekers.join');
    }


    public function index2()
    {
        $jobseekers = Jobseeker::paginate(10);
        return view('backend.jobseekers.index_datatable',compact('jobseekers'));   
    }


    public function sendcv($id){

        $jobseekers = Jobseeker::find($id);
        return view('backend.jobseekers.send', compact('jobseekers'));

    }

    public function send(Request $request ){
       
        $jobseekers = Jobseeker::where('jobseeker_id', $request->input('cv_no'))->first();
        $name = $jobseekers->name;

        
       $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false) ;

        $pdf->SetAuthor('Neh Thit');
        $pdf->SetTitle($jobseekers->fullname);
        $pdf->SetSubject('CV Form');
        $pdf->SetKeywords('CV');
        
//         $font = $pdf->addTTFfont("fonts/zawgyi.ttf");
// // use the font
// $pdf->SetFont($font, '', 14, '', false);

        // $pdf = new TCPDF("L", "mm", "A4", true, "UTF-8" );
        // $font = $pdf->addTTFfont('fonts/zawgyi.ttf');
        // $pdf->SetFont($font , '', 32,'',true);
        // $pdf->Text(0, 0, "PDFに表示する文字" );

        $fontname = MMTCPDF::font('Zawgyi-One');
//$fontname = MMTCPDF::font('Myanmar3');
        $pdf->SetFont($fontname , 11);
// MMTCPDF::SetTitle('Record Report');
// MMTCPDF::AddPage('P','A4');
// MMTCPDF::writeHtml("Myanmar Words");
// MMTCPDF::Output('report.pdf', 'I');

        // $pdf = new TCPDF("L", "mm", "A4", true, "UTF-8" );
        // $font = new TCPDF_FONTS();
        // $fontX = $font->addTTFfont('fonts/zawgyi.ttf');
        // $pdf->SetFont($fontX , '', 32,'',true);
        // $pdf->Text(0, 0, "PDFに表示する文字" );
        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(true);

    // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, 15 , PDF_MARGIN_RIGHT);

    // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    // set image scale factor

        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $pdf->AddPage();

        

  $html = '<style>
    .abc ul{
    font-size: 63px !important;
    list-style: none !important;
      padding-bottom:30px !important;
    padding-left: 0 !important;
    }
    .a{
    position: relative !important;
    padding-left: 11px !important;
    margin:0 0 3px 0 !important;
    }


         body {
         font-size: 8px;
         }
        #wrapper {
            width: 500px;
            margin: 0 auto;
            margin-top: 60px;
            margin-bottom: 100px;
        }

        .wrapper-top {
            width: 500px;
            height: 19px;
            margin: 0 auto;
            background-repeat: no-repeat;
        }

        .wrapper-mid {
            width: 500px;
            margin: 0 auto;
            background-repeat: repeat-y;
            padding-bottom: 40px;
        }

        .wrapper-bottom {
            width: 500px;
            height: 22px;
            padding: 0;
            margin: 0 auto;
            background-repeat: no-repeat;
        }

        #paper {
            width: 800px;
            margin: 0 auto;
        }

        #wrapper, .wrapper-mid {
            background: none;
        }

        .wrapper-top, .wrapper-bottom {
            display: none;
        }

        .print-pp p {
            font-size: 13px;
        }
        .haha{
            text-align:center;
        }

        h3 {
            font-size: 17px;
            color: #000000;
        }

        h5 {
            font-weight: bold;
            font-size: 10px;
        }

        td{
            font-size: 15px;
        }
        div.line {
            border-bottom: 1px solid #7dc24b;
        }

        .list-symbol-bullet li {
            line-height: 30px;
            list-style-type: none;
        }

        .address {
            margin-top: 20px;
        }

        .fileAttachmentAnnotation{
            background: black;
        }


    </style>
    <table>
        <tr>
            
            <td colspan="3" class="haha"><h1>Curriculum Vitae</h1></td>
            
        </tr>
        <tr>
            <td colspan="3">
                <h2>PERSONAL</h2>
               
                <hr/>
            
            </td>
        </tr>
        <tr>
            <td>
            <br><br>
                <b>Name:</b>
            </td>
            <td>
            <br><br>
              '.$jobseekers->fullname.'
            </td>
            <td rowspan="5" class="third">
                
                </td>
        </tr>';
        if(!empty($jobseekers->gender)){
                  $html .=  ' 
        <tr>
            <td>
            <br><br>
            <b>Gender:</b>
            </td>
            <td>
                 <br><br>

               '.\App\Models\Gender::where('id',$jobseekers->gender)->pluck('gender_description')->first().'
            </td>
        
        </tr>
        
                ';
            }
            if(!empty($jobseekers->age)){
         $html .=  ' 
        <tr>
            <td><br><br>
                <b>Age:</b>
            </td>
            <td><br><br>
               '.$jobseekers->age.'
            </td>
            
        </tr>
        ';}
               if(!empty($jobseekers->education)){
        $html.='
        <tr>
            <td><br><br>
                <b>Education:</b>
            </td>
            <td colspan="3"><br><br>
               '.\App\Models\Education::where('id',$jobseekers->education)->pluck('name_en')->first().'
            </td>
            
        </tr>

';}  

        if(!empty($jobseekers->nationality)){
                $html.='
                <tr>
                    <td><br><br>
                        <b>Nationality:</b>
                    </td>
                    <td><br><br>
                       '.$jobseekers->nationality.'
                    </td>
                    
                </tr>

        ';}        
        if(!empty($jobseekers->nrc_no)){
                $html.='
                <tr>
                    <td><br><br>
                        <b>N.R.C No. :</b>
                    </td>
                    <td><br><br>
                       '.$jobseekers->nrc_no.'
                    </td>
                    
                </tr>

        ';}        
        if(!empty($jobseekers->marital_status)){
                $html.='
                <tr>
                    <td><br><br>
                        <b>Marital Status:</b>
                    </td>
                    <td><br><br>
                       '.$jobseekers->marital_status.'
                    </td>
                    
                </tr>

        ';}

            if(!empty($jobseekers->wards) && $jobseekers->wards !=39){
                $html .=  ' 
                        <tr>
                            <td><br><br>
                                <b>Location:</b>
                                
                            </td>
                            <td colspan="2"><br><br>


                                '.\App\Models\Ward::where('id',$jobseekers->wards)->pluck('name')->first() .',';


                if(!empty($jobseekers->township)){
                     $html .=  '
                     '. \App\Models\Township::where('id',$jobseekers->township)->pluck('name')->first() .'
                     
                     ';
                }

                $html .='
                </td></tr>
                ';
            }

            else{
                    $html .=  ' 
                    <tr>
                        <td><br><br>
                            <b>Location:</b>
                            
                        </td>
                        <td colspan="2"><br><br>
               
                 
                        ';
                     if(!empty($jobseekers->township)){
                     $html .=  '
                     '. \App\Models\Township::where('id',$jobseekers->township)->pluck('name')->first() .'
                     
                     ';
                    }

                    $html .='
                    </td></tr>
                    ';
                }
               
    if( !empty($jobseekers->previous_position_other) || !empty($jobseekers->previous_position_other_2)|| !empty($jobseekers->previous_position_1) || !empty($jobseekers->desire_position_other_1) || !empty($jobseekers->desire_position_other_2)){
 $html .=  ' 



        <br >
        <tr>
            <td colspan="3" height="35">
                <h2>
                    PROFESSIONAL
                </h2>
                <hr>
            </td>
        </tr>';

        if (!empty($jobseekers->desire_position_1) || !empty($jobseekers->desire_position_other_1)) {
            $tip = \App\Models\JobTitle::where('job_title_key',$jobseekers->desire_position_1)->pluck('description')->first();
            $desire = ($jobseekers->desire_position_1!=301)?$tip:'';
            $html.=' 
                    <tr>
                        <td colspan="3" height="40">
                            <h3>
                                Applied Position:<br>
                            </h3>
                          
                        </td>
                    </tr>
                    <tr class="abc">
                        <td colspan="3" height="35">
                        <ul> <li>'.$desire.' '.$jobseekers->desire_position_other_1.
                        '</li></ul></td>
                    </tr>';
        }
        
        $html .=  ' 
        <tr>
            <td colspan="3" height="40">
                <h3>
                    Previous Experience:<br>
                </h3>
              
            </td>
        </tr>
                ';
            }
                if(!empty($jobseekers->previous_position_1)){
                    
                    if($jobseekers->previous_position_1!=301){

            $html.='
    
        <tr class="abc">
            <td colspan="3" height="35">
               <ul> <li>'.          
  \App\Models\JobTitle::where('job_title_key',$jobseekers->previous_position_1)->pluck('description')->first() 

                .checkExp($jobseekers->experience_month_1) .(isset($jobseekers->company_name_1)? ' at '. $jobseekers->company_name_1.'' : '') .'
            </li></ul></td>
        </tr>
        ';}

    
    else {
         if(!empty($jobseekers->previous_position_other)){
            $html.='
        
        <tr class="abc">
            <td colspan="3" height="30">
               <ul>   <li>'. 
             $jobseekers->previous_position_other
  . (isset($jobseekers->experience_month_1)? ' Position for '. checkExp($jobseekers->experience_month_1): '')  .(isset($jobseekers->company_name_1)? ' at '. $jobseekers->company_name_1.'' : '') .'
            </li></ul></td>
        </tr>
        ';
    }
}
}
  if($jobseekers->previous_position_2 !=301){
         if(!empty($jobseekers->previous_position_2)){
          
                            
        $html.=
        '
      <tr class="abc">
            <td colspan="3" >
              <ul> <li>'. \App\Models\JobTitle::where('job_title_key',$jobseekers->previous_position_2)->pluck('description')->first()

                .checkExp($jobseekers->experience_month_2).(isset($jobseekers->company_name_2)? ' at '. $jobseekers->company_name_2.'' : '') .'
              </li></ul></td>
            <td>
               
            </td>
        </tr>
        
        ';
    }  
}        else {
            if(!empty($jobseekers->previous_position_other_2)){
            $html.='
        
        <tr>
            <td colspan="3">
               <ul> <li>'. 

             $jobseekers->previous_position_other_2
                 
  . (isset($jobseekers->experience_month_2)? ' Position for '. checkExp($jobseekers->experience_month_2): '') .(isset($jobseekers->company_name_2)? ' at '. $jobseekers->company_name_2.'' : '') .'
            </li></ul></td>
        </tr>
        ';
    }

     }     
    if($jobseekers->skill_1 !=25 ){

        if(!empty($jobseekers->skill_1)){
            
            $html .=  ' 
                <tr>
                <td colspan="3">
                    <h3>
                        Skills:<br>
                    </h3>
                  
                </td>
            </tr>
            <tr class="abc">
            <td colspan="3" height="35">
                    <ul><li>'.($jobseekers->skill_1 == 25 ? '' : (($jobseekers->skill_1 == 24) ? $jobseekers->skill_other_1 : \App\Models\skill::where('id',$jobseekers->skill_1)->pluck('name')->first())) .'
                </li></ul>
                </td>
                </tr>


                ';
            }
        }
                if(!empty($jobseekers->skill_2) ){
                    if($jobseekers->skill_2 != 25){


                 $html .=  ' 
                <tr>
                <td>
                   <ul><li>'.($jobseekers->skill_2 == 25 ? '' : (($jobseekers->skill_2 == 24) ? $jobseekers->skill_other_2 : \App\Models\Skill2::where('id',$jobseekers->skill_2)->pluck('name')->first())) .

                  '
                       </li></ul>
                </td>
            </tr>'
            ;

        } 

    }   

    if(!empty($jobseekers->certificate) ){
     
        if($jobseekers->certificate != 9){

                $html .=  ' 
                   <tr>
                <td colspan="3">
                    <h3>
                        Certificate:<br>
                    </h3>
                  
                </td>
                </tr>
                    <tr class="abc">
            <td colspan="3" height="35">
                       <ul><li>'.($jobseekers->certificate == 9 ? '' : (($jobseekers->certificate == 8) ? $jobseekers->certificate_other : \App\Models\Certificate::where('id',$jobseekers->certificate)->pluck('description')->first())) .

                      '
                           </li></ul>
                    </td>
                </tr>'
                ;

        }     

        else { 
            if($jobseekers->certificate == 8){
             if(!empty($jobseekers->certificate_other)){
            $html.='
             <tr>
            <td colspan="3">
                <h3>
                    Certificate:<br>
                </h3>
              
            </td>
        </tr>
        
        <tr class="abc">
            <td colspan="3" height="35">
               <ul> <li>'.
             $jobseekers->certificate_other
                 .'
            </li></ul></td>
        </tr>
        ';
    }

     } 
 }

}


 

        if(!empty($jobseekers->additional_field)){
     
      $html .=  ' 
            <tr>
            <td colspan="3">
                <h3>
                    Additional: <br>
                </h3>
               
            </td>
        </tr>
        <tr><td height="30">
                <ul>
                <li> '.$jobseekers->additional_field .'</li>
                    </ul>
            </td>
            </tr>
            <tr>
            <td height="30">
                <ul>
                <li> '.$jobseekers->additional_field2 .'</li>
                </ul>
            </td>
            </tr>
            <tr>
            <td>
                <ul>
                <li> '.$jobseekers->additional_field3.'</li>

                </ul>
            </td>
            </tr>


            ';}
      
'
    

        
    </table>';


         $pdf->SetLineWidth(2);

// set alpha to semi-transparency
        $pdf->SetAlpha(0.2);

// draw jpeg image left top right
        $pdf->Image(public_path('img/water1.png'), 80, 50, 50, 50, '', '#', '', true, 72);  


        $pdf->Image(public_path('img/water1.png'), 80, 150, 50, 50, '', '#', '', true, 72);

// restore full opacity
        $pdf->SetAlpha(1);

        $pdf->writeHTML($html, true, false, true, false, '');

        $pdf->SetLineWidth(2);

// set alpha to semi-transparency
        $pdf->SetAlpha(0.2);

// draw jpeg image
        //$pdf->Image(public_path('img/water1.png'), 150, 10, 50, 60, '', '#', '', true, 72);

// set alpha to semi-transparency
        $pdf->SetAlpha(0.2);

// draw jpeg image
       // $pdf->Image(public_path('img/water1.png'), 80, 100, 120, 50, '', '#', '', true, 72);

// restore full opacity
        $pdf->SetAlpha(1);
        
 $pdf->Output($_SERVER['DOCUMENT_ROOT'] . '/pdf/cv/' .$jobseekers->jobseeker_id.'_'.$jobseekers->fullname.'.pdf', 'FD');



         $data = array('name'=>"Than Htut Oo");


    $data = array('name'=>"Virat Gandhi");
      // Mail::send('mail', $data, function($message)  use ($request,$jobseekers){
      //    $message->to($request->input('email'), 'Than Htut')->subject
      //       ($request->input('subject'));
      //     $message->attach(public_path() . '/pdf/cv/' . $jobseekers->jobseeker_id .'('.$jobseekers->fullname.').pdf');
      // });
      echo "HTML Email Sent. Check your inbox.";
    }




    // public function getJoinsData(Request $request)
    // {
    //     $jobseekers = DB::table('jobseekers')
           
    //                     ->join('townships','jobseekers.township','=','townships.id')
    //                     ->join('previous_positions','jobseekers.previous_position_1','=','previous_positions.id')
    //                     ->join('previous_position_2s','jobseekers.previous_position_2','=','previous_position_2s.id')
    //                     ->join('desirepositions','jobseekers.desire_position_1','=','desirepositions.id')
    //         ->select(['jobseekers.id', 'jobseekers.phone','jobseekers.fb_name','townships.name','jobseekers.age','jobseekers.fullname','desirepositions.desire_position_1_name','previous_position_2s.previous_position_2_name','previous_positions.previous_position_1_name', 'jobseekers.created_at', 'jobseekers.updated_at'])
    //         ;

    //     return Datatables::of($jobseekers)
    //         ->editColumn('township', '{!! str_limit($name, 60) !!}')
    //         ->editColumn('previous_position_1', '{!! str_limit($previous_position_1_name, 60) !!}')
    //         ->editColumn('previous_position_2', '{!! str_limit($previous_position_2_name, 60) !!}')
    //         ->editColumn('desire_position_1', '{!! str_limit($desire_position_1_name, 60) !!}')
    //           ->addColumn('action', function ($jobseekers) {
    //             return '<a href="jobseekers/' . $jobseekers->id . '/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a><a href="jobseekers/' . $jobseekers->id . '" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Show</a>';
    //         })
            
    //         ->filter(function ($query) use ($request) {
    //             if ($request->has('fullname')) {
    //                 $query->where('fullname', 'like', "%{$request->get('fullname')}%");
    //             }
    //             if ($request->has('desire_position_1')) {
    //                 $query->where('desire_position_1', 'like', "%{$request->get('desire_position_1')}%");
    //             }
    //              if ($request->has('previous_position_1')) {
    //                 $query->where('previous_position_1', 'like', "%{$request->get('previous_position_1')}%");
    //             }
    //               if ($request->has('previous_position_2')) {
    //                 $query->where('previous_position_2', 'like', "%{$request->get('previous_position_2')}%");
    //             }               
    //              if ($request->has('age')) {
    //                 $query->where('age', 'like', "%{$request->get('age')}%");
    //             }

    //             if ($request->has('township')) {
    //                 $query->where('township', 'like', "%{$request->get('township')}%");
    //             }

    //         })
    //        ->editColumn('id', 'ID: {{$id}}')
    //         ->make(true);
    // }

   


    //for datatable
    // public function data() {

    //     return Datatables::of(Jobseeker::query())->make(true);

    //Y
  

    public function manualDischarged()
    {
        $end = date('Y-m-d');
        $start = date('Y-m-d',strtotime("-7 days"));
        $response['result'] = '!ok';

        $job = DB::table('jobposition_jobseeker')
                ->whereNotBetween('created_at',[$start,$end])
                ->where('stage',2)
                ->where('status',9)
                ->take(100);
        
        foreach ($job->get() as $key => $value) {
            $position = Jobposition::where('job_id',$value->jobposition_id)->first();
            $jobseeker = Jobseeker::find($value->jobseeker_id);
            
            if ($position && $jobseeker) {
                $response = $this->notMatch([
                  'user_id'=>$jobseeker->chatfuel_user_id,
                  'block'=>'5afc0194e4b03993fa4aa9ff',
                  'position'=>$position->job_title,
                  'company' => $position->employer['employer_name']
                ]);                  
            }
            $job->update(['status'=>8]);
        }

        return $response['result'];
    }

    protected function notMatch($data)
    { 
      $bot_id = '5a39b9e5e4b0561664a68819';
      $token = 'mELtlMAHYqR0BvgEiMq8zVek3uYUK3OJMbtyrdNPTrQB9ndV0fM7lWTFZbM4MZvD';
      $block = $data['block'];
      $user_id = $data['user_id'];

      $url = 'https://api.chatfuel.com/bots/'.$bot_id.'/users/'.$user_id.'/send?chatfuel_token='.$token.'&chatfuel_message_tag=APPLICATION_UPDATE&chatfuel_block_id='.$block.'&COMPANY='.$data['company'].'&apply_job_id='.$data['position'];
      
      $client = new Client;   
      try {
          $response = $client->post($url);
          return json_decode( $response->getBody()->getContents(),true );
      } catch (ClientException $e) {
        return json_decode( $e->getResponse()->getBody()->getContents(),true );
      }  
    }
}
