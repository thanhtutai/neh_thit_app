<?php

namespace App\Http\Controllers;

use App\Models\Expected_salary;
use Illuminate\Http\Request;

class ExpectedSalaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Expected_salary  $expected_salary
     * @return \Illuminate\Http\Response
     */
    public function show(Expected_salary $expected_salary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Expected_salary  $expected_salary
     * @return \Illuminate\Http\Response
     */
    public function edit(Expected_salary $expected_salary)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Expected_salary  $expected_salary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Expected_salary $expected_salary)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Expected_salary  $expected_salary
     * @return \Illuminate\Http\Response
     */
    public function destroy(Expected_salary $expected_salary)
    {
        //
    }
}
