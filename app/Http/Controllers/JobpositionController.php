<?php

namespace App\Http\Controllers;

use App\Models\Jobposition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use App\Models\Employer;
use App\Models\Education;
use App\Models\Certificate;
use App\Models\JobFunction;
use App\Models\Jobseeker;
use App\Models\Township;
use App\Models\skill;
use DB;
use Carbon\Carbon;
class JobpositionController extends Controller
{

    public function paidjob(Request $request)
    {
        $current = Carbon::today();
        if(count($request->all())!=0)
        {  
            // dd($request);
            $jobpositions = Jobposition::Search($request)->where('free_or_paid',1)->whereDate('jobpositions.deadline', '>=', $current)->orderby('deadline','DESC')->paginate(50);  
    
        }

        else{

            $jobpositions = Jobposition::where('free_or_paid',1)->whereDate('jobpositions.deadline', '>=', $current)->orderby('deadline','DESC')->paginate(50);

        }    

        
        return view('backend.jobpositions.paid_jobpositions',compact('jobpositions'));
    }

    public function jobtitleAjax($id)
    {
        $jobtitles = DB::table("job_titles")
                    ->where("job_function_id",$id)
                    ->orderby("job_titles.id",'asc')
                    ->pluck("description","job_title_key")
                    ;
        return json_encode($jobtitles);
    }


    public function freejob(Request $request)
    {
        $current = Carbon::today();

        if(count($request->all())!=0)
        {  



            $jobpositions = Jobposition::Search($request)->where('jobpositions.free_or_paid',0)->whereDate('jobpositions.deadline', '>=', $current)->paginate(50); 

            

        }
         else{
            $jobpositions = Jobposition::where('free_or_paid',0)->whereDate('jobpositions.deadline', '>=', $current)->paginate(50);
        }
        
        return view('backend.jobpositions.free_jobpositions',compact('jobpositions'));

    }

    public function jobmatchpaid(Request $request)
    {

         $fullname = $request->fullname;
         $fb_name  = $request->fb_name;
         $age      = $request->age;
         $gender   = $request->gender;
         $township = $request->township;
         $status   = $request->status;
         $dp1      = $request->desire_position_1;
         $dp2      = $request->desire_position_2;
         $pp1      = $request->previous_position_1;
         $pp2      = $request->previous_position_2;
         $js_id    = $request->js_id;

        $current = Carbon::today();

        $result = null;
        if(count($request->all())!=0){  

                $result=Jobseeker::Search($request)->select('jobseekers.id','jobseekers.jobseeker_id','time','date','fb_name','fullname','phone', 'viber_number','sign_up_from','jobseekers.gender','age','wards','jobseekers.education','previous_position_1','previous_position_other','experience_month_1','previous_position_2','previous_position_other_2','experience_month_2','certification_and_license','fabrics','type_of_license','desire_position_y_n','desire_position_1','desire_position_other_1','desire_position_2','desire_position_other_2','job_industry_1','job_industry_2','jobseekers.skill_1','jobseekers.skill_other_1','jobseekers.skill_2','skill_other_2','certificate','certificate_other','jobseekers.township','viber_number_2','jobseekers.remark','usertype_1','chatfuel_user_id','jobseekers.address','last_user_freedom_input','messenger_user_id','company_id','additional_field','additional_field2','additional_field3','nationality','nrc_no','marital_status', DB::raw('IFNULL(COUNT(require_position), 0) as total_requireposition'),DB::raw('IFNULL(COUNT(jobpositions.skill_1), 0) as total_skill_1'), DB::raw("IFNULL(SUM(free_or_paid = 1), 0) as total_free_or_paid"))
                ->where('active_inactive',1)
                ->whereNotNull('jobseekers.desire_position_1')
                ->whereNull('is_followup')
                ->leftjoin('jobpositions', 'jobseekers.desire_position_1', '=', 'jobpositions.require_position')
                ->whereDate('jobpositions.deadline', '>=', $current)
                ->groupBy('jobseekers.id')
                ->orderBy('total_free_or_paid','desc')
                ->orderBy('total_skill_1','desc')
                ->orderBy('jobseekers.id','desc')
                ->paginate(70);
            } 

        else{           
            $jobseekers =Jobseeker::select('jobseekers.id','jobseekers.jobseeker_id','time','date','fb_name','fullname','phone', 'viber_number','sign_up_from','jobseekers.gender','age','wards','jobseekers.education','previous_position_1','previous_position_other','experience_month_1','previous_position_2','previous_position_other_2','experience_month_2','certification_and_license','fabrics','type_of_license','desire_position_y_n','desire_position_1','desire_position_other_1','desire_position_2','desire_position_other_2','job_industry_1','job_industry_2','jobseekers.skill_1','jobseekers.skill_other_1','jobseekers.skill_2','skill_other_2','certificate','certificate_other','jobseekers.township','viber_number_2','jobseekers.remark','usertype_1','chatfuel_user_id','jobseekers.address','last_user_freedom_input','messenger_user_id','company_id','additional_field','additional_field2','additional_field3','nationality','nrc_no','marital_status', DB::raw('IFNULL(COUNT(require_position), 0) as total_requireposition'), DB::raw("IFNULL(SUM(free_or_paid = 1), 0) as total_free_or_paid"))
            ->where('active_inactive',1)
            ->whereNotNull('jobseekers.desire_position_1')
            ->whereNull('is_followup')
            ->leftjoin('jobpositions', 'jobseekers.desire_position_1', '=', 'jobpositions.require_position')
            ->groupBy('jobseekers.id')
            ->whereDate('jobpositions.deadline', '>=', $current)
            ->orderBy('total_free_or_paid','desc')
            ->orderBy('jobseekers.id','desc')
            ->paginate(10);
            
        }       
        //dd($jobseekers);

       return view('backend.jobmatch.jobpaidmatch',compact('jobseekers','result'))->with('js_id',$js_id)->with('fullname',$fullname)->with('fb_name',$fb_name)->with('age',$age)->with('gender',$gender)->with('township',$township)->with('dp1',$dp1)->with('dp2',$dp2)->with('pp1',$pp1)->with('pp2',$pp2)->with('status',$status); 
    } 
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {

        $jobpositions = Employer::where('emp_id',$id)->first();
        // dd($id);
        $township = Township::pluck('name','id');
        $jobfunctions = JobFunction::all();
        $education = Education::all();
        $certificate = Certificate::all();
        $skill = skill::all();
        return view("backend.jobpositions.create", compact('jobpositions','township','jobfunctions','education','certificate','skill'));       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $last_id = Jobposition::orderBy('id','desc')->value('id');
        $emp = Employer::where('emp_id',$request->employer_id)->first();
        $redirect_route = ($request->free_or_paid == 0)?'freejob':'paidjob';
        $jobpositions = new Jobposition();
        $jobpositions->job_id = $last_id+1;
        $jobpositions->subtitle = $request->fullname;
        $jobpositions->employer_id = $request->employer_id;
        $jobpositions->employer_name = $emp->employer_name;
        $jobpositions->job_title =  Input::get('job_title');
        $jobpositions->free_or_paid = $request->free_or_paid;
        $jobpositions->post_date = Input::get('post_date');
        $jobpositions->deadline = Input::get('deadline');
        $jobpositions->status = Input::get('status');
        $jobpositions->gender = Input::get('gender');
        $jobpositions->req_nationality = Input::get('req_nationality');
        $jobpositions->req_quantity = Input::get('req_quantity');
        $jobpositions->age_requirement = Input::get('age_requirement');
        $jobpositions->experience_requirement = Input::get('experience_requirement');
        $jobpositions->job_requirement = $request->job_requirement;
        $jobpositions->certificate_1 = Input::get('certificate_1');
        $jobpositions->certificate_2 = Input::get('certificate_2');
        $jobpositions->minimum = Input::get('minimum');
        $jobpositions->maximum = Input::get('maximum');
        $jobpositions->education = Input::get('education');
        $jobpositions->industry =  Input::get('industry');
        $jobpositions->skill_1 = Input::get('skill_1');
        $jobpositions->skill_2= Input::get('skill_2');
        $jobpositions->experience_month= Input::get('experience_month');
        $jobpositions->salary_range= Input::get('salary_range');
        $jobpositions->ot_rate_per_hour= Input::get('ot_rate_per_hour');
        $jobpositions->ot_rate_per_day= Input::get('ot_rate_per_day');
        $jobpositions->attendance_bonous= Input::get('attendance_bonous');
        $jobpositions->zone= Input::get('zone');
        $jobpositions->bonous_monthly_annually= Input::get('bonous_monthly_annually');
        $jobpositions->other_benefits= Input::get('other_benefits');
        $jobpositions->particular_traits= Input::get('particular_traits');
        $jobpositions->any_special_requests= Input::get('any_special_requests');
        $jobpositions->any_special_requirements= Input::get('any_special_requirements');
        $jobpositions->township= Input::get('township');
        $jobpositions->transport= Input::get('transport');
        $jobpositions->accomodation= Input::get('accomodation');
        $jobpositions->food= Input::get('food');
        $jobpositions->nrc= Input::get('nrc');
        $jobpositions->police_and_ward_rec= Input::get('police_and_ward_rec');
        $jobpositions->labour_card= Input::get('labour_card');
        $jobpositions->remark= Input::get('remark');
        $jobpositions->job_function_id= Input::get('jobfunction');
        $jobpositions->require_position = Input::get('jobtitle');

        // $jobpositions->jobtitle= Input::get('jobfunction');

        
  
        if($jobpositions->save())
        {
            Session::flash('message','Job was successfully created');
            Session::flash('m-class','alert-success');
            return redirect($redirect_route)->with('success','Job successfully added!');
        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return redirect('order/create');
        }
       
 
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Jobposition  $jobposition
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jobpositions = Jobposition::where('job_id',$id)->first();
        $stages = DB::table("stages")->pluck("description","id");
       
        // return $jobseekers;
        //return view('posts.show')->withPost($post)->withComments($comments);
        return view('backend.jobpositions.job_detail', compact('jobpositions','stages'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Jobposition  $jobposition
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {   
        $jobpositions = Jobposition::where('job_id',$id)->first();  
        $township = Township::pluck('name','id');
        $jobfunctions = JobFunction::all();
        $education = Education::all();
        $certificate = Certificate::all();
        $skill = skill::all();
        return view("backend.jobpositions.edit", compact('jobpositions','township','jobfunctions','education','certificate','skill'));       
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Jobposition  $jobposition
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $redirect_route = ($request->free_or_paid == 0)?'freejob':'paidjob';
        $jobpositions = Jobposition::find($id);

        //$jobpositions->employer_id = $request->employer_id;
        $jobpositions->employer_name = $request->fullname;
        $jobpositions->free_or_paid = $request->free_or_paid;
        $jobpositions->job_title =  Input::get('job_title');
        $jobpositions->post_date = Input::get('post_date');
        $jobpositions->deadline = Input::get('deadline');
        $jobpositions->status = Input::get('status');
        // $jobpositions->require_position = Input::get('require_position');
        $jobpositions->gender = Input::get('gender');
        $jobpositions->req_nationality = Input::get('req_nationality');
        $jobpositions->req_quantity = Input::get('req_quantity');
        $jobpositions->certificate_1 = Input::get('certificate_1');
        $jobpositions->certificate_2 = Input::get('certificate_2');
        $jobpositions->minimum = Input::get('minimum');
        $jobpositions->maximum = Input::get('maximum');
        $jobpositions->education = Input::get('education');
        $jobpositions->industry =  Input::get('industry');
        $jobpositions->skill_1 = Input::get('skill_1');
        $jobpositions->skill_2= Input::get('skill_2');
        $jobpositions->experience_month= Input::get('experience_month');
        $jobpositions->salary_range= Input::get('salary_range');
        $jobpositions->ot_rate_per_hour= Input::get('ot_rate_per_hour');
        $jobpositions->ot_rate_per_day= Input::get('ot_rate_per_day');
        $jobpositions->attendance_bonous= Input::get('attendance_bonous');
        $jobpositions->zone= Input::get('zone');
        $jobpositions->bonous_monthly_annually= Input::get('bonous_monthly_annually');
        $jobpositions->other_benefits= Input::get('other_benefits');
        $jobpositions->particular_traits= Input::get('particular_traits');
        $jobpositions->any_special_requests= Input::get('any_special_requests');
        $jobpositions->any_special_requirements= Input::get('any_special_requirements');
        $jobpositions->township= Input::get('township');
        $jobpositions->transport= Input::get('transport');
        $jobpositions->accomodation= Input::get('accomodation');
        $jobpositions->food= Input::get('food');
        $jobpositions->nrc= Input::get('nrc');
        $jobpositions->police_and_ward_rec= Input::get('police_and_ward_rec');
        $jobpositions->labour_card= Input::get('labour_card');
        $jobpositions->remark= Input::get('remark');
        $jobpositions->job_function_id= Input::get('jobfunction');
        $jobpositions->require_position = Input::get('jobtitle');
        $jobpositions->age_requirement = Input::get('age_requirement');
        $jobpositions->experience_requirement = Input::get('experience_requirement');
        $jobpositions->job_requirement = Input::get('job_requirement');
        
        if($jobpositions->save())
        {
            Session::flash('message','Job Positions was successfully updated');
            Session::flash('m-class','alert-success');
            return redirect($redirect_route)->with('success','Job successfully updated!');

        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return redirect('jobseekers');
        }

    }    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Jobposition  $jobposition
     * @return \Illuminate\Http\Response
     */
    public function destroy(Jobposition $jobposition)
    {
        //
    }
}
