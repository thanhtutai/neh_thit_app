<?php

namespace App\Http\Controllers;

  use Illuminate\Http\Request;
  use Goutte;
  use Symfony\Component\DomCrawler\Crawler;
  use App\Http\Requests;
  use App\Models\Fabric;
  // use JonnyW\PhantomJs\Client;
  class WebScraperController extends Controller
  {


    public function scrape_24jobs_com_mm(){
    

    $crawler = Goutte::request('GET', 'https://www.24jobs.com.mm/job-search/?return=true&searchtext=security&skilltxt='); 


    //for 24job
    $job24_jobtitle = $crawler->filter('.job-post-container  .single-job-post .mb-10');
    $job24_company = $crawler->filter('.job-post-container  .single-job-post .position span a');
    $job24_jobfunction = $crawler->filter('.job-post-container  .single-job-post .address span.mt-10');
    $job24_location= $crawler->filter('.job-post-container  .single-job-post .address span.mtb-5');
    $job24_date= $crawler->filter('.job-post-container  .single-job-post .address span:last-child');
    // $job24_location= $crawler->filter('.job-post-container  .single-job-post .address span.mtb-5');
    // $myworld_jobtitle = $crawler->filter('.list-item-wrapper  .job-post-list-item');

    $arr_job = array();
    foreach ($job24_jobtitle as $node) {
       $arr_job[] = $node->nodeValue;
    }  

    $arr_company = array();
    foreach ($job24_company as $key => $value) {
        $arr_company[] = $value->nodeValue;
    }
    $arr_jobfunction = array();
    foreach ($job24_jobfunction as $key => $value) {
        $arr_jobfunction[] = $value->nodeValue;
    }

    $arr_location = array();
    foreach ($job24_location as $key => $value) {
        $arr_location[] = $value->nodeValue;
    }   

    $arr_date = array();
    foreach ($job24_date as $key => $value) {
        $arr_date[] = $value->nodeValue;
     
    }

    foreach ($arr_job as $index => $mylinklay) {
    
    //for job requirement
    $str = preg_replace('/\s{2,}/', ' ', $mylinklay);
    $links = $crawler->selectLink($str)->link();
    $crawlerer = Goutte::click($links);

    $myjobreq =  $crawlerer->filter('.col-md-12 .panel-info:first-child .panel-body .editor_changes');
    $mycompany_address =  $crawlerer->filter('#company-profile-blk .company-address span');

    // $myjobreq_array = array();
    // var_dump($myjobreq);
    $myjobreq_array = array();
    foreach ($myjobreq as $key => $myjobrequirements) {
        $myjobreq_array[] = $myjobrequirements->nodeValue;

    }
    $mycompany_address_array = array();
        foreach ($mycompany_address as $key => $mycom_address) {
            $mycompany_address_array[] = $mycom_address->nodeValue;

    }
//var_dump($mycompany_address_array);
    if(empty($mycompany_address_array)){

    $mycompany_address =  $crawlerer->filter('.job-company-name span');

    $mycompany_address_array = array();
        foreach ($mycompany_address as $key => $mycom_address) {
            $mycompany_address_array[] = $mycom_address->nodeValue;

    }

    }


    // var_dump($mycompany_address_array);

    //For company Info
    $arr_company_trim = trim($arr_company[$index]); 
    // var_dump($arr_company_trim); 
    // $links2 = $crawler->selectLink($arr_company_trim)->link();
    // $crawlerer2 = Goutte::click($links2);

    // $my_companyinfo_email = $crawlerer2->filter('#company-profile-blk .company-info  .row:nth-child(2) .col-md-6:first-child .data-area .table tr:nth-child(2) td:nth-child(2)');    

    // $my_companyinfo_phone = $crawlerer2->filter('#company-profile-blk .company-info  .row:nth-child(2) .col-md-6:first-child .data-area .table tr:nth-child(3) td:nth-child(2)');    

    // $my_companyinfo_address = $crawlerer2->filter('#company-profile-blk .company-info  .row:nth-child(2) .col-md-6:first-child .data-area .table tr:nth-child(4) td:nth-child(2)');

    // $my_companyinfo_email_array = array();
    // foreach ($my_companyinfo_email as $key => $value) {
    //   $my_companyinfo_email_array[] = $value->nodeValue;
    // } 
    // $my_companyinfo_phone_array = array();
    // foreach ($my_companyinfo_phone as $key => $value) {
    //   $my_companyinfo_phone_array[] = $value->nodeValue;
    // } 
    // $my_companyinfo_address_array = array();
    // foreach ($my_companyinfo_address as $key => $value) {
    //   $my_companyinfo_address_array[] = $value->nodeValue;
    // } 

    // var_dump($my_companyinfo_email_array);
    // var_dump($my_companyinfo_phone_array);

       // echo  $mylinklay . '=' . $arr_company[$index] .'='.$arr_location[$index] .'='.$arr_date[$index] .'='.$myjobreq_array[$key].'=='.$my_companyinfo_array[$key].'<br>'.'<br>'  ;


          Fabric::Create([
            'title' => $mylinklay,
            'company_name'=>$arr_company[$index],
            'postdate'=>$arr_date[$index],
            'township'=>$arr_location[$index],
            'job_requirement'=>$myjobreq_array[$key],
            'company_address'=>$mycompany_address_array[$key],
            // 'email'=>$my_companyinfo_email_array[$key],
            // 'phone'=>$my_companyinfo_phone_array[$key],
                                 ]);

    }

  }

  public function scrape_jobnet_com_mm(){
    

    $crawler = Goutte::request('GET', 'https://www.jobnet.com.mm/jobs-in-myanmar?keyword=finance'); 

      //for jobnet
    $my_jobtitle = $crawler->filter('.wrap-search-result .single-search-reslt h3 ');
    $com_address = $crawler->filter('.employer-hero-card-text-address-group .employer-group-name');
    $my_companyname = $crawler->filter('.wrap-search-result .single-search-reslt a.comp-name ');
    $my_postdate = $crawler->filter('.wrap-search-result .single-search-reslt .info span.post-date');
    $my_place = $crawler->filter('.wrap-search-result .single-search-reslt .amout-place .place');



    $arr_job = array();
    foreach ($my_jobtitle as $node) {
       $arr_job[] = $node->nodeValue;
    }  

    $arr_company = array();
    foreach ($my_companyname as $key => $value) {
        $arr_company[] = $value->nodeValue;
    }

    $arr_location = array();
    foreach ($my_place as $key => $value) {
        $arr_location[] = $value->nodeValue;
    }   

    $arr_date = array();
    foreach ($my_postdate as $key => $value) {
        $arr_date[] = $value->nodeValue;
     
    }

    $arr_removestringdate = str_replace('Posted :', '', $arr_date);


    // foreach ($remove_string as $key => $value) {
    //    $time = strtotime($value);
    //    $newformat = date('Y-m-d',$time);

    // }

  foreach ($arr_job as $index => $mylinklay) {
    
    //for job requirement
    $str = preg_replace('/\s{2,}/', ' ', $mylinklay);
    $str_trim = trim($str);
    $links = $crawler->selectLink($str_trim)->link();
    $crawlerer = Goutte::click($links);

    $myjobreq =  $crawlerer->filter('#job-detail-content .div-what-you-will-do div:last-child');

    $myjobreq_array = array();
      foreach ($myjobreq as $key => $myjobrequirements) {
        $myjobreq_array[] = $myjobrequirements->nodeValue;

      }
      
    // //For company Info
    $arr_company_trim = trim($arr_company[$index]); 
    // var_dump($arr_company_trim); 
    $links2 = $crawler->selectLink($arr_company_trim)->link();
    $crawlerer2 = Goutte::click($links2);

    $my_companyinfo = $crawlerer2->filter('.employer-hero-card-wrapper .employer-hero-card-text-address-group .employer-hero-card-info-group .employer-group-name');

    $my_companyinfo_array = array();
    foreach ($my_companyinfo as $key => $value) {
      $my_companyinfo_array[] = $value->nodeValue;
    } 

    // $arr_removestringdate = str_replace('Posted :', '', $arr_date[$index]);
    

    $arr_removespace_date = trim($arr_removestringdate[$index]);
    $arr_add_dash_date = str_replace(' ', '-', $arr_removespace_date);

    $myjobtitle = trim($mylinklay);
    
    //echo $arr_add_dash_date;


    // var_dump($my_companyinfo_array);


     //  echo  $mylinklay . '=' . $arr_company[$index].'='.$arr_location[$index] .'='.$arr_add_dash_date .'='.$myjobreq_array[$key].'=='.$my_companyinfo_array[$key].'<br>'.'<br>'  ;

          Fabric::Create([
            'title' => $myjobtitle,
            'company_name'=>$arr_company[$index],
            'postdate'=>$arr_add_dash_date,
            'township'=>$arr_location[$index],
            'job_requirement'=>$myjobreq_array[$key],
            'company_address'=>$my_companyinfo_array[$key],
                                 ]);


    }


  }

    public function index(){
  
    $jj = set_include_path('/usr/lib/pear');

  // dd($jj);

    $client = Client::getInstance();


    // dd($client);
    $client->getEngine()->setPath(base_path() . '/public/bin/phantomjs');
    /** 
     * @see JonnyW\PhantomJs\Http\Request
     **/
    $request = $client->getMessageFactory()->createRequest('https://www.work.com.mm/en/jobs/?term=', 'GET');
    // $request = $client->getMessageFactory()->createRequest('https://www.work.com.mm', 'GET');
        /**
         * @see JonnyW\PhantomJs\Http\Response
         **/
        $response = $client->getMessageFactory()->createResponse();
        // Send the request
        $client->send($request, $response);
        if ($response->getStatus() === 200) {
            // Dump the requested page content
            // echo $response->getContent();
              $crawler = $response->getContent();
              $crawler = str_split($crawler, 3);

        array_shift($crawler);
        array_pop($crawler);


         var_dump($crawler);



        }


    }


    public function scrapetest(){

    $data= file_get_contents('https://api.cinelist.co.uk/get/times/cinema/10565');
    $data = json_decode($data);

    foreach($data->listings as $listing){
        $title = $listing->title;
        $time = implode(', ', $listing->times);

       Fabric::updateOrCreate([
                             'name' => $title
                             ]);
    }

  }

 //  public function index()
 //  {


 //  	// return "hello";
 //  //  Create a new Goutte client instance
 //    $client = new Client();

 // //  Hackery to allow HTTPS
 //    $guzzleclient = new \GuzzleHttp\Client([
 //        'timeout' => 60,
 //        'verify' => false,
 //    ]);
    
 //    // Create DOM from URL or file
 //    $html = file_get_html('https://www.facebook.com');

 //    // Find all images
 //    foreach ($html->find('img') as $element) {
 //        echo $element->src . '<br>';
 //    }

 //    // Find all links
 //    foreach ($html->find('a') as $element) {
 //        echo $element->href . '<br>';
 //    }
 //  }


  // public function index(Request $request)
  // {
  //   dd($request);
  //  $crawler = Goutte::request('GET', 'https://malesbanget.com/');
  //      $crawler->filter('.content .card-author-name')->each(function (Request $request,$node) {
  //     // dump($node->attr('src'));
  //     // dd($node);
  //     // dump($node->text());

  //     return $node->text();

  //     // $fabrics = new Fabric();
  //     // $fabrics->name = $node->text();

  //   });
  // }


    
}
