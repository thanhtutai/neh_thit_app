<?php

namespace App\Http\Controllers;

use App\Models\JobFunction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class JobFunctionController extends Controller
{


     public function __construct()
     {
        $this->middleware('auth');
     }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobfunctions = JobFunction::orderBy('jobfunctions.id','asc')->get();
        // dd($desirepositions);
        return view('backend.jobfunctions.index',compact('jobfunctions')); 
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("backend.desireposition1.index");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $jobfunctions = new JobFunction();
        $jobfunctions->description = Input::get('description');
        $jobfunctions->description_mm = Input::get('description_mm');


        if($jobfunctions->save())
        {
            Session::flash('message','Job function was successfully created');
            Session::flash('m-class','alert-success');
            return back()->with('success','Job function successfully added!');
        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return redirect('jobseekers');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $jobfunctions = JobFunction::findOrFail($id);
      return view("backend.jobfunctions.edit", compact('jobfunctions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $jobfunctions = JobFunction::find($id);

        $jobfunctions->description = Input::get('description');
        $jobfunctions->description_mm = Input::get('description_mm');

        if($jobfunctions->save())
        {
            Session::flash('message','Job function was successfully updated');
            Session::flash('m-class','alert-success');
            return redirect('jobfunctions')->with('success','JobFunction successfully updated!');
        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return redirect('jobseekers');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
