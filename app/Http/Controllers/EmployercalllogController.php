<?php

namespace App\Http\Controllers;

use App\Models\Employercalllog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use DB;
use Excel;
class EmployercalllogController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         $js_id             = $request->js_id;
         $fullname          = $request->fullname;
         $call_reason       = $request->call_reason;
         $age               = $request->age;
         $caller_receiver   = $request->caller_receiver;
         $remark_calllog    = $request->remark_calllog;
         $type_of_call      = $request->type_of_call;
         $type_of_call      = $request->type_of_call;
         $created_at        = $request->created_at;
         $call_back_date    = $request->call_back_date;

        if(count($request->all())!=0){  
            
            $employers=Employercalllog::Search($request)->paginate(10);   

        }
        
        else{
        
            $employers = Employercalllog::orderBy('employercalllogs.created_at','DESC')->paginate(100);
        
        }       

        return view('backend.employer_calllogs.index',compact('employers'))->with('js_id',$js_id)->with('caller_receiver',$caller_receiver)->with('remark_calllog',$remark_calllog)->with('type_of_call',$type_of_call)->with('fullname',$fullname)->with('call_reason',$call_reason)->with('created_at',$created_at)->with('call_back_date',$call_back_date); 

    }
    
    public function todo_em(Request $request)
    {
         $js_id             = $request->js_id;
         $fullname          = $request->fullname;
         $call_reason       = $request->call_reason;
         $age               = $request->age;
         $caller_receiver   = $request->caller_receiver;
         $remark_calllog    = $request->remark_calllog;
         $type_of_call      = $request->type_of_call;
         $type_of_call      = $request->type_of_call;
         $created_at        = $request->created_at;
         $call_back_date    = $request->call_back_date;


        if(count($request->all())!=0){  
   
             $employers=Employercalllog::Search($request)->paginate(10);  
    
        }
    
        else{
    
            $employers = Employercalllog::orderBy('employercalllogs.created_at','DESC')->get();
    
        }       

        return view('backend.employer_calllogs.todo_em',compact('employers'))->with('js_id',$js_id)->with('caller_receiver',$caller_receiver)->with('remark_calllog',$remark_calllog)->with('type_of_call',$type_of_call)->with('fullname',$fullname)->with('call_reason',$call_reason)->with('created_at',$created_at)->with('call_back_date',$call_back_date); 

    }

    public function downloadExcel($type)
    {

        // $data1 = DB::table('employers')
                    
        //             ->select(['employer_name'])->get();
        // $data = json_decode($data1, true);
        $customizedcolumns = DB::table('employers')
                            ->select(['employer_name','email_address'])->get();

        $customizedcolumn_data  = json_decode($customizedcolumns,true);

        $employerdata = Employercalllog::get()->toArray();

        // $data2 = Employer::get()->toArray();
        // $data = Employer::get()->toArray();
        return Excel::create('employer_call_logs_excel', function($excel) use ($employerdata,$customizedcolumn_data) {

            $excel->sheet('employer_full_column', function($sheet) use ($employerdata)
                
            {
                $sheet->fromArray($employerdata);
            });  
        })->download($type);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("backend.employer_calllogs.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $employer = new Employercalllog();
        $employer->employer_id = Input::get('employer_id');
        $employer->call_back_date = Input::get('call_back_date');
        // $employer->call_back_date = Input::get('');
        $employer->type_of_call = Input::get('type_of_call');
        $employer->start_time = Input::get('start_time');
        $employer->minute = Input::get('minute');
        $employer->industry_zone = Input::get('industry_zone');
        $employer->receiver = Input::get('receiver');
        $employer->ph_number = Input::get('ph_number');
        $employer->call_reason = Input::get('call_reason');
        $employer->remark = Input::get('remark');
        $employer->status = Input::get('description');
        $employer->meeting = Input::get('meeting');
        $employer->email_sent = Input::get('email_sent');
        $employer->caller_receiver = $request->user()->id;

        if($employer->save())
        {
            Session::flash('message','Order was successfully created');
            Session::flash('m-class','alert-success');
            return back()->with('success','Calllog successfully added!');
        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return redirect('calllogs');
        }
    }

    public function employercallogstore(Request $request)
    {
        $employer = new Employercalllog();
        $employer->employer_id = Input::get('employer_id');
        $employer->company_name = Input::get('employer_name');
        $employer->call_back_date = Input::get('call_back_date');
        // $employer->call_back_date = Input::get('');
        $employer->type_of_call = Input::get('type_of_call');
        $employer->start_time = Input::get('start_time');
        $employer->minute = Input::get('minute');
        $employer->industry_zone = Input::get('industry_zone');
        $employer->receiver = Input::get('receiver');
        $employer->ph_number = Input::get('ph_number');
        $employer->call_reason = Input::get('call_reason');
        $employer->remark = Input::get('remark');
        $employer->status = Input::get('description');
        $employer->follow_up_time = Input::get('follow_up_time');
        $employer->meeting = Input::get('meeting');
        $employer->email_sent = Input::get('email_sent');
        $employer->caller_receiver = $request->user()->id;

        if($employer->save())
        {
            Session::flash('message','Order was successfully created');
            Session::flash('m-class','alert-success');
            return back()->with('success','Calllog successfully added!');
        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return redirect('calllogs');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employercalllog  $employercalllog
     * @return \Illuminate\Http\Response
     */
    public function show(Employercalllog $employercalllog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employercalllog  $employercalllog
     * @return \Illuminate\Http\Response
     */
    public function edit(Employercalllog $employercalllog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employercalllog  $employercalllog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employercalllog $employercalllog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employercalllog  $employercalllog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employercalllog $employercalllog)
    {
        //
    }
}
