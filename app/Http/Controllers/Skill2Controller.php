<?php

namespace App\Http\Controllers;

use App\Models\Skill2;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
class Skill2Controller extends Controller
{
    
     public function __construct()
     {
        $this->middleware('auth');
     }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $skills = Skill2::orderBy('skill2s.name','asc')->paginate(10);
          return view('backend.skill2.index',compact('skills')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $skills = new Skill2();
        $skills->name = Input::get('name');


        if($skills->save())
        {
            Session::flash('message','Skill 2 was successfully created');
            Session::flash('m-class','alert-success');
            return back()->with('success','Skill successfully added!');
        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return redirect('jobseekers');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Skill2  $skill2
     * @return \Illuminate\Http\Response
     */
    public function show(Skill2 $skill2)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Skill2  $skill2
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $skills = Skill2::findOrFail($id);
        return view("backend.skill2.edit", compact('skills'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Skill2  $skill2
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $skills = Skill2::find($id);

        $skills->name = Input::get('name');

        if($skills->save())
        {
            Session::flash('message','skills 2 was successfully updated');
            Session::flash('m-class','alert-success');
            return redirect('skill_2')->with('success','Skills 1 successfully updated!');
        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return redirect('jobseekers');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Skill2  $skill2
     * @return \Illuminate\Http\Response
     */
    public function destroy(Skill2 $skill2)
    {
        //
    }
}
