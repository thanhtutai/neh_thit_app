<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use App\Models\Checklist;
use DB;
use Carbon\Carbon;
use App\Models\Calllog;
use App\Models\Jobseeker;

class ChecklistController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $result = null;
        if(count($request->all())!=0){  
          
            $result=Checklist::Search($request)->paginate(10);  

        }
        else{
     
            $checklists = Checklist::paginate(16);
        }       

       return view('backend.checklists.employer_sales',compact('checklists','result')); 
    }

    public function test()
    {       $result = null;
             $jobseekers = Jobseeker::with('calllogs')->orderBy('created_at', 'desc')->paginate(10);
              return view('backend.checklists.test',compact('jobseekers','result')); 
        // return view('backend.checklists.test');
    }

    public function create()
    {
         return view("backend.checklists.create_employer_sale");
    }


    public function jobseeker_calls(Request $request)
    {
        
        $result = null;
        if(count($request->all())!=0){  
            if ($request->has('sub_search')) {
                $jobseekers = Jobseeker::Subsearch($request)->paginate(10); 
                dd($applicant_information);

            }else{
                $result=Jobseeker::Search($request)->paginate(10);  
              //  dd($orders);
            } 

        }
        else{
     
            $checklists = Checklist::paginate(16);
        }       


       return view('backend.checklists.jobseeker_calls',compact('checklists','result')); 
    }

    public function count(){

    $count = DB::table('calllogs')
             ->select('created_at', DB::raw('count(*) as peoples'))
             ->groupBy('created_at')
             ->get();


    $count2 = DB::table('jobseekers')
         ->select(DB::raw('DATE(created_at) as created_at'), DB::raw('count(*) as jobseekers'))      
         ->groupBy('created_at')
         ->get();   
// dd($count2);
         // dd($count);
         return view('backend.checklists.jobseeker_calls',compact('count','count2'));

// $start_date = Carbon::now()->subWeek();
// $end_date   = Carbon::now()->now();
// $dates      = [];

// for ( $date = $start_date; $date->lte( $end_date ); $date->addDay() ) {
//     $dates[] = $date;
// }

// $counts = [];
// foreach ( $dates as $date ) {
//     $dateLater                          = $date->copy()->addDay();
//     $callLogs                           = Calllog::whereBetween( $arrayName = array('a' => 'b','c' ) )->count();

//     $counts[ $date->format( 'Y-m-d' ) ] = [ $callLogs, $jobSeekers ];
// }

// print_r($counts);


    }
    
}
