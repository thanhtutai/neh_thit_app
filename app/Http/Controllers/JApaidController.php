<?php

namespace App\Http\Controllers;

use App\Models\JApaid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
class JApaidController extends Controller
{
      
    public function __construct()
    {
        $this->middleware('auth');
    }  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)  
    {
         $result = null;
        if(count($request->all())!=0){  
            if ($request->has('sub_search')) {
                $jobseekers = Jobseeker::Subsearch($request)->paginate(10); 
                dd($applicant_information);

            }else{
                $ja_paids=JApaid::Search($request)->paginate(10);  
              //  dd($orders);
            } 

        }
        else{
     
            $ja_paids = JApaid::paginate(16);
        }       

       return view('backend.jobavailables_paid.index',compact('ja_paids')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("backend.jobavailables_paid.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $jobavailable = new JApaid();
        $jobavailable->company_name = Input::get('company_name');
        $jobavailable->req_position = Input::get('req_position');
        $jobavailable->req_quantity = Input::get('req_quantity');
        // $jobavailable->call_back_date = Input::get('');
        $jobavailable->req_gender = Input::get('req_gender');
        $jobavailable->req_age = Input::get('req_age');
        $jobavailable->req_education = Input::get('req_education');
        $jobavailable->req_skill1 = Input::get('req_skill1');
        $jobavailable->req_skill2 = Input::get('req_skill2');
        $jobavailable->req_experience = Input::get('req_experience');
        $jobavailable->req_nationality = Input::get('req_nationality');
        $jobavailable->req_hiring_date = Input::get('req_hiring_date');
        $jobavailable->req_remark = Input::get('req_remark');
        $jobavailable->basic_salary = Input::get('basic_salary');
        $jobavailable->ot_rate_per_hour  = Input::get('ot_rate_per_hour');
        $jobavailable->ot_rate_per_day = Input::get('ot_rate_per_day');
        $jobavailable->attendance_bonous = Input::get('attendance_bonous');
        $jobavailable->food = Input::get('food');
        $jobavailable->township = Input::get('township');
        $jobavailable->zone = Input::get('zone');
        $jobavailable->transportation = Input::get('transportation');
        $jobavailable->accommodation = Input::get('accommodation');
        $jobavailable->ssb = Input::get('ssb');
        $jobavailable->bonous_monthly_annually = Input::get('bonous_monthly_annually');
        $jobavailable->other_benefits = Input::get('other_benefits');
        $jobavailable->particular_traits = Input::get('particular_traits');
        $jobavailable->any_special_requests = Input::get('any_special_requests');
        $jobavailable->any_special_requirements = Input::get('any_special_requirements');
        $jobavailable->additional_comments = Input::get('additional_comments');
        
       

        if($jobavailable->save())
        {
            Session::flash('message','Order was successfully created');
            Session::flash('m-class','alert-success');
            return back()->with('success','Job successfully added!');
        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return redirect('calllogs');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\JApaid  $jApaid
     * @return \Illuminate\Http\Response
     */
   public function show($id)
    {
       $jobavailables = jApaid::findOrFail($id);

        return view('backend.jobavailables_paid.show', compact('jobavailables'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JApaid  $jApaid
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $jobavailables = JApaid::findOrFail($id);
       
        return view("backend.jobavailables_paid.edit", compact('jobavailables'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\JApaid  $jApaid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $jobavailables = JApaid::find($id);

        $jobavailables->company_name = Input::get('company_name');
        $jobavailables->req_position = Input::get('req_position');
        $jobavailables->req_quantity = Input::get('req_quantity');
        $jobavailables->req_gender = Input::get('req_gender');
        $jobavailables->req_age = Input::get('req_age');
        $jobavailables->req_education = Input::get('req_education');
        $jobavailables->req_skill1 = Input::get('req_skill1');
        $jobavailables->req_skill2 = Input::get('req_skill2');
        $jobavailables->req_experience = Input::get('req_experience');
        $jobavailables->req_nationality = Input::get('req_nationality');
        $jobavailables->req_hiring_date = Input::get('req_hiring_date');
        $jobavailables->req_remark = Input::get('req_remark');
        $jobavailables->basic_salary = Input::get('basic_salary');
        $jobavailables->ot_rate_per_hour = Input::get('ot_rate_per_hour');
        $jobavailables->ot_rate_per_day = Input::get('ot_rate_per_day');
        $jobavailables->attendance_bonous = Input::get('attendance_bonous');
        $jobavailables->food = Input::get('food');
        $jobavailables->township = Input::get('township');
        $jobavailables->zone = Input::get('zone');
        $jobavailables->transportation = Input::get('transportation');
        $jobavailables->accommodation = Input::get('accommodation');
        $jobavailables->ssb = Input::get('ssb');
        $jobavailables->bonous_monthly_annually = Input::get('bonous_monthly_annually');
        $jobavailables->other_benefits = Input::get('other_benefits');
        $jobavailables->particular_traits = Input::get('particular_traits');
        $jobavailables->any_special_requests = Input::get('any_special_requests');
        $jobavailables->any_special_requirements = Input::get('any_special_requirements');
        $jobavailables->additional_comments = Input::get('additional_comments');
        

        if($jobavailables->save())
        {
            Session::flash('message','Data was successfully updated');
            Session::flash('m-class','alert-success');
             return back()->with('success','Data successfully updated!');

        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return redirect('jobsavailable-paid');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\JApaid  $jApaid
     * @return \Illuminate\Http\Response
     */
    public function destroy(JApaid $jApaid)
    {
        //
    }
}
