<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Fabric;

class JobTransformer extends TransformerAbstract
{
    public function transform(Fabric $fabric)
    {
        return [ "attachment"=>[
        		"type" => "template",
        		"payload"=>[
        			"template_type"=>"generic",
        			"image_aspect_ratio"=>"square",
        			"elements"=>[
        					
            'names' => $fabric->title,
            'email' => $fabric->company_name,
            'added' => $fabric->postdate		

        						]
        					]
        		]
        ];
    }
}
