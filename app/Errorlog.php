<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Errorlog extends Model
{
    protected $fillable = ['messenger_user_id','apply_job_id'];
}
