<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Http\AuthTraits\OwnsRecord;


class User extends Authenticatable
{
    use Notifiable;
    use OwnsRecord;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_users');
    }

      /**
     * Checks if User has access to $permissions.
     */
    public function hasAccess(array $permissions) : bool
    {
        // check if the permission is available in any role
        foreach ($this->roles as $role) {
            if($role->hasAccess($permissions)) {
                return true;
            }
        }
        return false;
    }
    public function calllogs()
    {
        return $this->hasMany('App\Models\Calllog','caller_receiver');
    }    
    public function jobpositions()
    {
        return $this->hasMany('App\Models\Jobposition','posted_by');
    }
    public function employercalllogs()
    {
        return $this->hasMany('App\Models\Employercalllogs','caller_receiver');
    }
    public function checklists()
    {
        return $this->hasMany('App\Models\Checklist','user_id');
    }
    public function checklistsemp()
    {
        return $this->hasMany('App\Models\Checklistemp','user_id');
    }
    public function checklistsjs()
    {
        return $this->hasMany('App\Models\Checklistjs','user_id');
    }


    /**
     * Checks if the user belongs to role.
     */
    public function inRole(string $roleSlug)
    {
        return $this->roles()->where('slug', $roleSlug)->count() == 1;
    }
}
