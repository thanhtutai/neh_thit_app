<?php

namespace App\Repositories;

class JobseekerRepository
{
 

    public function getAll($includePaid) {

        $jobseekers =Jobseeker::select('jobseekers.id','time','date','fb_name','fullname','phone', 'viber_number','sign_up_from','jobseekers.gender','age','wards','jobseekers.education','previous_position_1','previous_position_other','experience_month_1','previous_position_2','previous_position_other_2','experience_month_2','certification_and_license','fabrics','type_of_license','desire_position_y_n','desire_position_1','desire_position_other_1','desire_position_2','desire_position_other_2','job_industry_1','job_industry_2','jobseekers.skill_1','jobseekers.skill_other_1','jobseekers.skill_2','skill_other_2','certificate','certificate_other','jobseekers.township','viber_number_2','jobseekers.remark','usertype_1','chatfuel_user_id','jobseekers.address','last_user_freedom_input','messenger_user_id','company_id','additional_field','additional_field2','additional_field3','interview_time','nationality','nrc_no','marital_status', DB::raw('IFNULL(COUNT(require_position), 0) as total_requireposition'), DB::raw("IFNULL(SUM(free_or_paid = 1), 0) as total_free_or_paid"))
            ->whereIn('jobseekers.status',[22,10,13,4,28,])
            ->leftjoin('jobpositions', 'desire_position_1', '=', 'require_position')
            ->groupBy('jobseekers.id')
            ->orderBy('total_free_or_paid','desc')
            ->orderBy('jobseekers.id','desc')
            ->paginate(70);

    }
}