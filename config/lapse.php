<?php 

return [
    'slack_channel' => 'https://hooks.slack.com/services/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',
    // Currently three notification channels supported
    // Those are database, slack and email
    'via' => ['database', 'slack']
];