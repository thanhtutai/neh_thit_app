@extends('layouts.dashboard')
@section('section')
<script>
    
     jQuery(function ($) {
        $.fn.countTo = function (options) {
            options = options || {};

            return $(this).each(function () {
                // set options for current element
                var settings = $.extend({}, $.fn.countTo.defaults, {
                    from: $(this).data('from'),
                    to: $(this).data('to'),
                    speed: $(this).data('speed'),
                    refreshInterval: $(this).data('refresh-interval'),
                    decimals: $(this).data('decimals')
                }, options);

                // how many times to update the value, and how much to increment the value on each update
                var loops = Math.ceil(settings.speed / settings.refreshInterval),
                    increment = (settings.to - settings.from) / loops;

                // references & variables that will change with each update
                var self = this,
                    $self = $(this),
                    loopCount = 0,
                    value = settings.from,
                    data = $self.data('countTo') || {};

                $self.data('countTo', data);

                // if an existing interval can be found, clear it first
                if (data.interval) {
                    clearInterval(data.interval);
                }
                data.interval = setInterval(updateTimer, settings.refreshInterval);

                // initialize the element with the starting value
                render(value);

                function updateTimer() {
                    value += increment;
                    loopCount++;

                    render(value);

                    if (typeof (settings.onUpdate) == 'function') {
                        settings.onUpdate.call(self, value);
                    }

                    if (loopCount >= loops) {
                        // remove the interval
                        $self.removeData('countTo');
                        clearInterval(data.interval);
                        value = settings.to;

                        if (typeof (settings.onComplete) == 'function') {
                            settings.onComplete.call(self, value);
                        }
                    }
                }

                function render(value) {
                    var formattedValue = settings.formatter.call(self, value, settings);
                    $self.text(formattedValue);
                }
            });
        };

        $.fn.countTo.defaults = {
            from: 0, // the number the element should start at
            to: 0, // the number the element should end at
            speed: 1000, // how long it should take to count between the target numbers
            refreshInterval: 100, // how often the element should be updated
            decimals: 0, // the number of decimal places to show
            formatter: formatter, // handler for formatting the value before rendering
            onUpdate: null, // callback method for every time the element is updated
            onComplete: null // callback method for when the element finishes updating
        };

        function formatter(value, settings) {
            return value.toFixed(settings.decimals);
        }
   

      // custom formatting example
      $('#earth').data('countToOptions', {
        formatter: function (value, options) {
          return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
        }
      });
      
      // custom callback when counting completes
      $('#countdown').data('countToOptions', {
        onComplete: function (value) {
          $(this).text('BLAST OFF!').addClass('red');
        }
      });
      
      // another custom callback for counting to infinity
      $('#infinity').data('countToOptions', {
        onComplete: function (value) {
          count.call(this, {
            from: value,
            to: value + 1
          });
        }
      });
      
      // start all the timers
      $('.timer').each(count);
      
      // restart a timer when a button is clicked
      $('.restart').click(function (event) {
        event.preventDefault();
        var target = $(this).data('target');
        count.call($(target));
      });
      
      function count(options) {
        var $this = $(this);
        options = $.extend({}, options || {}, $this.data('countToOptions') || {});
        $this.countTo(options);
      }
    });

</script>
<br><br><br>
             <div class="container-fluid">
   

                <div class="row">

<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Today : {{Carbon\Carbon::now()}}</p>

    <div class="col-lg-3 col-md-6">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">

                    <div class="col-xs-3">
                        <i class="fa fa-file-text fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                      
                     <h1><p> <b class="timer" id="earth" data-to="{{$jobseekers}}" data-speed="3000"></b></p> </h1>


                        <div> Job Seekers</div>
                    </div>
                </div>
            </div>
            <a href="/jobseekers">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>  
   

                        <div class="col-lg-3 col-md-6">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-file-text fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
  <h1> <p> <b class="timer" id="earth" data-to="{{$employers}}" data-speed="3000"></b></p> </h1>
                                       
                                        <div> Employers</div>
                                    </div>
                                </div>
                            </div>
                            <a href="/employers">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>   

       <div class="col-lg-3 col-md-6">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-user fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
  <h1> <p> <b class="timer" id="earth" data-to="{{$j_paid}}" data-speed="3000"></b></p> </h1>
                                       
                                        <div>Contracted Jobs </div>
                                    </div>
                                </div>
                            </div>
                            <a href="/paidjob">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>

</div>




<div class="row">

                        <div class="col-lg-3 col-md-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-list fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">

                                  <h1>  <h1> <p> <b class="timer" id="earth" data-to="{{$todo_count}}" data-speed="3000"></b></p> </h1></h1>

                                   <div>Follow Up Job Seeker To Do</div>
                                    </div>
                                </div>
                            </div>
                            <a href="{{ route('todo.index') }}">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>


                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-list fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
  <h1> <p> <b class="timer" id="earth" data-to="{{$todo_em}}" data-speed="3000"></b></p> </h1>
                                       
                                        <div> Employers Todo</div>
                                    </div>
                                </div>
                            </div>
                            <a href="/todo-em">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>    
 
          <div class="col-lg-3 col-md-6">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-user fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
  <h1> <p> <b class="timer" id="earth" data-to="{{$j_available}}" data-speed="3000"></b></p> </h1>
                                       
                                        <div> Free Job</div>
                                    </div>
                                </div>
                            </div>
                            <a href="/freejob">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>               

               

                <div class="col-lg-3 col-md-6">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-comments fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
  <h1> <p> <b class="timer" id="earth" data-to="{{$employercalllog}}" data-speed="3000"></b></p> </h1>
                                       
                                        <div> Employers Call Log</div>
                                    </div>
                                </div>
                            </div>
                            <a href="/employercalllogs">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>                   

          


                      <div class="col-lg-3 col4md-6">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-comments fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">

                       <h1> <p> <b class="timer" id="earth" data-to="{{$calllogs}}" data-speed="3000"></b></p> </h1>
           
                                      <div>Job Seekers Call Log</div>
                                    </div>
                                </div>
                            </div>
                            <a href="{{ route('calllogs.index') }}">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
               <!--      <div class="col-lg-2 col-md-6">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-user fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
  <h1> <p> <b class="timer" id="earth" data-to="{{$duplicates}}" data-speed="3000"></b></p> </h1>
                                       
                                        <div> Duplicates</div>
                                    </div>
                                </div>
                            </div>
                            <a href="{{ route('duplicates.index') }}">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>  -->      


                </div>

                         

<div id="piechart-js" class="chart"></div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
// Load google charts
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

// Draw the chart and set the chart values
function drawChart() {
  var data = google.visualization.arrayToDataTable([
  ['Task', 'Hours per Day'],
  ['Jobseekers', {{$jobseekers}}],
  ['Jobseeker Call Logs', {{$calllogs}}],
  ['Jobseeker Todo', {{$todo_count}}],
  ['New Jobseekers', {{$newjs}}]
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'backgroundColor': 'transparent', 'width':400, 'height':400};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart-js'));
  chart.draw(data, options);
}
</script>






<div id="piechart-em" class="chart"></div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
// Load google charts
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

// Draw the chart and set the chart values
function drawChart() {
  var data = google.visualization.arrayToDataTable([
  ['Task', 'Hours per Day'],
  ['Employers', {{$employers}}],
  ['Employer Call Logs', {{$employercalllog}}],
  ['Employer Todo', {{$todo_em}}]
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'backgroundColor': 'transparent', 'width':400, 'height':400};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart-em'));
  chart.draw(data, options);
}
</script>





<div id="piechart-ja-jp" class="chart"></div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
// Load google charts
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

// Draw the chart and set the chart values
function drawChart() {
  var data = google.visualization.arrayToDataTable([
  ['Task', 'Hours per Day'],
  ['Paid Job', {{$j_paid}}],
  ['Job Availables', {{$j_available}}],
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'backgroundColor': 'transparent', 'width':400, 'height':400};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart-ja-jp'));
  chart.draw(data, options);
}
</script>

                <!-- /.row -->
</div>
<style type="text/css">
    .chart{
        display: inline-block;
    }
</style>
                @stop