@extends('layouts.dashboard')


@section('section')
<br>
<br>
<br>
<!--<div class="row">
    <a class="btn btn-success pull-left" href="{{ route('employers.index')}}">Back</a>
    </div> -->
     


  <div class="col-md-6">
    <form action="{{ route('employers.update', $employers->id) }}" enctype="multipart/form-data" method="POST">
      {{ csrf_field() }}
      <div class="form-group">
            <label for="title">Employer ID:</label>
            <input type="text" readonly name="employer_id" id="emp_id" class="form-control" value="{{ $employers->emp_id }}">
           </div>
          <div class="form-group">
            <label for="title">Employer Name:</label>
            <input type="text" name="employer_name" id="title" class="form-control" value="{{ $employers->employer_name }}">
           </div>
             
            <div class="form-group">
              {!! Form::label('Choose Status') !!}
              <?php $description = \App\Models\Employerstatus::all(); ?>
              <select id="val_select" name="description" class="form-control select2">
                <option value="{{ $employers->status}}">{{$employers->mystatus['description']}}</option>
                @foreach($description as $row)
                <option value="{{ $row->id }}" @if(old('description') == $row->id) selected @endif> {{ $row->description }} </option>
                @endforeach                 
              </select>
             </div>
              {!! Form::file('emp_image', array('class' => 'form-control')) !!}
              @if($errors->has('emp_image'))
                  <div class="error">{{$errors->first('emp_image')}}
                  </div>
              @endif
             <div class="form-group">
              <label for="title">Phone Number:</label>
              <input type="text" name="phone_number" id="title" class="form-control" value="{{ $employers->phone_number }}">    
             </div>  

             <div class="form-group">
              <label for="title">Company Type:</label>
              <input type="text" name="company_type" id="title" class="form-control" value="{{ $employers->company_type }}">    
             </div>

             <div class="form-group">
              <label for="title">Note:</label>
              <input type="text" name="notes" id="title" class="form-control" value="{{ $employers->company_type }}">    
             </div>

             <div class="form-group">
              <label for="title">Address:</label>
              <input type="text" name="address" id="title" class="form-control" value="{{ $employers->address }}">
             </div>

             <div class="form-group">
              <label for="title">Zone:</label>
              <input type="text" name="zone" id="title" class="form-control" value="{{ $employers->zone }}">
             </div>
   
              <div class="form-group">
              <label for="title">Email Address:</label>
              <input type="email" name="email_address" id="title" class="form-control" value="{{ $employers->email_address }}">
              </div>
   
              <div class="form-group">
              <label for="title">Authorized Person:</label>
              <input type="text" name="authorized_person" id="title" class="form-control" value="{{ $employers->authorized_person }}">
              </div>

              <div class="form-group">
              <label for="title">Latitude:</label>
              <input type="text" name="latitude" id="title" class="form-control" value="{{ $employers->latitude }}">
              </div>       
            
              <div class="form-group">
              <label for="title">longitude:</label>
              <input type="text" name="longitude" id="title" class="form-control" value="{{ $employers->longitude }}">
              </div>

              <div class="form-group">
              <label for="title">Number of call:</label>
              <input type="text" name="number_of_call" id="title" class="form-control" value="{{ $employers->number_of_call }}">
              </div>

              <div class="form-group">
              <label for="title">Interest:</label>
              <input type="text" name="interest" id="title" class="form-control" value="{{ $employers->interest }}">
              </div>

              <div class="form-group">
              <label for="title">Understanding:</label>
              <input type="text" name="understanding" id="title" class="form-control" value="{{ $employers->understanding }}">
              </div>

              <div class="form-group">
              <label for="title">Notes:</label>
              <input type="text" name="notes" id="title" class="form-control" value="{{ $employers->notes }}">
              </div>

              <div class="form-group"> 
              <form action="{{ route('employers.update', $employers->id) }}">
                 {{ csrf_field() }}
                 {{ method_field("patch") }}
              <button type="submit" class="btn btn-primary">Update Info</button>
              </form>
      </div> 
      </div>
    </form>
  </div>

</div>
  
  @stop