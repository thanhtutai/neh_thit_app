@extends('layouts.dashboard')


@section('section')
<style type="text/css">
  .right-wrapper-info{
    display: inline-block;
  }
</style>

<br><br><br>
<h2 style="text-align:center">Employer Profile</h2>

<div class="col-md-8 col-md-offset-2">
   
  <div class="panel panel-info">

          <div class="panel-footer">
                      
            <a href="{{ route('employers.edit', isset($employers->id)?$employers->id:0) }}" title="Edit this Employer" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a> 

  
        <span class="pull-right">
          <div class="right-wrapper">
   
                <div class="cv-and-create right-wrapper-info">
                    <a href="{{ route('employers.create') }}" data-toggle="tooltip" data-placement="right" title="Employer Create!"  type="button" class="btn btn-sm btn-success">
                    <i class="fa fa-user-plus" aria-hidden="true"></i>   </a>
                </div> 
          </div>
        </span>
</div>
</span>



            <div class="panel-heading">
              <h3 class="panel-title">
               
               {{isset($employers->employer_name)?$employers->employer_name:null}}</h3>
            </div>
            <div class="panel-body">
                <div class=" col-md-12 col-lg-12 "> 
                  <table class="table table-user-information">
                    <tbody>
                       <tr style="width: 100%">
                        <td style="width: 30%">ID:</td>
                        <td>        {{isset($employers->emp_id)?$employers->emp_id:null}}</td>
                        </tr>
                        <tr>
                        <td>Email Address</td>
                        <td>   {{isset($employers->email_address)?$employers->email_address:null}}</td>
                        </tr>
                        <tr>
                        <td>Address</td>
                        <td>    {{isset($employers->address)?$employers->address:null}}</td>
                        </tr>
                        <tr>
                        <tr>
                        <td>Zone</td>
                        <td>   {{isset($employers->zone)?$employers->zone:null}}</td>
                        </tr>
                        <tr>
                        <td>Authorized person</td>
                        <td> {{isset($employers->authorized_person)?$employers->authorized_person:null}}</td>
                        </tr>
                        <tr>
                        <td>Status</td>
                        <td>
                          

<div class="row">
  <div class="col-md-4">

    {{isset($employers->mystatus)?$employers->mystatus['description']:null}}
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Change Status</h4>
        </div>
        <div class="modal-body">
         
       
       <form action="{{ route('employers.updatestatus', isset($employers->id)?$employers->id:0) }}" method="POST">
            {{ csrf_field() }}

              <div class="form-group">
                  <label for="title"> Choose Status:</label>
                  <?php $status = \App\Models\Employerstatus::all(); ?>
                  <select id="val_select" name="status" class="form-control select2">
                    @if(isset($employers->status))
                      <option value="{{ $employers->status }}">
                        {{ isset($employers->mystatus['description'])?$employers->mystatus['description']:null }}
                      </option>
                    @endif
                      @foreach($status as $row)
                          <option value="{{ $row->id }}">
                              {{ $row->description }} 
                          </option>
                      @endforeach
                  </select>
              </div>
            <div class="form-group hide" id="date-input">
          
                <div class='input-group date' id='datetimepicker4'>
                      <input class="form-control" type="text" value="" name="follow_up_time" id="time1"/>
                      <span class="input-group-addon">
                         <span class="glyphicon glyphicon-time"></span>
                      </span>
                </div>        
                     
</div>


<script>
    $('#time1').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
    });
</script>  


          <div class="form-group"> 
               <form action="{{ route('employers.updatestatus', isset($employers->id)?$employers->id:0) }}">
                       {{ csrf_field() }}
                       {{ method_field("patch") }}
                  <button type="submit" class="btn btn-primary">Update Status</button>
                </form>
          </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      </div>
    </div>
  </div>

                        </td>
                      </tr>
                        <td>Phone Number</td>
                        <td>
                            {{isset($employers->phone_number)?$employers->phone_number:null}}
                        </td>                           
                      </tr>
                     
                    </tbody>
                  </table>
                  
                  
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <table class="table table-bordered table-responsive">
           <h3><b>Job Positions (Contracted + Not Contracted)</b></h3>
      <thead>
        <tr style="background-color: #99ccff">            
            <th>Action</th>
            <th>Job Title Actual</th>
            <th>Jobseeker Name</th>
            <th>Not Contracted or Contracted</th>
            <th>Post Date</th>
            <th>Deadline</th>
            <th>Status</th>
            <th>Job Title Key</th>
            <th>Gender</th>
            <th>Require Nationality</th>
            <th>Require Quantity</th>
        
      </thead>
      <tbody>
        @if(isset($employers->jobpositions))
          @foreach($employers->jobpositions as $job)
          <tr class="success">
            <td>  

            <a href="{{ route('jobposition.edit', $job->job_id) }}" style="display: inline-block;" class="btn btn-warning btn-xs action-item" target="_blank"><i class="glyphicon glyphicon-edit"></i></a>

            <div class="dropdown action-item action-class-two right-wrapper-info">
            <button class="btn btn-primary dropdown-toggle btn-xs" type="button" data-toggle="dropdown"><i class="glyphicon glyphicon-envelope"></i> 
            <span class="caret"></span></button>
            
            <ul class="dropdown-menu">
      <li> <a href="{{ route('employers.send', $employers->emp_id) }}" class="btn btn-success btn-xs " target="_blank">HR Consulting Firm Introduction</a></li>
      <li> <a href="{{ route('employers.send2', $employers->emp_id) }}" class="btn btn-primary btn-xs" target="_blank">Template two</a></li>
      <li> <a href="{{ route('employers.send3', $employers->emp_id) }}" class="btn btn-warning btn-xs" target="_blank"> Interview Confirmation</a></li>
          </ul>
          </div></td>
            <td><a href="/jobposition/{{$job->job_id}}">{{$job->job_title}}</a></td>
            <td><?php $jobseeker = \App\Models\Jobposition::findOrFail($job->job_id);?>
              <?php 

              // var_dump($jobseeker);
              foreach ($job->My_jobseekers as $key => $value) {
               // var_dump($value->fullname);
                    echo  "<a href='/jobseekers/show/$value->jobseeker_id'>$value->fullname </a>";
                  echo ",";
              }

              ?>
            </td>
            <td> @if($job->free_or_paid == 1) Contracted Job @else Not Contracted Job @endif</td>
            <td> {{$job->post_date}}</td>
            <td> {{$job->deadline}}</td>
            <td> @if($job->status == 'open' || $job->status == 1)
                  Open
                  @else
                  Not-open
                  @endif
            </td>
            <td> {{ \App\Models\JobTitle::where('job_title_key',$job->require_position)->pluck('description')->first() }} </td>
            <td> @if($job->gender == 1) Male @elseif($job->gender == 2) Female @else @endif</td>
            <td> {{$job->req_nationality}}</td>
            <td> {{$job->req_quantity}}</td>
        </tr>
          @endforeach
        @endif
        </tbody>
       </table>  
      </div>
    </div> 
</div></div>
     <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <table class="table table-bordered table-responsive">
           <h3><b>Recommend Job Seekers List</b></h3>
            <thead>
              <tr style="background-color: #99ccff">            
                  <th>Action</th>
                  <th>Id</th>
                  <th> Status</th>
                  <th>Interview Time</th>
                  <th>Interview Date</th>
                  <th>Fullname</th>
                  <th>Facebook Name</th>
                  <th>Phone Number  </th>
                  <th>Type of Call</th>
                  <th>Call Reason</th>
                  <th>Township</th>
                  <th> Remark Call Log</th>
                  <th> Caller/Receiver</th>
            </thead>
      <tbody>
        @if(isset($employers->jobseekers))
        @foreach($employers->jobseekers as $job)
        <tr class="success">
            <td></td>
            <td> {{$job->id}}</td>
            <td> {{ \App\Models\Status::where('id',$job->status)->pluck('description')->first() }} </td>
            <td>{{Carbon\Carbon::parse($job->interview_time)->format('g:i a')}}</td>
            <td> {{Carbon\Carbon::parse($job->interview_time)->format('j-M-y')}}</td>
            <td> {{$job->fullname}}</td>
            <td> {{$job->fb_name}}</td>
            <td> {{$job->phone}}</td>
            <td>{{ \App\Models\Calllog::orderBy('id','desc')->where('jobseeker_id',$job->id)->pluck('type_of_call')->first() }}</td>
            <td> 
            {{ \App\Models\Callreason::where('id',\App\Models\Calllog::orderBy('id','desc')->where('jobseeker_id',$job->id)->pluck('call_reason')->first() )->pluck('description')->first() }}

            </td>
            <td>   {{ \App\Models\Mytownship::where('id',$job->township)->pluck('mytownship_name')->first() }}</td>
            <td>  {{ \App\Models\Calllog::orderBy('id','desc')->where('jobseeker_id',$job->id)->pluck('remark_calllog')->first() }}</td>
            <td> </td>   
        </tr>
        @endforeach
        @endif
        </tbody>
       </table>  
      </div>
    </div>


<hr style="border-style: groove">
         @if(isset($employers->employercalllogs) && !$employers->employercalllogs->count()) 
      
          <h1> <b>There is no Calllog</b></h1>
          <hr style="border-style: groove">
     @else
     <div class="container-fluid">

   <table class="table table-bordered table-responsive">
   <h3><b>Call Log History</b></h3>
      <thead>
        <tr style="background-color: #99ccff">            
            <th>Call Back Date</th>
            <th>Type of Call</th>
            <th>Start Time</th>
            <th>Duration(minute)</th>
            <th>Phone Number  </th>
            <th>Receiver</th>
            <th> Call Reason</th>
            <th> Status</th>
            <th> Follow up time</th>
            <th> Remark</th>
            <th> Caller/Receiver</th>
      </thead>
      <tbody>
        @if(isset($employers->employercalllogs))
        @foreach($employers->employercalllogs as $calllog)
        <tr class="success">
            <td>{{ Carbon\Carbon::parse($calllog['call_back_date'])->format('j-M-y') }} </td> 
            <td> {{$calllog['type_of_call']}}</td>
            <td> {{$calllog['created_at']}}</td>
            <td> {{$calllog['minute']}}</td> 
            <td> {{$calllog->employer['phone_number']}}</td>
            <td> {{$calllog['receiver']}}</td>
            <td> {{$calllog['call_reason']}}</td>
            <td> {{$calllog->mystatus['description']}}</td>
            <td> {{ Carbon\Carbon::parse($calllog->follow_up_time)->format('j-F-Y H:m') }}</td>
            <td> {{$calllog['remark']}}</td>
            <td> {{$calllog->author['name']}}</td>
        </tr>
        @endforeach
        @endif
      </tbody>
    </table>  
       @endif  
     </div>



<div class="container-fluid"> 
<hr style="border-style: groove;">
<div class="col-md-12"> 

<h3><b>Add Call Log</b></h3>
<br>
<br>
 {!! Form::open(['route' => 'employercalllogs.store', 'method' => 'post']) !!}
            {!! Form::hidden('employer_id', isset($employers->id)?$employers->id:null) !!}


                  <div class="form-group">
                    <label>Type of call</label>
                       <select class="form-control select2" name="type_of_call">
                          <option value="{{ request()->input('age')}}">Choose Type of Call</option>
                          <option value="Incoming Call">Incoming Call</option>
                          <option value="Outgoing Call">Outgoing Call</option>                              
                       </select>
                    </div>

            <div class="form-group">
            {!! Form::label('Minute') !!}
            {!! Form::text('minute', null, 
                array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'Minute')) !!}
             </div>
              
            <div class="form-group">
                  {!! Form::label('Phone Number') !!}
                {!! Form::textarea('ph_number', null, ['class' => 'form-control', 'placeholder' => __('Phone Number'), 'rows' => '1']) !!}

            </div>
          
            <div class="form-group">
                  {!! Form::label('Industrial Zone') !!}
                {!! Form::textarea('industry_zone', null, ['class' => 'form-control', 'placeholder' => __('Industrial Zone'), 'rows' => '1']) !!}

            </div>            
             <div class="form-group">
                  {!! Form::label('Receiver') !!}
                {!! Form::textarea('receiver', null, ['class' => 'form-control', 'placeholder' => __('Receiver'), 'rows' => '1']) !!}
            </div>              
             <div class="form-group">
               {!! Form::label('Choose Call Reason') !!}
                <?php $call_reason = \App\Models\Employercallreason::all(); ?>
                <select id="val_select" name="call_reason" class="form-control select2">
                  <option value="{{ request()->input('call_reason')}}">{{ request()->input('call_reason')}}</option>
                  @foreach($call_reason as $row)
                  <option value="{{ $row->id }}" @if(old('call_reason') == $row->id) selected @endif> {{ $row->description }} </option>
                  @endforeach                 
                </select>
            </div>
                        
<!--             <div class="form-group">
             {!! Form::label('Call Reason') !!}
                {!! Form::textarea('call_reason', null, ['class' => 'form-control', 'placeholder' => __('Call Reason'), 'rows' => '3']) !!}

            </div> 
 -->

           <div class="form-group">
               {!! Form::label('Choose Status') !!}
                <?php $description = \App\Models\Employerstatus::all(); ?>
                <select id="val_select" name="description" class="form-control select2">
                  <option value="{{ request()->input('description')}}">{{ request()->input('description')}}</option>
                  @foreach($description as $row)
                  <option value="{{ $row->id }}" @if(old('description') == $row->id) selected @endif> {{ $row->description }} </option>
                  @endforeach                 
                </select>
            </div>
            <div class="form-group hide" id="date-input-calllog">
           <!--  {!! Form::label('Hour') !!}
            {!! Form::text('interview_time_hour', null, 
                array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'2 pm')) !!}
           
     
            {!! Form::label('Interview Time') !!}
            {!! Form::date('interview_time', null, 
                array('null', 
                      'class'=>'form-control', 
                      'placeholder'=>'Interview Time')) !!} -->
                      <div class='input-group date' id='datetimepicker4'>
                      <input class="form-control" type="text" value="" name="follow_up_time" id="time"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div>        
                     

</div>

    <script>
    $('#time').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
    });
</script>     
            <div class="form-group">
            {!! Form::label('Call Back Date') !!}
            {!! Form::date('call_back_date', null, 
                array('null', 
                      'class'=>'form-control', 
                      'placeholder'=>'Call BackDate')) !!}
             </div>

           <div class="form-group">
                  {!! Form::label('Meeting') !!}
                {!! Form::textarea('meeting', null, ['class' => 'form-control', 'placeholder' => __('Meeting'), 'rows' => '1']) !!}

            </div>

            <div class="form-group">
             {!! Form::label('Remark') !!}
                {!! Form::textarea('remark', null, ['class' => 'form-control', 'placeholder' => __('Remark'), 'rows' => '3']) !!}
            </div> 

            <div class="form-group">                    
                        <select class="form-control select2" name="email_sent">
                          <option value="{{ request()->input('email_sent')}}">Choose Email Sent? </option>
                          <option value="Yes">Yes</option>
                          <option value="No">No</option>                              
                       </select>
             </div>  

            {!! Form::submit(__('Create'), ['class' => 'btn btn-primary pull-right']) !!}
            {!! Form::close() !!}

    </div>
  </div>
</div>



  </div>
<script type="text/javascript">
    $(function () {
        $('#datetimepicker6').datetimepicker();
        $('#datetimepicker7').datetimepicker({
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });
    });
</script>
<script type="text/javascript">
  $(function () {
    $('select[name="status"]').change(function () {
        //remove the hide class from date-input
        if ($(this).find(":selected").val() == '12' || $(this).find(":selected").val() == '7'   ) {
            $('#date-input').removeClass('hide')
        }
    })
})
  $(function () {
    $('select[name="description"]').change(function () {
        //remove the hide class from date-input
        if ($(this).find(":selected").val() == '12' || $(this).find(":selected").val() == '7') {
            $('#date-input-calllog').removeClass('hide')
        }
    })
})

</script>
@stop