@extends('layouts.dashboard')
@section('page_heading','Call Log Create')

@section('section')


<br>
<br>
<br>

<div class="row">
              @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div>
                @endif
  <div class="col-md-6">
		
    <form action="{{ route('employers.store') }}" method="POST" enctype="multipart/form-data">
      {{ csrf_field() }}
       <div class="form-group">
            <label for="title">Employer ID:</label>
            <input type="text" readonly name="employer_id" id="employer_id" class="form-control">
           </div>    
            <div class="form-group">
              {!! Form::label('Employer Name') !!}
              {!! Form::text('employer_name', null, 
                array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'Employer Name')) !!}
            </div>

            <div class="form-group">
              {!! Form::label('Phone Number') !!}
              {!! Form::text('phone_number', null, 
                  array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'099999')) !!}
             </div>
            
             {!! Form::file('emp_image', array('class' => 'form-control')) !!}
            <div class="form-group">
              {!! Form::label('Company Type') !!}
              {!! Form::text('company_type', null, 
                  array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'Company Type')) !!}
            </div>           
            
            <div class="form-group">
              {!! Form::label('Notes') !!}
              {!! Form::text('notes', null, 
                  array(
                      'class'=>'form-control', 
                      'placeholder'=>'Notes')) !!}
             </div>

             <div class="form-group">
               {!! Form::label('Choose Status') !!}
                <?php $description = \App\Models\Employerstatus::all(); ?>
                <select id="val_select" name="description" class="form-control select2">
                  <option value="{{ request()->input('description')}}">{{ request()->input('description')}}</option>
                  @foreach($description as $row)
                  <option value="{{ $row->id }}" @if(old('description') == $row->id) selected @endif> {{ $row->description }} </option>
                  @endforeach                 
                </select>
             </div>
                               

             <div class="form-group">
              {!! Form::label('Address') !!}
              {!! Form::text('address', null, 
                  array( 
                      'class'=>'form-control', 
                      'placeholder'=>'Address')) !!}
             </div>            
            
             <div class="form-group">
                {!! Form::label('Zone') !!}
                {!! Form::textarea('zone', null, ['class' => 'form-control', 'placeholder' => __('Zone'), 'rows' => '1']) !!}
             </div>

             <div class="form-group">
                {!! Form::label('Email Address') !!}
                {!! Form::email('email_address', null, 
                    array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'Email')) !!}
             </div>           
             
             <div class="form-group">
              {!! Form::label('Authorized Person') !!}
              {!! Form::text('authorized_person', null, 
                  array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'Authorized Person')) !!}
             </div>             
             
             <div class="form-group">
              {!! Form::label('Latitude') !!}
              {!! Form::text('latitude', null, 
                  array(
                      'class'=>'form-control', 
                      'placeholder'=>'Latitude')) !!}
             </div>               
             
             <div class="form-group">
              {!! Form::label('Number of Call') !!}
              {!! Form::text('number_of_call', null, 
                  array( 
                      'class'=>'form-control', 
                      'placeholder'=>'Number of Call')) !!}
             </div>            
             
             <div class="form-group">
              {!! Form::label('Interest') !!}
              {!! Form::text('interest', null, 
                  array(
                      'class'=>'form-control', 
                      'placeholder'=>'Interest')) !!}
             </div>              
             
             <div class="form-group">
              {!! Form::label('Understanding') !!}
              {!! Form::text('understanding', null, 
                  array(
                        'class'=>'form-control', 
                      'placeholder'=>'Understanding')) !!}
             </div>
             
             <div class="form-group">
                {!! Form::label('Remark') !!}
                {!! Form::textarea('remark', null, ['class' => 'form-control', 'placeholder' =>('Notes'), 'rows' => '3']) !!}
             </div>
        
     
      <div class="form-group"> 
				<button type="submit" class="btn btn-success">Create Employer</button>
			</div>
		</form>


  </div>

@stop