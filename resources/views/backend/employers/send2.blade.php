@extends('layouts.dashboard')
@section('page_heading','Email Section')

@section('section')
<br>
<br>
<br>
<script type="text/javascript" src="{{ asset('/js/tinymce/tinymce.min.js') }}"></script>
<script type="text/javascript">
  tinymce.init({
    selector : "textarea",
    plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste"],
    toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
  }); 
</script>
<div class="container">


    <div class="row">

                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div>
                @endif

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('sending') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-2 control-label">Email</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{$jobpositions->employer['email_address']}}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
                            <label for="subject" class="col-md-2 control-label">Subject</label>

                            <div class="col-md-6">
                                <input id="subject" type="subject" class="form-control" name="subject" value="CVs for {{$jobpositions->Myrequire_position_1['desire_position_1_name']}} Position" required autofocus>

                                @if ($errors->has('subject'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('subject') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                            <label for="body" class="col-md-2 control-label">Message</label>

                            <div class="col-md-6">
                                <textarea name="body" id="body" class="form-control" required>
            
                                Dear {{$jobpositions->employer['employer_name']}}, <br><br>

                                Good Day! 
                                <br>
                                I have attached the CVs of the potential candidates for  {{$jobpositions->Myrequire_position_1['desire_position_1_name']}} position. Can you please review them and please let me know whom and when you would like to have an interview?
                                <br><br>
                                <strong><i>Thanks & Best Regards,</i></strong><br>
                                <p><img src="http://foodfaremyanmar.com/emaillogo.png"><br></p><br>

                                <i>{{Auth::user()->name}}</i><br>
                                <i>{{Auth::user()->position}}</i><br>
                                Email: <i>{{Auth::user()->email}}</i><br>
                                  Office Ph No: +95 (0) 9762378849, (0) 9977665704

                                <br>Contact No +959975905173
                                </textarea >

                                @if ($errors->has('body'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('body') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-2 control-label">File</label>

                            <div class="col-md-6">
                                <label for="file" class="btn btn-success">Choose File</label><br>
                                <input id="file" style="display: none" type="file" class="form-control"required><br>
                                <div id="render"></div>
                            </div>
                        </div>

                       <div class="form-group">
                            <div class="col-md-3 control-label">
                                <button type="submit" class="btn btn-primary">
                               Send
                                </button>
                            </div>
                        </div>
                    </form>
    </div>
</div>
<script type="text/javascript">
        $(document).ready(function(event) {

          $("input#file").on('change',function(event){ 
              var data = this;
              var image_holder = $("#render");   
              return imageCircle(data,image_holder);
              
          });
          
          // image live time showing function  
          function imageCircle(data,image_holder){
            
            var imgPath = data.value;
            var countFiles = data.files.length;   
            var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
            var file_name = data.files[0].name;
            if (typeof(FileReader) != "undefined") {
                  //loop for each file selected for uploaded.
                  for (var i = 0; i < countFiles; i++)
                  {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                      var img = e.target.result;          
                      $("<img />", {
                        "src": e.target.result,
                        "style":"padding:5px;height:80px;width:80px"               
                      }).appendTo(image_holder);
                      $('<input type="hidden" name="files[]" value="'+img+'">').appendTo(image_holder);
                    }
                    image_holder.show(); 
                    reader.readAsDataURL(data.files[i]);
                  }
                } else {
                alert("This browser does not support FileReader.");
                }
            }
        });
    </script> 

@endsection
