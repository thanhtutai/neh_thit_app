@extends('layouts.dashboard')
@section('page_heading','Email Section')

@section('section')
<br>
<br>
<br>
<script type="text/javascript" src="{{ asset('/js/tinymce/tinymce.min.js') }}"></script>
<script type="text/javascript">
  tinymce.init({
    selector : "textarea",
    plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste"],
    toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
  }); 
</script>
<div class="container">

    <div class="row">

                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div>
                @endif

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('sending') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-2 control-label">Email</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $employers->email_address }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
                            <label for="subject" class="col-md-2 control-label">Subject</label>

                            <div class="col-md-6">
                                <input id="subject" type="subject" class="form-control" name="subject" value="Hello, {{ $employers->employer_name }}" required autofocus>

                                @if ($errors->has('subject'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('subject') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                            <label for="body" class="col-md-2 control-label">Message</label>

                            <div class="col-md-6">
                                <textarea name="body" id="body" class="form-control" required>
                                  <!--  Hi,<b> {{ $employers->employer_name }} </b> -->
                                 မဂၤလာပါ <br>  
ေန႔သစ္ company မွစကားေျပာခြင့္ရတ့ဲအတြက္ အရမ္း၀မ္းသာပါတယ္။ ေနာက္တစ္ပတ္ အဂၤါေန႔မွာ appointment အတြက္ဖုန္းေခၚဆိုလိုက္ပါမယ္ရွင္။ ေန႔သစ္အေၾကာင္းကိုု Emailကေန အရင္ မိတ္ဆက္ခ်င္ပါတယ္။<br>
 <br>
ေန႕သစ္ဟာ မိုဘိုင္းဖုန္းကိုအေျခခံျပီး စက္မႈဇုန္မ်ားႏွင့္ စက္ရုံအလုပ္သမားမ်ားကို အဓိကထား ဝန္ေဆာင္မႈေပးေနေသာ အလုပ္သမားရွာေဖြေရး ပလက္ေဖာင္းတခု ျဖစ္ပါသည္။
လူႀကီးမင္းတို႔၏ ကုမၸဏီမ်ားအတြက္ လိုအပ္တဲ့ အေျခခံဝန္ထမ္းမ်ားကို  နည္းပညာ အသံုးျပဳၿပီး အျခားသမာ႐ိုးက် ေအဂ်င္စီမ်ားထက္ ပိုမိုလွ်င္ျမန္စြာ ရွာေဖြေပးလွ်က္ရွိပါတယ္။
ဝန္ေဆာင္ခကို တစ္ႀကိမ္တည္း အျပတ္ေပးစရာမလိုဘဲ  ဝန္ထမ္းမ်ား မိမိလုပ္ငန္းမွာ ေနမွသာ ၆လအတြင္း လစဥ္ဝန္ေဆာင္ခကို ခြဲေပးရမွာ ျဖစ္ပါတဲ့အတြက္ ဝန္ထမ္းအဝင္အထြက္ မ်ားတဲ့ လုုပ္ငန္းမ်ားအေနနဲ႔ ဝန္ေဆာင္ခအမ်ားႀကီး ေလ်ာ့႔ခ်နိုုင္မွာ ျဖစ္ပါတယ္။ <br> 
 <br>
<strong>ကၽြန္မတို႔၏ အလုပ္ရွာေဖြျခင္း နည္းလမ္းမ်ားကို အသံုးျပဳျခင္းျဖင့္ သင္လုပ္ေဆာင္နိုင္သည္မွာ</strong>
<li>ျမန္ဆန္စြာ ၀န္ထမ္းရွာေဖြႏိုင္ျခင္း။</li>
<li>ပိုမိုေကာင္းမြန္ေသာ ၀န္ထမ္းမ်ားႏွင့္ ခ်ိတ္ဆက္နိုင္ျခင္း။</li>
<li>HR ပိုင္း၀န္ထမး္ရွာေဖြေရး ကုန္က်စရိတ္မ်ားကို ေလွ်ာ့ခ်နိုုုုင္ျခင္း။</li>
<li>၀န္ထမ္းမ်ားကို ေရရွည္ျမဲေအာင္ ထိန္းသိမ္းႏိုင္ျခင္း။</li>
<li>ကုန္က်စရိတ္နည္းပါးေသာ Trainingမ်ား ေထာက္ပံ့ေပးႏိုင္ျခင္း။</li>
  <br>
<strong>ေန႔သစ္၏ ၀န္ထမ္းရွာေဖြျခင္း စနစ္</strong>
<li>ရိုးရွင္း၊ လြယ္ကူစြာ စာရင္းသြင္း ျဖည့္စြက္ႏိုင္ျခင္း။
 </li>
<li> သံုုးစြဲသူ/အလုုပ္အကိုုင္ရွာေဖြသူေပါင္း ၁၀၀၀၀ ေက်ာ္ စနစ္အတြင္းရွိျခင္း။</li>
<li>
နည္းပညာအသံုးျပဳသည္႔အတြက္ အခ်ိန္နွင့္တေျပးညီ ၀န္ထမ္း လိုုအပ္ခ်က္မ်ားကိုု ျဖည့္ေပးနိုုင္ျခင္း။ </li>
<li> 
အျခားအလုုပ္သမား ရွာသည့္ နည္းလမ္းမ်ားထက္ ၅၀%အထိ ပိုုမိုုေစ်းသက္သာျခင္း။</li>

 <br>
<strong>ေန႔သစ္၏ ရည္မွန္းခ်က္</strong><br><br>

အလုပ္ရွင္မ်ားအတြက္ ေကာင္းမြန္စိတ္ခ်ရေသာ၀န္ထမ္းမ်ားကိုု လ်ွင္ျမန္စြာ ရရွိေစရန္ႏွင့္ အလုပ္ရွာသူမ်ားအတြက္ အလုပ္ေကာင္း မ်ားရရွိနိုင္ရန္ ရည္ရြယ္ပါသည္။
<br>

သင့္္လုုပ္ငန္းမွာ လိုုအပ္ေနတဲ့ ၀န္ထမ္းေတြကို အျမန္ဆံုုးျဖည့္ဖိုု့ ကၽြန္မတို႔ႏွင့္ ဆက္သြယ္လိုက္ပါ။
<br><br>
<strong><i>Thanks & Best Regards,</i></strong><br>
<p><img src="http://foodfaremyanmar.com/emaillogo.png"><br></p>

<i>{{Auth::user()->name}}</i><br>
<i>{{Auth::user()->position}}</i><br>
Email: <i>{{Auth::user()->email}}</i><br>
  Office Ph No: +95 (0) 9762378849, (0) 9977665704

<br>Contact No +959975905173
                                </textarea >

                                @if ($errors->has('body'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('body') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-2 control-label">File</label>

                            <div class="col-md-6">
                                <label for="file" class="btn btn-success">Choose File</label><br>
                                <input id="file" value="/emaillogo.png" style="display: none" type="file" class="form-control"><br>
                                <div id="render"></div>
                            </div>
                        </div>

                       <div class="form-group">
                            <div class="col-md-3 control-label">
                                <button type="submit" class="btn btn-primary">
                               Send
                                </button>
                            </div>
                        </div>
                    </form>
    </div>
</div>
<script type="text/javascript">
        $(document).ready(function(event) {

          $("input#file").on('change',function(event){ 
              var data = this;
              var image_holder = $("#render");   
              return imageCircle(data,image_holder);
              
          });
          
          // image live time showing function  
          function imageCircle(data,image_holder){
            
            var imgPath = data.value;
            var countFiles = data.files.length;   
            var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
            var file_name = data.files[0].name;
            if (typeof(FileReader) != "undefined") {
                  //loop for each file selected for uploaded.
                  for (var i = 0; i < countFiles; i++)
                  {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                      var img = e.target.result;          
                      $("<img />", {
                        "src": e.target.result,
                        "style":"padding:5px;height:80px;width:80px"               
                      }).appendTo(image_holder);
                      $('<input type="hidden" name="files[]" value="'+img+'">').appendTo(image_holder);
                    }
                    image_holder.show(); 
                    reader.readAsDataURL(data.files[i]);
                  }
                } else {
                alert("This browser does not support FileReader.");
                }
            }
        });
    </script> 

@endsection
