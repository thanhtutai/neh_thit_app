@extends('layouts.dashboard')

@section('section')
<br>
<br>
<br>

<div class="row">              
              @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div>
                @endif
			  <div class="col-md-6">
			    <form action="{{ route('status.store')}}" method="POST">
			      {{ csrf_field() }}

				      <div class="form-group">
				        <label for="title">Status:</label>
				        <input type="text" name="description" id="title" class="form-control" value="">
				          
				      </div>


                                  
                      <div class="form-group pull-right">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                                
                            </div>
                        </div>
                          </form>         
        
      </div> 
     

  <div class="col-md-6">

    <table class="table table-bordered">
      <thead>
        <tr>
     
                        <th>Action</th>
                        <th>ID</th>
                        <th>Status</th>
  

      </thead>
      <tbody>
             @foreach($status as $mystatus )
        <tr class="success">
            
              <td>  <a href="{{ route('status.edit', $mystatus->id) }}" class="btn btn-success btn-xs">Edit</a></td>
              <td>{{ $mystatus->id }}</td>
              <td>{{ $mystatus->description }}</td>   
        </tr>

         @endforeach
      </tbody>
    </table>  
          <nav>
       <ul class="pagination">
       <li><?php echo $status->render();?></li>
       </ul>
     </nav>
</div>
 
</div>

 </div>

  @stop