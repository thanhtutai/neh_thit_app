@extends('layouts.dashboard')


@section('section')
<style type="text/css">
  .search-fix {
  overflow: hidden;

  position: fixed;
  top: 60px;
  width: 100%;


}
.user{
  width: 100px;
}
</style>
<br><br><br>

<div class="row">

  <div class="col-md-12">
    <center><h2 class="" style="display: inline-block;">Jobseeker Calls </h2></center>

  </div>

   @if(!empty($js_id) || !empty($fullname))
        <div class="col-md-12 alert alert-success alert-block search-fix">
          @isset($js_id)

          ID : <b>{{$js_id}} </b>
          @endisset
          @isset($fullname)
          Full Name : <b>{{$fullname}} </b>
          @endisset

          @isset($call_reason)
          Call Reason : <b>{{$call_reason}} </b>
          @endisset
          
          @isset($remark_calllog)
          Remark : <b>{{$remark_calllog}} </b>
          @endisset

          @isset($type_of_call)
          Type of Call : <b>{{$type_of_call}} </b>
          @endisset

          @isset($caller_receiver)
          Caller Receiver : <b>{{$caller_receiver}} </b>
          @endisset        

          @isset($status)
          Status : <b>{{$status}} </b>
          @endisset

          @isset($call_back_date)
          Call Back Date : <b>{{$call_back_date}} </b>
          @endisset
         
          @isset($created_at)
          Created Date : <b>{{$created_at}} </b>
          @endisset

    @if ($message = Session::get('success'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
      </div>
    @endif

 </div>
 @endif
</div>

    <a href="{{ route('checklists-js.create') }}" class="btn btn-primary pull-right">Create Report</a>
    <a href="{{ URL::to('downloadExcelCLJS/xls') }}">
      <button class="btn btn-success pull-right">Excel xls</button>
    </a>
    <a href="{{ URL::to('downloadExcelCLJS/csv') }}"><button class="btn btn-success pull-right"> CSV</button></a>
    <button class="btn btn-default for-hide-search-form">Show Search Form</button>

<div class="row">
  <div class="container-fluid">
      <div class="row">
            <div class="col-md-12">

                <div class="box box-body">
                    {!! Form::open(['url'=>'/checklists-js','method'=>'GET', 'class'=>'form', 'id'=>'search_data']) !!}
      
            
                    <div class="form-group col-md-3">
                        {!! Form::label('User') !!}
                            <?php $authors = \App\User::all(); ?>
                          <select id="val_select" name="user" class=form-control select2">
                             <option value="{{ request()->input('user')}}">
                              <?php $label =request()->input('user');
                              if(isset($label))
                                echo $label;
                              else
                                echo "User";
                             ?></option>
                                 @foreach($authors as $row)
                                        <option value="{{ $row->name }}" @if(old('user') == $row->name) selected @endif> {{ $row->name }} </option>
                                  @endforeach
                              </select>
                    </div>
                  <div class="form-group col-md-3">
                  {!! Form::label('Status') !!}
                 <select id="val_select_1" name="status" class=form-control select2">
                    <option value="applied">Applied</option>
                    <option value="schedule">Scheduled Interview</option>
                    <option value="interview">Interviewed</option>
                    <option value="got_job">Got Job</option>
                    <option value="job_browsed">Job Browsed</option>
                    <option value="followed_up">Followed Up</option>
                  </select>                   
                  </div>

                    <div class="form-group col-md-3">
                     {!! Form::label('From') !!}
                     {!! Form::date('from_created_at', null, 
                        array('null', 
                              'class'=>'form-control', 
                              'placeholder'=>'Created Date')) !!}
                     </div>    

                    <div class="form-group col-md-3">
                     {!! Form::label('To') !!}
                     {!! Form::date('to_created_at', null, 
                        array('null', 
                              'class'=>'form-control', 
                              'placeholder'=>'Created Date')) !!}
                     </div>
                       
                        <div class="form-group col-md-3">
                            <a href="{{url('checklists-js')}}" class="btn btn-flat btn-danger">Reset</a>
                            <button class="btn btn-flat btn-primary">Search</button>
                        </div>
                      
                      {!! Form::close() !!}
                     
                     </div>
              </div>

  </div>
    </div>


<div class="container-fluid">
  <!-- end search -->
  <div class="sticky-table sticky-headers sticky-ltr-cells">
    <table class="table table-striped table-striped">
      <thead>
        <!-- <tr class="sticky-row"> -->
         <tr class="sticky-row " style="background-color: #99ccff">
          <th class="sticky-cell user">ID</th>  
          <th class="sticky-cell user">User</th>                         
          <th class="user">Date</th> 
          <th class="user">Applied</th>
          <th class="user">Scheduled Interviewed</th>
          <th class="user">Interviewed</th>
          <th class="user">Got Job</th>    
          <th class="user">Job Browsed </th>   
          <th class="user">Followed Up</th> 
          <th>Total number of calls</th>      
        </tr>

      </thead>
      <tbody>
         @foreach($checklistjs as $checklist )
       <tr class="" style="background-color:#cce6ff;">
         <!--  <td class="sticky-cell">Ford</td> -->
              <td>{{ $checklist->id}}</td>
          <td>{{ $checklist->author['name']}}</td>
          <td>{{ $checklist->date }}</td>
          <td>{{ $checklist->applied }}</td>
          <td>{{ $checklist->scheduled_interview }}</td>
          <td>{{ $checklist->interviewed }}</td>
          <td>{{ $checklist->got_job }}</td>
          <td>{{ $checklist->job_browsed   }}</td>
          <td>{{ $checklist->followed_up   }}</td>
          <td>{{ $checklist->total_number_of_calls   }}</td>


      </tr>
        @endforeach
        </tbody>
       </table> 
         <nav>
       <ul class="pagination">
       <li><?php //  echo $jobseekers->render(); ?></li>
       </ul>
     </nav>

  
  </div>
</div>
</div>
<style type="text/css">
  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
width: 1000px;
}
.sticky-table table {
  width: 1300px;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

th.last-called-date{
  width: 100px;
}
th.call-back-date{
  width: 100px;
}
</style>

@stop