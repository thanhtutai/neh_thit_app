@extends('layouts.dashboard')
@section('section')

<br>
<br>
<br>
<br>
<br>
<br>

<div class="box box-body">
                    {!! Form::open(['url'=>'/calllogs','method'=>'GET', 'class'=>'form', 'id'=>'search_data']) !!}
      
                       <div class="form-group col-md-6"> 
                            <input type="text" name="js_id" placeholder="ID" value="{{ request()->input('js_id')}}" class="form-control"/>
                        </div>
                        
                        <div class="form-group col-md-6">    
                          <input type="text" name="fullname" placeholder="Full Name" value="{{ request()->input('fullname')}}" class="form-control"/>
                        </div>

                        <div class="form-group col-md-6">    
                            <input type="text" name="call_reason" placeholder="Call Reason" value="{{ request()->input('call_reason')}}" class="form-control"/>
                        </div>

                        <div class="form-group col-md-6">    
                            <input type="text" name="remark_calllog" placeholder="Remark" value="{{ request()->input('remark_calllog')}}" class="form-control"/>
                        </div>

                        <div class="form-group col-md-6">
                          {!! Form::label('Type of Call') !!}
                          <select class="form-control select2" name="type_of_call">
                            <option value="{{ request()->input('type_of_call')}}">{{ request()->input('type_of_call')}}</option>
                            <option value="Incoming Call">Incoming Call</option>
                            <option value="Outgoing Call">Outgoing Call</option>                              
                          </select>
                        </div>

                       <div class="form-group col-md-6">
                        {!! Form::label('Caller Receiver') !!}
                            <?php $authors = \App\User::all(); ?>
                            <select id="val_select" name="caller_receiver" class="form-control select2">
                                <option value="{{ request()->input('caller_receiver')}}">{{ request()->input('caller_receiver')}}</option>
                                @foreach($authors as $row)
                                <option value="{{ $row->name }}" @if(old('caller_receiver') == $row->name) selected @endif> {{ $row->name }} </option>
                                @endforeach
                             </select>
                        </div>

                    <div class="form-group col-md-6">
                    {!! Form::label('Call back Date') !!}
                    {!! Form::date('call_back_date', null, 
                        array('null', 
                              'class'=>'form-control', 
                              'placeholder'=>'Call Back Date')) !!}
                     </div>

                     <div class="form-group col-md-6">
                     {!! Form::label('Created Date') !!}
                     {!! Form::date('created_at', null, 
                        array('null', 
                              'class'=>'form-control', 
                              'placeholder'=>'Created Date')) !!}
                     </div>
                       
                     <div class="form-group col-md-4">
                        <a href="{{url('calllogs')}}" class="btn btn-flat btn-danger">Reset</a>
                        <button class="btn btn-flat btn-primary">Search</button>
                     </div>
                   
                    {!! Form::close() !!}
                
                </div>
            </div>

          <table class="table table-bordered">
                        <thead>
                          <tr>
                        <!-- <th style="text-align: right;">Action</th>
                        <th style="text-align: center;">ID</th> -->
                        <th style="text-align: center;">New Employers Called</th>
                        <th>New Employers Answered</th>
                        <th>New Employers Leads (Int + Need)</th>
                        <th>New Employer Meetings Sched.</th>
                        <th>Follow Ups Employers Called</th>
                        <th>Follow Ups Employers Answered</th>
                        <th>Follow Ups Employers Leads (Int + Need)</th>
                        <th>Follow Ups Employer Meetings Sched.</th>
                        <th>User</th>
                        </tr>
                        </thead>
                        <tbody>

                   @foreach($checklists as $checklist )
       
                        <tr class="success">
                        <td>{{ $checklist->new_employers_call }}</td>
                        <td>{{ $checklist->new_employers_answer }}</td>
                        <td>{{ $checklist->new_employers_leads }}</td>
                        <td>{{ $checklist->new_employers_meetings_schedule }}</td>

                        <td>{{ $checklist->follow_ups_employers_call }}</td>
                        <td>{{ $checklist->follow_ups_employers_answer }}</td>
                        <td>{{ $checklist->follow_ups_employers_leads }}</td>
                        <td>{{ $checklist->follow_ups_employers_meetings_schedule }}</td>
                        <td>PSN</td>
                       </tr>
       
                    @endforeach
        
              </tbody>
            </table> 

@stop