@extends('layouts.dashboard')
@section('page_heading','Call Log Create')

@section('section')


<br>
<br>
<br>

<div class="row">
              @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div>
                @endif
  <div class="col-md-6">
		<form action="{{ route('checklists-js.store') }}" method="POST">
          {{ csrf_field() }}
        
            <div class="form-group">
              <label for="title"> Date:</label>
              <input type="date" name="date" id="title" class="form-control" value="" >
            </div>

             <div class="form-group">
              {!! Form::label('Applied') !!}
              {!! Form::text('applied', null, 
                  array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>
             <div class="form-group">
              {!! Form::label('Scheduled Interviewed') !!}
              {!! Form::text('scheduled_interview', null, 
                  array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>
             <div class="form-group">
              {!! Form::label(' Interviewed') !!}
              {!! Form::text('interviewed', null, 
                  array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>
            <div class="form-group">
              {!! Form::label(' Got Job') !!}
              {!! Form::text('got_job', null, 
                  array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>
             <div class="form-group">
              {!! Form::label('Job Browsed') !!}
              {!! Form::text('job_browsed', null, 
                  array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>         
             <div class="form-group">
              {!! Form::label('Followed Up') !!}
              {!! Form::text('followed_up', null, 
                  array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>
             <div class="form-group">
                {!! Form::label('Total Number of calls') !!}
                {!! Form::text('total_number_of_calls', null, 
                    array('required', 
                        'class'=>'form-control', 
                        'placeholder'=>'')) !!}
               </div>

           
     
      <div class="form-group"> 
				<button type="submit" class="btn btn-success">Create Job Seeker Calls</button>
			</div>
		</form>


  </div>

@stop