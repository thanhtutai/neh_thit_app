@extends('layouts.dashboard')

@section('section')
<style type="text/css">
  .search-fix {
  overflow: hidden;

  position: fixed;
  top: 60px;
  width: 100%;

}
</style>
<br><br><br>

<div class="row">

  <div class="col-md-12">
    <center><h2 class="" style="display: inline-block;">Employer Sale List </h2></center>

  </div>

   @if(!empty($js_id) || !empty($fullname))
        <div class="col-md-12 alert alert-success alert-block search-fix">

          @isset($js_id)
          ID : <b>{{$js_id}} </b>
            @endisset

          @isset($fullname)
          Full Name : <b>{{$fullname}} </b>
          @endisset
          
          @isset($call_reason)
          Call Reason : <b>{{$call_reason}} </b>
          @endisset
          
          @isset($remark_calllog)
          Remark : <b>{{$remark_calllog}} </b>
          @endisset

          @isset($type_of_call)
          Type of Call : <b>{{$type_of_call}} </b>
          @endisset

          @isset($caller_receiver)
          Caller Receiver : <b>{{$caller_receiver}} </b>
          @endisset        

          @isset($status)
          Status : <b>{{$status}} </b>
          @endisset

          @isset($call_back_date)
          Call Back Date : <b>{{$call_back_date}} </b>
          @endisset
          
          @isset($created_at)
          Created Date : <b>{{$created_at}} </b>
          @endisset

  @if ($message = Session::get('success'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
      </div>
      @endif

 </div>
 @endif
</div>

    <a href="{{ route('checklists-emp.create') }}" class="btn btn-primary pull-right">Create Report</a>
    <a href="{{ URL::to('downloadExcelCLEMP/xls') }}">
      <button class="btn btn-success pull-right">Excel xls</button>
    </a>
    <a href="{{ URL::to('downloadExcelCLEMP/csv') }}">
      <button class="btn btn-success pull-right"> CSV</button>
    </a>
    <button class="btn btn-default for-hide-search-form">Show Search Form</button>


<div class="row">
  <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-body">
                    {!! Form::open(['url'=>'/checklists-emp','method'=>'GET', 'class'=>'form', 'id'=>'search_data']) !!}
      

                    <div class="form-group col-md-6">
                        {!! Form::label('User') !!}
                            <?php $authors = \App\User::all(); ?>
                          <select id="val_select" name="user" class=form-control select2">
                             <option value="{{ request()->input('user')}}">
                              <?php $label =request()->input('user');
                              if(isset($label))
                                echo $label;
                              else
                                echo "User";
                             ?></option>
                                 @foreach($authors as $row)
                                        <option value="{{ $row->name }}" @if(old('user') == $row->name) selected @endif> {{ $row->name }} </option>
                                  @endforeach
                          </select>
                   </div>
                          

                    <div class="form-group col-md-6">
                      {!! Form::label('Created Date') !!}
                      {!! Form::date('created_at', null, 
                      array('null', 
                          'class'=>'form-control', 
                          'placeholder'=>'Created Date')) !!}
                    </div>
                       
                    <div class="form-group col-md-4">
                        <a href="{{url('checklists-emp')}}" class="btn btn-flat btn-danger">Reset</a>
                        <button class="btn btn-flat btn-primary">Search</button>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>




    <table>
      <thead>
        <tr style="background-color: #99ccff">

                        <th>Id</th>
                        <th>User</th>
                        <th>Date</th>
                        <th>New Employers Called</th>
                        <th>New Employers Answered</th>
                        <th>New Employers Leads (Int + Need)</th>
                        <th>New Employer Meetings Sched</th>
                        <th>Follow ups Employers Called</th>
                        <th>Follow ups Employers Answered </th>
                        <th>Follow ups Employers Leads (Int + Need) </th>
                        <th>Follow ups Employer Meetings Sched </th>
                        <th>Total Employers Called - Total </th>
                        <th>Total Employer Sales </th>
                        <!-- <th>Total Employer called - Total (/hr) </th> -->


      </thead>
      <tbody>

        @foreach($checklistemps as $checklistemp )
          <tr class="" style="background-color:#cce6ff;">

          <td>{{ $checklistemp->id}}</td>
          <td>{{ $checklistemp->author['name']}}</td>
          <td>{{ $checklistemp->created_at}}</td>
          <td>{{ $checklistemp->new_employers_called }}</td>
          <td>{{ $checklistemp->new_employers_answered }}</td>
          <td>{{ $checklistemp->new_employers_leads }}</td>
          <td>{{ $checklistemp->new_employers_meeting_schedule }}</td>
          <td>{{ $checklistemp->followups_employers_called }}</td>
          <td>{{ $checklistemp->followups_employers_answered   }}</td>
          <td>{{ $checklistemp->followups_employers_leads   }}</td>
          <td>{{ $checklistemp->followups_employers_meeting_schedule   }}</td>
          <td>{{ $checklistemp->followups_employers_called_total   }}</td>
          <td>{{ $checklistemp->followups_employer_sale   }}</td>
     
        </tr>   
         @endforeach
     
      </tbody>
    </table> 

    </div>
    </div>
         <nav>
       <ul class="pagination">
       <li><?php //  echo $jobseekers->render(); ?></li>
       </ul>
     </nav>

  
  </div>
</div>
</div>
<style type="text/css">
  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
width: 2000px;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

th.last-called-date{
  width: 100px;
}
th.call-back-date{
  width: 100px;
}
</style>

@stop