@extends('layouts.dashboard')
@section('page_heading','Call Log Create')

@section('section')


<br>
<br>
<br>

<div class="row">
              @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div>
                @endif
  <div class="col-md-6">
		<form action="{{ route('checklists-emp.store') }}" method="POST">
         {{ csrf_field() }}
      
            <div class="form-group">
              {!! Form::label('New Employer Called') !!}
              {!! Form::text('new_employers_called', null, 
                array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

             <div class="form-group">
              {!! Form::label('New Employers Answered') !!}
              {!! Form::text('new_employers_answered', null, 
                  array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

             <div class="form-group">
              {!! Form::label('New Employers Leads') !!}
              {!! Form::text('new_employers_leads', null, 
                  array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>    

             <div class="form-group">
              {!! Form::label('New Employers Meeting Schedule') !!}
              {!! Form::text('new_employers_meeting_schedule', null, 
                  array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>             

             <div class="form-group">
              {!! Form::label('Follow Ups Employers Called') !!}
              {!! Form::text('followups_employers_called', null, 
                  array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>  

             <div class="form-group">
              {!! Form::label('Follow Ups Employers Answered') !!}
              {!! Form::text('followups_employers_answered', null, 
                array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

             <div class="form-group">
              {!! Form::label('Follow Ups Employers Leads') !!}
              {!! Form::text('followups_employers_leads', null, 
                  array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div> 

             <div class="form-group">
              {!! Form::label('Follow Ups Employers Meeting Schedule') !!}
              {!! Form::text('followups_employers_meeting_schedule', null, 
                  array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>    

             <div class="form-group">
              {!! Form::label('Follow Ups Employers Called Total') !!}
              {!! Form::text('followups_employers_called_total', null, 
                array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

             <div class="form-group">
              {!! Form::label('Follow Ups Employer Sale') !!}
              {!! Form::text('followups_employer_sale', null, 
                  array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

          <div class="form-group"> 
    				<button type="submit" class="btn btn-success">Create Employer Sale</button>
    			</div>

		</form>

  </div>

@stop