@extends('layouts.dashboard')


@section('section')
<br>
<br>
<br>
<div class="row">
  <div class="col-md-6">
    <form action="{{ route('jobseekers.update', $jobseekers->id) }}" method="POST">
      {{ csrf_field() }}
      <div class="form-group">
        <label for="title">Name:</label>
        <input type="text" name="fullname" id="title" class="form-control" value="{{ $jobseekers->fullname }}">
          
      </div>

       <div class="form-group">
          <select class="form-control select2">
            <option value="Sumyat">Sumyat</option>
            <option value="Khine">Khine</option>
            <option value="CSR">CSR</option>
            <option value="audi" selected>Select Receiver</option>
          </select>
        </div>   

       <div class="form-group"> 
        <form action="{{ route('jobseekers.update', $jobseekers->id) }}"
        >
          {{ csrf_field() }}
          {{ method_field("patch") }}
          <button type="submit" class="btn btn-primary">Update Info</button>
        </form>

        
      </div> 
      </div>
    </form>
  </div>

</div>
  
  @stop