@extends('layouts.dashboard')
@section('page_heading','Duplicates Section')

@section('section')
<br>
<br>
<br>
<div class="row">
 <table class="table table-bordered">
      <thead>
        <tr>
                        <th style="text-align: center;">Action</th>
                        <th>Fb Name</th>
                        <th>Chatfuel</th>
                        <th>FullName</th>
                        <th>Phone</th>
                        <th>Age</th>
                        <th>Township</th>
                        
      </thead>
      <tbody>
         @foreach($duplicates as $jobseeker )
         @empty($jobseeker->messenger_user_id)
        <tr class="success">
              <td style="text-align: center;"> <a href="{{ route('jobseekers.jobseeker_detail', $jobseeker->id) }}" class="btn btn-primary btn-xs">Show</a><a href="{{ route('jobseekers.send_cv', $jobseeker->id) }}" class="btn btn-success btn-xs">Send</a></td>
              <td>{{ $jobseeker->fb_name }}</td>
              <td>{{$jobseeker->chatfuel_user_id}}</td>
              <td>{{ $jobseeker->fullname }} </td>
              <td>{{ $jobseeker->phone }} </td>
              <td>{{ $jobseeker->age }} </td>
             <td> {{ \App\Models\Township::where('id',$jobseeker->township)->pluck('name')->first() }} </td> 
         
   
           
<!--              <td> <a href="{{ route('jobseekers.mypdf', $jobseeker->id) }}" class="btn btn-primary">Show</a></td> -->

   
        </tr>
        @endempty
         @endforeach
      </tbody>
    </table>  
 <nav>
       <ul class="pagination">
       <li><?php echo $duplicates->render(); ?></li>
       </ul>
     </nav>


    
@stop