@extends('layouts.dashboard')

@section('section')
<br>
<br>
<br>


<div class="row">              
              @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div>
                @endif
			  <div class="col-md-6">
			    <form action="{{ route('users.store')}}" method="POST">
			      {{ csrf_field() }}

				      <div class="form-group">
				        <label for="title">User Name:</label>
				        <input type="text" name="name" id="title" class="form-control" value="">
				          
				      </div>

				      <div class="form-group">
				        <label for="title">Email:</label>
				        <input type="text" name="email" id="title" class="form-control" value="">
				          
				      </div>

				      <div class="form-group">
				        <label for="title">Job Title:</label>
				          <select name="is_admin" class="form-control select2 pull-right">
				            <option value="1">Admin</option>
				            <option value="0">CSR</option>
				           
				            <option value="" selected>Position</option>
				          </select>
				 	   </div>

				      <div class="form-group">
				        <label for="title">Password:</label>
				        <input type="password" name="password" id="title" class="form-control" value="">
				          
				      </div>

                                  
                      <div class="form-group pull-right">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                                
                            </div>
                        </div>
                          </form>         
        
      </div> 
     

  @stop