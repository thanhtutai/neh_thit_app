@extends('layouts.dashboard')

@section('section')
<br>
<br>
<br>
<div class="row">
<a href="{{ route('users.create') }}" class="btn btn-primary">Create New User</a>
</div>
 <div class="col-md-6">

    <table class="table table-bordered">
      <thead>
        <tr>
     
                        <th>Action</th>
                        <th>Delete</th>
                        <th>User Name</th>
                        <th>Email</th>
                        <th>Job Title</th>
  

      </thead>
      <tbody>
             @foreach($users as $user )
        <tr class="success">
            
              <td>  <a href="{{ route('users.edit', $user->id) }}" class="btn btn-success btn-xs">Edit</a></td>
              <td> <form action="{{ route('users.destroy', $user->id) }}" method="post">
            {{ csrf_field() }}
            {{ method_field('delete')}}
            <button class="btn btn-danger btn-xs">Delete</button>
          </form></td>
              <td>{{ $user->name }}</td>   
              <td>{{ $user->email }}</td>   
              <td>@if($user->is_admin===1) Admin @elseif($user->is_admin===2) CSR @else EMP @endif</td>   
        </tr>

         @endforeach
      </tbody>
    </table>  
          <nav>
       <ul class="pagination">
       <li><?php echo $users->render();?></li>
       </ul>
     </nav>
</div>
 
</div>

 </div>

  @stop