@extends('layouts.dashboard')


@section('section')
<br>
<br>
<br>

<div class="row">
  <div class="col-md-6">
  
    <form action="{{ route('users.update', $users->id) }}" method="POST">
      {{ csrf_field() }}
      <div class="form-group">
        <label for="title">User Name:</label>
        <input type="text" name="name" id="title" class="form-control" value="{{ $users->name }}">
      </div>
      <div class="form-group">
        <label for="title">Email:</label>
        <input type="text" name="email" id="title" class="form-control" value="{{ $users->email }}">
      </div>
      <div class="form-group">
        <label for="title">Password:</label>
        <input type="password" name="password" id="title" class="form-control" value="">
      </div>

              <div class="form-group">
                <label for="title">Job Title:</label>
                  <select name="is_admin" class="form-control select2 pull-right">
                    <option value="1">Admin</option>
                    <option value="2">CSR</option>
                    <option value="3">EMP</option>
                   
                    <option value="{{ $users->is_admin}}" selected>@if($users->is_admin===1) Admin @elseif($users->is_admin===2) CSR @else EMP @endif</option>
                  </select>
             </div>
                               <div class="form-group"> 
                                <form action="{{ route('users.update', $users->id) }}"
                                >
                                  {{ csrf_field() }}
                                  {{ method_field("patch") }}
                                  <button type="submit" class="btn btn-primary">Update User</button>
                                </form>

        
      </div> 
      </div>
    </form>
  </div>

</div>
  
  @stop