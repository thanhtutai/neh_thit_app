@extends('layouts.dashboard')

@section('section')
<br>
<br>
<br>
<!-- <center><h2>Job Title Section</h2></center> -->
<div class="row">              
              @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div>
                @endif
			  <div class="col-md-6">
			    <form action="{{ route('jobtitles.store')}}" method="POST">
			      {{ csrf_field() }}

				      <div class="form-group">
                <label for="title">Job Titles Key:</label>
                <input type="text" name="job_title_key" id="title" class="form-control" value="">
                  
              </div> 

              <div class="form-group">
				        <label for="title">Job Titles Name:</label>
				        <input type="text" name="description" id="title" class="form-control" value="">
				          
				      </div>


              <div class="form-group">
                       {!! Form::label('Choose Job Function') !!}
                <?php $j_function = \App\Models\JobFunction::all(); ?>
                <select id="val_select" name="j_function" class="form-control select2">
                  <option value="{{ request()->input('j_function')}}">{{ request()->input('j_function')}}</option>
                  @foreach($j_function as $row)
                  <option value="{{ $row->id }}" @if(old('j_function') == $row->id) selected @endif> {{ $row->description }} </option>
                  @endforeach

                  
                </select>
              </div>



                                  
                      <div class="form-group pull-right">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                                
                            </div>
                        </div>
                          </form>         
        
      </div> 
     

  <div class="col-md-6">

    <table class="table table-bordered">
      <thead>
        <tr>
     
                        <th>Action</th>
                        <th>ID</th>
                        <th>Job Title Name</th>
  

      </thead>
      <tbody>
             @foreach($jobtitles as $row )
        <tr class="success">
            
              <td>  <a href="{{ route('jobtitles.edit', $row->id) }}" class="btn btn-success btn-xs">Edit</a></td>
              <td>{{ $row->job_title_key }}</td>
              <td>{{ $row->description }}</td>   
        </tr>

         @endforeach
      </tbody>
    </table>  
     
</div>
<div class="row">
<div class="col-md-6 pull-right">
      <nav>
       <ul class="pagination">
       <li><?php // echo $jobtitles->render();?></li>
       </ul>
     </nav>
</div>
</div>
</div>

 </div>

  @stop