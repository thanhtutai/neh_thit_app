@extends('layouts.dashboard')
@section('section')

<br><br>
 <style type="text/css">
   th.fb-name{
    width: 100px;
   }

   .close-wrapper{
    padding: 0;
   }
   .close-wrapper-width{
    width: 12px;
   }
 </style>


<div class="container-fluid">
    
</body>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>



  <div class="col-md-12">
    <center><h2 class="" style="display: inline-block;">Job Match List</h2></center>

  </div>

  <div class="row">
    <div class="col-sm-12 search-fix">

    @isset($result)
        <div class="col-md-12 alert alert-success alert-block">
     You search for
     @isset($js_id)

         <span> ID : <b>{{$js_id}} </b></span>
    @endisset 
    @isset($fullname)
         <span> Full Name : <b>{{$fullname}} </b></span>
    @endisset
    @isset($fb_name)
          Facebook Name : <b>{{$fb_name}}</b>
    @endisset 
    @isset($sk1)
          Skill 1  : <b>{{$sk1}}</b>
    @endisset
    @isset($sk2)
          Skill 2  : <b>{{$sk2}}</b>
    @endisset
    @isset($gender)
           Gender : <b> {{$gender}} </b>
    @endisset
    @isset($township)
           Township : <b>{{$township}}</b>
    @endisset
    @isset($age)
             Age: <b>{{$age}}</b>
    @endisset
    @isset($dp1)
              Desire Job Title 1 : <b>{{$dp1}}</b>
    @endisset
    @isset($dp2)
               Desire Job Title 2 : <b>{{$dp2}}</b>
    @endisset
    @isset($pp1)
                Previous Job Title 1: <b>{{$pp1}}</b> 
    @endisset
    @isset($pp2)
                Previous Job Title 2: <b>{{$pp2}}</b> 
    @endisset    
    @isset($status)
               Status: <b>{{$status}}</b> 
    @endisset
     </div>
    @endisset
   
      @if ($message = Session::get('success'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
      </div>
      @endif
</div>
</div>

<!-- search start -->
<div class="container-fluid">
  <button class="btn btn-default for-hide-search-form">Show Search Form</button> 
  <a class="btn btn-primary pull-right" href="{{ route('jobseekers.create')}}" target="_blank">Create Job Seeker</a>
 
  <a href="{{ URL::to('downloadExcelJS/xls') }}"><button class="btn btn-success pull-right">Excel xls</button></a>
  <a href="{{ URL::to('downloadExcelJS/csv') }}"><button class="btn btn-success pull-right"> CSV</button></a>
</div>
<br>

            <div class="box box-body" style="z-index: -1;">
              {!! Form::open(['url'=>'/jobmatch-paid','method'=>'GET', 'class'=>'form', 'id'=>'search_data']) !!}
                 <div class="form-group col-md-3">

                <input type="text" name="js_id" placeholder="ID" value="{{ request()->input('js_id')}}" class="form-control"/>
              </div>
              <div class="form-group col-md-3">

              <input type="text" name="fullname" placeholder="Name" value="{{ request()->input('fullname')}}" class="form-control"/>
              </div>

              <div class="form-group col-md-3">            
                <input type="text" name="phone" placeholder="Phone" value="{{ request()->input('phone')}}" class="form-control"/>
               </div>

              <div class="form-group col-md-3">
             <!--  <div class="container-fluid">
                <div class="row">
                 <div class="col-md-12">
                  <div class="col-md-10 close-wrapper"> -->
              
                       <!-- {!! Form::label('Choose Township') !!} -->
                <?php $status = \App\Models\Status::all(); ?>
                <select id="ddlFruits" name="status" class="form-control select2">

                  <option value="{{ request()->input('status')}}">
                  <?php $label =request()->input('status');
                    if(isset($label))
                      echo $label;
                    else
                      echo "Choose Status ";

                   ?>
                  </option>
                   <?php if(isset($label)){ ?>
                  <option value="">Choose Status</option>
                  <?php }?>
                  @foreach($status as $row)
                    <option value="{{ $row->description }}" @if(old('status') == $row->id) selected @endif> {{ $row->description }} </option>
                  @endforeach          
                </select>
                </div>
               <!--  <div class="col-md-2 close-wrapper close-wrapper-width">
                  <button id="btnResetstatus" type="button" class="close" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>  
                </div> -->
       
          
                <!-- 
                              <script type="text/javascript">
                    $(function () {
                        $("#btnResetstatus").bind("click", function () {
                            $("#ddlFruits")[0].selectedIndex = 1;
                        });
                    });
                </script> -->


              <div class="form-group col-md-3">
                 <select name="first_age" class="form-control select2">
                <option value="{{ request()->input('first_age')}}">
                <?php $label =request()->input('first_age');
                if(isset($label))
                  echo $label;
                else
                  echo "Choose Age From ";

               ?>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
                <option value="14">14</option>
                <option value="15">15</option>
                <option value="16">16</option>
                <option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
                <option value="20">20</option>
                <option value="21">21</option>
                <option value="22">22</option>
                <option value="23">23</option>
                <option value="24">24</option>
                <option value="25">25</option>
                <option value="26">26</option>
                <option value="27">27</option>
                <option value="28">28</option>
                <option value="29">29</option>
                <option value="30">30</option>
                <option value="31">31</option>
                <option value="32">32</option>
                <option value="33">33</option>
                <option value="34">34</option>
                <option value="35">35</option>
                <option value="36">36</option>
                <option value="37">37</option>
                <option value="38">38</option>
                <option value="39">39</option>
                <option value="40">40</option>
                <option value="41">41</option>
                <option value="42">42</option>
                <option value="43">43</option>
                <option value="44">44</option>
                <option value="45">45</option>
                <option value="46">46</option>
                <option value="47">47</option>
                <option value="48">48</option>
                <option value="49">49</option>
              </select>
              </div>

              <div class="form-group col-md-3">
              <select name="second_age" class="form-control select2">
                <option value="{{ request()->input('second_age')}}">
                <?php $label =request()->input('second_age');
                if(isset($label))
                  echo $label;
                else
                  echo "Choose Age To ";

               ?>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
                <option value="14">14</option>
                <option value="15">15</option>
                <option value="16">16</option>
                <option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
                <option value="20">20</option>
                <option value="21">21</option>
                <option value="22">22</option>
                <option value="23">23</option>
                <option value="24">24</option>
                <option value="25">25</option>
                <option value="26">26</option>
                <option value="27">27</option>
                <option value="28">28</option>
                <option value="29">29</option>
                <option value="30">30</option>
                <option value="31">31</option>
                <option value="32">32</option>
                <option value="33">33</option>
                <option value="34">34</option>
                <option value="35">35</option>
                <option value="36">36</option>
                <option value="37">37</option>
                <option value="38">38</option>
                <option value="39">39</option>
                <option value="40">40</option>
                <option value="41">41</option>
                <option value="42">42</option>
                <option value="43">43</option>
                <option value="44">44</option>
                <option value="45">45</option>
                <option value="46">46</option>
                <option value="47">47</option>
                <option value="48">48</option>
                <option value="49">49</option>
              </select>
              </div>
              

              <div class="form-group col-md-3">
                <!-- {!! Form::label('Choose Gender') !!} -->
                 <?php $gender = \App\Models\Gender::all(); ?>
                 <select id="val_select"  placeholder="Choose Gender" name="gender" class="form-control select2">
                            <option value="{{ request()->input('gender')}}">
                            <?php $label =request()->input('gender');
                              if(isset($label))
                                echo $label;
                              else
                                echo "Choose Gender ";
                             ?>                               
                             </option>
                             <?php if(isset($label)){ ?>
                          <option value="">Choose Gender</option>
                          <?php }?>
                              @foreach($gender as $row)
                    <option value="{{ $row->gender_description }}"> {{ $row->gender_description }} </option>
                              @endforeach
                  </select>
              </div>                 

     
              <div class="form-group col-md-3">
                       <!-- {!! Form::label('Choose Township') !!} -->
                <?php $township = \App\Models\Township::all(); ?>
                <select id="val_select" name="township" class="form-control select2">
                  <option value="{{ request()->input('township')}}">
                  <?php $label =request()->input('township');
                    if(isset($label))
                      echo $label;
                    else
                      echo "Choose Township ";

                   ?>
                  </option>
                  <?php if(isset($label)){ ?>
                          <option value="">Choose Township</option>
                          <?php }?>
                  @foreach($township as $row)
                  <option value="{{ $row->name }}" @if(old('township') == $row->id) selected @endif> {{ $row->name }} </option>
                  @endforeach

                  
                </select>
              </div>              



              <div class="form-group col-md-3">
                       <!-- {!! Form::label('Choose Desire Position 1') !!} -->
                <?php $desire_position_1 = \App\Models\JobTitle::all(); ?>
                <select id="val_select" name="desire_position_1" class="form-control select2">
                  <option value="{{ request()->input('desire_position_1')}}">
                  <?php $label =request()->input('desire_position_1');
                    if(isset($label))
                      echo $label;
                    else
                      echo "Choose Desire Job Title 1 ";

                   ?>
                  </option>
                  <?php if(isset($label)){ ?>
                          <option value="">Choose Desire Position 1</option>
                          <?php }?>
                  @foreach($desire_position_1 as $row)
                  <option value="{{ $row->description }}" @if(old('desire_position_1') == $row->description) selected @endif> {{ $row->description }} </option>
                  @endforeach

                  
                </select>
              </div> 

              <div class="form-group col-md-3">
                       <!-- {!! Form::label('Choose Desire Position 1') !!} -->
                <?php $desire_position_2 = \App\Models\JobTitle::all(); ?>
                <select id="val_select" name="desire_position_2" class="form-control select2">
                  <option value="{{ request()->input('desire_position_2')}}">
                  <?php $label =request()->input('desire_position_2');
                    if(isset($label))
                      echo $label;
                    else
                      echo "Choose Desire Job Title 2 ";

                   ?>
                  </option>
                  <?php if(isset($label)){ ?>
                          <option value="">Choose Desire Job Title 2</option>
                          <?php }?>
                  @foreach($desire_position_2 as $row)
                  <option value="{{ $row->description }}" @if(old('desire_position_2') == $row->description) selected @endif> {{ $row->description }} </option>
                  @endforeach

                  
                </select>
              </div> 

  


              <div class="form-group col-md-3">
                       <!-- {!! Form::label('Choose Previous Position 1') !!} -->
                <?php $skill_1 = \App\Models\skill::all(); ?>
                <select id="val_select" name="skill_1" class="form-control select2">
                  <option value="{{ request()->input('skill_1')}}">
                   <?php $label =request()->input('skill_1');
                    if(isset($label))
                      echo $label;
                    else
                      echo "Choose Skill 1";

                   ?>
                  </option>
                  <?php if(isset($label)){ ?>
                          <option value="">Choose Skill 1</option>
                  <?php }?> 
                  @foreach($skill_1 as $row)
                  <option value="{{ $row->name }}" @if(old('skill_1') == $row->name) selected @endif> {{ $row->name }} </option>
                  @endforeach

                  
                </select>
              </div>              
               <div class="form-group col-md-3">
                       <!-- {!! Form::label('Choose Previous Position 1') !!} -->
                <?php $skill_2 = \App\Models\Skill2::all(); ?>
                <select id="val_select" name="skill_2" class="form-control select2">
                  <option value="{{ request()->input('skill_2')}}">
                   <?php $label =request()->input('skill_2');
                    if(isset($label))
                      echo $label;
                    else
                      echo "Choose Skill 2";

                   ?>
                  </option>
                <?php if(isset($label)){ ?>
                          <option value="">Choose Skill 2</option>
                  <?php }?>   
                  @foreach($skill_2 as $row)
                  <option value="{{ $row->name }}" @if(old('skill_2') == $row->name) selected @endif> {{ $row->name }} </option>
                  @endforeach

                  
                </select>
              </div>           
              
               <div class="form-group col-md-3">
                       <!-- {!! Form::label('Choose Previous Position 1') !!} -->
                <?php $previous_position_1 = \App\Models\JobTitle::all(); ?>
                <select id="val_select" name="previous_position_1" class="form-control select2">
                  <option value="{{ request()->input('previous_position_1')}}">
                   <?php $label =request()->input('previous_position_1');
                    if(isset($label))
                      echo $label;
                    else
                      echo "Choose Previous Job Title 1";

                   ?>
                  </option>
                 <?php if(isset($label)){ ?>
                          <option value="">Choose Previous Job Title 1</option>
                  <?php }?>  
                  @foreach($previous_position_1 as $row)
                  <option value="{{ $row->description }}" @if(old('previous_position_1') == $row->description) selected @endif> {{ $row->description }} </option>
                  @endforeach

                  
                </select>
              </div>

              <div class="form-group col-md-3">
                       <!-- {!! Form::label('Choose Previous Position 2') !!} -->
                <?php $previous_position_2 = \App\Models\JobTitle::all(); ?>
                <select id="val_select" name="previous_position_2" class="form-control select2">
                  <option value="{{ request()->input('previous_position_2')}}">
                   <?php $label =request()->input('previous_position_2');
                    if(isset($label))
                      echo $label;
                    else
                      echo "Choose Previous Job Title 2";

                   ?>
                  </option>
                 <?php if(isset($label)){ ?>
                          <option value="">Choose Previous Job Title 2</option>
                  <?php }?>  
                  @foreach($previous_position_2 as $row)
                  <option value="{{ $row->description }}" @if(old('previous_position_2') == $row->description) selected @endif> {{ $row->description }} </option>
                  @endforeach               
                </select>
              </div>


             <div class="form-group col-md-3">
                    
                     {!! Form::date('date', null, 
                        array('null', 
                              'class'=>'form-control', 
                              'placeholder'=>'Created Date')) !!}
                     </div>


              <div class="form-group col-md-3">
                <a href="{{url('jobmatch-paid')}}" class="btn btn-flat btn-danger" >Reset</a>
                <button class="btn btn-flat btn-primary">Search</button>
              </div>
              {!! Form::close() !!}
            </div>


        
          @if($result!=null)



            <form>  

             <input type="hidden" name="fn" value="<?php 

                if(isset($_GET["fullname"])) {

            $fn = $_GET["fullname"];
          
            echo $fn;
            }
                 ?>">      


            <input type="hidden" name="sk1" value="<?php 

                if(isset($_GET["skill_1"])) {

            $sk1 = $_GET["skill_1"];
          
            echo $sk1;
            }
                 ?>">

            <input type="hidden" name="sk2" value="<?php 

                if(isset($_GET["skill_2"])) {

            $sk2 = $_GET["skill_2"];
          
            echo $sk2;
            }
                 ?>">

            <input type="hidden" name="age" value="<?php 

                if(isset($_GET["age"])) {

            $age = $_GET["age"];
          
            echo $age;
            }
                 ?>">           
            <input type="hidden" name="tsp" value="<?php 

                if(isset($_GET["township"])) {

            $tsp = $_GET["township"];
          
            echo $tsp;
            }
                 ?>">           

            <input type="hidden" name="dp1" value="<?php 

                if(isset($_GET["desire_position_1"])) {

            $dp1 = $_GET["desire_position_1"];
          
            echo $dp1;
            }
                 ?>">            

                 <input type="hidden" name="dp2" value="<?php 

                if(isset($_GET["desire_position_2"])) {

            $dp2 = $_GET["desire_position_2"];
          
            echo $dp2;
            }
                 ?>">            <input type="hidden" name="pp1" value="<?php 

                if(isset($_GET["previous_position_1"])) {

            $pp1 = $_GET["previous_position_1"];
          
            echo $pp1;
            }
                 ?>">            <input type="hidden" name="pp2" value="<?php 

                if(isset($_GET["previous_position_2"])) {

            $pp2 = $_GET["previous_position_2"];
          
            echo $pp2;
            }
                 ?>">
              

<br/>


       <div class="form-group col-md-3" >
        <div class="button-group" >
          <button type="button"  class="btn btn-default btn-sm dropdown-toggle form-control select2" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> <span class="caret"></span>Select Skill 1</button>
            <ul class="dropdown-menu">
                    @foreach($skill_1 as $row)

                      <?php

                      if(isset($_GET["skill_1"])) {
                       $mysk1 = $_GET["skill_1"];
                       $sk1 = explode(',',$mysk1 );    

                       ?>
             <li><a href="#" class="small" data-value="option1" tabIndex="-1"><input onclick="checkFitler('{{$row->name}}','SK1')"  type="checkbox" name="skill_1" {{in_array($row->name,$sk1)?'checked':''}}>{{$row->name}}</a></li>

             <?php }?>

                    @endforeach
             </ul>
          </div>
        </div>   

        <div class="form-group col-md-3" >
        <div class="button-group" >
          <button type="button"  class="btn btn-default btn-sm dropdown-toggle form-control select2" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> <span class="caret"></span>Select Skill 2</button>
            <ul class="dropdown-menu">
                    @foreach($skill_2 as $row)

                      <?php

                      if(isset($_GET["skill_2"])) {
                       $mysk2 = $_GET["skill_2"];
                       $sk2 = explode(',',$mysk2 );    

                       ?>
             <li><a href="#" class="small" data-value="option1" tabIndex="-1"><input onclick="checkFitler('{{$row->name}}','SK2')"  type="checkbox" name="skill_2" {{in_array($row->name,$sk2)?'checked':''}}>{{$row->name}}</a></li>

             <?php }?>

                    @endforeach
             </ul>
          </div>
        </div>        
           <div class="form-group col-md-3" >
        <div class="button-group" >
          <button type="button"  class="btn btn-default btn-sm dropdown-toggle form-control select2" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> <span class="caret"></span>Select Township</button>
            <ul class="dropdown-menu">
                    @foreach($township as $row)

                      <?php

                      if(isset($_GET["township"])) {
                       $mytown = $_GET["township"];
                       $town = explode(',',$mytown );    

                       ?>
             <li><a href="#" class="small" data-value="option1" tabIndex="-1"><input onclick="checkFitler('{{$row->name}}','TSP')"  type="checkbox" name="township" {{in_array($row->name,$town)?'checked':''}}>{{$row->name}}</a></li>

             <?php }?>

                    @endforeach
             </ul>
          </div>
        </div>
            

       <div class="form-group col-md-3">
        <div class="button-group">
          <button type="button" class="btn btn-default btn-sm dropdown-toggle form-control select2" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> <span class="caret"></span>Select Desire Job Title</button>
            <ul class="dropdown-menu">
                  @foreach($desire_position_1 as $row)

                      <?php

                      if(isset($_GET["desire_position_1"])) {
                       $my_dp_1 = $_GET["desire_position_1"];
                       $dp_1 = explode(',',$my_dp_1 );    

                       ?>

                   
             <li><a href="#" class="small" data-value="option1" tabIndex="-1"><input onclick="checkFitler('{{$row->description}}','DP1')" type="checkbox" name="desire_position_1" {{in_array($row->description,$dp_1)?'checked':''}}>{{$row->description}}</a></li>
                 <?php }?>
                    @endforeach
             </ul>
          </div>
        </div>

       <div class="form-group col-md-3">
        <div class="button-group">
          <button type="button" class="btn btn-default btn-sm dropdown-toggle form-control select2" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> <span class="caret"></span>Select Desire Job Title 2</button>
            <ul class="dropdown-menu">
                  @foreach($desire_position_2 as $row)
                      <?php
                      
                      if(isset($_GET["desire_position_2"])) {
                       $my_dp_2 = $_GET["desire_position_2"];
                       $dp_2 = explode(',',$my_dp_2 );    

                       ?>

             <li><a href="#" class="small" data-value="option1" tabIndex="-1"><input onclick="checkFitler('{{$row->description}}','DP2')" type="checkbox" name="desire_position_2" {{in_array($row->description,$dp_2)?'checked':''}}>{{$row->description}}</a></li>
               <?php }?>
                    @endforeach
             </ul>
        </div>
       </div>

       <div class="form-group col-md-3">
        <div class="button-group">
          <button type="button" class=" form-control select2 btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> <span class="caret"></span>Previous Job Title 1 </button>
            <ul class="dropdown-menu">
              @foreach($previous_position_1 as $row)
                      <?php
                      if(isset($_GET["previous_position_1"])) {
                       $my_pp_1 = $_GET["previous_position_1"];
                       $pp_1 = explode(',',$my_pp_1 );    

                       ?>
                    
             <li><a href="#" class="small" data-value="option1" tabIndex="-1"><input onclick="checkFitler('{{$row->description}}','PP1')" type="checkbox" name="previous_position_1" {{in_array($row->description,$pp_1)?'checked':''}}>{{$row->description}} </a></li>
                         <?php }?>
                    @endforeach
             </ul>
          </div>
        </div>

       <div class="form-group col-md-3">
        <div class="button-group">
          <button type="button" class=" form-control select2 btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> <span class="caret"></span>Previous Job Title 2 </button>
            <ul class="dropdown-menu">
                  @foreach($previous_position_2 as $row)
                                        <?php

                      if(isset($_GET["previous_position_2"])) {
                       $my_pp_2 = $_GET["previous_position_2"];
                       $pp_2 = explode(',',$my_pp_2 );    

                       ?>
                
             <li><a href="#" class="small" data-value="option1" tabIndex="-1"><input onclick="checkFitler('{{$row->description}}','PP2')" type="checkbox" name="previous_position_2" {{in_array($row->description,$pp_2)?'checked':''}}>{{$row->description}} </a></li>
                  <?php }?>
                    @endforeach
             </ul>
          </div>
        </div>                        
                  
                       
            
            </form>
          @endif

</option></select></div></option></select></div></div></div>
<?php echo ""; ?>

<!-- end search -->

<div class="container-fluid">
<!-- table -->
            <!-- <table class="table table-bordered"> -->
            <table>
              <thead class="title-fix">
                <tr style="background-color: #99ccff">
                  <th style="text-align: center;" class="action-class">Action</th>

                  <!-- <th>Total Match</th> -->

                  <!-- <th>Job Title</th> -->
                  <th>Id</th>
                  <th>Contracted Job </th>
                  <th>Not Contracted Job </th>
                  <th>Fb Name</th>
                  <th>FullName</th>  
                  <th>Signup Date</th>  
                  <th>Phone</th>      
                  <th>Viber </th>
                  <th>Gender</th>
                  <th>Age</th>
                  <th class="wards">Wards</th>
                
                  <th>Education</th>
                  <th>Previous Job Title 1</th>
                  <th>Experience 1</th>
                  <th>Previous Job Title 2</th>     
                  <th>Experience 2</th>     
                  <th>Skill 1</th>     
                  <th>Skill 2</th>     
                  <th>Certificate</th>     
                  <th>Township</th>
                  <th>Desire Job Title 1</th>
                  <th>Desire Job Title 2</th>
                
                  <th>Certification and License</th>
                  <th>Type of License</th>
                  <th>Fabrics</th>
                  <th>Remark</th>
                  <th>User Type </th>                 
                  <th>Last User Free Input</th>
                  <th>Sign up from </th>
                  <th>Remark</th>
                </thead>
                <tbody>

                       

                  @if($result==null)
                    @foreach($jobseekers as $jobseeker )
<?php 
  $current = Carbon\Carbon::today();
 $jobmatches = \App\Models\Jobposition::where('require_position',$jobseeker->desire_position_1)->orderBy('free_or_paid','asc')->pluck('id');
 
 $freejob_matches_id = \App\Models\Jobposition::where('require_position',$jobseeker->desire_position_1)->where('free_or_paid',0)->whereDate('jobpositions.deadline', '>=', $current)->pluck('id');
 $paidjob_matches_id = \App\Models\Jobposition::where('require_position',$jobseeker->desire_position_1)->where('free_or_paid',1)->whereDate('jobpositions.deadline', '>=', $current)->pluck('id'); 

 $skill_1_job_matches_id = \App\Models\Jobposition::where('skill_1',$jobseeker->skill_1)->pluck('id');
 // dd($jobmatches);
 $jobmatches_id = \App\Models\Jobposition::where('require_position',$jobseeker->desire_position_1)->pluck('id');
// dd($jobmatches);
 $arrlength = count($jobmatches);
     // print($arrlength);
 $arrlength_1 = count($jobmatches_id);
 $arrlength_freejob_match = count($freejob_matches_id);
 $arrlength_paidjob_match = count($paidjob_matches_id);
 $arrlength_skill_1_match = count($skill_1_job_matches_id);

?>
                               
        @if($arrlength != 0)
        @if($jobseeker->desire_position_1 == $jobseeker->Mypaidrequirepoistion['require_position'])
              

                        <tr class="" style="background-color:#cce6ff;">
                          <td style="text-align: center;"> 
                           <a href="{{ route('jobseekers.edit', $jobseeker->jobseeker_id) }}" class="btn btn-warning btn-xs action-item" target="_blank"><i class="glyphicon glyphicon-edit"></i></a><a href="{{ route('jobseekers.jobseeker_detail', $jobseeker->jobseeker_id) }}" class="btn btn-info btn-xs action-item" target="_blank"><i class="glyphicon glyphicon-eye-open"></i></a><!-- <a href="{{ route('jobseekers.send_cv', $jobseeker->id) }}" class="btn btn-success btn-xs">Send</a><br> -->
                          </td>
                          <td>{{$jobseeker->jobseeker_id}}</td>
                         
                          <!-- <td>      -->
 <?php   
     
    // foreach ($jobmatches_id as $index =>$jobmatch_id) {

     //  $job_title =\App\Models\Jobposition::where('id',$jobmatches[$index])->pluck('job_title');

// var_dump($employer_name[0]);
  //print $jobmatch_id .$jobmatches[$index];

     //print "<a href='/employers/$jobmatch_id' target='_blank'> 

    // $job_title[0]</a>";
    // echo " , "; 

     
// for($x = 0; $x < $arrlength; $x++) {
//     echo "<a href='/jobsavailable/$jobmatches_id'>".$jobmatches[$x]."</a>";
//     echo ",";
// }
  // }
?>

                        <!--   </td> -->
                          <td>{{   $arrlength_paidjob_match  }}</td>
                          <td>{{   $arrlength_freejob_match  }}</td>
                        
                    
                          <td><a href="{{ route('jobseekers.jobseeker_detail', $jobseeker->jobseeker_id) }}" target="_blank"> {{ $jobseeker->fb_name }}</a></td>
                          <td><a href="{{ route('jobseekers.jobseeker_detail', $jobseeker->jobseeker_id) }}" target="_blank"> {{ $jobseeker->fullname }} </a></td>
                         <!--  <td><a href="{{ route('jobseekers.jobseeker_detail', $jobseeker->id) }}" target="_blank"> {{ $jobseeker->fullname }} </a></td> -->
                          <td>  {{Carbon\Carbon::parse($jobseeker->date)->format('j-M-y')}}</td>
                          <td>{{ $jobseeker->phone }} </td>  
                           
                                                   
                          <td>{{ $jobseeker->viber_number }} </td>              

                          <td>{{ \App\Models\Gender::where('id',$jobseeker->gender)->pluck('gender_description')->first() }} </td>
 
                          <td>{{ $jobseeker->age }} </td>
                           <td> {{ \App\Models\Ward::where('id',$jobseeker->wards)->pluck('name')->first() }} </td>
                          
                           <td> {{ \App\Models\Education::where('id',$jobseeker->education)->pluck('name_en')->first() }} </td>   
                          <td>{{$jobseeker->previous_position_1}} {{ \App\Models\JobTitle::where('job_title_key',$jobseeker->previous_position_1)->pluck('description')->first() }} </td> 
                            <td> {{ checkExp($jobseeker->experience_month_1) }} </td> 
                          <td> {{$jobseeker->previous_position_2}}{{ \App\Models\JobTitle::where('job_title_key',$jobseeker->previous_position_2)->pluck('description')->first() }} </td>    
                          <td> {{ checkExp($jobseeker->experience_month_2) }} </td> 
                           <td> {{ \App\Models\skill::where('id',$jobseeker->skill_1)->pluck('name')->first() }} </td>
                           <td> {{ \App\Models\Skill2::where('id',$jobseeker->skill_2)->pluck('name')->first() }} </td>
                           <td> {{ \App\Models\Certificate::where('id',$jobseeker->certificate)->pluck('description')->first() }} {{$jobseeker->certificate_other}}</td>
                           <td>{{$jobseeker->mytownship['mytownship_name']}}</td>
                          <td>{{$jobseeker->desire_position_1}} {{ \App\Models\JobTitle::where('job_title_key',$jobseeker->desire_position_1)->pluck('description')->first() }} </td>
                           <td> {{ \App\Models\JobTitle::where('job_title_key',$jobseeker->desire_position_2)->pluck('description')->first() }} </td>
                      
                          <td>{{ $jobseeker->certification_and_license }}</td>
                          <td>{{ $jobseeker->type_of_license }}</td>
                          <td>{{ $jobseeker->fabrics }}</td>                 
                          <td>{{ $jobseeker->remark }}</td>
                          <td>{{ $jobseeker->usertype_1 }}</td>         
                          <td>{{ $jobseeker->last_user_freedom_input }}</td>              
                          <td>{{ $jobseeker->sign_up_from }} </td>  
                            <td>
                            {{ \App\Models\Calllog::orderBy('created_at','desc')->where('jobseeker_id',$jobseeker->id)->pluck('remark_calllog')->first() }}</td>




                          <!--              <td> <a href="{{ route('jobseekers.mypdf', $jobseeker->id) }}" class="btn btn-primary">Show</a></td> -->
                        </tr>
                        {{--     <!-- @endempty -->--}}
                  
                   @endif
                  @endif
   
                    @endforeach
               
                  @else
                    @foreach($result as $jobseeker )
                      @empty($result->published_at)
                         
   <?php  
  $current = Carbon\Carbon::today();
 $jobmatches = \App\Models\Jobposition::where('require_position',$jobseeker->desire_position_1)->orderBy('free_or_paid','asc')->pluck('id');

 $freejob_matches_id = \App\Models\Jobposition::where('require_position',$jobseeker->desire_position_1)->where('free_or_paid',0)->whereDate('jobpositions.deadline', '>=', $current)->pluck('id');
 $paidjob_matches_id = \App\Models\Jobposition::where('require_position',$jobseeker->desire_position_1)->where('free_or_paid',1)->whereDate('jobpositions.deadline', '>=', $current)->pluck('id');
 // dd($jobmatches);
 $jobmatches_id = \App\Models\Jobposition::where('require_position',$jobseeker->desire_position_1)->pluck('id');
// dd($jobmatches);
 $arrlength = count($jobmatches);
     // print($arrlength);
 $arrlength_1 = count($jobmatches_id);
 $arrlength_freejob_match = count($freejob_matches_id);
 $arrlength_paidjob_match = count($paidjob_matches_id);

  ?>                 
                               
        @if($arrlength != 0)
        @if($jobseeker->desire_position_1 == $jobseeker->Mypaidrequirepoistion['require_position'])
              
                   {{-- <!--          @empty($jobseeker->published_at) -->--}} 
                        <tr class="" style="background-color:#cce6ff;">
                          <td style="text-align: center;"> 
                           <a href="{{ route('jobseekers.edit', $jobseeker->id) }}" class="btn btn-warning btn-xs action-item" target="_blank"><i class="glyphicon glyphicon-edit"></i></a><a href="{{ route('jobseekers.jobseeker_detail', $jobseeker->id) }}" class="btn btn-info btn-xs action-item" target="_blank"><i class="glyphicon glyphicon-eye-open"></i></a><!-- <a href="{{ route('jobseekers.send_cv', $jobseeker->id) }}" class="btn btn-success btn-xs">Send</a><br> -->
                          </td>
                          <td>{{$jobseeker->id}}</td>
                          <!-- <td>      -->
 <?php   
     
//      foreach ($jobmatches_id as $index =>$jobmatch_id) {

//        $job_title =\App\Models\Jobposition::where('id',$jobmatches[$index])->pluck('job_title');

// // var_dump($employer_name[0]);
//   //print $jobmatch_id .$jobmatches[$index];

//      print "<a href='/employers/$jobmatch_id' target='_blank'> 

//      $job_title[0]</a>";
//      echo " , "; 

     
// for($x = 0; $x < $arrlength; $x++) {
//     echo "<a href='/jobsavailable/$jobmatches_id'>".$jobmatches[$x]."</a>";
//     echo ",";
// }
   // }
?>

                         <!--  </td> -->
                          <td>{{   $arrlength_paidjob_match  }}</td>
                          <td>{{   $arrlength_freejob_match  }}</td>
                         
                        
                          <td><a href="{{ route('jobseekers.jobseeker_detail', $jobseeker->id) }}" target="_blank"> {{ $jobseeker->fb_name }}</a></td>
                          <td><a href="{{ route('jobseekers.jobseeker_detail', $jobseeker->id) }}" target="_blank"> {{ $jobseeker->fullname }} </a></td>
                           <td>  {{Carbon\Carbon::parse($jobseeker->date)->format('j-M-y')}}</td>
                          <td>{{ $jobseeker->phone }} </td> 
                                                                                         
                          <td>{{ $jobseeker->viber_number }} </td>              

                          <td>{{ \App\Models\Gender::where('id',$jobseeker->gender)->pluck('gender_description')->first() }} </td>
 
                          <td>{{ $jobseeker->age }} </td>
                           <td> {{ \App\Models\Ward::where('id',$jobseeker->wards)->pluck('name')->first() }} </td>
                         
                           <td> {{ \App\Models\Education::where('id',$jobseeker->education)->pluck('name_en')->first() }} </td>   
                          <td> {{ \App\Models\JobTitle::where('job_title_key',$jobseeker->previous_position_1)->pluck('description')->first() }} </td> 
                            <td> {{ checkExp($jobseeker->experience_month_1) }} </td> 
                          <td> {{$jobseeker->Myprevious_position_2['description'] }} </td>    
                          <td> {{ checkExp($jobseeker->experience_month_2) }} </td> 
                           <td> {{ \App\Models\skill::where('id',$jobseeker->skill_1)->pluck('name')->first() }} </td>
                           <td> {{ \App\Models\Skill2::where('id',$jobseeker->skill_2)->pluck('name')->first() }} </td>
                           <td> {{ \App\Models\Certificate::where('id',$jobseeker->certificate)->pluck('description')->first() }} {{$jobseeker->certificate_other}}</td>
                           <td>{{$jobseeker->mytownship['mytownship_name']}}</td>
                          <td> {{ \App\Models\JobTitle::where('job_title_key',$jobseeker->desire_position_1)->pluck('description')->first() }} </td>
                           <td> {{ \App\Models\JobTitle::where('job_title_key',$jobseeker->desire_position_2)->pluck('description')->first() }} </td>
                          
                          <td>{{ $jobseeker->certification_and_license }}</td>
                          <td>{{ $jobseeker->type_of_license }}</td>
                          <td>{{ $jobseeker->fabrics }}</td>                      
                          <td>{{ $jobseeker->remark }}</td>
                          <td>{{ $jobseeker->usertype_1 }}</td>                                                  
                          <td>{{ $jobseeker->last_user_freedom_input }}</td>                       
                          <td>{{ $jobseeker->sign_up_from }} </td>       
                            <td>
                            {{ \App\Models\Calllog::orderBy('created_at','desc')->where('jobseeker_id',$jobseeker->id)->pluck('remark_calllog')->first() }}</td>

                        <!--              <td> <a href="{{ route('jobseekers.mypdf', $jobseeker->id) }}" class="btn btn-primary">Show</a></td> -->
                      </tr>
                       @endif
                      @endif 
                      @endempty

                    @endforeach
                  @endif
                </tbody>
            </table> 
<!-- end table -->
             <nav>
         <ul class="pagination">
           <li><?php  echo ($result==null)?$jobseekers->render():$result->render(); ?></li>
         </ul>
        </nav>
      @section ('cotable_panel_body')
      <div class="table-responsive">

        <div class="row">
       @endsection

     </div>
   </div>
 </div>

 <script type="text/javascript">
  function removeItem(ary,item)
  {
    var filteredAry = ary.filter(function(e) { return e !== item })
    return filteredAry;
  }
   function checkFitler(value,type)
   {
      var fn = $('input[name=fn]').val();
      var age = $('input[name=age').val();
      var tsp = $('input[name=tsp]').val();
      var dp1 = $('input[name=dp1]').val();
      var dp2 = $('input[name=dp2]').val();
      var pp1 = $('input[name=pp1]').val();
      var pp2 = $('input[name=pp2]').val();
      switch(type) {
          case 'TSP':
            if (this.event.target.checked==true) {
              tsp = (tsp=='')?value:tsp+','+value;
            }else{
              let n_tsp = tsp.split(',');
              tsp = removeItem(n_tsp,value);    
            }break;
             case 'MA':
            if (this.event.target.checked==true) {
              age = (age=='')?value:age+','+value;
            }else{
              let n_age = age.split(',');
              age = removeItem(n_age,value);  
            }break;
          case 'DP1':
            if (this.event.target.checked==true) {
              dp1 = (dp1=='')?value:dp1+','+value;
            }else{
              let n_dp1 = dp1.split(',');
              dp1 = removeItem(n_dp1,value);  
            }break;
          case 'DP2':
              if (this.event.target.checked==true) {
                dp2 = (dp2=='')?value:dp2+','+value;
              }else{
                let n_dp2 = dp2.split(',');
                dp2 = removeItem(n_dp2,value);
              }
              break;
          case 'PP1':
              if (this.event.target.checked==true) {
                pp1 = (pp1=='')?value:pp1+','+value;
              }else{
                let n_pp1 = pp1.split(',');
                pp1 = removeItem(n_pp1,value);
              }
              break;
          case 'PP2':
              if (this.event.target.checked==true) {
                pp2 = (pp2=='')?value:pp2 +','+value;
              }else{
                let n_pp2 = pp2.split(',');
                pp2 = removeItem(n_pp2,value);
              }
              break;
      }
      let req_field = 'fullname='+fn+'&age='+age+'&township='+tsp+'&desire_position_1='+dp1+'&desire_position_2='+dp2+'&previous_position_1='+pp1+'&previous_position_2='+pp2;
      
      $.get('/jobseekers?'+req_field,function(response){
          document.open();
          document.write(response);
          document.close();
      });
   }
 </script>
<style type="text/css">

/*
td.second {
    width: 100px;
display: block;
    height: 137px;
}
td.third{
  width:100px;
  display: block;
  height: 137px;

}*/
</style>
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
width: 4000px;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

th.last-called-date{
  width: 100px;
}
th.call-back-date{
  width: 100px;
}
th.action-class{
  width: 70px;

}
.action-item{
  display: inline-block;
}
th.wards{
  width: 200px;
}
th.company-recommend{
  width: 250px;
}
</style>
 @stop