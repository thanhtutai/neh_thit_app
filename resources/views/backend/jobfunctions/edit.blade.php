@extends('layouts.dashboard')


@section('section')
<br>
<br>
<br>
<div class="row">
  <div class="col-md-6">
  
    <form action="{{ route('jobfunctions.update', $jobfunctions->id) }}" method="POST">
      {{ csrf_field() }}
      
      <div class="form-group">
        <label for="title">Job Function Name:</label>
        <input type="text" name="description" id="title" class="form-control" value="{{ $jobfunctions->description }}"> 
      </div>
      <div class="form-group">
        <label for="title">Job Function Name (MM):</label>
        <input type="text" name="description_mm" id="title" class="form-control" value="{{ $jobfunctions->description_mm }}"> 
      </div>

     <div class="form-group"> 
      <form action="{{ route('jobfunctions.update', $jobfunctions->id) }}" >
        {{ csrf_field() }}
        {{ method_field("patch") }}
        <button type="submit" class="btn btn-primary">Update Info</button>
      </form>
      </div> 

      </div>
    </form>
  </div>

</div>
  
  @stop