@extends('layouts.dashboard')

@section('section')
<br>
<br>
<br>

<div class="row">              
              @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div>
                @endif
			  <div class="col-md-6">
			    <form action="{{ route('jobfunctions.store')}}" method="POST">
			      {{ csrf_field() }}

				      <div class="form-group">
				        <label for="title">Job Functions Name:</label>
				        <input type="text" name="description" id="title" class="form-control" value="">
				          
				      </div>
              <div class="form-group">
                <label for="title">Job Function Name (MM):</label>
                <input type="text" name="description_mm" id="title" class="form-control"> 
              </div>
               
            <div class="form-group pull-right">
                  <div class="col-md-6 col-md-offset-4">
                      <button type="submit" class="btn btn-primary">
                          Create
                      </button>    
                  </div>
            </div>

          </form>         
      </div> 
     


  <div class="col-md-6">

    <table class="table table-bordered">
      <thead>
        <tr>
     
                        <th>Action</th>
                        <th>ID</th>
                        <th>Job Function Name</th>
                        <th>Job Function Name (MM)</th>
  

      </thead>
      <tbody>
             @foreach($jobfunctions as $row )
        <tr class="success">
            
              <td>  <a href="{{ route('jobfunctions.edit', $row->id) }}" class="btn btn-success btn-xs">Edit</a></td>
              <td>{{ $row->id }}</td>
              <td>{{ $row->description }}</td>  
              <td>{{$row->description_mm}}</td> 
        </tr>

         @endforeach
      </tbody>
    </table>  
     
</div>
<div class="row">
<div class="col-md-6 pull-right">
      <nav>
       <ul class="pagination">
       <li><?php //echo $jobfunctions->render();?></li>
       </ul>
     </nav>
</div>
</div>
</div>

 </div>

  @stop