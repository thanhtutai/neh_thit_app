@extends('layouts.dashboard')
@section('section')

<br>
<br>
<style type="text/css">
  .emp-id{
    display: none;
  }
</style>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet" />



<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>  

<h2 style="text-align:center">Contracted Jobs</h2>

<!-- <div class="row">
<a href="{{ route('employers.create') }}" class="btn btn-primary pull-right">Create Employer</a>
<a href="{{ URL::to('downloadExcelEMP/xls') }}"><button class="btn btn-success pull-right">Excel xls</button></a>
<a href="{{ URL::to('downloadExcelEMP/csv') }}"><button class="btn btn-success pull-right"> CSV</button></a>
</div> -->
<!--             <header class="panel-heading">
                All Orders Data
                <a href="jsdata">click</a>
                <button onClick ="$('#table').tableExport({type:'pdf',escape:'false',pdfFontSize:12,separator: ','});" class="btn btn-default btn-xs pull-right">PDF</i></button>
                <button onClick ="$('#table').tableExport({type:'csv',escape:'false'});" class="btn btn-default btn-xs pull-right">CSV</button>
                <button onClick ="$('#table').tableExport({type:'excel',escape:'false'});" class="btn btn-default btn-xs pull-right">Excel</i></button>
                <button onClick ="$('#table').tableExport({type:'sql',escape:'false',tableName:'orders'});" class="btn btn-default btn-xs pull-right">SQL</i></button>
                <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i>
            </header> -->
<div class="box box-body">
 
                    {!! Form::open(['url'=>'/paidjob','method'=>'GET', 'class'=>'form', 'id'=>'search_data']) !!}
      
                        <div class="form-group col-md-4"> 
                            <input type="text" name="job_title" placeholder="Job Title" value="{{request()->input('job_title')}}" class="form-control"/>
                        </div>
                          
                        <div class="form-group col-md-4"> 
                            <input class="typeahead form-control" value="{{request()->input('employer_name')}}" type="text" name="employer_name" id="fooInput" placeholder="Employer Name">
                        </div>

<!-- <div class="form-group col-md-4 emp-id"> 
 {!! Form::label('Company Id ') !!}
    <input class="typeahead form-control"  type="text" name="company_id" id="haha">
</div>
 -->


                        <div class="form-group col-md-4">
                            <a href="{{url('paidjob')}}" class="btn btn-flat btn-danger">Reset</a>
                            <button class="btn btn-flat btn-primary">Search</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>

    <table id="table" data-ride="datatables">
      <thead>
           <tr style="background-color: #99ccff">
                        <th>Action</th>
                        <th>Id</th>
                        <th>Number of Recommended Job Seeker</th>
                        <th>Job Title Actual</th>
                        <th>Post Date</th>
                        <th>Deadline</th>
                        <th>Employer ID</th>
                        <th>Employer Name</th>
                        <th>Employer Phone Number</th>
                        <th>Employer Email Address</th>
                        <th>Status</th>
                        <th>Job Title Key</th>
                        <th>Gender</th>
                        <th>Nationality</th>
                        <th>Quantity</th>
                        <th>Certificate 1</th>
                        <th>Certificate 2</th>
                        <th>Minimum</th>
                        <th>Maximum</th>
                        <th>Education</th>
                        <th>Industry</th>
                        <th>Skill 1</th>
                        <th>Skill 2</th>
                        <th>Experience Month</th>
                        <th>Salary Range</th>
                        <th>OT Rate per Hour</th>
                        <th>OT Rate per Day</th>
                        <th>Attendance Bonous</th>
                        <th>Zone</th>
                        <th>Bonous Monthly Annually</th>
                        <th>Other benefits</th>
                        <th>Particular Triats</th>
                        <th>Any special requests</th>
                        <th>Any special requirements</th>
                        <th>Township</th>
                        <th>Transport</th>
                        <th>Accomodation</th>
                        <th>Food</th>
                        <th>NRC</th>
                        <th>Police and ward REc</th>
                        <th>Labour Card</th>
                        <th>Remark</th>
                        <!-- <th></th> --> 
              
                        </tr>

      </thead>
      <tbody>


            @foreach($jobpositions as $jobposition )
        
         <tr class="" style="background-color:#cce6ff;">
         <td class="action-column">
<style type="text/css">
  
  .action-class-two{
    display: inline-block;
  }
  .action-item-two{
    width: 100px;
  }
</style>
<div class="action-item-two">

  <a href="{{ route('jobposition.edit', $jobposition->job_id) }}" style="display: inline-block;" class="btn btn-warning btn-xs action-item" target="_blank"><i class="glyphicon glyphicon-edit"></i></a>
         <div class="action-class-two"><a href="{{ route('jobpositions.create', $jobposition->employer_id) }}" class="btn btn-success btn-xs " target="_blank">   <i class="fa fa-user-plus" aria-hidden="true"></a></i></div>

  <div class="dropdown action-item action-class-two">
                   <button class="btn btn-primary dropdown-toggle btn-xs" type="button" data-toggle="dropdown"><i class="glyphicon glyphicon-envelope"></i>
        <span class="caret"></span></button>
    <ul class="dropdown-menu">
      <li> <a href="{{ route('employers.send', $jobposition->job_id) }}" class="btn btn-success btn-xs " target="_blank">HR Consulting Firm Introduction</a></li>
      <li> <a href="{{ route('employers.send2', $jobposition->job_id) }}" class="btn btn-primary btn-xs" target="_blank">Template two</a></li>
      <li> <a href="{{ route('employers.send3', $jobposition->job_id) }}" class="btn btn-warning btn-xs" target="_blank"> Interview Confirmation</a></li>
          </ul>
          </div>

  </div>
         </td><td>{{$jobposition->job_id}}</td>
            <td><?php //$jobseeker = \App\Models\Jobposition::findOrFail($jobposition->id);?>
              <?php               
              // var_dump($jobseeker);
              // foreach ($jobposition->JustFlowUp as $key => $value) {
              //  // var_dump($value->fullname);
              //       echo  "<a href='/jobseekers/show/$value->jobseeker_id'>$value->fullname </a>";
              //     echo ",";
              // }
              $js_count = count($jobposition->JustFlowUp);
              echo $js_count;

              ?>
            </td>
            <td><a href="jobposition/{{$jobposition->job_id}}">{{$jobposition->job_title}}</a></td>
            <td>{{ $jobposition->post_date }}</td>
            <td>{{ $jobposition->deadline }}</td>
            <!-- <td>{{ $jobposition->id }}</td> -->
            <td>{{ $jobposition->employer_id }}</td>
            <td><a href="{{ route('employers.show', $jobposition->employer_id) }}">{{ $jobposition->employer_name}}@if(empty($jobposition->employer_name)){{ $jobposition->employer['employer_name']}} @endif </a></td>
            <td>{{ $jobposition->employer['phone_number'] }}</td>
            <td>{{ $jobposition->employer['email_address'] }}</td>
            <td>{{ ($jobposition->status==0)?'Non-Open':'Open' }}</td>
             <td>{{ $jobposition->My_position_1['description'] }}</td>
            <td>{{ \App\Models\Gender::where('id',$jobposition->gender)->pluck('gender_description')->first() }}</td>
            <td>{{ $jobposition->req_nationality }}</td>
            <td>{{ $jobposition->req_quantity }}</td>
            <td>{{ isset($jobposition->Certificate_1['description'])?$jobposition->Certificate_1['description']:'' }}</td>
            <td>{{ isset($jobposition->Certificate_1['description'])?$jobposition->Certificate_1['description']:'' }}</td>
            <td>{{ $jobposition->minimum }}</td>
            <td>{{ $jobposition->maximum }}</td>
            <td>{{ isset($jobposition->My_education['name'])?$jobposition->My_education['name']:'' }}</td>
            <td>{{ $jobposition->industry }}</td>
            <td>{{ isset($jobposition->Myrequire_skill_1['name'])?$jobposition->Myrequire_skill_1['name']:'' }}</td>
            <td>{{ isset($jobposition->Myrequire_skill_2['name'])?$jobposition->Myrequire_skill_2['name']:'' }}</td>
            
            <td>{{ isset(getExp()[$jobposition->experience_month])?getExp()[$jobposition->experience_month]:'' }}</td>

            <td>{{ isset(expSalary()[$jobposition->salary_range])?expSalary()[$jobposition->salary_range]:'' }}</td>
            <td>{{ $jobposition->ot_rate_per_hour }}</td>
            <td>{{ $jobposition->ot_rate_per_day }}</td>
            <td>{{ $jobposition->attendance_bonous }}</td>
            <td>{{ $jobposition->zone }}</td>
            <td>{{ $jobposition->bonous_monthly_annually }}</td>
            <td>{{ $jobposition->other_benefits }}</td>
            <td>{{ $jobposition->particular_traits }}</td>
            <td>{{ $jobposition->any_special_requests }}</td>
            <td>{{ $jobposition->any_special_requirements }}</td>
            <td>{{ $jobposition->Township['name'] }}</td>
            <td>{{ $jobposition->transport }}</td>
            <td>{{ $jobposition->accomodation }}</td>
            <td>{{ $jobposition->food }}</td>
            <td>{{ $jobposition->nrc }}</td>
            <td>{{ $jobposition->police_and_ward_rec }}</td>
            <td>{{ $jobposition->labour_card }}</td>
            <td>{{ $jobposition->remark }}</td>

          </tr>
          
         @endforeach
     
      </tbody>
    </table> 
         <nav>
       <ul class="pagination">
       <li><?php //  echo $employers->render(); ?></li>
       </ul>
     </nav>

  </div>
</div>
</div>
</div>
<!-- <style type="text/css">
td.second {
    width: 300px;
    display: block;
    height: 137px;
}
td.call-back{
  width:100px;
  display: block;
}
     </style>
 -->

<script>$("#fooInput").autocomplete({
    source: "{{ route('searchajax') }}",
    select: function(event, ui) {
      var e = ui.item;
      var result =  e.id;
      $("#result").append(result);


      var input = $( "#haha" );
        input.val( input.val() + result );
    }
  });
</script>

<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 4000px;

}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}
th.status{
  width: 500px;
}
th.emp-name{
  width: 350px;
}th.action-class{
  width: 200px;
}
th.phone{
  width:350px;
}
.action-item{
  display: inline-block;
}
th.call-back-date{
  width: 150px;
}
th.zone{
  width:300px;
}
th.created-date{
  width: 150px;
}
th.address{
  width:600px;
}
th.notes{
  width:500px;
}
th.action-column{
  width: 100px;
}
</style>

@stop