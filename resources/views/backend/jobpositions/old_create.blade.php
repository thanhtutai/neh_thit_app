@extends('layouts.dashboard')
@section('page_heading','Jobposition Create')

@section('section')
<style type="text/css">
  
  .emp-id{
    display: none;
  }
  .company-id{
    display: none;
  }
</style>

<br>
<br>
<br>

<div class="row">
              @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div>
                @endif
  <div class="col-md-6">
		<form action="{{route('jobposition.store')}}" method="POST">
      {{ csrf_field() }}
            

            <div class="form-group emp-id">
            <label for="title">Employer Id:</label>
            <input type="text" name="employer_id" id="title" class="form-control" value="{{isset($jobpositions->emp_id)?$jobpositions->emp_id:null}}">
            </div>


            <div class="form-group">
            <label for="title">Employer Full Name:</label>
            <input type="text" name="fullname" id="title" class="form-control" value="{{isset($jobpositions->employer_name)?$jobpositions->employer_name:null}}" >
            </div>

            <div class="form-group">
             {!! Form::label('Employer Name ') !!}
                <input class="typeahead form-control" value="{{isset($jobpositions->employer_name)?$jobpositions->employer_name:null}}" readonly type="text" name="employer_name" id="fooInput">
            </div>

            <div class="form-group">
              {!! Form::label('Job Title Assigned:') !!}
              {!! Form::text('job_title', null,array('required', 'class'=>'form-control', 
              'placeholder'=>'','required')) !!}
            </div>

            <div class="form-group">
            <label for="title">Post Date:</label>
            <input type="date" name="post_date" required id="title" class="form-control" value="" >
            </div>
           
            <div class="form-group">
            <label for="title">Expire Date:</label>
            <input type="date" name="deadline" required id="title" class="form-control" value="" >
            </div>

            <div class="form-group">
              <select name="free_or_paid" required class="form-control select2 pull-right" >
                <option value="">Not Contracted or Contracted</option>
                <option value="0">Not Contracted</option>
                <option value="1">Contracted</option>   <!--      
                <option value="{{ request()->input('status')}}" selected>Free or paid</option> -->
              </select>
            </div>  

            <div class="form-group">
                <label for="title">Select Job Function:</label>
                <?php $jobfunctions = \App\Models\JobFunction::all();?>
                <select name="jobfunction" required class="form-control" >
                    <option value="">--- Select Jobfunction ---</option>
                    @foreach ($jobfunctions as $key => $value)
                        <option value="{{ $value->id }}">{{ $value->description }}{{$value->id}}</option>
                    @endforeach
                </select>
            </div>

             <div class="form-group">
                <label for="title">Select Job Title:</label>
                <select name="jobtitle" required class="form-control">
                </select>
            </div>

            <div class="form-group">
              <select name="gender" required class="form-control select2 pull-right">
                <option value="">Choose Gender</option>
                <option value="1">Male</option>
                <option value="2">Female</option>   
                <option value="3">Both</option>
              </select>
             </div>

             <div class="form-group">
            {!! Form::label('Nationality') !!}
            {!! Form::text('req_nationality', null, 
                array('', 
                      'class'=>'form-control', 
                      'placeholder'=>'','required')) !!}
             </div>

             <div class="form-group">
              <select name="status" required class="form-control select2 pull-right">
                <option value="" selected>Choose Status</option>
                <option value="1">Open</option>
                <option value="0">Not-Open</option>   
                <!-- <option value="{{ request()->input('status')}}" selected>Choose Status</option> -->
              </select>
             </div>
           

            <div class="form-group company-id">
                 {!! Form::label('Company Id ') !!}
                <input class="typeahead form-control"  type="text" name="employer_id_test" id="haha">
            </div>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
                <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
                <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet" />

            <script type="text/javascript">
                // var path = "{{ route('autocomplete') }}";
                // $('input.typeahead').typeahead({
                //     source:  function (query, process) {
                //     return $.get(path, { query: query }, function (data) {
                //             return process(data);
                //         });
                //     }
                // });

            $("#fooInput").autocomplete({
                source: "{{ route('searchajax') }}",
                select: function(event, ui) {
                  var e = ui.item;
                  var result =   e.id;
                  $("#result").append(result);


                  var input = $( "#haha" );
                    input.val( input.val() + result );
                }
              });

            </script>         
             <div class="form-group">
            {!! Form::label('Require quantity for job browsing') !!}
            
              <select name="req_quantity" class="form-control" required>
                <option value="">Select one</option>
                @foreach(totalPost() as $key=>$value)
                  <option value="{{$key}}">{{$value}}</option>
                @endforeach
              </select>
             </div>    
  
                                
            <div class="uk-grid" data-uk-grid-margin>
             {!! Form::label('Job Description') !!}
                <div class="uk-width-medium-1-1 parsley-row">
                    <textarea id="wysiwyg_tinymce" name="job_requirement" cols="40"
                              rows="20"></textarea>
                </div>
            </div>

            <!-- <div class="form-group">
            {!! Form::label('Age Requirement for job browsing') !!}
            {!! Form::text('age_requirement', null, 
                array('class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>        -->     

            <!--  <div class="form-group">
            {!! Form::label('Experience Requirement for job browsing') !!}
            {!! Form::text('experience_requirement', null, 
                array(
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div> -->
           <!--   <div class="form-group">
            {!! Form::label('Job Requirement for job browsing') !!}
            {!! Form::text('job_requirement', null, 
                array( 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div> -->

            <div class="form-group">
            {!! Form::label('Township for job browsing') !!}
            
              <select name="township" class="form-control" required="required">
                <option value="">Select one</option>
                @foreach($township as $key=>$value)
                  <option value="{{$key}}">{{$value}}</option>
                @endforeach
              </select>
             </div> 
            <div class="form-group">
            {!! Form::label('Education for job browsing') !!}
            <?php $education = \App\Models\Education::all(); ?>
                <select id="val_select" name="education" class="form-control select2">
                    <option value=""></option>
                    @foreach($education as $row)
                        <option value="{{ $row->id }}">
                            {{ $row->name_en }} 
                        </option>
                    @endforeach
                </select>
             </div>   

             <div class="form-group">
            {!! Form::label('Certificate 1') !!}
            {!! Form::text('certificate_1', null, 
                array( 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>
             <div class="form-group">
            {!! Form::label('Certificate 2') !!}
            {!! Form::text('certificate_2', null, 
                array(
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

             <div class="form-group">
            {!! Form::label('Minimum') !!}
            {!! Form::text('minimum', null, 
                array(
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

             <div class="form-group">
            {!! Form::label('Maximum') !!}
            {!! Form::text('maximum', null, 
                array(
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

             <div class="form-group">
            {!! Form::label('Industry') !!}
            {!! Form::text('industry', null, 
                array(
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

             <div class="form-group">
            {!! Form::label('Skill 1') !!}
            {!! Form::text('skill_1', null, 
                array(
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

             <div class="form-group">
            {!! Form::label('Skill 2') !!}
            {!! Form::text('skill_2', null, 
                array( 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

             <div class="form-group">
            {!! Form::label('Experience Month') !!}
            <select name="experience_month" class="form-control">
              <option value="">Select one</option>
              @foreach(getExp() as $key=>$value)
              <option value="{{$key}}">{{$value}}</option>
              @endforeach                    
            </select>
             </div>

             <div class="form-group">
            {!! Form::label('Salary Range') !!}
            {!! Form::text('salary_range', null, 
                array( 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

             <div class="form-group">
            {!! Form::label('OT Rate per Hour') !!}
            {!! Form::text('ot_rate_per_hour', null, 
                array( 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

            <div class="form-group">
            {!! Form::label('OT Rate per Day') !!}
            {!! Form::text('ot_rate_per_day', null, 
                array(
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>
            <div class="form-group">
            {!! Form::label('Attendance Bonous') !!}
            {!! Form::text('attendance_bonous', null, 
                array( 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>            

             <div class="form-group">
            {!! Form::label('Zone') !!}
            {!! Form::text('zone', null, 
                array(
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>


             <div class="form-group">
            {!! Form::label('Bonous Monthly Annually') !!}
            {!! Form::text('bonous_monthly_annually', null, 
                array( 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

             <div class="form-group">
            {!! Form::label('Other Benefits') !!}
            {!! Form::text('other_benefits', null, 
                array( 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>
             <div class="form-group">
            {!! Form::label('Particular Traits') !!}
            {!! Form::text('particular_traits', null, 
                array(
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

             <div class="form-group">
            {!! Form::label('Any Special Requests') !!}
            {!! Form::text('any_special_requests', null, 
                array( 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>
             <div class="form-group">
            {!! Form::label('Any Special Requirements') !!}
            {!! Form::text('any_special_requirements', null, 
                array( 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

             <div class="form-group">
            {!! Form::label('Transportation') !!}
            {!! Form::text('transport', null, 
                array( 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>
             <div class="form-group">
            {!! Form::label('Accomodation') !!}
            {!! Form::text('accomodation', null, 
                array(
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

             <div class="form-group">
            {!! Form::label('Food') !!}
            {!! Form::text('food', null, 
                array(
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>
             <div class="form-group">
            {!! Form::label('NRC') !!}
            {!! Form::text('nrc', null, 
                array(
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>
             <div class="form-group">
            {!! Form::label('Police and ward Rec') !!}
            {!! Form::text('police_and_ward_rec', null, 
                array(
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

             <div class="form-group">
            {!! Form::label('Labour Card') !!}
            {!! Form::text('labour_card', null, 
                array(
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>
             <div class="form-group">
            {!! Form::label('Remark') !!}
            {!! Form::text('remark', null, 
                array(
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>
         
      <div class="form-group"> 
				<button type="submit" class="btn btn-primary">
                        Create Job
                    </button>
			</div>
		</form>


  </div>
 

<script type="text/javascript">
    $(document).ready(function() {
        $('select[name="jobfunction"]').on('change', function() {
            var stateID = $(this).val(); 
            if(stateID) {
                $.ajax({
                    url: '/jobtitle/ajax/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {                        
                        $('select[name="jobtitle"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="jobtitle"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });

                    }
                });
            }else{
                $('select[name="jobtitle"]').empty();
                $('select[name="city"]').empty();
            }
        });
    });
</script>
@stop