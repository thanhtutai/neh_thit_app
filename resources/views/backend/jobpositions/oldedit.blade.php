@extends('layouts.dashboard')
@section('page_heading','Jobposition Create')

@section('section')
<style type="text/css">
  
  .emp-id{
    display: none;
  }
.company-id{
  display:none;
}
</style>
<br>
<br>
<br>
<div class="row">
              @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div>
                @endif
  <div class="col-md-6">
    <form action="{{ route('jobposition.update', $jobpositions->job_id) }}" method="POST">
      {{ csrf_field() }}

            <div class="form-group emp-id">
            <label for="title">Employer Id:</label>
            <input type="text" name="employer_id" id="title" class="form-control" value="{{$jobpositions->employer_id}}">
            </div>
            <div class="form-group">
            <label for="title">Job Title:</label>
            <input type="text" name="job_title" id="title" class="form-control" value="{{$jobpositions->job_title}}">
            </div>


              <div class="form-group">
                <label for="title">Select Jobfunction:</label>
                <?php $jobfunctions = \App\Models\JobFunction::all();?>
                <select name="jobfunction" class="form-control" >
                    <option value="">--- Select Jobfunction ---</option>
                    @foreach ($jobfunctions as $key => $value)
                        <option {{$jobpositions->job_function_id==$value->id?'selected':''}} value="{{ $value->id }}">{{ $value->description }}{{$value->id}}</option>
                    @endforeach
                </select>
            </div>



            <div class="form-group">
              <label for="title">Job Title Key :</label>
              <?php $previous_position_1 = \App\Models\JobTitle::all(); ?>
              <select id="val_select1" name="jobtitle" class="form-control select2">
                  <option value="{{ $jobpositions->require_position }}">{{ \App\Models\JobTitle::where('job_title_key',$jobpositions->require_position)->pluck('description')->first() }}  </option>id
                  @foreach($previous_position_1 as $row)
                      <option value="{{ $row->job_title_key }}">
                          {{ $row->description }} 
                      </option>
                  @endforeach
              </select>
            </div>
            <div class="form-group">
             {!! Form::label('Employer Name ') !!}
                <input class="typeahead form-control" value="{{isset($jobpositions->employer)?$jobpositions->employer['employer_name']:null}}"  type="text" name="employer_name" id="fooInput">
            </div>

            <div class="form-group company-id">
                 {!! Form::label('Company Id ') !!}
                <input class="typeahead form-control"  type="text" name="employer_id" id="haha">
            </div>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
                <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
                <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet" />

            <script type="text/javascript">
                // var path = "{{ route('autocomplete') }}";
                // $('input.typeahead').typeahead({
                //     source:  function (query, process) {
                //     return $.get(path, { query: query }, function (data) {
                //             return process(data);
                //         });
                //     }
                // });

            $("#fooInput").autocomplete({
                source: "{{ route('searchajax') }}",
                select: function(event, ui) {
                  var e = ui.item;
                  var result =   e.id;
                  $("#result").append(result);


                  var input = $( "#haha" );
                    input.val( input.val() + result );
                }
              });

            </script>  
            <div class="form-group">
            <label for="title">Post Date:</label>
            <input type="date" name="post_date" id="title" class="form-control" value="{{$jobpositions->post_date}}" >
            </div>
           
            <div class="form-group">
            <label for="title">Expire Date:</label>
            <input type="date" name="deadline" id="title" class="form-control" value="{{$jobpositions->deadline}}" >
            </div>

            <div class="form-group">
            <label for="title">Job:</label>
              <select name="free_or_paid" class="form-control select2 pull-right" >
                <option value="0" {{$jobpositions->free_or_paid==0?'selected':''}}>Free</option>
                <option value="1" {{$jobpositions->free_or_paid==1?'selected':''}}>Paid</option>        
              </select>
            </div> 

            <div class="form-group>
             <label for="title">Gender:</label>
             <?php $gender = \App\Models\Gender::all();?>
                <select id="val_select" name="gender" class="form-control select2">
                    <option value="{{ $jobpositions->gender }}"><?php 
                      if($jobpositions->gender==1){
                        echo "Male";
                      } elseif($jobpositions->gender==3){
                        echo "Both";
                      } else{
                        echo "Female";
                      }?>     
                    </option>
                    @foreach($gender as $row)
                        <option value="{{ $row->id }}">
                            {{ $row->gender_description }} 
                        </option>
                    @endforeach
                </select>
            </div>

             <div class="form-group">
            {!! Form::label('Nationality') !!}
            {!! Form::text('req_nationality', $jobpositions->req_nationality, 
                array('', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

             <div class="form-group">
              <select name="status" class="form-control select2 pull-right">
                <option value="1" {{$jobpositions->status==1?'selected':''}}>Open</option>
                <option value="0" {{$jobpositions->status==0?'selected':''}}>Not-Open</option>   
                
              </select>
             </div>
             
             <div class="form-group">
            {!! Form::label('Require quantity') !!}
            {!! Form::text('req_quantity', $jobpositions->req_quantity, 
                array('', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>
             <div class="form-group">
            {!! Form::label('Age Requirement for job browsing') !!}
            {!! Form::text('age_requirement', isset($jobpositions->age_requirement)?$jobpositions->age_requirement:null, 
                array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>            

             <div class="form-group">
            {!! Form::label('Experience Requirement for job browsing') !!}
            {!! Form::text('experience_requirement', isset($jobpositions->experience_requirement)?$jobpositions->experience_requirement:null, 
                array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>
             <div class="form-group">
            {!! Form::label('Job Description Full to show in Job Browsing') !!}
            
               {{ Form::textarea('job_requirement', isset($jobpositions->job_requirement)?$jobpositions->job_requirement:null, 
                array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) }}       
             </div>
            <div class="form-group">
            {!! Form::label('Township for job browsing') !!}
            {!! Form::text('township', $jobpositions->township, 
                array('', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>
            <div class="form-group">
            {!! Form::label('Education for job browsing') !!}
            <?php $education = \App\Models\Education::all(); ?>
                <select id="val_select" name="education" class="form-control select2">
                    <option value=""></option>
                    @foreach($education as $row)
                        <option value="{{ $row->id }}" {{$jobpositions->education==$row->id?'selected':''}}>
                            {{ $row->name_en }} 
                        </option>
                    @endforeach
                </select>
             </div>
             <div class="form-group">
            {!! Form::label('Certificate 1') !!}
            {!! Form::text('certificate_1', $jobpositions->certificate_1, 
                array('', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>
             <div class="form-group">
            {!! Form::label('Certificate 2') !!}
            {!! Form::text('certificate_2', $jobpositions->certificate_2, 
                array('', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

             <div class="form-group">
            {!! Form::label('Minimum') !!}
            {!! Form::text('minimum', $jobpositions->minimum, 
                array('', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

             <div class="form-group">
            {!! Form::label('Maximum') !!}
            {!! Form::text('maximum', $jobpositions->maximum, 
                array('', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>


             <div class="form-group">
            {!! Form::label('Industry') !!}
            {!! Form::text('industry', $jobpositions->industry, 
                array('', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

             <div class="form-group">
            {!! Form::label('Skill 1') !!}
            {!! Form::text('skill_1', $jobpositions->skill_1, 
                array('', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

             <div class="form-group">
            {!! Form::label('Skill 2') !!}
            {!! Form::text('skill_2', $jobpositions->skill_2, 
                array('', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

             <div class="form-group">
            {!! Form::label('Experience Month') !!}
            <select name="experience_month" class="form-control">
              <option value="">Select one</option>
              @foreach(getExp() as $key=>$value)
              <option value="{{$key}}" {{$jobpositions->experience_month==$key?'selected':''}}>{{$value}}</option>
              @endforeach                    
            </select>
             </div>

             <div class="form-group">
            {!! Form::label('Salary Range') !!}
            {!! Form::text('salary_range', $jobpositions->salary_range, 
                array('', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

             <div class="form-group">
            {!! Form::label('OT Rate per Hour') !!}
            {!! Form::text('ot_rate_per_hour', $jobpositions->ot_rate_per_hour, 
                array('', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

            <div class="form-group">
            {!! Form::label('OT Rate per Day') !!}
            {!! Form::text('ot_rate_per_day',  $jobpositions->ot_rate_per_day, 
                array('', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>
            <div class="form-group">
            {!! Form::label('Attendance Bonous') !!}
            {!! Form::text('attendance_bonous', $jobpositions->attendance_bonous, 
                array('', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>            

             <div class="form-group">
            {!! Form::label('Zone') !!}
            {!! Form::text('zone', $jobpositions->zone, 
                array('', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>
           
             <div class="form-group">
            {!! Form::label('Bonous Monthly Annually') !!}
            {!! Form::text('bonous_monthly_annually', $jobpositions->bonous_monthly_annually, 
                array('', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

             <div class="form-group">
            {!! Form::label('Other Benefits') !!}
            {!! Form::text('other_benefits',  $jobpositions->other_benefits, 
                array('', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>
             <div class="form-group">
            {!! Form::label('Particular Traits') !!}
            {!! Form::text('particular_traits', $jobpositions->particular_traits, 
                array('', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

             <div class="form-group">
            {!! Form::label('Any Special Requests') !!}
            {!! Form::text('any_special_requests',  $jobpositions->any_special_requests, 
                array('', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>
             <div class="form-group">
            {!! Form::label('Any Special Requirements') !!}
            {!! Form::text('any_special_requirements', $jobpositions->any_special_requirements, 
                array('', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

             <div class="form-group">
            {!! Form::label('Transportation') !!}
            {!! Form::text('transport', $jobpositions->transport, 
                array('', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>
             <div class="form-group">
            {!! Form::label('Accomodation') !!}
            {!! Form::text('accomodation  ', $jobpositions->accomodation, 
                array('', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

             <div class="form-group">
            {!! Form::label('Food') !!}
            {!! Form::text('food', $jobpositions->food, 
                array('', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>
             <div class="form-group">
            {!! Form::label('NRC') !!}
            {!! Form::text('nrc', $jobpositions->nrc, 
                array('', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>
             <div class="form-group">
            {!! Form::label('Police and ward Rec') !!}
            {!! Form::text('police_and_ward_rec', $jobpositions->police_and_ward_rec, 
                array('', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

             <div class="form-group">
            {!! Form::label('Labour Card') !!}
            {!! Form::text('labour_card', $jobpositions->labour_card, 
                array('', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>
             <div class="form-group">
            {!! Form::label('Remark') !!}
            {!! Form::text('remark', $jobpositions->remark, 
                array('', 
                      'class'=>'form-control', 
                      'placeholder'=>'')) !!}
             </div>

              <form action="{{ route('jobposition.update', $jobpositions->id) }}">
                {{ csrf_field() }}
                {{ method_field("patch") }}
                <button type="submit" class="btn btn-primary">Update Info</button>
              </form>
		</form>


  </div>
<script type="text/javascript">
    $(document).ready(function() {
        $('select[name="jobfunction"]').on('change', function() {
            var stateID = $(this).val();
            if(stateID) {
                $.ajax({
                    url: '/jobtitle/ajax/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        
                        $('select[name="jobtitle"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="jobtitle"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });

                    }
                });
            }else{
                $('select[name="city"]').empty();
            }
        });
    });
</script>
@stop