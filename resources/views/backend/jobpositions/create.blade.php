@extends('layouts.dashboard')
@section('page_heading','Jobposition Create')

@section('section')
<style type="text/css">
  
  .emp-id{
    display: none;
  }
  .company-id{
    display: none;
  }
</style>

<br>
<br>
<br>

<div class="container">
  <div class="row">
  @if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button> 
      <strong>{{ $message }}</strong>
    </div>
  @endif
  <div class="col-md-12">
    <h2 class="text-center">Job Position</h2>
    <form action="{{route('jobposition.store')}}" method="POST" class="form-horizontal form-label-left">
      {{ csrf_field() }}
      <div class="form-group emp-id">

        <label for="title">Employer Id:</label>
        <input type="text" name="employer_id" id="title" class="form-control" value="{{isset($jobpositions->emp_id)?$jobpositions->emp_id:null}}">
      </div>

      <div class="form-group">
        <label for="title" class="col-md-2">Employer Name:</label>
          <div class="col-md-6 col-sm-6 col-xs-12">            
            <input type="text" readonly name="fullname" id="title" class="form-control" value="{{isset($jobpositions->employer_name)?$jobpositions->employer_name:null}}" >
          </div>
      </div>
      <div class="form-group">
        <label for="title" class="col-md-2">Job Functions:</label>
        <div class="col-md-6 col-sm-6 col-xs-12"> 
          <select name="jobfunction" required class="form-control" >
            <option value="">--- Select Jobfunction ---</option>
            @foreach ($jobfunctions as $key => $value)
            <option value="{{ $value->id }}">{{ $value->description }}{{$value->id}}</option>
            @endforeach
          </select>
        </div>
      </div>
      <div class="form-group">
        <label for="title" class="col-md-2">Job Title:</label>
          <div class="col-md-6 col-sm-6 col-xs-12">            
            <select name="jobtitle" required class="form-control">
            </select>
          </div>
      </div>
      <div class="form-group">
        <label for="title" class="col-md-2">Job Title Assigned:</label>
          <div class="col-md-6 col-sm-6 col-xs-12">            
            <input type="text" name="job_title" id="title" class="form-control" value="">
          </div>
      </div>      
      <div class="form-group">
        <label for="title" class="col-md-2">Post Date:</label>
          <div class="col-md-6 col-sm-6 col-xs-12">            
            <input type="date" required name="post_date" id="title" class="form-control" value="">
          </div>
      </div>
      <div class="form-group">
        <label for="title" class="col-md-2">Expire Date:</label>
          <div class="col-md-6 col-sm-6 col-xs-12">            
            <input type="date" required name="deadline" id="title" class="form-control" value="">
          </div>
      </div>
      <div class="form-group">
        <label for="title" class="col-md-2">Job type:</label>
        <div class="col-md-6 col-sm-6 col-xs-12"> 
          <select name="free_or_paid" required class="form-control" >
            <option value="">Not Contracted or Contracted</option>
            <option value="0">Not Contracted</option>
            <option value="1">Contracted</option>  
          </select>
        </div>
      </div>
      <div class="form-group">
        <label for="title" class="col-md-2">Position Status:</label>
          <div class="col-md-6 col-sm-6 col-xs-12">            
            <select name="status" required class="form-control">
                <option value="" selected>Choose Status</option>
                <option value="1">Open</option>
                <option value="0">Not-Open</option>  
              </select>
          </div>
      </div>
      <div class="form-group">
        <label for="title" class="col-md-2">Gender:</label>
          <div class="col-md-6 col-sm-6 col-xs-12">            
            <select name="gender" required class="form-control">
                <option value="">Choose Gender</option>
                <option value="1">Male</option>
                <option value="2">Female</option>   
                <option value="3">Both</option>
              </select>
          </div>
      </div>
      <div class="form-group">
        <label for="title" class="col-md-2">Age Requirement:</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" class="form-control" name="age_requirement">
          </div>
      </div>
      <div class="form-group">
        <label for="title" class="col-md-2">Nationality:</label>
          <div class="col-md-6 col-sm-6 col-xs-12">            
            <input type="text" name="req_nationality" id="title" class="form-control" value="">
          </div>
      </div>

      <div class="form-group">
        <label for="title" class="col-md-2">Total Job Posts:</label>
          <div class="col-md-6 col-sm-6 col-xs-12">            
             <select name="req_quantity" class="form-control" required>
                <option value="">Select one</option>
                @foreach(totalPost() as $key=>$value)
                  <option value="{{$key}}">{{$value}}</option>
                @endforeach
              </select>
          </div>
      </div>
      
      <div class="form-group">
        <label for="title" class="col-md-2">Townships:</label>
          <div class="col-md-6 col-sm-6 col-xs-12">            
             <select name="township" class="form-control" required="required">
                <option value="">Select one</option>
                @foreach($township as $key=>$value)
                  <option value="{{$key}}">{{$value}}</option>
                @endforeach
              </select>
          </div>
      </div>
      <div class="form-group">
        <label for="title" class="col-md-2">Educations:</label>
          <div class="col-md-6 col-sm-6 col-xs-12">            
            <select name="education" required class="form-control">
                <option value="">Select one</option>
                @foreach($education as $row)
                <option value="{{ $row->id }}">
                  {{ $row->name_en }} 
                </option>
                @endforeach
              </select>
          </div>
      </div>

      <div class="form-group">
        <label for="title" class="col-md-2">Experience :</label>
          <div class="col-md-6 col-sm-6 col-xs-12">            
            <select name="experience_month" class="form-control" required>
              <option value="">Select one</option>
              @foreach(getExp() as $key=>$value)
              <option value="{{$key}}">{{$value}}</option>
              @endforeach                    
            </select>
          </div>
      </div>
      <div class="form-group">
        <label for="title" class="col-md-2">Salary Range :</label>
          <div class="col-md-6 col-sm-6 col-xs-12">            
            <select name="salary_range" class="form-control" required>
              <option value="">Select one</option>
              @foreach(expSalary() as $key=>$value)
              <option value="{{$key}}" {{$jobpositions->salary_range==$key?
                'selected':''}}>{{$value}}</option>
              @endforeach                    
            </select>
          </div>
      </div>
      <div class="form-group">
        <label for="title" class="col-md-2">Certificate 1:</label>
          <div class="col-md-6 col-sm-6 col-xs-12">   
              <select name="certificate_1" class="form-control">
                <option value="">Select one</option>
                @foreach($certificate as $row)
                <option value="{{ $row->id }}">
                  {{ $row->description }} 
                </option>
                @endforeach
              </select>        
          </div>
      </div>
      <div class="form-group">
        <label for="title" class="col-md-2">Certificate 2:</label>
          <div class="col-md-6 col-sm-6 col-xs-12">   
              <select name="certificate_2" class="form-control">
                <option value="">Select one</option>
                @foreach($certificate as $row)
                <option value="{{ $row->id }}">
                  {{ $row->description }} 
                </option>
                @endforeach
              </select>        
          </div>
      </div>
      <div class="form-group">
        <label for="title" class="col-md-2">Skill 1:</label>
          <div class="col-md-6 col-sm-6 col-xs-12">            
            <select name="skill_1" class="form-control">
                <option value="">Select one</option>
                @foreach($skill as $row)
                <option value="{{ $row->id }}">
                  {{ $row->name }} 
                </option>
                @endforeach
              </select>      
          </div>
      </div>
      <div class="form-group">
        <label for="title" class="col-md-2">Skill 2:</label>
          <div class="col-md-6 col-sm-6 col-xs-12">            
            <select name="skill_2" class="form-control">
                <option value="">Select one</option>
                @foreach($skill as $row)
                <option value="{{ $row->id }}">
                  {{ $row->name }} 
                </option>
                @endforeach
              </select>      
          </div>
      </div>
      <div class="form-group">
        <label for="title" class="col-md-2">Other Benefits:</label>
          <div class="col-md-6 col-sm-6 col-xs-12">            
            <input type="text" class="form-control" name="other_benefits">  
          </div>
      </div>
      <div class="form-group">
        <label for="title" class="col-md-2">Job Descriptions:</label>
          <div class="col-md-6 col-sm-6 col-xs-12">            
             <textarea required id="wysiwyg_tinymce" name="job_requirement" cols="40"
                              rows="20"></textarea>
          </div>
      </div>
      <div class="form-group">
        <label for="title" class="col-md-2">Job Requirements:</label>
          <div class="col-md-6 col-sm-6 col-xs-12">            
             <textarea required id="wysiwyg_tinymce" name="any_special_requirements" cols="40"
                              rows="20"></textarea>
          </div>
      </div>
      <button type="submit" class="btn btn-primary">
                        Create Job
                    </button>
    </form>
  </div>
</div>
</div>
 

<script type="text/javascript">
    $(document).ready(function() {
        $('select[name="jobfunction"]').on('change', function() {
            var stateID = $(this).val(); 
            if(stateID) {
                $.ajax({
                    url: '/jobtitle/ajax/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {                        
                        $('select[name="jobtitle"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="jobtitle"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });

                    }
                });
            }else{
                $('select[name="jobtitle"]').empty();
                $('select[name="city"]').empty();
            }
        });
    });
</script>
@stop