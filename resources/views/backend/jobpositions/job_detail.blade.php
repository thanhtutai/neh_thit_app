@extends('layouts.dashboard')


@section('section')
<style type="text/css">
  .right-wrapper-info{
    display: inline-block;
  }
  .jbrowse{
    font-weight: bold;
  }
</style>

<br><br><br>
<h2 style="text-align:center">Job Detail</h2>

<div class="col-md-8 col-md-offset-2">
   
  <div class="panel panel-info">

          <div class="panel-footer">
               <a href="{{ route('jobposition.edit', $jobpositions->job_id) }}" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a>         
           
  
        <span class="pull-right">
          <div class="right-wrapper">
   
                <div class="cv-and-create right-wrapper-info">
                
                </div> 
          </div>
        </span>
</div>
</span>


  
            <div class="panel-heading">
              <h3 class="panel-title"> {{ isset($jobpositions->job_title)?$jobpositions->job_title:null }}</h3>
            </div>
            <div class="panel-body">
                <div class=" col-md-12 col-lg-12 "> 
                  <table class="table table-user-information">
                    <tbody>
                       <tr style="width: 100%">
                        <td style="width: 30%">Company Name:</td>
                        <td> <a href="/employers/{{$jobpositions->employer['emp_id']}} "> {{$jobpositions->employer['employer_name']}}    </a> </td>
                        </tr> 
                        <tr style="width: 100%">
                        <td style="width: 30%">Phone:</td>
                        <td>    {{$jobpositions->employer['phone_number']}}     </td>
                        </tr>
                        <tr>
                        <td>Email Address</td>
                        <td>  {{$jobpositions->employer['email_address']}}   </td>
                        </tr>
                        <tr>
                        <td>Address</td>
                        <td>{{$jobpositions->employer['address']}}   </td>
                        </tr>
                        <tr>
                        <tr>
                        <td>Zone</td>
                        <td> {{$jobpositions->employer['zone']}} </td>
                        </tr>
                        <tr>
                        <td>Authorized person</td>
                        <td>  {{$jobpositions->employer['authorized_person']}}</td>
                        </tr> 
                       
                        <tr class="jbrowse">
                        <td>For Job Browse - Gender</td>
                        
                        <td> <?php 
                            switch ($jobpositions->gender) {
                              case "1":
                                $gender = 'က်ား';
                                break;
                              case "2":
                                $gender = 'မ';
                                break;
                              case "3":
                                $gender = 'က်ား/မ';
                              default:
                                $gender = 'က်ား/မ';
                              break;
                            }

                            echo $gender;

                        ?></td>
                        </tr> 
                        <tr class="jbrowse">
                        <td>For Job Browse - Require Quantity</td>
                        <td>  {{$jobpositions->req_quantity}}</td>
                        </tr> 
                        <tr class="jbrowse">
                        <td>For Job Browse - Township</td>
                        <td>  {{$jobpositions->township}}</td>
                        </tr> 
                        <tr class="jbrowse">
                        <td>For Job Browse - Job Title</td>
                        <td>  {{$jobpositions->job_title}}</td>
                        </tr>
                        <tr class="jbrowse">
                        <td>For Job Browse - Age requirement</td>
                        <td>  {{$jobpositions->age_requirement}}</td>
                        </tr>
                        <tr class="jbrowse">
                        <td>For Job Browse - Education requirement</td>
                        <td> 
                        {{\App\Models\Education::where('id',$jobpositions->education)->pluck('name_en')}}</td>
                        </tr>
                        <tr class="jbrowse">
                        <td>For Job Browse - Experience requirement</td>
                        <td>  {{$jobpositions->experience_requirement}}</td>
                        </tr>  
                        <tr class="jbrowse">
                        <td>For Job Browse - Job requirement</td>
                        <td>  {{$jobpositions->job_requirement}}</td>
                        </tr>
                      
                        <tr>
                        <td>Status</td>
                        <td>
     
                        </td>
                      </tr>
                      
                           
                      </tr>
                     
                    </tbody>
                  </table>
                  
                  
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>


  </div>




<div class="container-fluid">

<table class="table table-bordered table-responsive">
   <h3><b>Recommended Job Seeker List</b></h3>
      <thead>
         <tr style="background-color: #99ccff">   
          
            <th>Fullname</th>
            <th>Fb Name</th>
            <th>Desire Position</th>
            <th>Skill 1</th>
              

      </thead>
      <tbody>
    <?php               
              // var_dump($jobseeker);
              foreach ($jobpositions->JustFlowUp as $key => $value) {
               // var_dump($value->fullname);
                  
              // $js_count = count($jobposition->JustFlowUp);
              // echo $js_count;

              ?>
        <tr class="success"> 
        
            <td><a href='/jobseekers/show/{{$value->jobseeker_id}}'>{{$value->fullname}} </a></td>
            <td><a href='/jobseekers/show/{{$value->jobseeker_id}}'>{{$value->fb_name}} </a></td>
            <td> {{\App\Models\JobTitle::where('job_title_key',$value->desire_position_1)->pluck('description')}} </td>
            <td>{{\App\Models\Skill2::where('id',$value->skill_1)->pluck('name')}} </td>
    
        </tr>
          
            <?php   
              }?>
      </tbody>
    </table>  
    
</div><div class="container-fluid">

<table class="table table-bordered table-responsive">
   <h3><b>Job Seeker Matched</b></h3>
      <thead>
         <tr style="background-color: #99ccff">   
          
            <th>Fullname</th>
            <th>Fb Name</th>
            <th>Desire Position</th>
            <th>Skill 1</th>
              

      </thead>
      <tbody>
      <?php $jobeeker_list = DB::table('jobseekers')->where('desire_position_1',isset($jobpositions->require_position)?$jobpositions->require_position:null)->wherenotNull('is_followup')->get();?>


        @foreach($jobeeker_list as $jobseeker)
        
        <tr class="success"> 
            <td><a href="/jobseekers/show/{{$jobseeker->jobseeker_id}}">{{$jobseeker->fullname}}</a></td>
            <td><a href="/jobseekers/show/{{$jobseeker->jobseeker_id}}">{{$jobseeker->fb_name}}</a></td>
            <td>
                {{\App\Models\JobTitle::where('job_title_key',$jobseeker->desire_position_1)->pluck('description')}}
            </td>
            <td> {{\App\Models\Skill2::where('id',$jobseeker->skill_1)->pluck('name')}}</td>
        
        </tr>
         @endforeach
          
          
      </tbody>
    </table>  
    
</div>

<script type="text/javascript">
    $(function () {
        $('#datetimepicker6').datetimepicker();
        $('#datetimepicker7').datetimepicker({
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });
    });
</script>
<script type="text/javascript">
  $(function () {
    $('select[name="status"]').change(function () {
        //remove the hide class from date-input
        if ($(this).find(":selected").val() == '12' || $(this).find(":selected").val() == '7'   ) {
            $('#date-input').removeClass('hide')
        }
    })
})
  $(function () {
    $('select[name="description"]').change(function () {
        //remove the hide class from date-input
        if ($(this).find(":selected").val() == '12' || $(this).find(":selected").val() == '7') {
            $('#date-input-calllog').removeClass('hide')
        }
    })
})

</script>
@stop