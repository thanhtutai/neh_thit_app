@extends('layouts.dashboard')

@section('section')
<br><br><br>
	<div class="container">
<!-- 		<form action="/search" method="POST" role="search">
			{{ csrf_field() }}
			<div class="input-group">
				<input type="text" class="form-control" name="q"
					placeholder="Search users"> <span class="input-group-btn">
					<button type="submit" class="btn btn-default">
						<span class="glyphicon glyphicon-search"></span>
					</button>
				</span>
			</div>
		</form> -->
		<div class="container">
			@if(isset($details))
			<p> The Search results for your query <b> {{ $query }} </b> are :</p>
			<!-- <h2>Sample User details</h2> -->
<!-- 			<table class="table table-striped">
				<thead>
					<tr>
						<th>Name</th>
						<th>Email</th>
					</tr>
				</thead>
				<tbody>
					@foreach($details as $user)
					<tr>
						<td><a href="{{ route('jobseekers.jobseeker_detail', $user->id) }}" target="_blank"> {{$user->fullname}}</a></td>
						<td></td>
					</tr>
					@endforeach
				</tbody>
			</table> -->
			@elseif(isset($message))
			<p>{{ $message }}</p>
			@endif
		</div>



<br>
<br>

<h2 style="text-align:center">Employer List</h2>

<div class="row">
<a href="{{ route('employers.create') }}" class="btn btn-primary pull-right">Create Employer</a>
<a href="{{ URL::to('downloadExcelEMP/xls') }}"><button class="btn btn-success pull-right">Excel xls</button></a>
<a href="{{ URL::to('downloadExcelEMP/csv') }}"><button class="btn btn-success pull-right"> CSV</button></a>
</div>
<!--             <header class="panel-heading">
                All Orders Data
                <a href="jsdata">click</a>
                <button onClick ="$('#table').tableExport({type:'pdf',escape:'false',pdfFontSize:12,separator: ','});" class="btn btn-default btn-xs pull-right">PDF</i></button>
                <button onClick ="$('#table').tableExport({type:'csv',escape:'false'});" class="btn btn-default btn-xs pull-right">CSV</button>
                <button onClick ="$('#table').tableExport({type:'excel',escape:'false'});" class="btn btn-default btn-xs pull-right">Excel</i></button>
                <button onClick ="$('#table').tableExport({type:'sql',escape:'false',tableName:'orders'});" class="btn btn-default btn-xs pull-right">SQL</i></button>
                <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i>
            </header> -->
<div class="box box-body">
 
                    {!! Form::open(['url'=>'/employers','method'=>'GET', 'class'=>'form', 'id'=>'search_data']) !!}
      
                        <div class="form-group col-md-4"> 
                            <input type="text" name="employer_name" placeholder="Company Name" value="{{request()->input('employer_name')}}" class="form-control"/>
                        </div>
                          
                        <div class="form-group col-md-4"> 
                            <input type="text" name="zone" placeholder="Zone" value="{{ request()->input('zone') }}" class="form-control"/>
                        </div>
                      
                        <div class="form-group col-md-4">
                            <a href="{{url('employers')}}" class="btn btn-flat btn-danger">Reset</a>
                            <button class="btn btn-flat btn-primary">Search</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>

    <table id="table" data-ride="datatables">
      <thead>
           <tr style="background-color: #99ccff">
                        <th style="text-align: center;" class="action-class">Action</th>
                        <th class="emp-name">Employer Name</th>
                        <th class="call-back-date">Call Back Date</th>
                        <th class="status">Status</th>
                        <th class="created-date">Created Date</th>
                        <th class="phone">Phone</th>
                       
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Authorized Person</th>
                        <th class="zone">Zone</th>

                        <th>Number of Call</th>
                        <th>Given Materials</th>
                        <th class="notes">Notes</th>
                        <th>Interest</th>
                        <th>Understanding</th>
                        <th>Remark</th>
                        <th class="address">Address</th>
                        <th>Latitude</th>
                        <th>Longitude</th>
                        </tr>

      </thead>
      <tbody>

@if(!empty($employers))
            @foreach($employers as $employer )
         <tr class="" style="background-color:#cce6ff;">
               <td class="action-class"> 
  <div class="dropdown action-item">
                   <button class="btn btn-primary dropdown-toggle btn-xs" type="button" data-toggle="dropdown"><i class="glyphicon glyphicon-envelope"></i>
    <span class="caret"></span></button>
    <ul class="dropdown-menu">
      <li> <a href="{{ route('employers.send', $employer->id) }}" class="btn btn-success btn-xs " target="_blank">Template one</a></li>
      <li> <a href="{{ route('employers.send2', $employer->id) }}" class="btn btn-primary btn-xs" target="_blank">Template two</a></li>
      <li> <a href="{{ route('employers.send3', $employer->id) }}" class="btn btn-warning btn-xs" target="_blank">Template three</a></li>
          </ul>
          </div>

<!--                 <a href="{{ route('employers.send', $employer->id) }}" class="btn btn-success btn-xs" target="_blank"><i class="glyphicon glyphicon-envelope"></i></a> -->
                  
                <a href="{{ route('employers.show', $employer->id) }}" class="btn btn-info btn-xs action-item" target="_blank"><i class="glyphicon glyphicon-eye-open"></i></a>
                <a href="{{ route('employers.edit', $employer->id) }}" class="btn btn-warning btn-xs action-item" target="_blank"><i class="glyphicon glyphicon-edit"></i></a>
                
                              </td>
              <td>{{ $employer->employer_name }}</td>
              <td class="call-back">
                {{ Carbon\Carbon::parse( \App\Models\Employercalllog::orderBy('created_at','desc')->where('employer_id',$employer->id)->pluck('call_back_date')->first())->format('j-M-y') }}
              </td>
              <td>
                 {{$employer->mystatus['description']}}</td>
              <td class="call-back"> 
                 {{ Carbon\Carbon::parse(\App\Models\Employercalllog::orderBy('created_at','desc')->where('employer_id',$employer->id)->pluck('created_at')->first())->format('j-M-y') }}
              </td>
              <td>{{ $employer->phone_number }}</td>
              <td>{{ $employer->email_address }}</td>
              <td>{{ $employer->company_type }}</td>
              <td>{{ $employer->authorized_person }}</td>
              <td class="zone">{{ $employer->zone }}</td>
              <td>{{ $employer->number_of_call }}</td>
              <td>{{ $employer->given_materials }}</td>
              <td>{{ $employer->notes }}</td>
              <td>{{ $employer->interest }}</td>
              <td>{{ $employer->understanding }}</td>
              <td>{{ $employer->remark }}</td>
              <td class="second">{{ $employer->address }}</td>
              <td>{{ $employer->latitude }}</td>
              <td>{{ $employer->longitude }}</td>

          </tr>
    
         @endforeach
  @endif   
      </tbody>
    </table> 
         <nav>
       <ul class="pagination">
       <li><?php // echo $employers->render(); ?></li>
       </ul>
     </nav>

  </div>
</div>
</div>
</div>
<!-- <style type="text/css">
td.second {
    width: 300px;
    display: block;
    height: 137px;
}
td.call-back{
  width:100px;
  display: block;
}
     </style>
 -->

<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 4000px;

}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}
th.status{
  width: 500px;
}
th.emp-name{
  width: 350px;
}th.action-class{
  width: 200px;
}
th.phone{
  width:350px;
}
.action-item{
  display: inline-block;
}
th.call-back-date{
  width: 150px;
}
th.zone{
  width:300px;
}
th.created-date{
  width: 150px;
}
th.address{
  width:600px;
}
th.notes{
  width:500px;
}
</style>
<script type="text/javascript" src="{{ asset('js/tableExport/tableExport.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/tableExport/jquery.base64.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/tableExport/jspdf/libs/sprintf.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/tableExport/jspdf/jspdf.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/tableExport/jspdf/libs/base64.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/select2/select2.min.js') }}"></script>
@stop