@extends('layouts.dashboard')
@section('page_heading','Call Log Create')

@section('section')


<br>
<br>
<br>

<div class="row">
              @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div>
              @endif
  <div class="col-md-6">
		{!! Form::open(['route' => 'employercalllogs-create.store', 'method' => 'post']) !!}
          
            <div class="form-group">
                {!! Form::label('Employer ID') !!}
                {!! Form::textarea('employer_id', null, ['class' => 'form-control employer_id', 'placeholder' => __(''), 'rows' => '1']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('Employer Name') !!}
                {!! Form::textarea('employer_name', null, ['class' => 'form-control employer_name', 'placeholder' => __(''), 'rows' => '1']) !!}
                <div class="dataBox" id="result"></div>
            </div> 

            <div class="form-group">
                <label>Type of Call</label>
                  <select class="form-control select2" name="type_of_call">
                    <option value="{{ request()->input('age')}}">Choose Type of Call</option>
                    <option value="Incoming Call">Incoming Call</option>
                    <option value="Outgoing Call">Outgoing Call</option>                              
                 </select>
            </div>

            <div class="form-group">
              {!! Form::label('Minute') !!}
              {!! Form::text('minute', null, 
                  array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'Minute')) !!}
            </div>
              
            <div class="form-group">
                {!! Form::label('Phone Number') !!}
                {!! Form::textarea('ph_number', null, ['class' => 'form-control', 'placeholder' => __('Phone Number'), 'rows' => '1']) !!}
            </div>
          
            <div class="form-group">
                {!! Form::label('Industrial Zone') !!}
                {!! Form::textarea('industry_zone', null, ['class' => 'form-control', 'placeholder' => __('Industrial Zone'), 'rows' => '1']) !!}
            </div> 

             <div class="form-group">
                {!! Form::label('Receiver') !!}
                {!! Form::textarea('receiver', null, ['class' => 'form-control', 'placeholder' => __('Receiver'), 'rows' => '1']) !!}
            </div>              

             <div class="form-group">
               {!! Form::label('Choose Call Reason') !!}
                <?php $call_reason = \App\Models\Employercallreason::all(); ?>
                <select id="val_select" name="call_reason" class="form-control select2">
                  <option value="{{ request()->input('call_reason')}}">{{ request()->input('call_reason')}}</option>
                  @foreach($call_reason as $row)
                  <option value="{{ $row->id }}" @if(old('call_reason') == $row->id) selected @endif> {{ $row->description }} </option>
                  @endforeach                 
                </select>
            </div>
                        
            <!-- <div class="form-group">
             {!! Form::label('Call Reason') !!}
                {!! Form::textarea('call_reason', null, ['class' => 'form-control', 'placeholder' => __('Call Reason'), 'rows' => '3']) !!}

            </div> 
            -->
    
             <div class="form-group">
               {!! Form::label('Choose Status') !!}
                <?php $description = \App\Models\Employerstatus::all(); ?>
                <select id="val_select" name="description" class="form-control select2">
                  <option value="{{ request()->input('description')}}">{{ request()->input('description')}}</option>
                  @foreach($description as $row)
                  <option value="{{ $row->id }}" @if(old('description') == $row->id) selected @endif> {{ $row->description }} </option>
                  @endforeach                 
                </select>
            </div>

            <div class="form-group">
            {!! Form::label('Call Back Date') !!}
            {!! Form::date('call_back_date', null, 
                array('null', 
                      'class'=>'form-control', 
                      'placeholder'=>'Call BackDate')) !!}
             </div>

           <div class="form-group">
             {!! Form::label('Meeting') !!}
             {!! Form::textarea('meeting', null, ['class' => 'form-control', 'placeholder' => __('Meeting'), 'rows' => '1']) !!}
            </div> 

            <div class="form-group">
             {!! Form::label('Remark') !!}
                {!! Form::textarea('remark', null, ['class' => 'form-control', 'placeholder' => __('Remark'), 'rows' => '3']) !!}
            </div>

            <div class="form-group">                
              <select class="form-control select2" name="email_sent">
                <option value="{{ request()->input('email_sent')}}">Choose Email Sent? </option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>                              
             </select>
             </div>  

            {!! Form::submit(__('Create'), ['class' => 'btn btn-primary pull-right']) !!}
        {!! Form::close() !!}


  </div>
<style>
  .dataBox{
 
}

.dataBox span{
  float: left; 
    width: 100%;
    padding: 3px 10px;
    border-bottom: 1px solid var(--gray3);
    cursor: pointer;
}
</style>
@stop