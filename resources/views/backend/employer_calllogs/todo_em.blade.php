@extends('layouts.dashboard')


@section('section')
<br><br><br>

<div class="row">
  <div class="col-md-12">
    <center><h2 class="" style="display: inline-block;">Employer Todo List</h2></center>

  </div>
</div>
 
<div class="row">




    <div class="col-sm-12 search-fix">

   
        <div class="col-md-12 alert alert-success alert-block">
     You search for
          @isset($js_id)
          ID : <b>{{$js_id}} </b>
          @endisset

          @isset($fullname)
          Full Name : <b>{{$fullname}} </b>
          @endisset
          
          @isset($call_reason)
          Call Reason : <b>{{$call_reason}} </b>
          @endisset
  
          @isset($remark_calllog)    
          Remark : <b>{{$remark_calllog}} </b>
          @endisset

          @isset($type_of_call)
          Type of Call : <b>{{$type_of_call}} </b>
          @endisset

          @isset($caller_receiver)
          Caller Receiver : <b>{{$caller_receiver}} </b>
          @endisset

          @isset($call_back_date)
          Call Back Date : <b>{{$call_back_date}} </b> 
          @endisset
   
          @isset($created_at)
          Created Date : <b>{{$created_at}} </b>
          @endisset

 </div>
</div>
<div class="row">


  <div class="container-fluid">
    <a href="{{ route('employercalllogs.create') }}" class="btn btn-primary pull-right">Create Employer Call Log</a>
       <!--  <input type="button" value="Refresh Page" onClick="window.location.reload()" class="btn btn-flat btn-primary pull-right"> -->
    <a href="{{ URL::to('downloadExcelEMPCL/xls') }}"><button class="btn btn-success pull-right">Excel xls</button></a>
    <a href="{{ URL::to('downloadExcelEMPCL/csv') }}"><button class="btn btn-success pull-right"> CSV</button></a>
    
    <button class="btn btn-default for-hide-search-form">Show Search Form</button>
  </div>
    <br>

<div class="row">

            <div class="col-md-12">

                <div class="box box-body">
                    {!! Form::open(['url'=>'/todo-em','method'=>'GET', 'class'=>'form', 'id'=>'search_data']) !!}
      
                        <div class="form-group col-md-6">    
                            <input type="text" name="employer_id" placeholder="Employer Id" value="{{ request()->input('employer_id')}}" class="form-control"/>
                        </div>

                        <div class="form-group col-md-6">
                            <input type="text" name="employer_name" placeholder="Employer Name" value="{{ request()->input('employer_name')}}" class="form-control"/>
                        </div>
                    
                        <div class="form-group col-md-6">
                            <?php $call_reason = \App\Models\Employercallreason::all(); ?>
                            <select id="val_select" name="call_reason" class="form-control select2">
                                <option value="{{ request()->input('call_reason')}}">
                                <?php $label =request()->input('call_reason');
                                if(isset($label))
                                  echo $label;
                                else
                                  echo "Choose Call Reason";
                               ?></option>
                                @foreach($call_reason as $row)
                                      <option value="{{ $row->description }}" @if(old('call_reason') == $row->description) selected @endif> {{ $row->description }} </option>
                                @endforeach
                            </select>
                        </div>                       

                         <div class="form-group col-md-6">
                            <?php $status = \App\Models\Employerstatus::all(); ?>
                            <select id="val_select" name="status" class="form-control select2">
                                <option value="{{ request()->input('status')}}">
                                <?php $label =request()->input('status');
                                if(isset($label))
                                  echo $label;
                                else
                                  echo "Choose Status";
                               ?></option>
                                @foreach($status as $row)
                                      <option value="{{ $row->description }}" @if(old('call_reason') == $row->description) selected @endif> {{ $row->description }} </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-6">    
                            <input type="text" name="remark" placeholder="Remark" value="{{ request()->input('remark')}}" class="form-control"/>
                        </div>

                        <div class="form-group col-md-6">
                          <select class="form-control select2" name="type_of_call">
                            <option value="{{ request()->input('type_of_call')}}">
                            <?php $label =request()->input('type_of_call');
                              if(isset($label))
                                echo $label;
                              else
                                echo "Type of Call";

                             ?></option>
                            <option value="Incoming Call">Incoming Call</option>
                            <option value="Outgoing Call">Outgoing Call</option>                              
                          </select>
                        </div>

                       <div class="form-group col-md-6">
                            <?php $authors = \App\User::all(); ?>
                            <select id="val_select" name="caller_receiver" class="form-control select2">
                                <option value="{{ request()->input('caller_receiver')}}">
                                <?php $label =request()->input('caller_receiver');
                                  if(isset($label))
                                    echo $label;
                                  else
                                    echo "Caller Receiver";

                                 ?></option>
                                @foreach($authors as $row)
                                      <option value="{{ $row->name }}" @if(old('caller_receiver') == $row->name) selected @endif> {{ $row->name }} </option>
                                @endforeach
                            </select>
                        </div>
                     
                        <div class="form-group col-md-4">
                            <a href="{{url('todo-em')}}" class="btn btn-flat btn-danger">Reset</a>
                            <button class="btn btn-flat btn-primary">Search</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>

    <table class="table table-bordered">
      <thead>
          <tr style="background-color: #99ccff">
              <th>Action</th>
              <th>Call log ID</th>
              <th>Employer ID</th>
              <th>Type of Call</th>
              <th>Name</th>
              <th>Call Back Date</th>
              <th>Status</th>
              <th>Start Time</th>
              <th>Receiver</th>
              <th>Ph Number</th>
              <th>Industrial Zone</th>
              <th>Call Reason</th>                     
              <th>Meeting</th>      
              <th>Remark</th>
              <th>Email Sent?</th>
              <th>Caller / Receiver</th>

      </thead>
      <tbody>

        @foreach($employers as $employer )
       
         @if($employer->call_back_date!=null)
         <tr class="" style="background-color:#cce6ff;">
             <td><a href="{{ route('employers.show', $employer->employer_id) }}" class="btn btn-info btn-xs" target="_blank"><i class="glyphicon glyphicon-eye-open"></i> </a></td>
             <td>{{$employer->id}}</td>
             <td>{{$employer->employer['id']}}</td>
              <td>{{ $employer->type_of_call }}</td>
              <td><a href="{{ route('employers.show', $employer->employer_id) }}" target="_blank">{{ $employer->employer['employer_name'] }}</a></td>
              <td>{{Carbon\Carbon::parse($employer->call_back_date)->format('j-M-y')}}</td>
              <td>{{ $employer->Mystatus['description'] }}</td>
              <td>{{ $employer->created_at }}</td>
              <td>{{ $employer->receiver }}</td>
              <td>{{ $employer->ph_number }}</td>
              <td>{{ $employer->industry_zone }}</td>
              <td>{{ $employer->Myempcallreason['description'] }}</td>
              <td>{{ $employer->meeting }}</td>
              <td>{{ $employer->remark }}</td>
              <td>{{ $employer->email_sent }}</td>
              <td>{{ $employer->author['name'] }}</td> 

        </tr>
          @endif
         @endforeach
     
      </tbody>
    </table> 

    </div>
    </div>
         <nav>
       <ul class="pagination">
       <li><?php //  echo $jobseekers->render(); ?></li>
       </ul>
     </nav>

  
  </div>
</div>
</div>
@stop