@extends('layouts.dashboard')

@section('section')

<br>
<br>
<br>
<div class="row">              
              @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div>
                @endif
        <div class="col-md-6">
          <form action="{{ route('skill_1.store')}}" method="POST">
            {{ csrf_field() }}

              <div class="form-group">
                <label for="title"> Skill 1 Name:</label>
                <input type="text" name="name" id="title" class="form-control" value="">
                  
              </div>


                                  
                      <div class="form-group pull-right">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                                
                            </div>
                        </div>
                          </form>         
        
      </div> 
     

  <div class="col-md-6">

    <table class="table table-bordered">
      <thead>
        <tr>
     
                        <th>Action</th>
                        <th>ID</th>
                        <th>Skill 1 Name</th>
  

      </thead>
      <tbody>
             @foreach($skills as $row )
        <tr class="success">
            <td>  <a href="{{ route('skill_1.edit', $row->id) }}" class="btn btn-success btn-xs">Edit</a></td>
              <td>{{ $row->id }}</td>
              <td>{{ $row->name }}</td>   
        </tr>

         @endforeach
      </tbody>
    </table>  
     
</div>
<div class="row">
<div class="col-md-6 pull-right">
      <nav>
       <ul class="pagination">
       <li><?php echo $skills->render();?></li>
       </ul>
     </nav>
</div>
</div>
</div>

 </div>

  @stop