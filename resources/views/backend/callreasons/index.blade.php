@extends('layouts.dashboard')

@section('section')
<br>
<br>

<div class="row">              
              @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div>
                @endif
			  <div class="col-md-6">
			    <form action="{{ route('callreasons.store')}}" method="POST">
			      {{ csrf_field() }}

				      <div class="form-group">
				        <label for="title">Call Reason:</label>
				        <input type="text" name="description" id="title" class="form-control" value="">  
				      </div>
                                  
              <div class="form-group pull-right">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Create
                    </button>    
                </div>
              </div>

          </form>         
        
      </div> 
     

  <div class="col-md-6">

    <table class="table table-bordered">
      <thead>
        <tr>

          <th>Action</th>
          <th>ID</th>
          <th>Call Reason</th>

      </thead>
      <tbody>
             @foreach($callreasons as $callreason )
            <tr class="success">
    
              <td>  <a href="{{ route('callreasons.edit', $callreason->id) }}" class="btn btn-success btn-xs">Edit</a></td>
              <td>{{ $callreason->id }}</td>
              <td>{{ $callreason->description }}</td>   
    
             </tr>

             @endforeach
      </tbody>
    </table>  
          <nav>
       <ul class="pagination">
       <li><?php echo $callreasons->render();?></li>
       </ul>
     </nav>
</div>
 
</div>

 </div>

  @stop