@extends('layouts.dashboard')

@section('section')
<br>
<br>
<br>
<div class="row">
  <div class="col-md-6">
  
    <form action="{{ route('callreasons.update', $callreasons->id) }}" method="POST">
      {{ csrf_field() }}
      <div class="form-group">
        <label for="title">Callreason :</label>
        <input type="text" name="description" id="title" class="form-control" value="{{ $callreasons->description }}">
          
      </div>

       <div class="form-group"> 
        <form action="{{ route('callreasons.update', $callreasons->id) }}"
        >
          {{ csrf_field() }}
          {{ method_field("patch") }}
          <button type="submit" class="btn btn-primary">Update Info</button>
        </form>
        
      </div> 
      </div>
    </form>
  </div>

</div>
  
  @stop