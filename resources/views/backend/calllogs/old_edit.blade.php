@extends('layouts.dashboard')


@section('section')
<br>
<br>
<br>
<div class="row">
    <a class="btn btn-success pull-left" href="{{ route('todo.index')}}">Back</a>
</div>

<div class="row">
  <div class="col-md-6">
    <form action="{{ route('calllogs.update', $calllogs->id) }}" method="POST">
      {{ csrf_field() }}


       <div class="form-group">
            {!! Form::label('Job Seeker ID') !!}
            {!! Form::text('jobseeker_id', $calllogs->jobseeker_id, 
                array('required', 
                      'class'=>'form-control', 'readonly' => 'true',
                      'placeholder'=>'Job Seeker ID')) !!}
        </div>

       <div class="form-group">
            {!! Form::label('Job Seeker Name') !!}
            {!! Form::text('fullname', $calllogs->jobseeker['fullname'], 
                array('required', 
                      'class'=>'form-control', 'readonly' => 'true',
                      'placeholder'=>'Job Seeker Name')) !!}
        </div>

        <div class="form-group">
                  {!! Form::label('Phone Number') !!}
                {!! Form::textarea('phone_number', $calllogs->phone_number, ['class' => 'form-control', 'placeholder' => __('Phone Number'), 'rows' => '1']) !!}

         </div>
                  
         <div class="form-group">             
            <select class="form-control select2" name="type_of_call">
              <option value="">Choose Type of Call</option>
              <option value="Incoming Call">Incoming Call</option>
              <option value="Outgoing Call">Outgoing Call</option>                              
            </select>
          </div>


          <div class="form-group">
            {!! Form::label('Call Back Date') !!}
            {!! Form::date('call_back_date', $calllogs->call_back_date, 
                array('null', 
                      'class'=>'form-control', 
                      'placeholder'=>'Call BackDate')) !!}
          </div>

           <div class="form-group">
            {!! Form::label('Start Time') !!}
            <input name="start_time" class="form-control" value="{{$calllogs->start_time}}" type="text" id="time"/>
           </div>

            <div class="form-group">
            {!! Form::label('Minute') !!}
            {!! Form::text('minute', $calllogs->minute, 
                array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'Minute')) !!}
             </div>

             <div class="form-group">
            {!! Form::label('Second') !!}
            {!! Form::text('second', $calllogs->second, 
                array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'Second')) !!}
             </div>            

            <div class="form-group">
             {!! Form::label('Call Reason') !!}
                {!! Form::textarea('call_reason', $calllogs->call_reason, ['class' => 'form-control', 'placeholder' => __('Call Reason'), 'rows' => '3']) !!}

            </div>

             <div class="form-group">
              {!! Form::label('Remark Calllog') !!}
              {!! Form::text('remark_calllog', $calllogs->remark_calllog, 
                  array('required', 
                        'class'=>'form-control', 
                        'placeholder'=>'Remark Calllog')) !!}
             </div> 

              <div class="form-group">
              <label for="title"> Status:</label>
               <?php $status = \App\Models\Status::all(); ?>
                  <select id="val_select" name="status" class="form-control select2">
                      <option value="{{ $calllogs->status }}">{{ $calllogs->mystatus['description'] }}</option>
                      @foreach($status as $row)
                          <option value="{{ $row->id }}">
                              {{ $row->description }} 
                          </option>
                      @endforeach
                  </select>
              </div>

              <div class="form-group pull-right">
                  <select name="published_at" class="form-control select2 pull-right">
                    <option value="1">archive</option>
                   
                    <option value="" selected>Archive</option>
                  </select>
              </div>

             <div class="form-group"> 
              <form action="{{ route('calllogs.update', $calllogs->id) }}">
                {{ csrf_field() }}
                {{ method_field("patch") }}
                <button type="submit" class="btn btn-primary">Update Info</button>
              </form>
              </div> 
      </div>
    </form>
  </div>

</div>
  
  @stop