@extends('layouts.dashboard')


@section('section')
<br>
<br>
<br>


<div class="row">
  <div class="col-md-6">
    <form action="{{ route('calllogs.update', $calllogs->id) }}" method="POST">
      {{ csrf_field() }}
        <input type="hidden" value="{{$calllogs->joinToPivot()['jobposition_id']}}" name="job_position">
        <div class="form-group">
            {!! Form::hidden('jobseeker_id', $calllogs->jobseeker_id, 
                array('required', 
                      'class'=>'form-control', 'readonly' => 'true',
                      'placeholder'=>'Job Seeker ID')) !!}
        </div>

        <div class="form-group">
            {!! Form::label('Job Seeker Name') !!}
            {!! Form::text('fullname', $calllogs->jobseeker['fullname'], 
                array('required', 
                      'class'=>'form-control', 'readonly' => 'true',
                      'placeholder'=>'Job Seeker Name')) !!}
        </div>

        <div class="form-group">
                  {!! Form::label('Phone Number') !!}
                  {!! Form::text('phone_number', $calllogs->jobseeker['phone'], 
                array('required', 
                      'class'=>'form-control', 'readonly' => 'true',
                      'placeholder'=>'Phone Number')) !!}

         </div>
                  
         <div class="form-group">       
          {!! Form::label('Type of Call') !!}      
          <?php $toc = ['Incoming Call'=>'Incoming Call','Outgoing Call'=>'Outgoing Call'] ?>       
            {{ Form::select('type_of_call',$toc,$calllogs->type_of_call,['class'=>'form-control','placeholder'=>'Choose type of call']) }}
          </div>


          <div class="form-group">
            {!! Form::label('Call Back Date') !!}
            {!! Form::date('call_back_date', $calllogs->call_back_date, 
                array('null', 
                      'class'=>'form-control', 
                      'placeholder'=>'Call BackDate')) !!}
          </div>
          <div class="form-group">
            <label for="title">Select Stage:</label>
            <select name="stage" class="form-control" onclick="setStatus(this.value)">
              <option value="">--- Select Stage ---</option>
              @foreach ($stages as $key => $value)
              <option value="{{ $key }}" {{$calllogs->joinToPivot()['stage']==$key?'selected':''}}>{{ $value }}</option>
              @endforeach
            </select>
          </div>

          <div class="form-group">
            <label for="title">Select Status:</label>
            <select name="status" class="form-control" required="required" id="status">
                @foreach ($statuses as $key => $value)
                  <option value="{{ $key }}" {{$calllogs->status==$key?'selected':''}}>{{ $value }}</option>
                @endforeach
            </select>
          </div>
           <div class="form-group">
              {!! Form::label('Interview Time') !!}
              {!! Form::text('interview_time',$calllogs->joinToPivot()['interview_time'],['class'=>'form-control time1']) !!}
            </div>
          <!--  <div class="form-group">
            {!! Form::label('Start Time') !!}
            <input name="start_time" class="form-control" value="{{$calllogs->start_time}}" type="text" id="time"/>
           </div> -->

            <div class="form-group">
            {!! Form::label('Minute') !!}
            {!! Form::text('minute', $calllogs->minute, 
                array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'Minute')) !!}
             </div>

             <div class="form-group">
            {!! Form::label('Second') !!}
            {!! Form::text('second', $calllogs->second, 
                array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'Second')) !!}
             </div>            
           
            <div class="form-group">

             {!! Form::label('Remark Calllog') !!}
                {!! Form::textarea('remark_calllog', $calllogs->remark_calllog, ['class' => 'form-control', 'placeholder' => __('Remark Calllog'), 'rows' => '3']) !!}

            </div>

            <!--  <div class="form-group">
              {!! Form::label('Remark Calllog') !!}
              {!! Form::text('remark_calllog', $calllogs->remark_calllog, 
                  array('required', 
                        'class'=>'form-control', 
                        'placeholder'=>'Remark Calllog')) !!}
             </div>  -->

             <!--  <div class="form-group pull-right">
                  <select name="published_at" class="form-control select2 pull-right">
                    <option value="1">archive</option>
                   
                    <option value="" selected>Archive</option>
                  </select>
              </div> -->

             <div class="form-group">      
                <button type="submit" class="btn btn-primary pull-right">Update Info</button>
                 <a class="btn btn-success pull-left" href="{{ route('calllogs.index')}}">Back</a>
              </form>
              </div> 
      </div>
    </form>
  </div>

</div>
  <script>
    $('.time1').datetimepicker({
      format: 'YYYY-MM-DD HH:mm:ss'
    });
  </script>
  <script src="{{asset('js/phyo.js')}}"></script>
  @stop