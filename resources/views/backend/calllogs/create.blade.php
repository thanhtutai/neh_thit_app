@extends('layouts.dashboard')
@section('page_heading','Call Log Create')

@section('section')

<br>
<br>
<br>


    <div class="row">

              @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div>
                @endif
      <div class="col-md-6">

		  <form action="{{ route('calllogs.store') }}" method="POST">
      {{ csrf_field() }}
            <div class="form-group">
            {!! Form::label('Job Seeker ID') !!}
            {!! Form::text('jobseeker_id', null, 
                array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'Job Seeker ID')) !!}
             </div>
            <div class="form-group">
              <label>Type of call</label>
              <select class="form-control select2" name="type_of_call">
                <option value="">Choose Type of Call</option>
                <option value="Incoming Call">Incoming Call</option>
                <option value="Outgoing Call">Outgoing Call</option>                              
               </select>
            </div>
        

             <div class="form-group">
              {!! Form::label('Call Back Date') !!}
              {!! Form::date('call_back_date', null, 
                array('null', 
                      'class'=>'form-control', 
                      'placeholder'=>'Call Back Date')) !!}
             </div>

            <div class="form-group">
            {!! Form::label('Minute') !!}
            {!! Form::text('minute', null, 
                array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'Minute')) !!}
             </div>

             <div class="form-group">
              {!! Form::label('Second') !!}
              {!! Form::text('second', null, 
                array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'Second')) !!}
             </div>  

            <div class="form-group">
                {!! Form::label('Phone Number') !!}
                {!! Form::textarea('phone_number', null, ['class' => 'form-control', 'placeholder' => __('Phone Number'), 'rows' => '1']) !!}

            </div>


             <div class="form-group">
               {!! Form::label('Choose Call Reason') !!}
                <?php $status = \App\Models\Callreason::all(); ?>
                <select id="val_select" name="call_reason" class="form-control select2">
                  <option value="{{ request()->input('call_reason')}}">{{ request()->input('call_reason')}}</option>
                  @foreach($status as $row)
                  <option value="{{ $row->id }}" @if(old('status') == $row->id) selected @endif> {{ $row->description }} </option>
                  @endforeach                 
                </select>
            </div>
                          
            <div class="form-group">
             {!! Form::label('Remark Calllog') !!}
                {!! Form::textarea('remark_calllog', null, ['class' => 'form-control', 'placeholder' => __('Remark Calllog'), 'rows' => '3']) !!}

            </div>        
              
            <div class="form-group">
                 <!-- {!! Form::label('Caller Receiver') !!} -->
                 <?php $status = \App\Models\Status::all(); ?>
                  <select id="val_select" name="status" class="form-control select2">
                      <option value="{{ request()->input('status')}}">
                      <?php $label =request()->input('status');
                        if(isset($label))
                          echo $label;
                        else
                          echo "Choose Status";
                       ?>
                      </option>
                      @foreach($status as $row)
                            <option value="{{ $row->description }}" @if(old('status') == $row->description) selected @endif> {{ $row->description }} </option>
                      @endforeach
                  </select>
              </div>

           <div class="form-group"> 
          				<button type="submit" class="btn btn-success">Create Call Log</button>
           </div>
      </form>
    </div>

@stop