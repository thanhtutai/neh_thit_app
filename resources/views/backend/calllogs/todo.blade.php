@extends('layouts.dashboard')

@section('section')

<style type="text/css">
  .search-fix {
  overflow: hidden;

  position: fixed;
  top: 60px;
  width: 100%;

}
</style>
<br><br><br>

<div class="row">

  <div class="col-md-12">
    <center><h2 class="" style="display: inline-block;">Job Seeker Todo List  </h2></center>

  </div>

   @if(!empty($js_id) || !empty($fullname))
        <div class="col-md-12 alert alert-success alert-block search-fix">
          @isset($js_id)
            ID : <b>{{$js_id}} </b>
          @endisset
          
          @isset($fullname)
            Full Name : <b>{{$fullname}} </b>
          @endisset
          
          @isset($call_reason)
            Call Reason : <b>{{$call_reason}} </b>
          @endisset
          
          @isset($remark_calllog)
            Remark : <b>{{$remark_calllog}} </b>
          @endisset

          @isset($type_of_call)
            Type of Call : <b>{{$type_of_call}} </b>
          @endisset

          @isset($caller_receiver)
            Caller Receiver : <b>{{$caller_receiver}} </b>
          @endisset        

          @isset($status)
            Status : <b>{{$status}} </b>
          @endisset

          @isset($call_back_date)
            Call Back Date : <b>{{$call_back_date}} </b>
          @endisset
          
          @isset($created_at)
            Created Date : <b>{{$created_at}} </b>
          @endisset

      @if ($message = Session::get('success'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
      </div>
      @endif

 </div>
 @endif
</div>

<div class="container-fluid">
    <a href="{{ route('calllogs.create') }}" class="btn btn-primary pull-right">Create New</a>
    
    <a href="{{ URL::to('downloadExcelCL/xls') }}"><button class="btn btn-success pull-right">Excel xls</button></a>
    <a href="{{ URL::to('downloadExcelCL/csv') }}"><button class="btn btn-success pull-right"> CSV</button></a>
    <button class="btn btn-default for-hide-search-form">Show Search Form</button>
</div>
<br>

<div class="row">
  <div class="container-fluid">
      <div class="row">
            <div class="col-md-12">

                <div class="box box-body">
                  {!! Form::open(['url'=>'/todo','method'=>'GET', 'class'=>'form', 'id'=>'search_data']) !!}
      
                      <div class="form-group col-md-4">      
                            <input type="text" name="js_id" placeholder="ID" value="{{ request()->input('js_id')}}" class="form-control"/>
                      </div>

                      <div class="form-group col-md-4">                         
                            <input type="text" name="fullname" placeholder="Full Name" value="{{ request()->input('fullname')}}" class="form-control"/>
                      </div>

                      <div class="form-group col-md-4">
                        <!-- {!! Form::label('Caller Receiver') !!} -->
                       <?php $call_reason = \App\Models\Callreason::all(); ?>
                            <select id="val_select" name="call_reason" class="form-control select2">
                                <option value="{{ request()->input('call_reason')}}">
                                <?php $label =request()->input('call_reason');
                                if(isset($label))
                                  echo $label;
                                else
                                  echo "Choose Call Reason";
                               ?></option>
                                @foreach($call_reason as $row)
                                      <option value="{{ $row->description }}" @if(old('call_reason') == $row->description) selected @endif> {{ $row->description }} </option>
                                @endforeach
                            </select>
                        </div>

                      <div class="form-group col-md-4">                          
                        <input type="text" name="remark_calllog" placeholder="Remark" value="{{ request()->input('remark_calllog')}}" class="form-control"/>
                      </div>

                      <div class="form-group col-md-4">
                                    <!-- {!! Form::label('Type of Call') !!} -->
                            <select class="form-control select2" name="type_of_call">
                              <option value="{{ request()->input('type_of_call')}}">
                              <?php $label =request()->input('type_of_call');
                                if(isset($label))
                                  echo $label;
                                else
                                  echo "Type of Call ";
                               ?></option>
                              <option value="Incoming Call">Incoming Call</option>
                              <option value="Outgoing Call">Outgoing Call</option>                              
                           </select>
                        </div>

                       <div class="form-group col-md-4">
                        <!-- {!! Form::label('Caller Receiver') !!} -->
                            <?php $status = \App\Models\Status::all(); ?>
                            <select id="val_select" name="status" class="form-control select2">
                                <option value="{{ request()->input('status')}}">
                                <?php $label =request()->input('status');
                                if(isset($label))
                                  echo $label;
                                else
                                  echo "Choose Status";
                               ?></option>
                                @foreach($status as $row)
                                      <option value="{{ $row->description }}" @if(old('status') == $row->description) selected @endif> {{ $row->description }} </option>
                                @endforeach
                            </select>
                        </div>


                    <div class="form-group col-md-4">
                        {!! Form::label('Caller Receiver') !!}
                            <?php $authors = \App\User::all(); ?>
                          <select id="val_select" name="caller_receiver" class=form-control select2">
                             <option value="{{ request()->input('caller_receiver')}}">
                              <?php $label =request()->input('caller_receiver');
                              if(isset($label))
                                echo $label;
                              else
                                echo "Caller Receiver";
                             ?>                               
                             </option>
                             @foreach($authors as $row)
                                 <option value="{{ $row->name }}" @if(old('caller_receiver') == $row->name) selected @endif> {{ $row->name }} </option>
                             @endforeach
                            </select>
                    </div>

                    <div class="form-group col-md-4">
                    {!! Form::label('Call back Date') !!}
                    {!! Form::date('call_back_date', null, 
                        array('null', 
                              'class'=>'form-control', 
                              'placeholder'=>'Call Back Date')) !!}
                     </div>

                     <div class="form-group col-md-4">
                     {!! Form::label('Created Date') !!}
                     {!! Form::date('created_at', null, 
                        array('null', 
                              'class'=>'form-control', 
                              'placeholder'=>'Created Date')) !!}
                     </div>
                     
                      <div class="form-group col-md-4">
                          <a href="{{url('todo')}}" class="btn btn-flat btn-danger">Reset</a>
                          <button class="btn btn-flat btn-primary">Search</button>
                      </div>

                    {!! Form::close() !!}
                </div>
            </div>




    <table>
      <thead>
        <tr style="background-color: #99ccff">
          <th>ID</th>
          <th >Status</th>
          <th>Call Back Date</th>
          <th>Start Time</th>
          <th>Type of Call</th>
          <th>Name</th>
          <th>FB Name</th>
          <th>Signup Date</th>
          <th>Ph Number</th>
          <th>Call Reason</th>
          <th>Interviewed Date</th>
          <th>Interviewed Time</th>
          <th>Remark</th>
          <th>Caller / Receiver</th>
      </thead>
      <tbody>

        @foreach($jobseekers as $jobseeker )
  
        <?php $cl_js=App\Models\Calllog::where('id',$jobseeker->myid)->first(); ?>
        <tr class="" style="background-color:#cce6ff;">


            
             <?php  $js=App\Models\Jobseeker::where('id',$cl_js['jobseeker_id'])->first(); 
                 
                    $user=App\User::where('id',$cl_js['caller_receiver'])->first();
                    $status=App\Models\Status::where('id',$js['status'])->first();
             ?>
           
             <td> {{$js['jobseeker_id']}}</td>
             <td>  {{$status['description']}}</td>
             <td>  {{Carbon\Carbon::parse($cl_js['call_back_date'])->format('j-M-y')}}</td>
             <td>  {{$cl_js['start_time']}}</td>
             <td>  {{$cl_js['type_of_call']}}</td>
             <td>  <a href="/jobseekers/show/{{$js['jobseeker_id']}}" target="_blank">{{$js['fullname']}}</a></td>
             <td>  <a href="/jobseekers/show/{{$js['jobseeker_id']}}" target="_blank">{{$js['fb_name']}}</a></td>
             <td>  {{Carbon\Carbon::parse($js['date'])->format('j-M-y')}}</td>
             <td>  {{$cl_js['phone_number']}}</td>
             <td>  {{$cl_js['call_reason']}}</td>
             <td>  {{$cl_js['interview_date']}}</td>
             <td>  {{$cl_js['interview_time']}}</td>
             <td>  {{$cl_js['remark_calllog']}}</td>
             <td>  {{$user['name'] }}</td>
        </tr>
         @endforeach
      </tbody>
    </table> 

    </div>
    </div>
     <nav>
       <ul class="pagination">
       <li><?php //  echo $jobseekers->render(); ?></li>
       </ul>
     </nav>
  </div>
</div>
</div>
<style type="text/css">
  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
width: 2000px;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

th.last-called-date{
  width: 100px;
}
th.call-back-date{
  width: 100px;
}
.action2{
  display: inline-block;
}
</style>

@stop