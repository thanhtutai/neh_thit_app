@extends('layouts.dashboard')
@section('page_heading','Tables')

@section('section')

<div class="row">
  <div class="col-sm-12">
    @section ('cotable_panel_title','Coloured Table')
    @section ('cotable_panel_body')
       <div class="table-responsive">

<div class="row">
            <div class="col-md-12">
                <div class="box box-body">
                    {!! Form::open(['url'=>'/newjobseekers','method'=>'GET', 'class'=>'form', 'id'=>'search_data']) !!}
      
                        <div class="form-group col-md-4">
                            
                            <input type="text" name="fullname" placeholder="Name" value="{{ old('fullname') }}" class="form-control"/>
                        </div>
                    
                        <div class="form-group col-md-4">
                            <input placeholder="Age" type="text" name="age" value="{{ old('age') }}" class="form-control"/>
                        </div>

                     
                       <div class="form-group col-md-4">
                            <?php $township = \App\Township::all(); ?>
                                        <select id="val_select" name="township" class="form-control select2">
                                            <option value="{{ old('township') }}">Select Township</option>
                                            @foreach($township as $row)
                                                  <option value="{{ $row->name }}" @if(old('township') == $row->name) selected @endif> {{ $row->name }} </option>
                                            @endforeach

                  
                                        </select>
                        </div>

                       <div class="form-group col-md-4">
                            <?php $desire_position_1 = \App\Desireposition::all(); ?>
                                        <select id="val_select" name="desire_position_1" class="form-control select2">
                                            <option value="{{ old('desire_position_1') }}">Select DesirePosition</option>
                                            @foreach($desire_position_1 as $row)
                                                  <option value="{{ $row->desire_position_1_name }}" @if(old('desire_position_1') == $row->desire_position_1_name) selected @endif> {{ $row->desire_position_1_name }} </option>
                                            @endforeach

                  
                                        </select>
                        </div>

                       <div class="form-group col-md-4">
                            <?php $desire_position_2 = \App\DesirePosition_2::all(); ?>
                                        <select id="val_select" name="desire_position_2" class="form-control select2">
                                            <option value="{{ old('desire_position_2') }}">Select DesirePosition 2</option>
                                            @foreach($desire_position_2 as $row)
                                                  <option value="{{ $row->desire_position_2_name }}" @if(old('desire_position_2') == $row->desire_position_2_name) selected @endif> {{ $row->desire_position_2_name }} </option>
                                            @endforeach

                  
                                        </select>
                        </div>


                       <div class="form-group col-md-4">
                            <?php $previous_position_1 = \App\PreviousPosition::all(); ?>
                                        <select id="val_select" name="previous_position_1" class="form-control select2">
                                            <option value="{{ old('previous_position_1') }}">Select Previous Position</option>
                                            @foreach($previous_position_1 as $row)
                                                  <option value="{{ $row->previous_position_1_name }}" @if(old('previous_position_1') == $row->previous_position_1_name) selected @endif> {{ $row->previous_position_1_name }} </option>
                                            @endforeach

                  
                                        </select>
                        </div>


                       <div class="form-group col-md-4">
                            <?php $previous_position_2 = \App\PreviousPosition_2::all(); ?>
                                        <select id="val_select" name="previous_position_2" class="form-control select2">
                                            <option value="{{ old('previous_position_2') }}">Select Previous Position 2</option>
                                            @foreach($previous_position_2 as $row)
                                                  <option value="{{ $row->previous_position_2_name }}" @if(old('previous_position_2') == $row->previous_position_2_name) selected @endif> {{ $row->previous_position_2_name }} </option>
                                            @endforeach

                  
                                        </select>
                        </div>


      
                       
                        <div class="form-group col-md-4">
                            <a href="{{url('newjobseekers')}}" class="btn btn-flat btn-danger">Reset</a>
                            <button class="btn btn-flat btn-primary">Search</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>




    <table class="table table-bordered">
      <thead>
        <tr>
                        <th>ID</th>
                        <th>Fb Name</th>
                        <th>FullName</th>
                        <th>Phone</th>
                        <th>Age</th>
                        <th>Township</th>
                        <th>Desire Position 1</th>
                        <th>Desire Position 2</th>
                        <th>Previous Position 1</th>
                        <th>Previous Position 2</th>
                        <th>Show</th>
                        <th>Send</th>
        </tr>
      </thead>
      <tbody>
        @foreach($jobseekers as $jobseeker )
               @empty($jobseeker->published_at)
        <tr class="success">
              <td>{{ $jobseeker->id }}</td>
              <td>{{ $jobseeker->fb_name }}</td>
              <td>{{ $jobseeker->fullname }} </td>
              <td>{{ $jobseeker->phone }} </td>
              <td>{{ $jobseeker->age }} </td>
             <td>{{$jobseeker->mytownship['mytownship_name']}}</td>
              <td> {{ \App\Desireposition::where('id',$jobseeker->desire_position_1)->pluck('desire_position_1_name')->first() }} </td> <td> {{ \App\DesirePosition_2::where('id',$jobseeker->desire_position_2)->pluck('desire_position_2_name')->first() }} </td>
              <td> {{ \App\PreviousPosition::where('id',$jobseeker->previous_position_1)->pluck('previous_position_1_name')->first() }} </td> 
              <td> {{ \App\PreviousPosition_2::where('id',$jobseeker->previous_position_2)->pluck('previous_position_2_name')->first() }} </td>    
              
             <td> <a href="{{ route('jobseekers.jobseeker_detail', $jobseeker->id) }}" class="btn btn-primary">Show</a></td>

<!--              <td> <a href="{{ route('jobseekers.mypdf', $jobseeker->id) }}" class="btn btn-primary">Show</a></td> -->

              <td> <a href="{{ route('jobseekers.send_cv', $jobseeker->id) }}" class="btn btn-success">Send</a></td>
        </tr>
        @endempty
         @endforeach
      </tbody>
    </table>  
    </div>
         <nav>
       <ul class="pagination">
       <li><?php echo $jobseekers->render(); ?></li>
       </ul>
     </nav>
    @endsection
    @include('widgets.panel', array('header'=>true, 'as'=>'cotable'))
  </div>
</div>
</div>
@stop