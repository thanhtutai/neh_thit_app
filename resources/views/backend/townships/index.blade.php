@extends('layouts.dashboard')

@section('section')
<br>
<br>
<br>

<div class="row">              
              @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div>
                @endif
			  <div class="col-md-6">
			    <form action="{{ route('townships.store')}}" method="POST">
			      {{ csrf_field() }}

				      <div class="form-group">
				        <label for="title">Township Name:</label>
				        <input type="text" name="name" id="title" class="form-control" value="">
				          
				      </div>


                                  
                      <div class="form-group pull-right">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                                
                            </div>
                        </div>
                          </form>         
        
      </div> 
     

  <div class="col-md-6">

    <table class="table table-bordered">
      <thead>
        <tr>
     
                        <th>Action</th>
                        <th>ID</th>
                        <th>Township Name</th>
  

      </thead>
      <tbody>
             @foreach($townships as $township )
        <tr class="success">
            
              <td>  <a href="{{ route('townships.edit', $township->id) }}" class="btn btn-success btn-xs">Edit</a></td>
              <td>{{ $township->id }}</td>
              <td>{{ $township->name }}</td>   
        </tr>

         @endforeach
      </tbody>
    </table>  
          <nav>
       <ul class="pagination">
       <li><?php echo $townships->render();?></li>
       </ul>
     </nav>
</div>
 
</div>

 </div>

  @stop