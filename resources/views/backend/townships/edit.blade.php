@extends('layouts.dashboard')


@section('section')
<br>
<br>
<br>
<div class="row">
  <div class="col-md-6">
  
    <form action="{{ route('townships.update', $townships->id) }}" method="POST">
      {{ csrf_field() }}
      <div class="form-group">
        <label for="title">Township Name:</label>
        <input type="text" name="name" id="title" class="form-control" value="{{ $townships->name }}">
          
      </div>


                               <div class="form-group"> 
                                <form action="{{ route('townships.update', $townships->id) }}"
                                >
                                  {{ csrf_field() }}
                                  {{ method_field("patch") }}
                                  <button type="submit" class="btn btn-primary">Update Info</button>
                                </form>

        
      </div> 
      </div>
    </form>
  </div>

</div>
  
  @stop