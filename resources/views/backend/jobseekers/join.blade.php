@extends('layouts.dashboard')


@section('section')
<br>
<br>
<br>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Custom Filter</h3>
    </div>
    <div class="panel-body">
        <form method="POST" id="search-form" class="form-inline" role="form">

            <div class="form-group">
                <label for="name">Full Name</label>
                <input type="text" class="form-control" name="fullname" id="name" placeholder="search name">
            </div>
            <div class="form-group">
                <label for="age">Age</label>
                <input type="text" class="form-control" name="age" id="age" placeholder="search age">
            </div>
             <div class="form-group">
                <?php $job_type = \App\Township::all() ?>
                                    <select id="township" name="township" data-md-selectize class="form-control select2">
                                        <option value="">Select Township</option>
                                        @foreach($job_type as $row)
                                            <option value="{{ $row->id }}">
                                                {{ $row->name }}
                                            </option>
                                        @endforeach
                                    </select>
                        </div>
                <div class="form-group">
         <?php $desireposition_1 = \App\Desireposition::all() ?>
                                    <select id="desire_position_1" name="desire_position_1" data-md-selectize class="form-control select2">
                                        <option value="">Select Desire Position 1</option>
                                        @foreach($desireposition_1 as $row)
                                            <option value="{{ $row->id }}">
                                                {{ $row->desire_position_1_name }}
                                            </option>
                                        @endforeach
                                    </select>
                        </div>          

            <div class="form-group">
         <?php $previous_position_1 = \App\PreviousPosition::all() ?>
                                    <select id="previous_position_1" name="previous_position_1" data-md-selectize class="form-control select2">
                                        <option value="">Select Previous Position 1</option>
                                        @foreach($previous_position_1 as $row)
                                            <option value="{{ $row->id }}">
                                                {{ $row->previous_position_1_name }}
                                            </option>
                                        @endforeach
                                    </select>
                        </div>

            <div class="form-group">
         <?php $previous_position_2 = \App\PreviousPosition_2::all() ?>
                                    <select id="previous_position_2" name="previous_position_2" data-md-selectize class="form-control select2">
                                        <option value="">Select Previous Position 2</option>
                                        @foreach($previous_position_2 as $row)
                                            <option value="{{ $row->id }}">
                                                {{ $row->previous_position_2_name }}
                                            </option>
                                        @endforeach
                                    </select>
                        </div>

                        
                       
</div>



            <button type="submit" class="btn btn-primary">Search</button>
        </form>
    </div>
</div>
<table id="users-table" class="table table-condensed">
    <thead>
        <tr>
            <th>Id</th>
            <th>Full Name</th>
            <th>Age</th>
            <th>Township</th>
            <th>Desire Position1</th>
            <th>Previous Position 1</th>
            <th>Previous Position 2</th>
            <th>Created At</th>
            <th>Updated At</th>
            <th>Option</th>
        </tr>
    </thead>
</table>
@stop
@push('scripts')
<script>

 var oTable = $('#users-table').DataTable({
        dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
            "<'row'<'col-xs-12't>>"+
            "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
        processing: true,
        serverSide: true,
        ajax: {
            url: '{!! route('jobseekers.datajoin') !!}',
            data: function (d) {
                d.fullname = $('input[name=fullname]').val();
                d.age = $('input[name=age]').val();
                d.township = $('select[name=township]').val();
                d.desire_position_1 = $('select[name=desire_position_1]').val();
                d.previous_position_1 = $('select[name=previous_position_1]').val();
                d.previous_position_2 = $('select[name=previous_position_2]').val();
            }
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'fullname', name: 'jobseekers.fullname'},
            {data: 'age', name: 'jobseekers.age'},
            {data: 'township', name: 'townships.name'},
            {data: 'desire_position_1_name', name: 'desirepositions.desire_position_1_name'},
            {data: 'previous_position_1', name: 'previous_positions.previous_position_1_name'},
            {data: 'previous_position_2', name: 'previous_position_2s.previous_position_2_name'},
            {data: 'created_at', name: 'created_at'},
            {data: 'updated_at', name: 'updated_at'},
         {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });

    $('#search-form').on('submit', function(e) {
        oTable.draw();
        e.preventDefault();
    });





</script>
@endpush
