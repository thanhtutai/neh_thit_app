@extends('layouts.dashboard')


@section('section')
<br>
<br>
<br>
<div class="row">
    <a class="btn btn-success pull-left" href="/jobseekers/show/{{$jobseekers->jobseeker_id}}">Back</a>
    </div>
      @if ($message = Session::get('success'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
      </div>
      @endif

  <div class="col-md-6">
    <form action="{{ route('jobseekers.update', $jobseekers->jobseeker_id) }}" method="POST">
      {{ csrf_field() }}
      <div class="form-group">
        <label for="title">Facebook Name:</label>
        <input type="text" name="fb_name" id="title" class="form-control" value="{{ $jobseekers->fb_name }}">
         </div>
      <div class="form-group">
        <label for="title">Name:</label>
        <input type="text" name="fullname" id="title" class="form-control" value="{{ $jobseekers->fullname }}"> 
      </div>
      <div class="form-group">
        <label for="title">Nationality:</label>
        <input type="text" name="nationality" id="title" class="form-control" value="{{ $jobseekers->nationality }}">    
      </div>
      <div class="form-group">
        <label for="title">NRC No:</label>
        <input type="text" name="nrc_no" id="title" class="form-control" value="{{ $jobseekers->nrc_no }}">    
      </div>

        <div class="form-group">
          <label for="title">Marital Status:</label>
            <select name="marital_status" class="form-control select2 pull-right">
              <option value="Single">Single</option>
              <option value="Married">Married</option>
              <option value="Divorced">Divorced</option>
             
              <option value="" selected>Choose Marital Status</option>
            </select>
         </div>
                       <br></br>

         <div class="form-group">
            <label for="title"> Status:</label>
           <!--  <?php $status = \App\Models\Status::all(); ?> -->
                <select id="val_select" name="active_inactive" class="form-control select2">
                   @if($jobseekers->active_inactive =="1")
                    <option  selected  value="1">Active</option>
                    <option value="0">
                            InActive </option>
                    @else
                        <option  selected  value="0">
                            InActive 
                    @endif    </option>
                    <option   value="1">Active</option>
                    
                </select>
         </div>

               <div class="form-group">
                <label for="title">Phone:</label>
                <input type="text" name="phone" id="title" class="form-control" value="{{ $jobseekers->phone }}">
              </div>

               <div class="form-group">
                <label for="title">Viber Number:</label>
                <input type="text" name="viber_number" id="title" class="form-control" value="{{ $jobseekers->viber_number }}">
              </div>

              <div class="form-group">
                <label for="title">Sign Up From:</label>
                <input type="text" name="sign_up_from" id="title" class="form-control" value="{{ $jobseekers->sign_up_from }}">
              </div>

              <div class="form-group>
               <label for="title">Gender:</label>
               <?php $gender = \App\Models\Gender::all(); ?>
                  <select id="val_select" name="gender" class="form-control select2">
                      <option value="{{ $jobseekers->gender }}">{{ $jobseekers->mygender['gender_description'] }}</option>
                      @foreach($gender as $row)
                          <option value="{{ $row->id }}">
                              {{ $row->gender_description }} 
                          </option>
                      @endforeach
                  </select>
              </div>


              <div class="form-group">
                <label for="title">Age:</label>
                <input type="text" name="age" id="title" class="form-control" value="{{ $jobseekers->age }}">
              </div>

               <div class="form-group">
                  <label for="title"> Ward:</label>
                   <?php $wards = \App\Models\Ward::all(); ?>
                      <select id="val_select" name="wards" class="form-control select2">
                          <option value="{{ $jobseekers->wards }}">{{ $jobseekers->myward['name'] }}</option>
                          @foreach($wards as $row)
                              <option value="{{ $row->id }}">
                                  {{ $row->name }} 
                              </option>
                          @endforeach
                      </select>
                  </div>


                <div class="form-group">
                    <label for="title">Bus Stop:</label>
                    <?php $bus_stops = \App\Models\Busstop::all(); ?>
                    <select id="val_select" name="bus_stops" class="form-control select2">
                        <option value="{{ $jobseekers->bus_stops }}">{{ $jobseekers->mybusstop['busstop'] }}</option>
                        @foreach($bus_stops as $row)
                            <option value="{{ $row->id }}">
                                {{ $row->busstop }} 
                            </option>
                        @endforeach
                    </select>
                </div>

               <div class="form-group">
                  <label for="title">Education:</label>
                  <?php $education = \App\Models\Education::all(); ?>
                  <select id="val_select" name="education" class="form-control select2">
                      <option value="{{ $jobseekers->education }}">{{ $jobseekers->myeducation['name_en'] }}</option>
                      @foreach($education as $row)
                          <option value="{{ $row->id }}">
                              {{ $row->name_en }} 
                          </option>
                      @endforeach
                  </select>
               </div>

                 
               <div class="form-group">
                  <label for="title">Previous Job Title 1:</label>
                  <?php $previous_position_1 = \App\Models\JobTitle::all(); ?>
                  <select id="val_select1" name="previous_position_1" class="form-control select2">
                      <option value="{{ $jobseekers->previous_position_1 }}">{{ \App\Models\JobTitle::where('job_title_key',$jobseekers->previous_position_1)->pluck('description')->first() }}  </option>id
                      @foreach($previous_position_1 as $row)
                          <option value="{{ $row->job_title_key }}">
                              {{ $row->description }} 
                          </option>
                      @endforeach
                  </select>
                </div>


               <div class="form-group">
                <label for="title">Experience Month:</label>
                <select name="experience_month_1" class="form-control">
                    <option value="">Select one</option>
                    @foreach(getExp() as $key=>$value)
                        <option value="{{$key}}" {{$jobseekers->experience_month_1==$key?'selected':''}}>{{$value}}</option>
                    @endforeach                    
                </select>
               </div>

              <div class="form-group">
                  <label for="title">Company Name 1:</label>
                  <input type="text" name="company_name_1" id="title" class="form-control" value="{{ $jobseekers->company_name_1 }}">    
              </div>   
               
              <div class="form-group">
                   <label for="title">Previous Job Title Other:</label>
                   <input type="text" name="previous_position_other" id="title" class="form-control" value="{{ $jobseekers->previous_position_other }}">
              </div>      


                 <div class="form-group">
                    <label for="title">Previous Job Title 2:</label>
                    <?php $previous_position_2 = \App\Models\JobTitle::all(); ?>
                    <select id="val_select1" name="previous_position_2" class="form-control select2">
                        <option value="{{ $jobseekers->previous_position_2 }}">{{ \App\Models\JobTitle::where('job_title_key',$jobseekers->previous_position_2)->pluck('description')->first() }}  </option>id
                        @foreach($previous_position_2 as $row)
                            <option value="{{ $row->job_title_key }}">
                                {{ $row->description }} 
                            </option>
                        @endforeach
                    </select>
                </div>


              <div class="form-group">
                  <label for="title">Company Name 2:</label>
                  <input type="text" name="company_name_2" id="title" class="form-control" value="{{ $jobseekers->company_name_2 }}">    
              </div>   

                <div class="form-group">
                  <label for="title">Previous Job Title Other 2:</label>
                  <input type="text" name="previous_position_other_2" id="title" class="form-control" value="{{ $jobseekers->previous_position_other_2 }}">
                </div>
                <div class="form-group">
                    <label for="title">Experience Months 2:</label>
                    <select name="experience_month_2" class="form-control">
                      <option value="">Select one</option>
                      @foreach(getExp() as $key=>$value)
                          <option value="{{$key}}" {{$jobseekers->experience_month_2==$key?'selected':''}}>{{$value}}</option>
                      @endforeach                    
                    </select>
                </div>                         
                 <div class="form-group">
                    <label for="title">Desire Job Title 1:</label>
                    <?php $desire_position_1 = \App\Models\JobTitle::all(); ?>
                    <select id="val_select" name="desire_position_1" class="form-control select2">
                        <option value="{{ $jobseekers->desire_position_1 }}">{{ \App\Models\JobTitle::where('job_title_key',$jobseekers->desire_position_1)->pluck('description')->first() }}</option>
                        @foreach($desire_position_1 as $row)
                            <option value="{{ $row->job_title_key }}">
                                {{ $row->description }} 
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                  <label for="title">Desire Job Title Other 1:</label>
                  <input type="text" name="desire_position_other_1" id="title" class="form-control" value="{{ $jobseekers->desire_position_other_1 }}">
                </div>

                <div class="form-group">
                    <label for="title">Desire Job Title 2:</label>
                    <?php $desire_position_2 = \App\Models\JobTitle::all(); ?>
                    <select id="val_select" name="desire_position_2" class="form-control select2">
                        <option value="{{ $jobseekers->desire_position_2 }}">{{ \App\Models\JobTitle::where('job_title_key',$jobseekers->desire_position_2)->pluck('description')->first() }}</option>
                        @foreach($desire_position_2 as $row)
                            <option value="{{ $row->job_title_key }}">
                                {{ $row->description }} 
                            </option>
                        @endforeach
                    </select>
                  </div>
                <div class="form-group">
                  <label for="title">Desire Job Title Other 2:</label>
                  <input type="text" name="desire_position_other_2" id="title" class="form-control" value="{{ $jobseekers->desire_position_other_2 }}">
                </div>
                 <div class="form-group">
                  <label for="title">Skill 1:</label>
                   <?php $skill_1 = \App\Models\skill::all(); ?>
                      <select id="val_select" name="skill_1" class="form-control select2">
                          <option value="{{ $jobseekers->skill_1 }}">{{ $jobseekers->Myskill['name'] }}</option>
                          @foreach($skill_1 as $row)
                              <option value="{{ $row->id }}">
                                  {{ $row->name }} 
                              </option>
                          @endforeach
                      </select>
                  </div>

                  <div class="form-group">
                    <label for="title">Skill 1 Other:</label>
                    <input type="text" name="skill_other_1" id="title" class="form-control" value="{{ $jobseekers->skill_other_1 }}"> 
                  </div>

                <div class="form-group">
                    <label for="title">Skill 2:</label>
                     <?php $skill_2 = \App\Models\Skill2::all(); ?>
                        <select id="val_select" name="skill_2" class="form-control select2">
                            <option value="{{ $jobseekers->skill_2 }}">{{ $jobseekers->myskill2['name'] }}</option>
                            @foreach($skill_2 as $row)
                                <option value="{{ $row->id }}">
                                    {{ $row->name }} 
                                </option>
                            @endforeach
                        </select>
                </div>

              <div class="form-group">
                 <label for="title">Skill 2 Other:</label>
                 <input type="text" name="skill_other_2" id="title" class="form-control" value="{{ $jobseekers->  skill_other_2 }}"> 
               </div>

                <div class="form-group">
                  <label for="title">Township:</label>
                      <?php $township = \App\Models\Mytownship::all(); ?>
                      <select id="val_select" name="township" class="form-control select2">
                          <option value="{{ $jobseekers->township }}">{{ $jobseekers->mytownship['mytownship_name'] }}</option>
                          @foreach($township as $row)
                              <option value="{{ $row->id }}">
                                  {{ $row->mytownship_name }} 
                              </option>
                          @endforeach
                      </select>
                  </div>
                <div class="form-group">
                  <label for="title">Certificate:</label>
                      <?php $certificate = \App\Models\Certificate::all(); ?>
                      <select id="val_select" name="certificate" class="form-control select2">
                          <option value="{{ $jobseekers->certificate }}">{{ $jobseekers->mycertificate['description'] }}</option>
                          @foreach($certificate as $row)
                              <option value="{{ $row->id }}">
                                  {{ $row->description }} 
                              </option>
                          @endforeach
                      </select>
                  </div>

                <div class="form-group">
                    <label for="title">Certificate Other:</label>
                    <input type="text" name="certificate_other" id="title" class="form-control" value="{{ $jobseekers->certificate_other }}">
                 </div>
                <div class="form-group">
                    <label for="title">Remark:</label>
                    <input type="text" name="remark" id="title" class="form-control" value="{{ $jobseekers->remark }}">
                 </div>

              <div class="form-group">
                <label for="title">Additional Field 1:</label>
                <input type="text" name="additional_field" id="title" class="form-control" value="">   
              </div>
              <div class="form-group">
                <label for="title">Additional Field 2:</label>
                <input type="text" name="additional_field2" id="title" class="form-control" value="">       
              </div>

              <div class="form-group">
                <label for="title">Additional Field 3:</label>
                <input type="text" name="additional_field3" id="title" class="form-control" value="">    
              </div>

       <div class="form-group"> 
         <form action="{{ route('jobseekers.update', $jobseekers->id) }}">
                 {{ csrf_field() }}
                 {{ method_field("patch") }}
            <button type="submit" class="btn btn-primary">Update Info</button>
         </form>
      </div> 
      </div>
    </form>
  </div>

</div>
  
  @stop