  @extends('layouts.dashboard')


@section('section')
<!-- for pie chart -->
<style type="text/css">
.pie-title-center {
  display: inline-block;
  position: relative;
  text-align: center;
}

.pie-value {
  display: block;
  position: absolute;
  font-size: 14px;
  height: 40px;
  top: 50%;
  left: 0;
  right: 0;
  margin-top: -20px;
  line-height: 40px;
}
.cv-and-create{
  display: inline-block;
}
.company-id{
  display: none;
}

</style>

<br>
<br>
<br>
  <div class="row">
    @if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button> 
      <strong>{{ $message }}</strong>
    </div>
    @endif

    <div class="col-md-4"></div>
    <div class="col-md-4"></div>
    <div class="col-md-4"></div>
    <?php $jobeeker_list = DB::table('jobposition_jobseeker')->where('jobseeker_id',$jobseekers->jobseeker_id)->get();

    $post = ($jobseekers->desire_position_1)?$jobseekers->desire_position_1:null;
    ?>
  </div>
                          <div class="row">
                            <div class="col-md-6">
                            </div>
                            <div class="col-md-6 col-md-offset-3">
                              <div class="panel panel-info">
                                <div class="panel-footer">
                                  <a href="{{ route('jobseekers.edit', $jobseekers->jobseeker_id) }}" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a> 

                                  <span class="pull-right">
                                    <div class="cv-and-create">                 
                                      <form class="uk-form-stacked " action="{{ url('jobseekers/send') }}" enctype="multipart/form-data" method="post" id="cv_mail_data">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="cv_no" value="{{ $jobseekers->jobseeker_id }}">
                                        <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></i></button>
                                      </form> 
                                    </div>
                                    <div class="cv-and-create">
                                      <a href="{{ route('jobseekers.create') }}" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning">
                                        <i class="fa fa-user-plus" aria-hidden="true"></i></a></div> 
                                      </span>
                                    </div>
                                  </span>              

                                  <?php $jobeeker_list = DB::table('jobposition_jobseeker')->where('jobseeker_id',$jobseekers->jobseeker_id)->get();?>

                                  <div class="panel-heading">
                                    <h3 class="panel-title" style="height: 60px;padding:0"> <div class="fullname-js" style="padding-top: 20px">{{ $jobseekers->fullname }}</div> 
                                      <div class="cv-strength" style="margin-top: -30px">
                                        <div id="demo-pie-1" class="pie-title-center pull-right" data-percent="{{SiteHelper::cv_strength($jobseekers->jobseeker_id)}}"> <span class="pie-value"></span> </div>
                                      </div>  
                                    </h3>
                                  </div>

                                  <div class="panel-body">
                                    <div class=" col-md-12"> 
                                      <table class="table table-user-information">
                                        <tbody>


                                         <tr>
                                          <tr>
                <!--      <td>Stage:</td>
                      <td>@foreach($jobeeker_list as $jobseeker_status)
        <?php 
        // echo $jobseeker_status;
        // var_dump($jobseeker_status);

         $job_detail = \App\Models\Jobposition::where('job_id',$jobseeker_status->jobposition_id)->first();
         $job_stage = \App\Models\Stage::find($jobseeker_status->stage);
        
         $job_status = \App\Models\Status::find($jobseeker_status->status);
        ?>   {{$job_stage['description']}} 
                     </td>@endforeach
                        
                      </tr>
                      <tr> -->
                        <td>Status</td>
                       <!-- <td> @foreach($jobeeker_list as $jobseeker_status)
        <?php 
        // echo $jobseeker_status;
        // var_dump($jobseeker_status);

         $job_detail = \App\Models\Jobposition::where('job_id',$jobseeker_status->jobposition_id)->first();
         $job_stage = \App\Models\Stage::find($jobseeker_status->stage);
        
         $job_status = \App\Models\Status::find($jobseeker_status->status);
        ?>   
      {{$job_status['description']}}</td>@endforeach --><td> 
        @if($jobseekers->active_inactive == "0")

        {{"InActive"}}
        @else
        {{"Active"}} 
        @endif
      </td>

    </tr>

    <tr>
      <td>Phone:</td>
      <td>{{$jobseekers->phone}}</td>
    </tr>
    <tr>
      <td>Gender:</td>
      <td> {{ \App\Models\Gender::where('id',$jobseekers->gender)->pluck('gender_description')->first() }}</td>
    </tr>

    <tr>
      <td>Age:</td>
      <td>     {{  $jobseekers->age }}</td>
    </tr>                     

    <tr>
      <td>Nationality:</td>
      <td>     {{  $jobseekers->nationality }}</td>
    </tr>              

    <tr>
      <td>NRC No:</td>
      <td>     {{  ($jobseekers->nrc_no==9)?'Application in Progress':$jobseekers->nrc_no }}</td>
    </tr>                    
    <tr>
      <td>Martal Status:</td>
      <td>     {{  $jobseekers->marital_status }}</td>
    </tr>

    <tr>
      <td>Education:</td>
      <td>  {{ \App\Models\Education::where('id',$jobseekers->education)->pluck('name_en')->first() }}</td>
    </tr>
    <tr>
      <td>Certificate:</td>
      <td>
        @if($jobseekers->certificate != 8)
          {{ \App\Models\Certificate::where('id',$jobseekers->certificate)->pluck('description')->first() }}
        @endif  
       {{$jobseekers->certificate_other}}</td>
    </tr>

    <tr>
     <tr>
      <td>Location:</td>
      <td>  @if(isset($jobseekers->wards))
        {{ \App\Models\Ward::where('id',$jobseekers->wards)->pluck('name')->first() }},  
        @endif
        {{ \App\Models\Township::where('id',$jobseekers->township)->pluck('name')->first() }}</td>
      </tr> 

        <tr>
        <td> Desire Position 1: </td>
        <td> 
          @if($jobseekers->desire_position_1 != 301)
            {{ \App\Models\JobTitle::where('job_title_key',$jobseekers->desire_position_1)->pluck('description')->first() }}
          @endif
          {{$jobseekers->desire_position_other_1}}</td>
        </tr>  
        <tr>
        <td> Desire Position 2: </td>
        <td> 
          @if($jobseekers->desire_position_2 != 301)
            {{ \App\Models\JobTitle::where('job_title_key',$jobseekers->desire_position_2)->pluck('description')->first() }}
          @endif
          {{$jobseekers->desire_position_other_2}}</td>
        </tr>        
        <tr>
        <td> Previous Experience 1: </td>
        <td> 
          @if($jobseekers->previous_position_1 != 301)
            {{ \App\Models\JobTitle::where('job_title_key',$jobseekers->previous_position_1)->pluck('description')->first() }}
          @endif
          {{$jobseekers->previous_position_other}} {{ checkExp($jobseekers->experience_month_1) }} <br> {{$jobseekers->company_name_1}}</td>
        </tr>
        <td>Previous Experience 2:</td>
        <td>
          @if($jobseekers->previous_position_2 != 301)
            {{ \App\Models\JobTitle::where('job_title_key',$jobseekers->previous_position_2)->pluck('description')->first() }} 
          @endif  {{$jobseekers->previous_position_other_2}}
          {{checkExp($jobseekers->experience_month_2)}} <br> {{$jobseekers->company_name_2}}
        </td>           
        <tr>
          <td> Skill 1: </td>
          <td>@if(!empty($jobseekers->skill_1))
            @if($jobseekers->skill_1 != 24)
            {{ \App\Models\skill::where('id',$jobseekers->skill_1)->pluck('name')->first() }}
            @else {{$jobseekers->skill_other_1}}
          @endif @endif</td>
        </tr>
        <td>Skill 2:</td>

        <td>@if(!empty($jobseekers->skill_2))
          @if($jobseekers->skill_2 != 24) 
          {{ \App\Models\Skill2::where('id',$jobseekers->skill_2)->pluck('name')->first() }} 
          @else 
          {{$jobseekers->skill_other_2}}
          @endif
          @else 
          @endif
        </td>
 <tr>
        <td>Expected Salary :</td>

        <td>@if(!empty($jobseekers->expected_salary))
       
          {{ \App\Models\Expected_salary::where('id',$jobseekers->expected_salary)->pluck('description')->first() }} 
          @else 
          {{$jobseekers->expected_salary_other}}
          @endif
        </td>
</tr>
        <tr>                 
          <td>Additional Field 1</td>
          <td>{{$jobseekers->additional_field}}</td>
        </tr>
        <tr>
          <td>Additional Field 2</td>
          <td>{{$jobseekers->additional_field2}}</td>
        </tr>
        <tr>
          <td>Additional Field 3</td>
          <td>{{$jobseekers->additional_field3}}</td>
        </tr>     
        <tr>                          
          <td>
            <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal2">Add additional Fields</button>
          </td>
          <td></td>
        </tr>                 
      </tbody>
    </table>    
  </div>
</div>
</div>   
</div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal2" role="dialog">
  <div class="modal-dialog">            
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Additional Fields</h4>
      </div>
      <div class="modal-body">
        <form action="{{ route('jobseekers.add_additionalfield', $jobseekers->id) }}" method="POST">
          {{ csrf_field() }}
          {{ method_field("patch") }}
          <div class="form-group">
            <label for="title">Additional Field 1:</label>
            <input type="text" name="additional_field" id="title" class="form-control" value="">

          </div>
          <div class="form-group">
            <label for="title">Additional Field 2:</label>
            <input type="text" name="additional_field2" id="title" class="form-control" value="">

          </div>     
          <div class="form-group">
            <label for="title">Additional Field 3:</label>
            <input type="text" name="additional_field3" id="title" class="form-control" value="">      
          </div>        

          <div class="form-group">            
            <button type="submit" class="btn btn-primary">Add Additional</button>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

</div>
<!-- for job match -->
<div class="container-fluid">
  <?php
  $current = Carbon\Carbon::today();
  $jobmatches = \App\Models\Jobposition::where('require_position',$post)->whereDate('jobpositions.deadline', '>=', $current)->orderBy('free_or_paid','desc')->pluck('id');

  ?>


  <table class="table table-bordered table-responsive">
   <h3><b>Job Match</b></h3>
   <thead>
     <tr style="background-color: #99ccff">   

      <th> Id</th>
      <th> Job Title Key</th>
      <th> Job Title Actual</th>
      <th> Post Date</th>
      <th> Employer Name</th>
      <th> Phone Number</th>
      <th> Email </th>
      <th> Company Address </th>
      <th> Job Requirement </th>
      <th> Contracted or Not Contracted</th>
    </thead>
    <tbody>
      @foreach($jobmatches as $value)
      <tr class="success">
       <?php  
       $current = Carbon\Carbon::today();
       $job_list = \App\Models\Jobposition::where('id',$value)->whereDate('jobpositions.deadline', '>=', $current)->first(); ?>
       <td> {{ $job_list['job_id']}}</td>
       <td> {{ \App\Models\JobTitle::where('job_title_key',$job_list['require_position'])->pluck('description')->first() }} </td> 
       <td>{{$job_list['job_title']}}</td>
       <td> 
        <?php if(!empty($job_list['post_date'])){ ?> 
        {{Carbon\Carbon::parse($job_list['post_date'])->format('j-M-y') }}
        <?php }?>
      </td>
      <!-- <td>  {{ $job_list['employer_id']}} </td> -->
      <td> 
        <?php 
        $employer_name = \App\Models\Employer::where('emp_id',$job_list['employer_id'])->pluck('employer_name');
        $emp_id = $job_list['employer_id'];   
        print "<a href='/employers/$emp_id' target='_blank'> 
        $employer_name</a>";  
        ?>
      </td>
      <td>
        <?php 

        $emp_phone_number = \App\Models\Employer::where('emp_id',$job_list['employer_id'])->pluck('phone_number');
        print $emp_phone_number;

        ?>  
      </td>             
      <td>
        <?php 
        $emp_email = \App\Models\Employer::where('emp_id',$job_list['employer_id'])->pluck('email_address');
        print $emp_email;
        ?>    
      </td> 
      <td>
        {{$job_list['address']}}

      </td>             
      <td>{{$job_list['job_requirement']}}</td>
      <td> 

       <?php if($job_list['free_or_paid'] == 1){ echo "<b>Contracted</b>";} else {
        echo "Not Contracted";
      } 
      ?>

    </td>

  </tr>
  @endforeach
</tbody>
</table>  




<!-- for job match -->


</div>


<div class="container-fluid">

  <table class="table table-bordered table-responsive">
   <h3><b>Job Status History</b></h3>
   <thead>
     <tr style="background-color: #99ccff">   

      <th>Job Title</th>
      <th>Employer Name</th>
      <th>Job Expire Date</th>
      <th>Status</th>


    </thead>
    <tbody>
      <?php $jobeeker_list = DB::table('jobposition_jobseeker')->where('jobseeker_id',$jobseekers->jobseeker_id)->get();?>
      @foreach($jobeeker_list as $jobseeker_status)
      <?php 
        // echo $jobseeker_status;
        // var_dump($jobseeker_status);

      $job_detail = \App\Models\Jobposition::where('job_id',$jobseeker_status->jobposition_id)->first();
      $job_stage = \App\Models\Stage::find($jobseeker_status->stage);

      $job_status = \App\Models\Status::find($jobseeker_status->status);
      ?>

      <tr class="success">

        <td> {{$job_detail['job_title']}}</td>
        <td>{{isset($job_detail->employer)?$job_detail->employer['employer_name']:null}}</td>
        <td> {{$job_detail['deadline']}}</td>
        <!--<td>{{$job_stage['description']}}</td> -->
        <td>{{$job_status['description']}}</td>
        


      </tr>
      @endforeach


    </tbody>
  </table>  


</div>

<div class="container-fluid">
  <hr style="border-style: groove">
  @if(!$jobseekers->calllogs->count()) 
  <h1> <b>There is no Calllog</b></h1>
  <hr style="border-style: groove">
  @else

</td>
</tr>
</div>
</div>
</div>



<div class="container-fluid">

 <table class="table table-bordered table-responsive">
   <h3><b>Call Log History</b></h3>
   <thead>
     <tr style="background-color: #99ccff">   
      <th>Action</th>
      <th>Call Back Date</th>
      <th>Type of Call</th>
      <th>Recommended Jobs</th>
      <th>Start Time</th>
      <th>Duration(minute)</th>
      <th>Second</th>
      <th>Name</th>
      <th>Phone Number  </th>
      <th> Call Reason</th>
      <th> Remark</th>
      <th> Status</th>
      <th> Interview Time</th>
      <th> Company Recommend</th>
      <th> Caller/Receiver</th>
      <th>Active or Not </th>

    </thead>
    <tbody>

      @foreach($jobseekers->calllogs as $calllog)
      <tr class="success">
        <td> 
            <a href="{{ route('calllogs.edit', $calllog->id) }}" class="btn btn-primary btn-xs" target="_blank"><i class="glyphicon glyphicon-pencil"></i></a> 
            {!! Form::open(['route' =>['calllogs.destroy',$calllog->id], 'method' => 'delete']) !!}

              {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
            {!! Form::close() !!}
        </td>
        <td>
          @if(!empty($calllog['call_back_date'])){{ Carbon\Carbon::parse($calllog['call_back_date'])->format('j-F-Y') }} @else  @endif
        </td>
        <td> {{$calllog['type_of_call']}}</td>

        <td>
          <?php 

          $jobseeker = \App\Models\Jobseeker::where('jobseeker_id',$calllog->jobseeker_id)->first();

              // var_dump($jobseeker);
          foreach ($jobseeker->My_jobpositions as $key => $value) {
                 // var_dump($value->job_title);
            echo  $value->job_title;
            echo  $value->pivot->status;
            echo ",";
          }

          ?>
        </td>
        <td> {{$calllog['created_at']}}</td>
        <td> {{$calllog['minute']}} minutes</td>
        <td> {{$calllog['second']}} seconds</td>
        <td> {{$calllog->jobseeker['fullname']}}</td>
        <td> {{$calllog['phone_number']}}</td>
        <td> {{$calllog->mycallreason['description']}}</td>
        <td> {{$calllog['remark_calllog']}}</td>
        <td> {{$calllog->mystatus['description']}}</td>
        <td>
          @if($calllog->joinToPivot($calllog->id))              
          <!-- {{ Carbon\Carbon::parse($calllog->joinToPivot($calllog->id)->interview_time)->format('j-F-Y H:m') }} -->
          {{$calllog->joinToPivot($calllog->id)->interview_time}}
          @endif
        </td>
        <td> {{isset($calllog->employer)?$calllog->employer->employer_name:''}}</td>
        <td> {{$calllog->author['name']}}</td>
        <td>@if($calllog->active_inactive=="1")
          Active 
          @elseif($calllog->active_inactive=="2")
          Inactive
          @endif
        </td>

      </tr>
      @endforeach


    </tbody>
  </table>  
  @endif  
  <br>
</td>
</tr>

</table>
{!! Form::open(['route' => 'calllogs.store', 'method' => 'post']) !!}
<div class="row"> 

  <hr style="border-style: groove;">
  <div class="col-md-6"> 

    <h3><b>Add Call Log</b></h3>
    <br>
    <br>


    {!! Form::hidden('jobseeker_id', $jobseekers->jobseeker_id) !!}

    <div class="form-group">
      {!! Form::label('Type of Call') !!}        
      <select class="form-control" name="type_of_call">
        <option value="{{ request()->input('age')}}">Choose Type of Call</option>
        <option value="Incoming Call">Incoming Call</option>
        <option value="Outgoing Call">Outgoing Call</option>                              
      </select>
    </div>

    <div class="form-group">
      {!! Form::label('Minute') !!}
      {!! Form::text('minute', null, 
      array('required', 
      'class'=>'form-control', 
      'placeholder'=>'Minute')) !!}
    </div>
    <div class="form-group">
      {!! Form::label('Second') !!}
      {!! Form::text('second', null, array('required', 'class'=>'form-control','placeholder'=>'Second')) !!}
    </div>

    <div class="form-group">
      {!! Form::label('Job Seeker Status') !!}        
      <select class="form-control" name="active_key" required="required">
        <option value="0">Choose </option>
        <option value="1" {{isset($jobseeker) && $jobseeker->active_inactive==1?'selected':''}}>Active</option>
        <option value="2" {{isset($jobseeker) && $jobseeker->active_inactive==2?'selected':''}}>Inactive</option>                              
      </select>
    </div>            

  </div>

  <div class="col-md-6">
    <br><br>

    <br>
    <br>
    <br>



    <div class="form-group">
      <label for="title">Callback Date:</label>
      <input type="date" name="call_back_date" id="title" class="form-control" value="" >
    </div>

    <div class="form-group">
     {!! Form::label('Remark') !!}
     {!! Form::textarea('remark_calllog', null, ['class' => 'form-control', 'placeholder' => __('Remark '), 'rows' => '3']) !!}
   </div>
   <div class="form-group" id="employer"></div>
   <div class="form-group" id="job_title"></div>
   <div class="form-group" id="stage"></div>
   <div class="form-group" id="status"></div>  
   <div class="form-group hide" id="interview">
    <label for="title">Choose Interview Date:</label>
    <div class='input-group date' id='datetimepicker4'>
      <input class="form-control time1" type="text" name="interview_time[]" />
      <span class="input-group-addon">
        <span class="glyphicon glyphicon-time"></span>
      </span>
    </div>     
  </div> 

  <?php
  $current = Carbon\Carbon::today();
  $new = [$jobseekers->previous_position_1,$jobseekers->previous_position_2,$jobseekers->desire_position_1,$jobseekers->desire_position_2];

  // $jobmatches = \App\Models\Jobposition::where('require_position',$post)->whereDate('jobpositions.deadline', '>=', $current)->orderBy('free_or_paid','desc')->pluck('id');
  $jobmatches = \App\Models\Jobposition::whereIn('require_position',$new)->whereDate('jobpositions.deadline', '>=', $current)->orderBy('free_or_paid','desc')->pluck('id');

  ?>

  @foreach($jobmatches as $value)
  <?php  $job_list = \App\Models\Jobposition::where('id',$value)->first(); ?>
  <div class="job-list">

    <div class="form-group"> 
      <input type="checkbox" name="job_position[]" value="{{ $job_list['job_id']}}"> 
      {{ $job_list['job_title']}}<br>
    </div>

    <div class="form-group">
      <label for="title">Select Stage:</label>
      <select name="stage[]" class="form-control" >
        <option value="">--- Select Stage ---</option>
        @foreach ($stages as $key => $value)
        <option value="{{ $key }}">{{ $value }}</option>
        @endforeach
      </select>
    </div>

    <div class="form-group">
      <label for="title">Select Status:</label>
      <select name="status[]" class="form-control">
      </select>
    </div>

    <div class="form-group hide jan" id="date-input-calllog">
      <label for="title">Choose Interview Date:</label>
      <div class='input-group date' id='datetimepicker4'>
        <input class="form-control time1" type="text" value="{{$jobseekers->interview_time}}" name="interview_time[]" />
        <span class="input-group-addon">
          <span class="glyphicon glyphicon-time"></span>
        </span>
      </div>        


    </div>


  </div>
  @endforeach

<!-- <div class="form-group">
 {!! Form::label('Company Recommend ') !!}
    <input class="typeahead form-control"  type="text" name="company_recommended" id="fooInput">
  </div> -->

<!-- <div class="form-group company-id">
     {!! Form::label('Company Id ') !!}
    <input class="typeahead form-control"  type="text" name="company_id" id="haha">
</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet" /> -->




    <button data-status="show" id="manual" onclick="addManual({{$jobseekers->jobseeker_id}})" type="button" class="btn btn-warning"><i></i> Add Manual</button>

    {!! Form::submit(__('Update'), ['class' => 'btn btn-primary pull-right']) !!}
  </div>
</div>
</div>
{!! Form::close() !!}
<!-- for interview date and time -->

<script type="text/javascript">
  $('.time1').datetimepicker({
    format: 'YYYY-MM-DD HH:mm:ss'
  });
  $(function (e) {
    $('select[name="status[]"]').change(function (e) {
      $(this).find(":selected").val();
        //remove the hide class from date-input
        var v = $(this).find(":selected").val();
        if (v == 10 || v == 14) {
          $('.jan').removeClass('hide')
        }
      })
  })
  // $("#fooInput").autocomplete({
  //       source: "{{ route('searchajax') }}",
  //       select: function(event, ui) {
  //         var e = ui.item;
  //         var result =   e.id;
  //         $("#result").append(result);


  //         var input = $( "#haha" );
  //           input.val( input.val() + result );
  //       }
  // });
  //for chart
  $(document).ready(function () {

    var jsonData = {"7":"Send Viber Message","6":"Unpaid: Cannot Contact","5":"Paid: Cannot Contact","4":"Not Active","3":"Not interested","2":"Interested, ask for Application Date","1":"Interested, Awaiting CV"};

    $('#demo-pie-1').pieChart({
      barColor: '#68b828',
      trackColor: '#eee',
      lineCap: 'round',
      lineWidth: 8,
      size: 50,
      onStep: function (from, to, percent) {
        $(this.element).find('.pie-value').text(Math.round(percent) + '%');
      }
    });
    $('select[name="stage[]"]').on('change', function() {
      var stateID = $(this).val();
      var $select = $(this).closest('.job-list').find('select[name="status[]"]');
      if (stateID) {
        $.ajax({
          url: '/myform/ajax/'+stateID,
          type: "GET",
          dataType: "json",
          data: {
            json: JSON.stringify(jsonData)
          },
          success: function(data) {
            $select.empty();
            var sortedData = sortResults(data);
            $.each(sortedData, function(index, data) {
              $select.append('<option value="' + data.key + '">' + data.value + '</option>');
            });

          }
        });
      } else {
        $select.empty();
      }
    });
  });

//stages and status dynamic dropdown
function sortResults(dataObj) {
  var keys = Object.keys(dataObj);
  keys.sort(function(a, b) {
    var firstNumber = Number(a);
    var secondNumber = Number(b);
    if(firstNumber > secondNumber){
      return -1;
    } else {
      return 1;
    }
  });

  var sortedArray = [];
  for(var i=0; i<keys.length; i++) {
    var key = keys[i];
    var val = dataObj[key];
    sortedArray.push({"key":key, "value":val});
  }
  return sortedArray;
}

</script>
<script src="{{asset('js/phyo.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>  
@stop
