@extends('layouts.dashboard')

@section('section')
<br>
<br>
<br>
<div class="row">              
              @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div>
                @endif



  <div class="col-md-6">
    <form action="{{ route('jobseekers.store')}}" method="POST">
      {{ csrf_field() }}

      <div class="form-group">
        <label for="title">Name:</label>
        <input type="text" name="fullname" id="title" class="form-control" value="">    
      </div>     

      <div class="form-group">
        <label for="title">Nationality:</label>
        <input type="text" name="nationality" id="title" class="form-control" value="">    
      </div>

      <div class="form-group">
        <label for="title">NRC No:</label>
        <input type="text" name="nrc_no" id="title" class="form-control" value="">    
      </div>

      <div class="form-group">
        <label for="title">Marital Status:</label>
          <select name="marital_status" class="form-control select2 pull-right">
            <option value="Single">Single</option>
            <option value="Married">Married</option>
            <option value="Divorced">Divorced</option>
           
            <option value="" selected>Choose Marital Status</option>
          </select>
       </div>
       <br></br>

        <div class="form-group">
            <label for="title">Facebook Name:</label>
            <input type="text" name="fb_name" id="title" class="form-control" value="">
        </div>

       <div class="form-group">
       <label for="title">Phone:</label>
        <input type="text" name="phone" id="title" class="form-control" value="">
      </div>


       <div class="form-group">
        <label for="title">Viber Number:</label>
        <input type="text" name="viber_number" id="title" class="form-control" value="">
      </div>

        <div class="form-group">
        <label for="title">Sign up From</label>
        <input type="text" name="sign_up_from" id="title" class="form-control" value="">
        </div>


      <div class="form-group">
        <label for="title">Gender:</label>
          <select name="gender" class="form-control select2 pull-right">
            <option value="B">Male</option>
            <option value="G">Female</option>
           
            <option value="" selected>Choose Gender</option>
          </select>
       </div>
       <br></br>



      <div class="form-group">
        <label for="title">Age:</label>
        <input type="text" name="age" id="title" class="form-control" value="">
      </div>


         <div class="form-group">
            <label for="title">Ward:</label>
             <?php $wards = \App\Models\Ward::all(); ?>
                <select id="val_select" name="wards" class="form-control select2">
                    <option value=""> </option>
                    @foreach($wards as $row)
                        <option value="{{ $row->id }}">
                            {{ $row->name }} 
                        </option>
                    @endforeach
                </select>
            </div>


             <div class="form-group">
                <label for="title">Bus Stop:</label>
                <?php $bus_stops = \App\Models\Busstop::all(); ?>
                <select id="val_select" name="bus_stops" class="form-control select2">
                    <option value=""></option>
                    @foreach($bus_stops as $row)
                        <option value="{{ $row->id }}">
                            {{ $row->busstop }} 
                        </option>
                    @endforeach
                </select>
            </div>


             <div class="form-group">
                <label for="title">Education:</label>
                <?php $education = \App\Models\Education::all(); ?>
                <select id="val_select" name="education" class="form-control select2">
                    <option value=""></option>
                    @foreach($education as $row)
                        <option value="{{ $row->id }}">
                            {{ $row->name_en }} 
                        </option>
                    @endforeach
                </select>
             </div>

             <div class="form-group">
                <label for="title">Township:</label>
                    <?php $township = \App\Models\Mytownship::all(); ?>
                    <select id="val_select" name="township" class="form-control select2">
                        <option value=""></option>
                        @foreach($township as $row)
                            <option value="{{ $row->id }}">
                                {{ $row->mytownship_name }} 
                            </option>
                        @endforeach
                    </select>
                </div>

             <div class="form-group">
                <label for="title">Previous Job Title:</label>
                <?php $previous_position_1 = \App\Models\JobTitle::all(); ?>
                <select id="val_select" name="previous_position_1" class="form-control select2">
                    <option value=""></option>
                    @foreach($previous_position_1 as $row)
                        <option value="{{ $row->job_title_key }}">
                            {{ $row->description }} 
                        </option>
                    @endforeach
                </select>
             </div>


             <div class="form-group">
                <label for="title">Experience Months 1:</label>
                <select name="experience_month_1" class="form-control">
                    <option value="">Select one</option>
                    @foreach(getExp() as $key=>$value)
                        <option value="{{$key}}">{{$value}}</option>
                    @endforeach                    
                </select>
             </div>

            <div class="form-group">
                <label for="title">Company Name 1:</label>
                <input type="text" name="company_name_1" id="title" class="form-control" value="">    
            </div>

             <div class="form-group">
                <label for="title">Previous Job Title 2:</label>
                <?php $previous_position_2 = \App\Models\JobTitle::all(); ?>
                <select id="val_select" name="previous_position_2" class="form-control select2">
                    <option value=""></option>
                    @foreach($previous_position_2 as $row)
                        <option value="{{ $row->job_title_key }}">
                            {{ $row->description }} 
                        </option>
                    @endforeach
                </select>
             </div>

            <div class="form-group">
                <label for="title">Experience Months 2:</label>
                <select name="experience_month_2" class="form-control">
                    <option value="">Select one</option>
                    @foreach(getExp() as $key=>$value)
                        <option value="{{$key}}">{{$value}}</option>
                    @endforeach                    
                </select>
            </div>

            <div class="form-group">
                <label for="title">Company Name 2:</label>
                <input type="text" name="company_name_2" id="title" class="form-control" value="">    
            </div>      

            <div class="form-group">
                <label for="title">Desire Job Title 1:</label>
                <?php $desire_position_1 = \App\Models\JobTitle::all(); ?>
                <select id="val_select" name="desire_position_1" class="form-control select2">
                    <option value=""></option>
                    @foreach($desire_position_1 as $row)
                        <option value="{{ $row->job_title_key }}">
                            {{ $row->description }} 
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                  <label for="title">Desire Job Title Other 1:</label>
                  <input type="text" name="desire_position_other_1" id="title" class="form-control">
                </div>

            <div class="form-group">
                <label for="title">Desire Job Title 2:</label>
                <?php $desire_position_2 = \App\Models\JobTitle::all(); ?>
                 <select id="val_select" name="desire_position_2" class="form-control select2">
                    <option value=""></option>
                    @foreach($desire_position_2 as $row)
                        <option value="{{ $row->job_title_key }}">
                            {{ $row->description }} 
                        </option>
                    @endforeach
                </select>
            </div>
                       
            <div class="form-group">
              <label for="title">Desire Job Title Other 2:</label>
              <input type="text" name="desire_position_other_2" id="title" class="form-control" >
            </div>

            <div class="form-group">
                <label for="title">Fabrics:</label>
                <?php $fabrics = \App\Models\Fabric::all();  ?>
                <select id="val_select" name="fabrics" class="form-control select2">
                    <option value=""></option>
                    @foreach($fabrics as $row)

                        <option value="{{ $row->id }}">
                            {{ $row->title }} 
                        </option>
                    @endforeach
                </select>
            </div>   
                                            
            <div class="form-group">
                <label for="title">Skill 1:</label>
                <?php $skills = \App\Models\skill::all(); ?>
                <select id="val_select" name="skill_1" class="form-control select2">
                    <option value=""></option>
                    @foreach($skills as $row)
                        <option value="{{ $row->id }}">
                            {{ $row->name }} 
                        </option>
                    @endforeach
                </select>
            </div>   
                                                               
            <div class="form-group">
                <label for="title">Skill 2:</label>
                <?php $skills = \App\Models\Skill2::all(); ?>
                <select id="val_select" name="skill_2" class="form-control select2">
                    <option value=""></option>
                    @foreach($skills as $row)
                        <option value="{{ $row->id }}">
                            {{ $row->name }} 
                        </option>
                    @endforeach
                </select>
            </div>   


         
        <div class="form-group">
                  <label for="title">Certificate:</label>
                      <?php $certificate = \App\Models\Certificate::all(); ?>
                      <select id="val_select" name="certificate" class="form-control select2">
                          <option value=""></option>
                          @foreach($certificate as $row)
                              <option value="{{ $row->id }}">
                                  {{ $row->description }} 
                              </option>
                          @endforeach
        </select>
        </div>     
        <div class="form-group">
        <label for="title">Certification Other</label>
        <input type="text" name="certificate_other" id="title" class="form-control" value="">
        </div>
<!--         <div class="form-group">
        <label for="title">Certification and License</label>
        <input type="text" name="certification_and_license" id="title" class="form-control" value="">
        </div> -->
        <div class="form-group">
        <label for="title">Type of License  </label>
        <input type="text" name="type_of_license" id="title" class="form-control" value="">
        </div>               
        <div class="form-group">
        <label for="title">Viber Number 2 </label>
        <input type="text" name="viber_number_2" id="title" class="form-control" value="">
        </div>        
        <div class="form-group">
        <label for="title">Remark </label>
        <input type="text" name="remark" id="title" class="form-control" value="">
        </div>   
        <div class="form-group">
        <label for="title">User Type     </label>
        <input type="text" name="usertype_1" id="title" class="form-control" value="">
        </div>            
        <div class="form-group">
        <label for="title">Chatfuel User ID    </label>
        <input type="text" name="chatfuel_user_id" id="title" class="form-control" value="">
        </div>            
        <div class="form-group">
        <label for="title">Address   </label>
        <input type="text" name="address" id="title" class="form-control" value="">
        </div>   
        <div class="form-group">
        <label for="title">Last User Free Input    </label>
        <input type="text" name="last_user_freedom_input" id="title" class="form-control" value="">
        </div>           
        <div class="form-group">
        <label for="title">Messenger User Id      </label>
        <input type="text" name="messenger_user_id" id="title" class="form-control" value="">
        </div>   
        <div class="form-group">
        <label for="title">Additional Field 1:</label>
        <input type="text" name="additional_field" id="title" class="form-control" value="">   
        </div>
        <div class="form-group">
        <label for="title">Additional Field 2:</label>
        <input type="text" name="additional_field2" id="title" class="form-control" value="">       
        </div>     
        <div class="form-group">
        <label for="title">Additional Field 3:</label>
        <input type="text" name="additional_field3" id="title" class="form-control" value="">    
        </div>


          <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Create
                    </button>
                    
                </div>
            </div>
                               
        
      </div> 
      </div>
    </form>
  </div>

</div>
  

  @stop