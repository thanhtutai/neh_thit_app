@extends('layouts.dashboard')
@section('section')
<br>
<br><br>

<div class="container-fluid">
    
</body>



  <div class="col-md-12">
    <center><h2 class="" style="display: inline-block;">Appointed Job Seekers List</h2></center>

  </div>

  <div class="row">
    <div class="col-sm-12 search-fix">

    @isset($result)
        <div class="col-md-12 alert alert-success alert-block">
     You search for
     @isset($js_id)

         <span> ID : <b>{{$js_id}} </b></span>
    @endisset 
    @isset($fullname)
         <span> Full Name : <b>{{$fullname}} </b></span>
    @endisset
    @isset($fb_name)
          Facebook Name : <b>{{$fb_name}}</b>
    @endisset 
    @isset($sk1)
          Skill 1  : <b>{{$sk1}}</b>
    @endisset
    @isset($sk2)
          Skill 2  : <b>{{$sk2}}</b>
    @endisset
    @isset($gender)
           Gender : <b> {{$gender}} </b>
    @endisset
    @isset($township)
           Township : <b>{{$township}}</b>
    @endisset
    @isset($age)
             Age: <b>{{$age}}</b>
    @endisset
    @isset($dp1)
              Desire Job Title 1 : <b>{{$dp1}}</b>
    @endisset
    @isset($dp2)
               Desire Job Title 2 : <b>{{$dp2}}</b>
    @endisset
    @isset($pp1)
                Previous Job Title 1: <b>{{$pp1}}</b> 
    @endisset
    @isset($pp2)
                Previous Job Title 2: <b>{{$pp2}}</b> 
    @endisset    
    @isset($status)
               Status: <b>{{$status}}</b> 
    @endisset
     </div>
    @endisset
   
      @if ($message = Session::get('success'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
      </div>
      @endif
</div>
</div>

<!-- search start -->
<div class="container-fluid">
  <button class="btn btn-default for-hide-search-form">Show Search Form</button> 
  <a class="btn btn-primary pull-right" href="{{ route('jobseekers.create')}}" target="_blank">Create Job Seeker</a>
  
  <a href="{{ URL::to('downloadExcelJS/xlsx') }}"><button class="btn btn-success pull-right">Excel xls</button></a>
  <a href="{{ URL::to('downloadExcelJS/csv') }}"><button class="btn btn-success pull-right"> CSV</button></a>
</div>
<br>

            <div class="box box-body" style="z-index: -1;">
              {!! Form::open(['url'=>'/appointed-jobseeker','method'=>'GET', 'class'=>'form', 'id'=>'search_data']) !!}
                 <div class="form-group col-md-3">

                <input type="text" name="js_id" placeholder="ID" value="{{ request()->input('js_id')}}" class="form-control"/>
              </div>
              <div class="form-group col-md-3">

              <input type="text" name="fullname" placeholder="Name" value="{{ request()->input('fullname')}}" class="form-control"/>
              </div>

              <div class="form-group col-md-3">            
                <input type="text" name="phone" placeholder="Phone" value="{{ request()->input('phone')}}" class="form-control"/>
               </div>

              <div class="form-group col-md-3">
             <!--  <div class="container-fluid">
                <div class="row">
                 <div class="col-md-12">
                  <div class="col-md-10 close-wrapper"> -->
              
                       <!-- {!! Form::label('Choose Township') !!} -->
                <?php $status = \App\Models\Status::all(); ?>
                <select id="ddlFruits" name="status" class="form-control select2">

                  <option value="{{ request()->input('status')}}">
                  <?php $label =request()->input('status');
                    if(isset($label))
                      echo $label;
                    else
                      echo "Choose Status ";

                   ?>
                  </option>
                   <?php if(isset($label)){ ?>
                  <option value="">Choose Status</option>
                  <?php }?>
                  @foreach($status as $row)
                    <option value="{{ $row->description }}" @if(old('status') == $row->id) selected @endif> {{ $row->description }} </option>
                  @endforeach          
                </select>
                </div>

              <div class="form-group col-md-3">
                 <select name="first_age" class="form-control select2">
                <option value="{{ request()->input('first_age')}}">
                <?php $label =request()->input('first_age');
                if(isset($label))
                  echo $label;
                else
                  echo "Choose Age From ";

               ?>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
                <option value="14">14</option>
                <option value="15">15</option>
                <option value="16">16</option>
                <option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
                <option value="20">20</option>
                <option value="21">21</option>
                <option value="22">22</option>
                <option value="23">23</option>
                <option value="24">24</option>
                <option value="25">25</option>
                <option value="26">26</option>
                <option value="27">27</option>
                <option value="28">28</option>
                <option value="29">29</option>
                <option value="30">30</option>
                <option value="31">31</option>
                <option value="32">32</option>
                <option value="33">33</option>
                <option value="34">34</option>
                <option value="35">35</option>
                <option value="36">36</option>
                <option value="37">37</option>
                <option value="38">38</option>
                <option value="39">39</option>
                <option value="40">40</option>
                <option value="41">41</option>
                <option value="42">42</option>
                <option value="43">43</option>
                <option value="44">44</option>
                <option value="45">45</option>
                <option value="46">46</option>
                <option value="47">47</option>
                <option value="48">48</option>
                <option value="49">49</option>
              </select>
              </div>

              <div class="form-group col-md-3">
              <select name="second_age" class="form-control select2">
                <option value="{{ request()->input('second_age')}}">
                <?php $label =request()->input('second_age');
                if(isset($label))
                  echo $label;
                else
                  echo "Choose Age To ";

               ?>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
                <option value="14">14</option>
                <option value="15">15</option>
                <option value="16">16</option>
                <option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
                <option value="20">20</option>
                <option value="21">21</option>
                <option value="22">22</option>
                <option value="23">23</option>
                <option value="24">24</option>
                <option value="25">25</option>
                <option value="26">26</option>
                <option value="27">27</option>
                <option value="28">28</option>
                <option value="29">29</option>
                <option value="30">30</option>
                <option value="31">31</option>
                <option value="32">32</option>
                <option value="33">33</option>
                <option value="34">34</option>
                <option value="35">35</option>
                <option value="36">36</option>
                <option value="37">37</option>
                <option value="38">38</option>
                <option value="39">39</option>
                <option value="40">40</option>
                <option value="41">41</option>
                <option value="42">42</option>
                <option value="43">43</option>
                <option value="44">44</option>
                <option value="45">45</option>
                <option value="46">46</option>
                <option value="47">47</option>
                <option value="48">48</option>
                <option value="49">49</option>
              </select>
              </div>
              

              <div class="form-group col-md-3">
                <!-- {!! Form::label('Choose Gender') !!} -->
                 <?php $gender = \App\Models\Gender::all(); ?>
                 <select id="val_select"  placeholder="Choose Gender" name="gender" class="form-control select2">
                            <option value="{{ request()->input('gender')}}">
                            <?php $label =request()->input('gender');
                              if(isset($label))
                                echo $label;
                              else
                                echo "Choose Gender ";
                             ?>                               
                             </option>
                             <?php if(isset($label)){ ?>
                          <option value="">Choose Gender</option>
                          <?php }?>
                              @foreach($gender as $row)
                    <option value="{{ $row->gender_description }}"> {{ $row->gender_description }} </option>
                              @endforeach
                  </select>
              </div>                 

     
              <div class="form-group col-md-3">
                       <!-- {!! Form::label('Choose Township') !!} -->
                <?php $township = \App\Models\Township::all(); ?>
                <select id="val_select" name="township" class="form-control select2">
                  <option value="{{ request()->input('township')}}">
                  <?php $label =request()->input('township');
                    if(isset($label))
                      echo $label;
                    else
                      echo "Choose Township ";

                   ?>
                  </option>
                  <?php if(isset($label)){ ?>
                          <option value="">Choose Township</option>
                          <?php }?>
                  @foreach($township as $row)
                  <option value="{{ $row->name }}" @if(old('township') == $row->id) selected @endif> {{ $row->name }} </option>
                  @endforeach

                  
                </select>
              </div>              


           <div class="form-group col-md-3">
                       <!-- {!! Form::label('Choose Desire Position 1') !!} -->
                <?php $desire_position_1 = \App\Models\JobTitle::all(); ?>
                <select id="val_select" name="desire_position_1" class="form-control select2">
                  <option value="{{ request()->input('desire_position_1')}}">
                  <?php $label =request()->input('desire_position_1');
                    if(isset($label))
                      echo $label;
                    else
                      echo "Choose Desire Job Title 1 ";

                   ?>
                  </option>
                  <?php if(isset($label)){ ?>
                          <option value="">Choose Desire Job Title 1</option>
                          <?php }?>
                  @foreach($desire_position_1 as $row)
                  <option value="{{ $row->description }}" @if(old('desire_position_1') == $row->description) selected @endif> {{ $row->description }} </option>
                  @endforeach

                  
                </select>
              </div> 

              <div class="form-group col-md-3">
                       <!-- {!! Form::label('Choose Desire Position 1') !!} -->
                <?php $desire_position_2 = \App\Models\JobTitle::all(); ?>
                <select id="val_select" name="desire_position_2" class="form-control select2">
                  <option value="{{ request()->input('desire_position_2')}}">
                  <?php $label =request()->input('desire_position_2');
                    if(isset($label))
                      echo $label;
                    else
                      echo "Choose Desire Job Title 2 ";

                   ?>
                  </option>
                  <?php if(isset($label)){ ?>
                          <option value="">Choose Desire Job Title 2</option>
                          <?php }?>
                  @foreach($desire_position_2 as $row)
                  <option value="{{ $row->description }}" @if(old('desire_position_2') == $row->description) selected @endif> {{ $row->description }} </option>
                  @endforeach

                  
                </select>
              </div> 



              <div class="form-group col-md-3">
                       <!-- {!! Form::label('Choose Previous Position 1') !!} -->
                <?php $skill_1 = \App\Models\skill::all(); ?>
                <select id="val_select" name="skill_1" class="form-control select2">
                  <option value="{{ request()->input('skill_1')}}">
                   <?php $label =request()->input('skill_1');
                    if(isset($label))
                      echo $label;
                    else
                      echo "Choose Skill 1";

                   ?>
                  </option>
                  <?php if(isset($label)){ ?>
                          <option value="">Choose Skill 1</option>
                  <?php }?> 
                  @foreach($skill_1 as $row)
                  <option value="{{ $row->name }}" @if(old('skill_1') == $row->name) selected @endif> {{ $row->name }} </option>
                  @endforeach

                  
                </select>
              </div>              
               <div class="form-group col-md-3">
                       <!-- {!! Form::label('Choose Previous Position 1') !!} -->
                <?php $skill_2 = \App\Models\Skill2::all(); ?>
                <select id="val_select" name="skill_2" class="form-control select2">
                  <option value="{{ request()->input('skill_2')}}">
                   <?php $label =request()->input('skill_2');
                    if(isset($label))
                      echo $label;
                    else
                      echo "Choose Skill 2";

                   ?>
                  </option>
                <?php if(isset($label)){ ?>
                          <option value="">Choose Skill 2</option>
                  <?php }?>   
                  @foreach($skill_2 as $row)
                  <option value="{{ $row->name }}" @if(old('skill_2') == $row->name) selected @endif> {{ $row->name }} </option>
                  @endforeach

                  
                </select>
              </div>           
                <div class="form-group col-md-3">
                       <!-- {!! Form::label('Choose Previous Position 1') !!} -->
                <?php $previous_position_1 = \App\Models\JobTitle::all(); ?>
                <select id="val_select" name="previous_position_1" class="form-control select2">
                  <option value="{{ request()->input('previous_position_1')}}">
                   <?php $label =request()->input('previous_position_1');
                    if(isset($label))
                      echo $label;
                    else
                      echo "Choose Previous Job Title 1";

                   ?>
                  </option>
                 <?php if(isset($label)){ ?>
                          <option value="">Choose Previous Job Title 1</option>
                  <?php }?>  
                  @foreach($previous_position_1 as $row)
                  <option value="{{ $row->description }}" @if(old('previous_position_1') == $row->description) selected @endif> {{ $row->description }} </option>
                  @endforeach

                  
                </select>
              </div>

              <div class="form-group col-md-3">
                       <!-- {!! Form::label('Choose Previous Position 2') !!} -->
                <?php $previous_position_2 = \App\Models\JobTitle::all(); ?>
                <select id="val_select" name="previous_position_2" class="form-control select2">
                  <option value="{{ request()->input('previous_position_2')}}">
                   <?php $label =request()->input('previous_position_2');
                    if(isset($label))
                      echo $label;
                    else
                      echo "Choose Previous Job Title 2";

                   ?>
                  </option>
                 <?php if(isset($label)){ ?>
                          <option value="">Choose Previous Job Title 2</option>
                  <?php }?>  
                  @foreach($previous_position_2 as $row)
                  <option value="{{ $row->description }}" @if(old('previous_position_2') == $row->description) selected @endif> {{ $row->description }} </option>
                  @endforeach               
                </select>
              </div>



             <div class="form-group col-md-3">
                    
                     {!! Form::date('date', null, 
                        array('null', 
                              'class'=>'form-control', 
                              'placeholder'=>'Created Date')) !!}
                     </div>


              <div class="form-group col-md-3">
                <a href="{{url('appointed-jobseeker')}}" class="btn btn-flat btn-danger" >Reset</a>
                <button class="btn btn-flat btn-primary">Search</button>
              </div>
              {!! Form::close() !!}
            </div>


        
          @if($result!=null)



            <form>  

             <input type="hidden" name="fn" value="<?php 

                if(isset($_GET["fullname"])) {

            $fn = $_GET["fullname"];
          
            echo $fn;
            }
                 ?>">      


            <input type="hidden" name="sk1" value="<?php 

                if(isset($_GET["skill_1"])) {

            $sk1 = $_GET["skill_1"];
          
            echo $sk1;
            }
                 ?>">

            <input type="hidden" name="sk2" value="<?php 

                if(isset($_GET["skill_2"])) {

            $sk2 = $_GET["skill_2"];
          
            echo $sk2;
            }
                 ?>">

            <input type="hidden" name="age" value="<?php 

                if(isset($_GET["age"])) {

            $age = $_GET["age"];
          
            echo $age;
            }
                 ?>">           
            <input type="hidden" name="tsp" value="<?php 

                if(isset($_GET["township"])) {

            $tsp = $_GET["township"];
          
            echo $tsp;
            }
                 ?>">           

            <input type="hidden" name="dp1" value="<?php 

                if(isset($_GET["desire_position_1"])) {

            $dp1 = $_GET["desire_position_1"];
          
            echo $dp1;
            }
                 ?>">            

                 <input type="hidden" name="dp2" value="<?php 

                if(isset($_GET["desire_position_2"])) {

            $dp2 = $_GET["desire_position_2"];
          
            echo $dp2;
            }
                 ?>">            <input type="hidden" name="pp1" value="<?php 

                if(isset($_GET["previous_position_1"])) {

            $pp1 = $_GET["previous_position_1"];
          
            echo $pp1;
            }
                 ?>">            <input type="hidden" name="pp2" value="<?php 

                if(isset($_GET["previous_position_2"])) {

            $pp2 = $_GET["previous_position_2"];
          
            echo $pp2;
            }
                 ?>">
              

<br/>


       <div class="form-group col-md-3" >
        <div class="button-group" >
          <button type="button"  class="btn btn-default btn-sm dropdown-toggle form-control select2" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> <span class="caret"></span>Select Skill 1</button>
            <ul class="dropdown-menu">
                    @foreach($skill_1 as $row)

                      <?php

                      if(isset($_GET["skill_1"])) {
                       $mysk1 = $_GET["skill_1"];
                       $sk1 = explode(',',$mysk1 );    

                       ?>
             <li><a href="#" class="small" data-value="option1" tabIndex="-1"><input onclick="checkFitler('{{$row->name}}','SK1')"  type="checkbox" name="skill_1" {{in_array($row->name,$sk1)?'checked':''}}>{{$row->name}}</a></li>

             <?php }?>

                    @endforeach
             </ul>
          </div>
        </div>   

        <div class="form-group col-md-3" >
        <div class="button-group" >
          <button type="button"  class="btn btn-default btn-sm dropdown-toggle form-control select2" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> <span class="caret"></span>Select Skill 2</button>
            <ul class="dropdown-menu">
                    @foreach($skill_2 as $row)

                      <?php

                      if(isset($_GET["skill_2"])) {
                       $mysk2 = $_GET["skill_2"];
                       $sk2 = explode(',',$mysk2 );    

                       ?>
             <li><a href="#" class="small" data-value="option1" tabIndex="-1"><input onclick="checkFitler('{{$row->name}}','SK2')"  type="checkbox" name="skill_2" {{in_array($row->name,$sk2)?'checked':''}}>{{$row->name}}</a></li>

             <?php }?>

                    @endforeach
             </ul>
          </div>
        </div>        
           <div class="form-group col-md-3" >
        <div class="button-group" >
          <button type="button"  class="btn btn-default btn-sm dropdown-toggle form-control select2" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> <span class="caret"></span>Select Township</button>
            <ul class="dropdown-menu">
                    @foreach($township as $row)

                      <?php

                      if(isset($_GET["township"])) {
                       $mytown = $_GET["township"];
                       $town = explode(',',$mytown );    

                       ?>
             <li><a href="#" class="small" data-value="option1" tabIndex="-1"><input onclick="checkFitler('{{$row->name}}','TSP')"  type="checkbox" name="township" {{in_array($row->name,$town)?'checked':''}}>{{$row->name}}</a></li>

             <?php }?>

                    @endforeach
             </ul>
          </div>
        </div>
            

       <div class="form-group col-md-3">
        <div class="button-group">
          <button type="button" class="btn btn-default btn-sm dropdown-toggle form-control select2" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> <span class="caret"></span>Select Desire Job Title</button>
            <ul class="dropdown-menu">
                  @foreach($desire_position_1 as $row)



                      <?php

                      if(isset($_GET["desire_position_1"])) {
                       $my_dp_1 = $_GET["desire_position_1"];
                       $dp_1 = explode(',',$my_dp_1 );    

                       ?>

                   
             <li><a href="#" class="small" data-value="option1" tabIndex="-1"><input onclick="checkFitler('{{$row->description}}','DP1')" type="checkbox" name="desire_position_1" {{in_array($row->description,$dp_1)?'checked':''}}>{{$row->description}}</a></li>
                 <?php }?>
                    @endforeach
             </ul>
          </div>
        </div>

       <div class="form-group col-md-3">
        <div class="button-group">
          <button type="button" class="btn btn-default btn-sm dropdown-toggle form-control select2" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> <span class="caret"></span>Select Desire Job Title 2</button>
            <ul class="dropdown-menu">
                  @foreach($desire_position_2 as $row)
                      <?php
                      
                      if(isset($_GET["desire_position_2"])) {
                       $my_dp_2 = $_GET["desire_position_2"];
                       $dp_2 = explode(',',$my_dp_2 );    

                       ?>

             <li><a href="#" class="small" data-value="option1" tabIndex="-1"><input onclick="checkFitler('{{$row->description}}','DP2')" type="checkbox" name="desire_position_2" {{in_array($row->description,$dp_2)?'checked':''}}>{{$row->description}}</a></li>
               <?php }?>
                    @endforeach
             </ul>
        </div>
       </div>

       <div class="form-group col-md-3">
        <div class="button-group">
          <button type="button" class=" form-control select2 btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> <span class="caret"></span>Previous Job Title 1 </button>
            <ul class="dropdown-menu">
              @foreach($previous_position_1 as $row)
                      <?php
                      if(isset($_GET["previous_position_1"])) {
                       $my_pp_1 = $_GET["previous_position_1"];
                       $pp_1 = explode(',',$my_pp_1 );    

                       ?>
                    
             <li><a href="#" class="small" data-value="option1" tabIndex="-1"><input onclick="checkFitler('{{$row->description}}','PP1')" type="checkbox" name="previous_position_1" {{in_array($row->description,$pp_1)?'checked':''}}>{{$row->description}} </a></li>
                         <?php }?>
                    @endforeach
             </ul>
          </div>
        </div>

       <div class="form-group col-md-3">
        <div class="button-group">
          <button type="button" class=" form-control select2 btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> <span class="caret"></span>Previous Job Title 2 </button>
            <ul class="dropdown-menu">
                  @foreach($previous_position_2 as $row)
                                        <?php

                      if(isset($_GET["previous_position_2"])) {
                       $my_pp_2 = $_GET["previous_position_2"];
                       $pp_2 = explode(',',$my_pp_2 );    

                       ?>
                
             <li><a href="#" class="small" data-value="option1" tabIndex="-1"><input onclick="checkFitler('{{$row->description}}','PP2')" type="checkbox" name="previous_position_2" {{in_array($row->description,$pp_2)?'checked':''}}>{{$row->description}} </a></li>
                  <?php }?>
                    @endforeach
             </ul>
          </div>
        </div>                        
                  
                       
            
            </form>
          @endif

</option></select></div></option></select></div></div></div>
<?php echo ""; ?>
<div class="container-fluid">
<!-- end search -->
<div class="sticky-table sticky-headers sticky-ltr-cells">
        <table class="table table-striped table-striped">
            <thead>
                <!-- <tr class="sticky-row"> -->
                <tr class="sticky-row" style="background-color: #99ccff">
                  <th class="sticky-cell">Action</th>  
                  <th class="sticky-cell">ID</th>                         
                  <th>Name</th>
                  <th>Job Position</th>
                  <th>Company name</th>
                  <th>Phone</th>
                  <th>Interview Date</th>
                  <th>Want to interview? (Y/N)</th>
                  <th>Status</th>
                  <th class="last-called-date">Latest Called Date</th>   
                  <th>Phone</th>
                  <th class="call-back-date" >Call Back Date</th>        
                  <th>Call Receiver</th>
                  <th>Location</th>
                  

                </tr>

            </thead>
            <tbody>
                    @if($result==null)
                    @foreach($jobseekers as $jobseeker )
                   
          <tr class="" style="background-color:#cce6ff;">
                   <!--  <td class="sticky-cell">Ford</td> -->
              <td class="sticky-cell" style="text-align: center;"> 
                           <a href="{{ route('jobseekers.edit', $jobseeker->jobseeker_id) }}" class="btn btn-warning btn-xs action-item" target="_blank"><i class="glyphicon glyphicon-edit"></i></a><a href="{{ route('jobseekers.jobseeker_detail', $jobseeker->jobseeker_id) }}" class="btn btn-info btn-xs action-item" target="_blank"><i class="glyphicon glyphicon-eye-open"></i></a><!-- <a href="{{ route('jobseekers.send_cv', $jobseeker->id) }}" class="btn btn-success btn-xs">Send</a><br> -->
                          </td>
                          <td>{{$jobseeker->jobseeker_id}}</td>
                          <td class="sticky-cell"><a href="{{ route('jobseekers.jobseeker_detail', $jobseeker->jobseeker_id) }}" target="_blank"> {{ $jobseeker->fullname }}</a></td>
                         <td> 
                          {{$jobseeker->mystatus['description']}}
                          </td>
                          <td>{{Carbon\Carbon::parse($jobseeker->interview_time)->format('H-i A')}}</td>
                          <td> {{Carbon\Carbon::parse($jobseeker->interview_time)->format('j-M-y')}}</td>
                          <td>{{Carbon\Carbon::parse($jobseeker->date)->format('j-M-y')}}</td>
                          <td>{{ \App\Models\Calllog::orderBy('created_at','desc')->where('jobseeker_id',$jobseeker->id)->pluck('company_recommended')->first() }}</td>
                          <td><a href="{{ route('jobseekers.jobseeker_detail', $jobseeker->id) }}" target="_blank"> {{ $jobseeker->fb_name }}</a></td>
                          <td><a href="{{ route('jobseekers.jobseeker_detail', $jobseeker->id) }}" target="_blank"> {{ $jobseeker->fullname }} </a></td>
                          <td class="third">
                            {{ Carbon\Carbon::parse(\App\Models\Calllog::orderBy('created_at','desc')->where('jobseeker_id',$jobseeker->id)->pluck('created_at')->first())->format('j-M-y') }}
                            </td>   
                   
                          <td>{{ $jobseeker->phone }} </td>  
                         <td class="second">
                        <?php
                        $jstest = \App\Models\Calllog::orderBy('created_at','desc')->where('jobseeker_id',$jobseeker->id)->pluck('call_back_date')->first();
                        // var_dump($jstest);
                        if(!empty($jstest)){
                          echo Carbon\Carbon::parse(\App\Models\Calllog::orderBy('created_at','desc')->where('jobseeker_id',$jobseeker->id)->pluck('call_back_date')->first())->format('j-M-y');
                        }
                        ?>
                          </td>                   
                                                 
                        <td>{{ $jobseeker->viber_number }} </td>              

                          <td>{{ \App\Models\Gender::where('id',$jobseeker->gender)->pluck('gender_description')->first() }} </td>
 
                          <td>{{ $jobseeker->age }} </td>
                           <td> {{ \App\Models\Ward::where('id',$jobseeker->wards)->pluck('name')->first() }} </td>
                          <td> {{ \App\Models\Busstop::where('id',$jobseeker->bus_stops)->pluck('busstop')->first() }} </td>
                           <td> {{ \App\Models\Education::where('id',$jobseeker->education)->pluck('name_en')->first() }} </td>   
                        
                           <td> {{ \App\Models\JobTitle::where('job_title_key',$jobseeker->previous_position_1)->pluck('description')->first() }} </td> 
                            <td> {{ $jobseeker->experience_month_1 }} </td> 
                        

                           <td> {{ \App\Models\JobTitle::where('job_title_key',$jobseeker->previous_position_2)->pluck('description')->first() }} </td> 
                           
                          <td> {{ $jobseeker->experience_month_2 }} </td> 
                           <td> {{ \App\Models\skill::where('id',$jobseeker->skill_1)->pluck('name')->first() }} </td>
                           <td> {{ \App\Models\Skill2::where('id',$jobseeker->skill_2)->pluck('name')->first() }} </td>
                           <td> {{$jobseeker->mycertificate['description']}}{{$jobseeker->certificate_other}}</td>
                           <td>{{$jobseeker->mytownship['mytownship_name']}}</td>
                  
                           <td> {{ \App\Models\JobTitle::where('job_title_key',$jobseeker->desire_position_1)->pluck('description')->first() }} </td>  

                           <td> {{ \App\Models\JobTitle::where('job_title_key',$jobseeker->desire_position_2)->pluck('description')->first() }} </td> 
                         
                    
                          <td>{{ $jobseeker->certification_and_license }}</td>
                          <td>{{ $jobseeker->type_of_license }}</td>
                          <td>{{ $jobseeker->fabrics }}</td>
                          
                          <td>{{ $jobseeker->remark }}</td>
                          <td>{{ $jobseeker->usertype_1 }}</td>
                        
                          
                          <td>{{ $jobseeker->last_user_freedom_input }}</td>
                         
                              <td>{{ $jobseeker->sign_up_from }} </td>
       
                            <td>
                            {{ \App\Models\Calllog::orderBy('created_at','desc')->where('jobseeker_id',$jobseeker->id)->pluck('remark_calllog')->first() }}</td>

                </tr>
            
                 @endforeach
                  @else
                    @foreach($result as $jobseeker )
                      @empty($result->published_at)
            
                           <tr  style="background-color:#cce6ff;">
                                                     <td class="sticky-cell" style="text-align: center;"> 
                           <a href="{{ route('jobseekers.edit', $jobseeker->id) }}" class="btn btn-warning btn-xs action-item" target="_blank"><i class="glyphicon glyphicon-edit"></i></a><a href="{{ route('jobseekers.jobseeker_detail', $jobseeker->id) }}" class="btn btn-info btn-xs action-item" target="_blank"><i class="glyphicon glyphicon-eye-open"></i></a><!-- <a href="{{ route('jobseekers.send_cv', $jobseeker->id) }}" class="btn btn-success btn-xs">Send</a><br> -->
                          </td>
                        <td class="sticky-cell"><a href="{{ route('jobseekers.jobseeker_detail', $jobseeker->id) }}" target="_blank"> {{ $jobseeker->fb_name }}</a></td>
                          <td>{{$jobseeker->id}}</td>
                          <td> 
                              {{$jobseeker->mystatus['description']}}                 
                            </td>
                         <td>{{Carbon\Carbon::parse($jobseeker->interview_time)->format('H-i A')}}</td>
                          <td> {{Carbon\Carbon::parse($jobseeker->interview_time)->format('j-M-y')}}</td>
                          <td>{{Carbon\Carbon::parse($jobseeker->date)->format('j-M-y')}}</td>
                          <td>{{ \App\Models\Calllog::orderBy('created_at','desc')->where('jobseeker_id',$jobseeker->id)->pluck('company_recommended')->first() }}</td>
                          <td><a href="{{ route('jobseekers.jobseeker_detail', $jobseeker->id) }}" target="_blank"> {{ $jobseeker->fb_name }}</a></td>
                          <td><a href="{{ route('jobseekers.jobseeker_detail', $jobseeker->id) }}" target="_blank"> {{ $jobseeker->fullname }} </a></td>
                          <td class="third">
                            {{ Carbon\Carbon::parse(\App\Models\Calllog::orderBy('created_at','desc')->where('jobseeker_id',$jobseeker->id)->pluck('created_at')->first())->format('j-M-y') }}
                            </td>      
                            <td>{{ $jobseeker->phone }} </td> 
                             <td class="second">
                            {{ Carbon\Carbon::parse(\App\Models\Calllog::orderBy('created_at','desc')->where('jobseeker_id',$jobseeker->id)->pluck('call_back_date')->first())->format('j-M-y') }}
                            </td>                                                                  
                          <td>{{ $jobseeker->viber_number }} </td>              

                          <td>{{ \App\Models\Gender::where('id',$jobseeker->gender)->pluck('gender_description')->first() }} </td>
 
                          <td>{{ $jobseeker->age }} </td>
                           <td> {{ \App\Models\Ward::where('id',$jobseeker->wards)->pluck('name')->first() }} </td>
                          <td> {{ \App\Models\Busstop::where('id',$jobseeker->bus_stops)->pluck('busstop')->first() }} </td>
                           <td> {{ \App\Models\Education::where('id',$jobseeker->education)->pluck('name')->first() }} </td>   
                          
                          <td> {{ \App\Models\JobTitle::where('job_title_key',$jobseeker->previous_position_1)->pluck('description')->first() }} </td>   

                      
                            <td> {{ $jobseeker->experience_month_1 }} </td> 
                          <td> {{ \App\Models\JobTitle::where('job_title_key',$jobseeker->previous_position_2)->pluck('description')->first() }} </td> 
                          <td> {{ $jobseeker->experience_month_2 }} </td> 
                           <td> {{ \App\Models\skill::where('id',$jobseeker->skill_1)->pluck('name')->first() }} </td>
                           <td> {{ \App\Models\Skill2::where('id',$jobseeker->skill_2)->pluck('name')->first() }} </td>
                           <td>{{$jobseeker->mytownship['mytownship_name']}}</td>
                    
                             <td> {{ \App\Models\JobTitle::where('job_title_key',$jobseeker->desire_position_1)->pluck('description')->first() }} </td> 
                             <td> {{ \App\Models\JobTitle::where('job_title_key',$jobseeker->desire_position_2)->pluck('description')->first() }} </td> 
                      
                          <td>{{ $jobseeker->certification_and_license }}</td>
                          <td>{{ $jobseeker->type_of_license }}</td>
                          <td>{{ $jobseeker->fabrics }}</td>                      
                          <td>{{ $jobseeker->remark }}</td>
                          <td>{{ $jobseeker->usertype_1 }}</td>                                                  
                          <td>{{ $jobseeker->last_user_freedom_input }}</td>                       
                          <td>{{ $jobseeker->sign_up_from }} </td>       
                            <td>
                            {{ \App\Models\Calllog::orderBy('created_at','desc')->where('jobseeker_id',$jobseeker->id)->pluck('remark_calllog')->first() }}</td>
                          
                        <!--              <td> <a href="{{ route('jobseekers.mypdf', $jobseeker->id) }}" class="btn btn-primary">Show</a></td> -->
                      </tr>
                    
                      @endempty
                    @endforeach
                  @endif
            </tbody>
        </table>
    </div>
    </div>
<!-- table -->
            <!-- <table class="table table-bordered"> -->
          
<!-- end table -->
             <nav>
         <ul class="pagination">
           <li><?php echo ($result==null)?$jobseekers->render():$result->render(); ?></li>
         </ul>
        </nav>
      @section ('cotable_panel_body')
      <div class="table-responsive">

        <div class="row">
       @endsection

     </div>
   </div>
 </div>

 <script type="text/javascript">
  function removeItem(ary,item)
  {
    var filteredAry = ary.filter(function(e) { return e !== item })
    return filteredAry;
  }
   function checkFitler(value,type)
   {
      var fn  = $('input[name=fn]').val();
      var age = $('input[name=age').val();
      var tsp = $('input[name=tsp]').val();
      var dp1 = $('input[name=dp1]').val();
      var dp2 = $('input[name=dp2]').val();
      var pp1 = $('input[name=pp1]').val();
      var pp2 = $('input[name=pp2]').val();
      var sk1 = $('input[name=sk1]').val();
      var sk2 = $('input[name=sk2]').val();
      switch(type) {
          case 'TSP':
            if (this.event.target.checked==true) {
              tsp = (tsp=='')?value:tsp+','+value;
            }else{
              let n_tsp = tsp.split(',');
              tsp = removeItem(n_tsp,value);    
            }break;
             case 'MA':
            if (this.event.target.checked==true) {
              age = (age=='')?value:age+','+value;
            }else{
              let n_age = age.split(',');
              age = removeItem(n_age,value);  
            }break;
          case 'DP1':
            if (this.event.target.checked==true) {
              dp1 = (dp1=='')?value:dp1+','+value;
            }else{
              let n_dp1 = dp1.split(',');
              dp1 = removeItem(n_dp1,value);  
            }break;
          case 'DP2':
              if (this.event.target.checked==true) {
                dp2 = (dp2=='')?value:dp2+','+value;
              }else{
                let n_dp2 = dp2.split(',');
                dp2 = removeItem(n_dp2,value);
              }
              break;
          case 'PP1':
              if (this.event.target.checked==true) {
                pp1 = (pp1=='')?value:pp1+','+value;
              }else{
                let n_pp1 = pp1.split(',');
                pp1 = removeItem(n_pp1,value);
              }
              break;
          case 'PP2':
              if (this.event.target.checked==true) {
                pp2 = (pp2=='')?value:pp2 +','+value;
              }else{
                let n_pp2 = pp2.split(',');
                pp2 = removeItem(n_pp2,value);
              }
              break;
          case 'SK1':
              if (this.event.target.checked==true) {
                sk1 = (sk1=='')?value:sk1 +','+value;
              }else{
                let n_sk1 = sk1.split(',');
                sk1 = removeItem(n_sk1,value);
              }
              break;
          case 'SK2':
              if (this.event.target.checked==true) {
                sk2 = (sk2=='')?value:sk2 +','+value;
              }else{
                let n_sk2 = sk2.split(',');
                sk2 = removeItem(n_sk2,value);
              }
              break;
      }
      let req_field = 'fullname='+fn+'&age='+age+'&township='+tsp+'&desire_position_1='+dp1+'&desire_position_2='+dp2+'&skill_1='+sk1+'&skill_2='+sk2+'&previous_position_1='+pp1+'&previous_position_2='+pp2;
      
      $.get('/jobseekers?'+req_field,function(response){
          document.open();
          document.write(response);
          document.close();
      });
   }
 </script>


 @stop