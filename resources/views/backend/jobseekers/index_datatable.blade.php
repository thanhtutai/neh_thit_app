@extends('layouts.dashboard')


@section('section')
<div class="container">
    <div class="row">
      <div class="col-md-9">
        <div class="panel panel-success">
            <div class="panel-heading">Job Seekers Management</div>
            <div class="panel-body">
                <div class="row">
                    
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered" id="jobseekers-table">
                            <thead>
                                <tr>
                                      <th>ID</th>
                                        <th>Fb Name</th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>Age</th>
                                        
                              
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@push('scripts')
<script>
$(function() {
    $('#jobseekers-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('jobseekers.data') !!}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'fb_name', name: 'fb_name' },
              { data: 'name', name: 'name' },
              { data: 'phone', name: 'phone' },
              { data: 'age', name: 'age' },

          
            
        ]
    });
});
</script>
@endpush
