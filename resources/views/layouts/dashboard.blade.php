@extends('layouts.plane')

@section('body')



<style type="text/css">
    body{
        background-image: url('http://seamlesspatternchecker.com/hash/seamless-pattern-samples/a/3/5/a35-vector-vector-seamless-simple-pattern-curtain-design-modern-stylish-texture-repeating-abstract-158117696.jpg');
    }

    .navbar{
        border: none;
        border-radius: 0;
        background-color: rgba(0,0,0,0.5);
    }
</style>
<style type="text/css">
  

input {
  outline: none;
}
input[type=search] {
  -webkit-appearance: textfield;
  -webkit-box-sizing: content-box;
  font-family: inherit;
  font-size: 100%;
}
input::-webkit-search-decoration,
input::-webkit-search-cancel-button {
  display: none; 
}

input[type=search] {
  background: #ffffff url(https://static.tumblr.com/ftv85bp/MIXmud4tx/search-icon.png) no-repeat 9px center;
  border: solid 1px #ccc;
  padding: 9px 10px 9px 32px;
  width: 40px;
     opacity: .4;
  -webkit-border-radius: 10em;
  -moz-border-radius: 10em;
  border-radius: 10em;
  
  -webkit-transition: all .5s;
  -moz-transition: all .5s;
  transition: all .5s;
}

#demo-2 input[type=search] {
  width: 15px;
  padding-left: 10px;
  color: transparent;
  cursor: pointer;
}
#demo-2 input[type=search]:hover {
    background-color: #ffffff;
    opacity: .4;
}
#demo-2 input[type=search]:focus {
  width: 130px;
  padding-left: 32px;
  color: #000;
    background-color: #ffffff;
    opacity: .4;
  cursor: auto;
}
#demo-2 input:-moz-placeholder {
  color: transparent;
}
#demo-2 input::-webkit-input-placeholder {
  color: transparent;
}
</style>

<header id="header" style="display: block;">
 <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/" style="color: #fff !important"><img src="/logo.png" height="35px" style="    margin-top: -7px;"></a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
          

   <!--      <li class="{{ active_check('home') }}">
            <a href="{{ route('home') }}"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Home</a>
        </li> -->

        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Daily Report<span class="caret"></span></a>
            <ul class="dropdown-menu">
            @if(Auth::user()->is_admin == 1 || Auth::user()->is_admin == 3)   
            <li class="{{ active_check('checklists-emp') }}">
             <a href="/checklists-emp"><span class="fa fa-sitemap fa-fw"" aria-hidden="true"></span>Employer Sales</a>
            </li> 
            @endif
            @if(Auth::user()->is_admin == 1 || Auth::user()->is_admin == 2) 
            <li class="{{ active_check('checklists-js') }}">
             <a href="/checklists-js"><span class="fa fa-sitemap fa-fw"" aria-hidden="true"></span>Job Seeker Call</a>
            </li> 
            @endif
            </ul>
        </li>

       <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">To Do List<span class="caret"></span></a>
          <ul class="dropdown-menu">
        @if(Auth::user()->is_admin == 2 || Auth::user()->is_admin == 1) 

            <li class="{{ active_check('reminder') }}">
               <a href="/reminder"><span class="fa fa-dashboard fa-fw"" aria-hidden="true"></span>Reminder</a>
            </li>  
            <li class="{{ active_check('jobseeker-jobbrowse') }}">
               <a href="/jobseeker-jobbrowse"><span class="fa fa-dashboard fa-fw"" aria-hidden="true"></span>Job Browse Applicant</a>
            </li>  
           
            <li class="{{ active_check('jobmatch-paid') }}">
              <a href="/jobmatch-paid"><span class="fa fa-dashboard fa-fw"" aria-hidden="true"></span>Job Match</a>
            </li>              
            <li class="{{ active_check('todo') }}">
               <a href="/todo"><span class="fa fa-dashboard fa-fw"" aria-hidden="true"></span>Job Seeker Follow Up</a>
            </li> 
        @endif
        @if(Auth::user()->is_admin == 1 || Auth::user()->is_admin == 3) 
             <li class="{{ active_check('todo-em') }}">
        <a href="/todo-em"><span class="fa fa-dashboard fa-fw"" aria-hidden="true"></span>Employer Follow Up</a>
            </li>        
        @endif
           <!--  <li class="{{ active_check('jobmatch-free') }}">
        <a href="/jobmatch-free"><span class="fa fa-dashboard fa-fw"" aria-hidden="true"></span>Free Job Match</a>
            </li>  -->
          <!--   <li class="{{ active_check('jobmatch-paid') }}">
              <a href="/jobmatch-paid"><span class="fa fa-dashboard fa-fw"" aria-hidden="true"></span>Job Match</a>
            </li>  -->

           


          </ul>
          </li>


          <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Job Seeker<span class="caret"></span></a>
          <ul class="dropdown-menu">
             <li class="{{ active_check('jobseekers') }}">
             <a href="{{ route('jobseekers.index') }}"><span class="fa fa-sitemap fa-fw"" aria-hidden="true"></span>Job Seeker List</a>
            </li> 
             <li class="{{ active_check('jobseeker-jobapplied') }}">
               <a href="/jobseeker-jobapplied"><span class="fa fa-dashboard fa-fw"" aria-hidden="true"></span>Job Applicants</a>
            </li>  
            <!-- <li class="{{ active_check('appointed-jobseeker') }}">
             <a href="/appointed-jobseeker"><span class="fa fa-sitemap fa-fw"" aria-hidden="true"></span>Appointed Job Seeker List</a>
            </li>   -->
             </ul>
          </li>
<!--         <li class="{{ active_check('jobseekerslist') }}">
            <a href="{{ route('jobseekers.json') }}"><span class="fa fa-fw fa-table" aria-hidden="true"></span>Jobseeker DataTable</a>
        </li>
         -->


          <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Call Log<span class="caret"></span></a>
          <ul class="dropdown-menu">
            @if(Auth::user()->is_admin == 1 || Auth::user()->is_admin == 2) 
            <li class="{{ active_check('calllogs') }}">
             <a href="/calllogs"><span class="fa fa-sitemap fa-fw"" aria-hidden="true"></span>Call Log Job Seeker</a>
            </li>
            @endif
            @if(Auth::user()->is_admin == 1 || Auth::user()->is_admin == 3)  
            <li class="{{ active_check('employercalllogs') }}">
             <a href="/employercalllogs"><span class="fa fa-sitemap fa-fw"" aria-hidden="true"></span>Call Log Employer</a>
            </li> 
            @endif

            </ul>
            </li>



   
            <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Jobs<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li class="{{ active_check('freejob') }}">

             <a href="/freejob"><span class="fa fa-sitemap fa-fw"" aria-hidden="true"></span>Not Contracted</a>
            </li> 
            <li class="{{ active_check('paidjob') }}">

             <a href="/paidjob"><span class="fa fa-sitemap fa-fw"" aria-hidden="true"></span>Contracted</a>
            </li> 

                        </ul>
          </li>
        <li class="{{ active_check('employers') }}">
            <a href="{{ route('employers.index') }}"><span class="fa fa-table fa-fw"" aria-hidden="true"></span>Employers List</a>
        </li>



          @if(Auth::user()->is_admin == 1) 
          

          <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Admin<span class="caret"></span></a>
          <ul class="dropdown-menu">
                        <li class="{{ active_check('users') }}">
                        <a href="{{ route('users.index') }}"><span class="fa fa-sitemap fa-fw"" aria-hidden="true"></span>Users</a>
                        </li> 
                       <li class="{{ active_check('email') }}">
                          <a href="{{ route('home.index') }}"><span class="fa fa-bar-chart-o fa-fw"" aria-hidden="true"></span>Email</a>
                      </li>                       
                      <li class="{{ active_check('/status') }}">
                          <a href="status"><span class="fa fa-bar-chart-o fa-fw"" aria-hidden="true"></span>Status</a>
                      </li>                      

                      <li class="{{ active_check('/callreasons') }}">
                          <a href="callreasons"><span class="fa fa-bar-chart-o fa-fw"" aria-hidden="true"></span>Call Reason</a>
                      </li>

                       <li class="{{ active_check('duplicates') }}">
                            <a href="{{ route('duplicates.index') }}"><span class="fa fa-wrench fa-fw"" aria-hidden="true"></span>Duplicates Section</a>
                        </li>


                         <li class="{{ active_check('/townships') }}">
                            <a href="{{ route('townships.index') }}"><span class="fa fa-sitemap fa-fw"" aria-hidden="true"></span>Township</a>
                        </li>  
                                                   
                        <li class="{{ active_check('wards') }}">
                            <a href="{{ route('wards.index') }}"><span class="fa fa-sitemap fa-fw"" aria-hidden="true"></span>Ward</a>
                        </li>  
                          
                        <li class="{{ active_check('jobtitles') }}">
                        <a href="{{ route('jobtitles.index') }}"><span class="fa fa-sitemap fa-fw"" aria-hidden="true"></span>Job Titles</a>
                        </li> 
                        <li class="{{ active_check('jobfunctions') }}">
                        <a href="{{ route('jobfunctions.index') }}"><span class="fa fa-sitemap fa-fw"" aria-hidden="true"></span>Job Functions</a>
                        </li>            
                        <li class="{{ active_check('skill_1') }}">
                        <a href="{{ route('skill_1.index') }}"><span class="fa fa-sitemap fa-fw"" aria-hidden="true"></span>Skill 1</a>
                        </li>         
                        <li class="{{ active_check('skill_2') }}">
                        <a href="{{ route('skill_2.index') }}"><span class="fa fa-sitemap fa-fw"" aria-hidden="true"></span>Skill 2</a>
                        </li>   
                        <li class="{{ active_check('certificates') }}">
                        <a href="/certificates"><span class="fa fa-sitemap fa-fw"" aria-hidden="true"></span>Certificate</a>
                        </li>   
                 
                        </ul>
          </ul>
        </li>
  <li>



           @endif
                
                   
                
                    
                        
            <!--             <li {{ (Request::is('*charts') ? 'class="active"' : '') }}>
                            <a href="{{ url ('charts') }}"><i class="fa fa-bar-chart-o fa-fw"></i> Charts</a>
                       
                        </li>
                        <li {{ (Request::is('*tables') ? 'class="active"' : '') }}>
                            <a href="{{ url ('jobseekers.index') }}"><i class="fa fa-table fa-fw"></i> Jobseekers List</a>
                        </li>
                        <li {{ (Request::is('*forms') ? 'class="active"' : '') }}>
                            <a href="{{ url ('forms') }}"><i class="fa fa-edit fa-fw"></i> Forms</a>
                        </li>
                        <li >
                            <a href="#"><i class="fa fa-wrench fa-fw"></i> UI Elements<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li {{ (Request::is('*panels') ? 'class="active"' : '') }}>
                                    <a href="{{ url ('panels') }}">Panels and Collapsibles</a>
                                </li>
                                <li {{ (Request::is('*buttons') ? 'class="active"' : '') }}>
                                    <a href="{{ url ('buttons' ) }}">Buttons</a>
                                </li>
                                <li {{ (Request::is('*notifications') ? 'class="active"' : '') }}>
                                    <a href="{{ url('notifications') }}">Alerts</a>
                                </li>
                                <li {{ (Request::is('*typography') ? 'class="active"' : '') }}>
                                    <a href="{{ url ('typography') }}">Typography</a>
                                </li>
                                <li {{ (Request::is('*icons') ? 'class="active"' : '') }}>
                                    <a href="{{ url ('icons') }}"> Icons</a>
                                </li>
                                <li {{ (Request::is('*grid') ? 'class="active"' : '') }}>
                                    <a href="{{ url ('grid') }}">Grid</a>
                                </li>
                            </ul> -->
                            <!-- /.nav-second-level -->
                        <!-- </li> -->
                        <!-- <li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i> Multi-Level Dropdown<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#">Second Level Item</a>
                                </li>
                                <li>
                                    <a href="#">Second Level Item</a>
                                </li>
                                <li>
                                    <a href="#">Third Level <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                    </ul> -->
                                    <!-- /.nav-third-level -->
                              <!--   </li>
                            </ul> -->
                            <!-- /.nav-second-level -->
                        <!-- </li> -->
                       <!-- li>
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> Sample Pages<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li {{ (Request::is('*blank') ? 'class="active"' : '') }}>
                                    <a href="{{ url ('blank') }}">Blank Page</a>
                                </li>
                                <li>
                                    <a href="{{ url ('login') }}">Login Page</a>
                                </li>
                            </ul> -->
                            <!-- /.nav-second-level -->
                        <!-- </li> -->
                        <!-- <li {{ (Request::is('*documentation') ? 'class="active"' : '') }}>
                            <a href="{{ url ('documentation') }}"><i class="fa fa-file-word-o fa-fw"></i> Documentation</a>
                        </li> -->
      </ul>


        <ul class="nav navbar-top-links navbar-right pull-right" >
        <li id="demo-2">
          <?php 
            $array = explode('/', $_SERVER['REQUEST_URI']);
            $darray = explode('?', $array[1]);
           ?>
           <form action="{{url(''.$darray[0])}}/global/search" method="POST" role="search">
           {{ csrf_field() }}
  <!-- <input  placeholder="Search"> -->
                <input type="search" name="q">
              
<!-- </form> -->
            </form>
        </li>

                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="/todo">
                                <div>
                                    <i class="fa fa-comment fa-fw"></i> <?php  $todo =\App\Models\Calllog::orderBy('calllogs.created_at','DESC')->where('call_back_date', '<=', Carbon\Carbon::now()->toDateString())->get();

 $todo_count = $todo->count(); echo $todo_count;?> Follow Up
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                    
                        
                        <li>
                            <a href="/newjs">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i> <b><?php $latest_jobseekers = DB::table('jobseekers')->where('created_at', '>', Carbon\Carbon::yesterday()->toDateString())->count();  echo $latest_jobseekers; ?></b>  New Job Seekers Today
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                      
                       
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Alerts</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-alerts -->
                </li>
      <ul class="nav navbar-nav navbar-right">
        <!-- <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li> -->
        <li><a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>
                                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form></li>
      </ul>

          
          </ul>
         

        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
</header>
    <section id="main-slider" class="no-margin">
  <div class="container-fluid">
 @yield('section')
</div>

</section>
@stop


@section ('footer')
<script>
    CKEDITOR.replace('body');
</script>
@stop