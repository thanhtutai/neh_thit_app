<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
	<meta charset="utf-8"/>
	<title>Neh Thit</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>
	
<!-- for email -->
     
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>  
     <script src="{{ asset('js/jquery.stickytable.min.js') }}"></script>
         <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
            'baseURL' =>url('/')
        ]) !!};
    </script>

<!-- for email -->
	<!-- for datepicker -->
   
 <script src="{{ asset('js/pie-chart.js') }}" type="text/javascript"></script>
        <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
	<!-- datepicker -->
     <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">


    <link rel="stylesheet" href="{{ asset("assets/stylesheets/styles.css") }}"/>
    <link rel="stylesheet" href="{{ asset("css/jquery.stickytable.min.css") }}"/>
	<!-- <link rel="stylesheet" href="{{ asset("css/myTheme.css") }}"/> -->
    <link href="css/ScrollTabla.css" rel="stylesheet" media="screen" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

<style>
.tooltip {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black;
}

.tooltip .tooltiptext {
    visibility: hidden;
    width: 120px;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    
    /* Position the tooltip */
    position: absolute;
    z-index: 1;
    top: -5px;
    left: 105%;
}

.tooltip:hover .tooltiptext {
    visibility: visible;
}
</style>

@stack('scripts')
</head>
<body    style="background-color: #e6f9ff;" >
	@yield('body')

	<!-- <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> -->


    <!-- <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script> -->
<!--     <script>
        CKEDITOR.replace( 'body' );
    </script>
 -->
	<!-- date picker -->
 

    
<!--     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  --> 

    <script>
    var path = "{{ route('autocomplete') }}";
    $('input.typeahead').typeahead({
        source:  function (query, process) {
        return $.get(path, { query: query }, function (data) {
                return process(data);
            });
        }
    });
</script>
<script>

$("#friendname").autocomplete({
 source: '{{ url('getfriendname') }}',
 minLength: 2,
 });

</script>
<script>
$(document).ready(function(){
  // $(".box-body").hide();
    //$('.select2').select2();
    $(".for-hide-search-form").click(function(){
        $(".box-body").toggle();
    });
});
</script>
	<!-- date picker -->
@stack('scripts')
</body>
</html>