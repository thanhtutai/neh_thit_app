@extends('layouts.dashboard')
@section('page_heading','Grid System')
@section('section')
<br><br><br><br><br><br>
<div class="col-sm-12">
<div class="row">
	{!! Form::open(['url'=>'csv_import','files'=>true]) !!}    
	{{ csrf_field() }}	
		<div class="col-md-6">                
			{!! Form::file('file',['class'=>'form-control','required']) !!}
		</div>
		<div class="col-md-6">                
			<button type="submit" class="btn btn-warning">Upload</button>
		</div>
	{!! Form::close() !!}
</div>
</div>
@stop