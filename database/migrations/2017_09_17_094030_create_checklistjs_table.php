<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChecklistjsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checklistjs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hours_worked')->nullable();
            $table->string('new_sign_ups')->nullable();
            $table->string('new_called')->nullable();
            $table->string('new_recommended')->nullable();
            $table->string('new_confirm_interview_time')->nullable();
            $table->string('new_confirm_interviewed')->nullable();
            $table->string('new_got_job_free_rec')->nullable();
            $table->string('new_got_job_paying_client')->nullable();
            $table->string('followups_called')->nullable();
            $table->string('followups_recommended')->nullable();
            $table->string('followups_confirm_interview_time')->nullable();
            $table->string('followups_interviewed')->nullable();
            $table->string('followups_got_job_free_rec')->nullable();
            $table->string('followups_got_job_paying_client')->nullable();
            $table->string('total_js_called')->nullable();
            $table->string('total_js_called_per_hour')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checklistjs');
    }
}
