<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChecklistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checklists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('new_employers_call')->nullable();
            $table->string('new_employers_answer')->nullable();
            $table->string('new_employers_leads')->nullable();
            $table->string('follow_ups_employers_meetings_schedule')->nullable();
            $table->string('follow_ups_employers_call')->nullable();
            $table->string('follow_ups_employers_answer')->nullable();
            $table->string('follow_ups_employers_leads')->nullable();
            $table->string('employers_called_total')->nullable();
            $table->string('employers_sales')->nullable();
            $table->string('employers_called_total_per_hour')->nullable();
            $table->string('hours_worked')->nullable();
            $table->string('js_news_sign_ups')->nullable();
            $table->string('js_news_called')->nullable();
            $table->string('js_news_recommended')->nullable();
            $table->string('js_news_confirm_interview_time')->nullable();
            $table->string('js_news_interviewed')->nullable();
            $table->string('js_news_got_job_free_rec')->nullable();
            $table->string('js_news_got_news_job_paying_client')->nullable();
            $table->string('follow_ups_js_job_paying_client')->nullable();
            $table->string('js_follow_ups_called')->nullable();
            $table->string('js_follow_ups_recommended')->nullable();
            $table->string('js_follow_ups_confirm_interview_time')->nullable();
            $table->string('js_follow_ups_interviewed')->nullable();
            $table->string('js_follow_ups__got_job_free_rec')->nullable();
            $table->string('js_follow_ups__got_job_paying_client')->nullable();
            $table->string('js_called_total')->nullable();
            $table->string('js_called_total_per_our')->nullable();
            $table->string('user_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checklists');
    }
}
