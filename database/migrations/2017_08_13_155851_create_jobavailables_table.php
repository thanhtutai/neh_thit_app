<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobavailablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobavailables', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ja_date')->nullable();
            $table->string('ja_deadline')->nullable();
            $table->string('ja_status')->nullable();
            $table->string('ja_company_id')->nullable();
            $table->string('ja_job_id')->nullable();
            $table->string('ja_company_name')->nullable();
            $table->string('ja_company_phone')->nullable();
            $table->string('ja_position')->nullable();
            $table->string('ja_title')->nullable();
            $table->string('ja_certificate')->nullable();
            $table->string('ja_company_email')->nullable();
            $table->string('ja_company_location')->nullable();
            $table->string('ja_company_ward')->nullable();
            $table->string('ja_company_bus_stop')->nullable();
            $table->string('ja_gender')->nullable();
            $table->string('ja_min')->nullable();
            $table->string('ja_max')->nullable();
            $table->string('ja_education')->nullable();
            $table->string('ja_industry')->nullable();
            $table->string('ja_skill1')->nullable();
            $table->string('ja_skill2')->nullable();
            $table->string('ja_exp_months')->nullable();
            $table->string('ja_salary_range')->nullable();
            $table->string('ja_overtime')->nullable();
            $table->string('ja_transport')->nullable();
            $table->string('ja_accommodation')->nullable();
            $table->string('ja_food')->nullable();
            $table->string('ja_nrc')->nullable();
            $table->string('ja_police_ward_rec')->nullable();
            $table->string('ja_labour_card')->nullable();
            $table->string('ja_remark')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobavailables');
    }
}
