<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalllogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calllogs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jobseeker_id')->nullable();
            $table->text('status')->nullable();
            $table->text('id_number')->nullable();
            $table->date('call_back_date')->nullable();
            $table->text('type_of_call')->nullable();
            $table->text('start_time')->nullable();
            $table->text('minute')->nullable();
            $table->text('second')->nullable();
            $table->text('to_fullname')->nullable();
            $table->text('phone_number')->nullable();
            $table->text('call_reason')->nullable();
            $table->text('remark_calllog')->nullable();
            $table->text('caller_receiver')->nullable();
            $table->text('published_at_2')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calllogs');
    }
}
