<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJApaidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('j_apaids', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name')->nullable();
            $table->string('req_position')->nullable();
            $table->string('req_quantity')->nullable();
            $table->string('req_gender')->nullable();
            $table->string('req_age')->nullable();
            $table->string('req_education')->nullable();
            $table->string('req_skill1')->nullable();
            $table->string('req_skill2')->nullable();
            $table->string('req_experience')->nullable();
            $table->string('req_nationality')->nullable();
            $table->string('req_hiring_date')->nullable();
            $table->string('req_remark')->nullable();
            $table->string('basic_salary')->nullable();
            $table->string('ot_rate_per_hour')->nullable();
            $table->string('ot_rate_per_day')->nullable();
            $table->string('attendance_bonous')->nullable();
            $table->string('food')->nullable();
            $table->string('transportation')->nullable();
            $table->string('accommodation')->nullable();
            $table->string('ssb')->nullable();
            $table->string('bonous_monthly_annually')->nullable();
            $table->string('other_benefits')->nullable();
            $table->string('particular_traits')->nullable();
            $table->string('any_special_requests')->nullable();
            $table->string('any_special_requirements')->nullable();
            $table->string('please_provide_any_additional_comments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('j_apaids');
    }
}
