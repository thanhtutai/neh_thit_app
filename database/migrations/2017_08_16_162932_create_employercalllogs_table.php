<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployercalllogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employercalllogs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date')->nullable();
            $table->integer('employer_id')->nullable();
            $table->string('type_of_call')->nullable();
            $table->string('start_time')->nullable();
            $table->string('duration')->nullable();
            $table->string('ph_number')->nullable();
            $table->string('call_reason')->nullable();
            $table->string('remark')->nullable();
            $table->string('caller_receiver')->nullable();
            $table->string('call_back_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employercalllogs');
    }
}
