<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('emplyer_name')->nullable();
            $table->text('phone_number')->nullable();
            $table->text('email_address')->nullable();
            $table->text('address')->nullable();
            $table->text('zone')->nullable();
            $table->text('latitude')->nullable();
            $table->text('longitude')->nullable();
            $table->text('number_of_call')->nullable();
            $table->text('given_materials')->nullable();
            $table->text('interest')->nullable();
            $table->text('understanding')->nullable();
            $table->text('notes')->nullable();
            $table->text('authorized_person')->nullable();
            $table->text('position')->nullable();
            $table->text('status_1')->nullable();
            $table->text('meeting')->nullable();
            $table->text('status_2')->nullable();
            $table->text('emp_image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employers');
    }
}
