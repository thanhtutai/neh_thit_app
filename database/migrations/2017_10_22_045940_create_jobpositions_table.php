<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobpositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobpositions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('employer_id')->nullable();
            $table->string('post_date')->nullable();
            $table->string('deadline')->nullable();
            $table->string('status')->nullable();
            $table->string('require_position')->nullable();
            $table->string('gender')->nullable();
            $table->string('req_nationality')->nullable();
            $table->string('req_quantity')->nullable();
            $table->string('certificate_1')->nullable();
            $table->string('certificate_2')->nullable();
            $table->string('minimum')->nullable();
            $table->string('maximum')->nullable();
            $table->string('education')->nullable();
            $table->string('industry')->nullable();
            $table->string('skill_1')->nullable();
            $table->string('skill_2')->nullable();
            $table->string('experience_month')->nullable();
            $table->string('salary_range')->nullable();
            $table->string('ot_rate_per_hour')->nullable();
            $table->string('ot_rate_per_day')->nullable();
            $table->string('attendance_bonous')->nullable();
            $table->string('zone')->nullable();
            $table->string('bonous_monthly_annually')->nullable();
            $table->string('other_benefits')->nullable();
            $table->string('particular_traits')->nullable();
            $table->string('any_special_requests')->nullable();
            $table->string('any_special_requirements')->nullable();
            $table->string('township')->nullable();
            $table->string('transport')->nullable();
            $table->string('accomodation')->nullable();
            $table->string('food')->nullable();
            $table->string('nrc')->nullable();
            $table->string('police_and_ward_rec')->nullable();
            $table->string('labour_card')->nullable();
            $table->string('remark')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobpositions');
    }
}
