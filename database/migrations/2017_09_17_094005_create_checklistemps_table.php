<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChecklistempsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checklistemps', function (Blueprint $table) {
            $table->increments('id');
            $table->string('new_employers_called')->nullable();
            $table->string('new_employers_answered')->nullable();
            $table->string('new_employers_leads')->nullable();
            $table->string('new_employers_meeting_schedule')->nullable();
            $table->string('followups_employers_called')->nullable();
            $table->string('followups_employers_answered')->nullable();
            $table->string('followups_employers_leads')->nullable();
            $table->string('followups_employers_meeting_schedule')->nullable();
            $table->string('followups_employers_called_total')->nullable();
            $table->string('followups_employer_sale')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checklistemps');
    }
}
