<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobseekersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('jobseekers', function (Blueprint $table) {
            $table->increments('id');
            $table->text('time')->nullable();
            $table->text('date')->nullable();
            $table->string('fb_name')->nullable(); 
            $table->string('fullname')->nullable(); 
            $table->text('phone')->nullable();
            $table->text('viber_number')->nullable();
            $table->text('sign_up_from')->nullable();
            $table->string('gender')->nullable();
            $table->text('age')->nullable();
            $table->string('wards')->nullable();
            $table->string('bus_stops')->nullable();
            $table->string('education')->nullable();
            $table->string('previous_position_1')->nullable();
            $table->string('previous_position_other')->nullable();
            $table->string('experience_month_1')->nullable();
            $table->string('previous_position_2')->nullable();
            $table->string('previous_position_other_2')->nullable();
            $table->string('experience_month_2')->nullable();
            $table->string('certification_and_license')->nullable();
            $table->string('fabrics')->nullable();
            $table->string('type_of_license')->nullable();
            $table->string('desire_position_y_n')->nullable();
            $table->string('desire_position_1')->nullable();
            $table->string('desire_position_other_1')->nullable();
            $table->string('desire_position_2')->nullable();
            $table->string('desire_position_other_2')->nullable();
            $table->string('job_industry_1')->nullable();
            $table->string('job_industry_2')->nullable();
            $table->string('skill_1')->nullable();
            $table->string('skill_other_1')->nullable();
            $table->string('skill_2')->nullable();
            $table->string('skill_other_2')->nullable();
            $table->string('township')->nullable();
            $table->text('viber_number_2')->nullable();
            $table->text('remark')->nullable();
            $table->string('usertype_1')->nullable();
            $table->text('chatfuel_user_id')->nullable();
            $table->text('address')->nullable();
            $table->text('last_user_freedom_input')->nullable();
            $table->text('messenger_user_id')->nullable();
            $table->text('published_at')->nullable();
            $table->text('published_at2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobseekers');
    }
}
