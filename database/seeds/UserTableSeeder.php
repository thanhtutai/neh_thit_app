<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
   		[
            'name' => 'Admin',
            'position'=>'Super Admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('admin!@#'),
            'is_admin' => 1,
            'is_active' => 1,
            'created_at' =>date('Y-m-d'),
            'updated_at' => date('Y-m-d')
                        
        ],

       ]);
    }
}
